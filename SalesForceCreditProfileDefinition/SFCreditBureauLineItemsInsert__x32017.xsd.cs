namespace SalesForceCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"SFCreditBureauLineItemsInsertRequest", @"SFCreditBureauLineItemsInsertResponse"})]
    public sealed class SFCreditBureauLineItemsInsert__x32017 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""SFCreditBureauLineItemsInsertRequest"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""sync"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""after"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""Credit_Bureau_Line_Item__c"">
                      <xs:complexType>
                        <xs:attribute name=""Id"" type=""xs:string"" />
                        <xs:attribute name=""IsDeleted"" type=""xs:boolean"" />
                        <xs:attribute name=""Name"" type=""xs:string"" />
                        <xs:attribute name=""CreatedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""CreatedById"" type=""xs:string"" />
                        <xs:attribute name=""LastModifiedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastModifiedById"" type=""xs:string"" />
                        <xs:attribute name=""SystemModstamp"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastActivityDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastViewedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastReferencedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""Credit_Bureau_Line_Item_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Credit_Bureau__c"" type=""xs:string"" />
                        <xs:attribute name=""Public_Record_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Public_Record_Amount__c"" type=""xs:double"" />
                        <xs:attribute name=""Public_Record_Filed_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Public_Record_Satisified_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Creditor_Name__c"" type=""xs:string"" />
                        <xs:attribute name=""Account_Number__c"" type=""xs:string"" />
                        <xs:attribute name=""Balance_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Open_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Last_Activity__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""History_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Term_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Account_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Credit_Limit__c"" type=""xs:double"" />
                        <xs:attribute name=""Account_Condition__c"" type=""xs:string"" />
                        <xs:attribute name=""Payment_Status__c"" type=""xs:string"" />
                        <xs:attribute name=""Purpose_of_Account__c"" type=""xs:string"" />
                        <xs:attribute name=""Account_Balance__c"" type=""xs:double"" />
                        <xs:attribute name=""Payment_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Monthly_Payments__c"" type=""xs:double"" />
                        <xs:attribute name=""Public_Record_Status__c"" type=""xs:string"" />
                        <xs:attribute name=""Payment_Status_Code__c"" type=""xs:string"" />
                        <xs:attribute name=""Account_Condition_Code__c"" type=""xs:string"" />
                        <xs:attribute name=""Purpose_Of_Account_Code__c"" type=""xs:string"" />
                        <xs:attribute name=""Subcode__c"" type=""xs:string"" />
                        <xs:attribute name=""Kind_of_Business__c"" type=""xs:string"" />
                        <xs:attribute name=""Currency__c"" type=""xs:double"" />
                        <xs:attribute name=""Amount_Past_Due__c"" type=""xs:double"" />
                        <xs:attribute name=""Status_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""ECOA__c"" type=""xs:string"" />
                        <xs:attribute name=""Status__c"" type=""xs:string"" />
                        <xs:attribute name=""Is_Mortgage__c"" type=""xs:boolean"" />
                        <xs:attribute name=""High_Balance__c"" type=""xs:double"" />
                        <xs:attribute name=""Creditor_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Open_Or_Closed__c"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""SFCreditBureauLineItemsInsertResponse"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""row"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""Id"">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base=""xs:string"">
                      <xs:attribute name=""IsNull"" type=""xs:boolean"" />
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name=""AffectedRows"" type=""xs:int"" />
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public SFCreditBureauLineItemsInsert__x32017() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [2];
                _RootElements[0] = "SFCreditBureauLineItemsInsertRequest";
                _RootElements[1] = "SFCreditBureauLineItemsInsertResponse";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://www.cdata.com/SalesforceProvider/2017",@"SFCreditBureauLineItemsInsertRequest")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SFCreditBureauLineItemsInsertRequest"})]
        public sealed class SFCreditBureauLineItemsInsertRequest : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SFCreditBureauLineItemsInsertRequest() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SFCreditBureauLineItemsInsertRequest";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.cdata.com/SalesforceProvider/2017",@"SFCreditBureauLineItemsInsertResponse")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SFCreditBureauLineItemsInsertResponse"})]
        public sealed class SFCreditBureauLineItemsInsertResponse : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SFCreditBureauLineItemsInsertResponse() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SFCreditBureauLineItemsInsertResponse";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
