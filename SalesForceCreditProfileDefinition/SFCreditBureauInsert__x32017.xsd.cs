namespace SalesForceCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"SFCreditBureauInsertRequest", @"SFCreditBureauInsertResponse"})]
    public sealed class SFCreditBureauInsert__x32017 : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""SFCreditBureauInsertRequest"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""sync"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""after"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name=""Credit_Bureau__c"">
                      <xs:complexType>
                        <xs:attribute name=""Id"" type=""xs:string"" />
                        <xs:attribute name=""OwnerId"" type=""xs:string"" />
                        <xs:attribute name=""IsDeleted"" type=""xs:boolean"" />
                        <xs:attribute name=""Name"" type=""xs:string"" />
                        <xs:attribute name=""CreatedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""CreatedById"" type=""xs:string"" />
                        <xs:attribute name=""LastModifiedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastModifiedById"" type=""xs:string"" />
                        <xs:attribute name=""SystemModstamp"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastActivityDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastViewedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""LastReferencedDate"" type=""xs:dateTime"" />
                        <xs:attribute name=""Application__c"" type=""xs:string"" />
                        <xs:attribute name=""Bureau_Name__c"" type=""xs:string"" />
                        <xs:attribute name=""Applicant_Name__c"" type=""xs:string"" />
                        <xs:attribute name=""Requested_Type__c"" type=""xs:string"" />
                        <xs:attribute name=""Loan__c"" type=""xs:string"" />
                        <xs:attribute name=""CBR_Status__c"" type=""xs:string"" />
                        <xs:attribute name=""CBR_Pulled_Date__c"" type=""xs:dateTime"" />
                        <xs:attribute name=""Mailing_Address__c"" type=""xs:string"" />
                        <xs:attribute name=""City__c"" type=""xs:string"" />
                        <xs:attribute name=""State__c"" type=""xs:string"" />
                        <xs:attribute name=""Zip_Code__c"" type=""xs:string"" />
                        <xs:attribute name=""Credit_Score__c"" type=""xs:string"" />
                        <xs:attribute name=""Error_Message__c"" type=""xs:string"" />
                        <xs:attribute name=""Negative_Record__c"" type=""xs:boolean"" />
                        <xs:attribute name=""Score_Factor_One__c"" type=""xs:string"" />
                        <xs:attribute name=""Score_Factor_Two__c"" type=""xs:string"" />
                        <xs:attribute name=""Score_Factor_Three__c"" type=""xs:string"" />
                        <xs:attribute name=""Error_Code__c"" type=""xs:string"" />
                        <xs:attribute name=""Active__c"" type=""xs:boolean"" />
                        <xs:attribute name=""Comments__c"" type=""xs:string"" />
                        <xs:attribute name=""Score_Factor_Four__c"" type=""xs:string"" />
                        <xs:attribute name=""CBR_Request_Result__c"" type=""xs:string"" />
                        <xs:attribute name=""Applicant_First_Name_From_CBR__c"" type=""xs:string"" />
                        <xs:attribute name=""Applicant_Last_Name__c"" type=""xs:string"" />
                        <xs:attribute name=""SSN__c"" type=""xs:string"" />
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name=""SFCreditBureauInsertResponse"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""row"">
          <xs:complexType>
            <xs:sequence>
              <xs:element name=""Id"">
                <xs:complexType>
                  <xs:simpleContent>
                    <xs:extension base=""xs:string"">
                      <xs:attribute name=""IsNull"" type=""xs:boolean"" />
                    </xs:extension>
                  </xs:simpleContent>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name=""AffectedRows"" type=""xs:int"" />
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public SFCreditBureauInsert__x32017() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [2];
                _RootElements[0] = "SFCreditBureauInsertRequest";
                _RootElements[1] = "SFCreditBureauInsertResponse";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://www.cdata.com/SalesforceProvider/2017",@"SFCreditBureauInsertRequest")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SFCreditBureauInsertRequest"})]
        public sealed class SFCreditBureauInsertRequest : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SFCreditBureauInsertRequest() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SFCreditBureauInsertRequest";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.cdata.com/SalesforceProvider/2017",@"SFCreditBureauInsertResponse")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SFCreditBureauInsertResponse"})]
        public sealed class SFCreditBureauInsertResponse : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SFCreditBureauInsertResponse() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SFCreditBureauInsertResponse";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
