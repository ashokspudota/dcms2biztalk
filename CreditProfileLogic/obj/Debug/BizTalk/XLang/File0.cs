
#pragma warning disable 162

namespace CreditProfileLogic
{

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileJSONDefinition_SFApplicantionBatchedJSONRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _schema = new SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest();

        public __SalesForceCreditProfileJSONDefinition_SFApplicantionBatchedJSONRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileJSONDefinition_SFApplicantionBatchedJSONRequest__)
        },
        0,
        @"http://SBA.CBR.CreditProfile.SFApplicationRequest#SFApplicationRequest"
    )]
    [System.SerializableAttribute]
    sealed internal class SFApplicationJSONRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileJSONDefinition_SFApplicantionBatchedJSONRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileJSONDefinition_SFApplicantionBatchedJSONRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFApplicationJSONRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileJSONDefinition_SFApplicantsDebatchedJSONRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest _schema = new SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest();

        public __SalesForceCreditProfileJSONDefinition_SFApplicantsDebatchedJSONRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileJSONDefinition_SFApplicantsDebatchedJSONRequest__)
        },
        0,
        @"http://SBA.CBR.CreditProfile.Applicants#applicants"
    )]
    [System.SerializableAttribute]
    sealed internal class SFApplicantDebatchedJSONRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileJSONDefinition_SFApplicantsDebatchedJSONRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileJSONDefinition_SFApplicantsDebatchedJSONRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFApplicantDebatchedJSONRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __ExperianCreditProfileDefinition_NetConnectRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static ExperianCreditProfileDefinition.NetConnectRequest _schema = new ExperianCreditProfileDefinition.NetConnectRequest();

        public __ExperianCreditProfileDefinition_NetConnectRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(ExperianCreditProfileDefinition.NetConnectRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__ExperianCreditProfileDefinition_NetConnectRequest__)
        },
        0,
        @"http://www.experian.com/NetConnect#NetConnectRequest"
    )]
    [System.SerializableAttribute]
    sealed internal class ExperianCreditProfileRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __ExperianCreditProfileDefinition_NetConnectRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __ExperianCreditProfileDefinition_NetConnectRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public ExperianCreditProfileRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __ExperianCreditProfileDefinition_NetConnectResponse__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static ExperianCreditProfileDefinition.NetConnectResponse _schema = new ExperianCreditProfileDefinition.NetConnectResponse();

        public __ExperianCreditProfileDefinition_NetConnectResponse__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(ExperianCreditProfileDefinition.NetConnectResponse)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__ExperianCreditProfileDefinition_NetConnectResponse__)
        },
        0,
        @"http://www.experian.com/NetConnectResponse#NetConnectResponse"
    )]
    [System.SerializableAttribute]
    sealed internal class ExperianCreditProfileResponseType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __ExperianCreditProfileDefinition_NetConnectResponse__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __ExperianCreditProfileDefinition_NetConnectResponse__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public ExperianCreditProfileResponseType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest _schema = new SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest();

        public __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertRequest__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauInsertRequest"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauInsertRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauInsertRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertResponse__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse _schema = new SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse();

        public __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertResponse__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertResponse__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauInsertResponse"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauInsertResponseType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertResponse__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauInsert__x32017_SFCreditBureauInsertResponse__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauInsertResponseType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest _schema = new SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest();

        public __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertRequest__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauLineItemsInsertRequest"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauLineItemInsertRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauLineItemInsertRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertResponse__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertResponse _schema = new SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertResponse();

        public __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertResponse__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertResponse)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertResponse__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauLineItemsInsertResponse"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauLineItemInsertResponseType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertResponse__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauLineItemsInsert__x32017_SFCreditBureauLineItemsInsertResponse__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauLineItemInsertResponseType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventRequest__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest _schema = new SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest();

        public __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventRequest__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventRequest__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauEventRequest"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauEventRequestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventRequest__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventRequest__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauEventRequestType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventResponse__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventResponse _schema = new SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventResponse();

        public __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventResponse__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventResponse)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventResponse__)
        },
        0,
        @"http://www.cdata.com/SalesforceProvider/2017#SFCreditBureauEventResponse"
    )]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauEventResponseType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventResponse__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileDefinition_SFCreditBureauEventRequest__x32017_SFCreditBureauEventResponse__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public SFCreditBureauEventResponseType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileJSONDefinition_BTSMessageGUID__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileJSONDefinition.BTSMessageGUID _schema = new SalesForceCreditProfileJSONDefinition.BTSMessageGUID();

        public __SalesForceCreditProfileJSONDefinition_BTSMessageGUID__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileJSONDefinition.BTSMessageGUID)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileJSONDefinition_BTSMessageGUID__)
        },
        0,
        @"http://SBA.CBR.CreditProfile#BTSMessageGUID"
    )]
    [System.SerializableAttribute]
    sealed internal class BTSMessageGUIDType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileJSONDefinition_BTSMessageGUID__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileJSONDefinition_BTSMessageGUID__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public BTSMessageGUIDType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __Microsoft_XLANGs_BaseTypes_Any__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static Microsoft.XLANGs.BaseTypes.Any _schema = new Microsoft.XLANGs.BaseTypes.Any();

        public __Microsoft_XLANGs_BaseTypes_Any__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(Microsoft.XLANGs.BaseTypes.Any)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__Microsoft_XLANGs_BaseTypes_Any__)
        },
        0,
        Microsoft.XLANGs.Core.XMessage.AnyMessageTypeName
    )]
    [System.SerializableAttribute]
    sealed internal class TempSingleApplicantType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __Microsoft_XLANGs_BaseTypes_Any__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __Microsoft_XLANGs_BaseTypes_Any__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public TempSingleApplicantType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __SalesForceCreditProfileJSONDefinition_ExceptionInformation__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static SalesForceCreditProfileJSONDefinition.ExceptionInformation _schema = new SalesForceCreditProfileJSONDefinition.ExceptionInformation();

        public __SalesForceCreditProfileJSONDefinition_ExceptionInformation__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eNone,
        "",
        new System.Type[]{
            typeof(SalesForceCreditProfileJSONDefinition.ExceptionInformation)
        },
        new string[]{
            "MessagePart"
        },
        new System.Type[]{
            typeof(__SalesForceCreditProfileJSONDefinition_ExceptionInformation__)
        },
        0,
        @"http://SBACBRCreditProfile.ExceptionInformation#ExceptionInfo"
    )]
    [System.SerializableAttribute]
    sealed internal class ExceptionInformationType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __SalesForceCreditProfileJSONDefinition_ExceptionInformation__ MessagePart;

        private void __CreatePartWrappers()
        {
            MessagePart = new __SalesForceCreditProfileJSONDefinition_ExceptionInformation__(this, "MessagePart", 0);
            this.AddPart("MessagePart", 0, MessagePart);
        }

        public ExceptionInformationType(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation",
        new System.Type[]{
            typeof(CreditProfileLogic.SFApplicationJSONRequestType)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class SFJSONApplicationRequestPortType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public SFJSONApplicationRequestPortType(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public SFJSONApplicationRequestPortType(SFJSONApplicationRequestPortType p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            SFJSONApplicationRequestPortType p = new SFJSONApplicationRequestPortType(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation",
            System.Web.Services.Description.OperationFlow.OneWay,
            typeof(SFJSONApplicationRequestPortType),
            typeof(SFApplicationJSONRequestType),
            null,
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation" ] = Operation;
                return h;
            }
        }
        #endregion // port reflection support
    }

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation",
        new System.Type[]{
            typeof(CreditProfileLogic.SFCreditBureauLineItemInsertRequestType), 
            typeof(CreditProfileLogic.SFCreditBureauLineItemInsertResponseType)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauLineItemInsertRequestResponsePortType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public SFCreditBureauLineItemInsertRequestResponsePortType(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public SFCreditBureauLineItemInsertRequestResponsePortType(SFCreditBureauLineItemInsertRequestResponsePortType p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            SFCreditBureauLineItemInsertRequestResponsePortType p = new SFCreditBureauLineItemInsertRequestResponsePortType(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation",
            System.Web.Services.Description.OperationFlow.RequestResponse,
            typeof(SFCreditBureauLineItemInsertRequestResponsePortType),
            typeof(SFCreditBureauLineItemInsertRequestType),
            typeof(SFCreditBureauLineItemInsertResponseType),
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation" ] = Operation;
                return h;
            }
        }
        #endregion // port reflection support
    }

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation",
        new System.Type[]{
            typeof(CreditProfileLogic.SFCreditBureauEventRequestType), 
            typeof(CreditProfileLogic.SFCreditBureauEventResponseType)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauEventRequestResponsePortType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public SFCreditBureauEventRequestResponsePortType(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public SFCreditBureauEventRequestResponsePortType(SFCreditBureauEventRequestResponsePortType p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            SFCreditBureauEventRequestResponsePortType p = new SFCreditBureauEventRequestResponsePortType(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation",
            System.Web.Services.Description.OperationFlow.RequestResponse,
            typeof(SFCreditBureauEventRequestResponsePortType),
            typeof(SFCreditBureauEventRequestType),
            typeof(SFCreditBureauEventResponseType),
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation" ] = Operation;
                return h;
            }
        }
        #endregion // port reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.ePublic,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eThirdKind,
        "ExperianCreditProfileDefinition.NetConnectRequest",
        new System.Type[]{
            typeof(ExperianCreditProfileDefinition.NetConnectRequest)
        },
        new string[]{
            "part"
        },
        new System.Type[]{
            typeof(__ExperianCreditProfileDefinition_NetConnectRequest__)
        },
        0,
        @"http://www.experian.com/NetConnect#NetConnectRequest"
    )]
    [System.SerializableAttribute]
    sealed public class __messagetype_ExperianCreditProfileDefinition_NetConnectRequest : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __ExperianCreditProfileDefinition_NetConnectRequest__ part;

        private void __CreatePartWrappers()
        {
            part = new __ExperianCreditProfileDefinition_NetConnectRequest__(this, "part", 0);
            this.AddPart("part", 0, part);
        }

        public __messagetype_ExperianCreditProfileDefinition_NetConnectRequest(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation_1",
        new System.Type[]{
            typeof(CreditProfileLogic.__messagetype_ExperianCreditProfileDefinition_NetConnectRequest)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class TestType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public TestType(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public TestType(TestType p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            TestType p = new TestType(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation_1 = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation_1",
            System.Web.Services.Description.OperationFlow.OneWay,
            typeof(TestType),
            typeof(__messagetype_ExperianCreditProfileDefinition_NetConnectRequest),
            null,
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation_1" ] = Operation_1;
                return h;
            }
        }
        #endregion // port reflection support
    }

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation",
        new System.Type[]{
            typeof(CreditProfileLogic.SFCreditBureauInsertRequestType), 
            typeof(CreditProfileLogic.SFCreditBureauInsertResponseType)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class SFCreditBureauInsertRequestResponseType : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public SFCreditBureauInsertRequestResponseType(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public SFCreditBureauInsertRequestResponseType(SFCreditBureauInsertRequestResponseType p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            SFCreditBureauInsertRequestResponseType p = new SFCreditBureauInsertRequestResponseType(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation",
            System.Web.Services.Description.OperationFlow.RequestResponse,
            typeof(SFCreditBureauInsertRequestResponseType),
            typeof(SFCreditBureauInsertRequestType),
            typeof(SFCreditBureauInsertResponseType),
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation" ] = Operation;
                return h;
            }
        }
        #endregion // port reflection support
    }
    //#line 1703 "C:\Users\ashok.pudota\Documents\visual studio 2015\Projects\SBACBRCreditProfile\CreditProfileLogic\CBRProcessingLogic.odx"
    [Microsoft.XLANGs.BaseTypes.StaticSubscriptionAttribute(
        0, "SFJSONApplicationRequestPort", "Operation", -1, -1, true
    )]
    [Microsoft.XLANGs.BaseTypes.ServicePortsAttribute(
        new Microsoft.XLANGs.BaseTypes.EXLangSParameter[] {
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.ePort|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eImplements,
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.ePort|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses,
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.ePort|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses,
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.ePort|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses
        },
        new System.Type[] {
            typeof(CreditProfileLogic.SFJSONApplicationRequestPortType),
            typeof(CreditProfileLogic.SFCreditBureauInsertRequestResponseType),
            typeof(CreditProfileLogic.SFCreditBureauLineItemInsertRequestResponsePortType),
            typeof(CreditProfileLogic.SFCreditBureauEventRequestResponsePortType)
        },
        new System.String[] {
            "SFJSONApplicationRequestPort",
            "SFCreditBureauInsertRequestResponse",
            "SFCreditBureauLineItemInsertRequestResponsePort",
            "SFCreditBureauEventRequestResponsePort"
        },
        new System.Type[] {
            null,
            null,
            null,
            null
        }
    )]
    [Microsoft.XLANGs.BaseTypes.ServiceCallTreeAttribute(
        new System.Type[] {
            typeof(SBACBRCreditProfile.SendToExperian)
        },
        new System.Type[] {
        },
        new System.Type[] {
        }
    )]
    [Microsoft.XLANGs.BaseTypes.ServiceAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSServiceInfo.eNone
    )]
    [System.SerializableAttribute]
    [Microsoft.XLANGs.BaseTypes.BPELExportableAttribute(false)]
    sealed internal class CBRProcessingLogic : Microsoft.BizTalk.XLANGs.BTXEngine.BTXService
    {
        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        public static readonly bool __execable = false;
        [Microsoft.XLANGs.BaseTypes.CallCompensationAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSCallCompensationInfo.eHasRequestResponse
,
            new System.String[] {
                "SBACBRCreditProfile.SendToExperian"
            },
            new System.String[] {
            }
        )]
        public static void __bodyProxy()
        {
        }
        private static System.Guid _serviceId = Microsoft.XLANGs.Core.HashHelper.HashServiceType(typeof(CBRProcessingLogic));
        private static volatile System.Guid[] _activationSubIds;

        private static new object _lockIdentity = new object();

        public static System.Guid UUID { get { return _serviceId; } }
        public override System.Guid ServiceId { get { return UUID; } }

        protected override System.Guid[] ActivationSubGuids
        {
            get { return _activationSubIds; }
            set { _activationSubIds = value; }
        }

        protected override object StaleStateLock
        {
            get { return _lockIdentity; }
        }

        protected override bool HasActivation { get { return true; } }

        internal bool IsExeced = false;

        static CBRProcessingLogic()
        {
            Microsoft.BizTalk.XLANGs.BTXEngine.BTXService.CacheStaticState( _serviceId );
        }

        private void ConstructorHelper()
        {
            _segments = new Microsoft.XLANGs.Core.Segment[] {
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment0), 0, 0, 0),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment1), 1, 1, 1),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment2), 1, 2, 2),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment3), 1, 3, 3),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment4), 1, 2, 4),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment5), 1, 4, 5),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment6), 1, 5, 6),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment7), 1, 6, 7),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment8), 1, 6, 8),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment9), 1, 4, 9),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment10), 1, 4, 10)
            };

            _Locks = 0;
            _rootContext = new __CBRProcessingLogic_root_0(this);
            _stateMgrs = new Microsoft.XLANGs.Core.IStateManager[8];
            _stateMgrs[0] = _rootContext;
            FinalConstruct();
        }

        public CBRProcessingLogic(System.Guid instanceId, Microsoft.BizTalk.XLANGs.BTXEngine.BTXSession session, Microsoft.BizTalk.XLANGs.BTXEngine.BTXEvents tracker)
            : base(instanceId, session, "CBRProcessingLogic", tracker)
        {
            ConstructorHelper();
        }

        public CBRProcessingLogic(int callIndex, System.Guid instanceId, Microsoft.BizTalk.XLANGs.BTXEngine.BTXService parent)
            : base(callIndex, instanceId, parent, "CBRProcessingLogic")
        {
            ConstructorHelper();
        }

        private const string _symInfo = @"
<XsymFile>
<ProcessFlow xmlns:om='http://schemas.microsoft.com/BizTalk/2003/DesignerData'>      <shapeType>RootShape</shapeType>      <ShapeID>f2ffff82-6154-4551-9076-56aa4f33f97d</ShapeID>      
<children>                          
<ShapeInfo>      <shapeType>TaskShape</shapeType>      <ShapeID>c69b5a51-d449-44fc-8b19-89cc67056961</ShapeID>      <ParentLink>ServiceBody_Statement</ParentLink>                <shapeText>CBR</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>a4a545ba-ff35-4a9e-a619-ff1d781eb2ba</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive Application</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ScopeShape</shapeType>      <ShapeID>d1449556-428c-422a-9634-1202c0300ad2</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Call Experian ECAL</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>CatchShape</shapeType>      <ShapeID>c5e8fbcd-4105-49a6-9133-e0cbbd80bab4</ShapeID>      <ParentLink>Scope_Catch</ParentLink>                <shapeText>ECAL Rules Exception</shapeText>                      <ExceptionType>Microsoft.RuleEngine.PolicyExecutionException</ExceptionType>            
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>78530606-81d2-40b1-988c-e9fe6f7d80aa</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Construct Error CBR Request Msg</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>0382fd53-2ea0-4e98-9c55-3e9512f4e9a9</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Experian Exception</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>c003072e-741e-4453-858a-3294b7e2841e</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Transform</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>c2770fb9-7f4e-4c99-9217-121ae3a3315e</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_28</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>cc445481-4267-4923-a711-fe7c30ff43d6</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_27</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>1d9eb84b-0d09-442d-94fd-4175bb6692d5</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_30</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>2cda30ad-3a93-49e0-93c7-07f97cac2ff2</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>b2ecad1d-6975-419a-93fd-3d1a82b3747f</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>ea885e4a-633d-4859-b1ca-eca79b72fd4e</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>cfa1433f-d873-4d67-ab8a-cc8f6e66ae6a</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>34c352f3-c4e3-420b-b798-f6e07837a4c4</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CallRulesShape</shapeType>      <ShapeID>778ecee9-8a90-4b9e-8d8f-06087cc2704e</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Call Get Address Rules</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>RulesParameterRefShape</shapeType>      <ShapeID>c13d3b88-ae8f-48be-a95e-a9d775e27ed5</ShapeID>      <ParentLink>CallRules_RulesParameterRef</ParentLink>                <shapeText>RulesParameterRef_2</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TaskShape</shapeType>      <ShapeID>e153eaa3-ae0d-4293-81eb-9e355b33c92b</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>SF Experian Process Logic</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ScopeShape</shapeType>      <ShapeID>6341733d-b749-4128-80e9-59a88aa3758e</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Scope Experian SF</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>dde6eb62-66b1-4638-979a-c26c9c4cff9d</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Get Address</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionShape</shapeType>      <ShapeID>4b25ff9b-6dcd-4a75-8a84-dbe5e60342bf</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Address Exists</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>1065f2fe-42d6-46dd-a1b3-f02bdf869870</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Yes</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>2d9171d2-e3bb-46e7-bac5-cfa9824aca9b</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Applicants Count</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CallRulesShape</shapeType>      <ShapeID>36c2b8fa-17f9-4341-8874-55977be79319</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Get Experian Values</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>RulesParameterRefShape</shapeType>      <ShapeID>c9bae39e-d562-447f-862b-5e6387fa4423</ShapeID>      <ParentLink>CallRules_RulesParameterRef</ParentLink>                <shapeText>RulesParameterRef_1</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>WhileShape</shapeType>      <ShapeID>61b984fd-743f-449b-9f52-612cc56ce926</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Loop through Applicant</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>d12da5b5-c81a-4705-95ea-2b5a638ade56</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Applicant CBRRequest</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>9d71fb1e-85d2-49ef-a21b-e40f105dbeac</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make SingleApplicant</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>5cdf32fa-5ef3-4d86-b485-b5d6b2aba9ed</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Applicant Experian Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>bc5a7f29-6779-4a0f-a9e3-76112fad7076</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_17</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>f433840c-0152-4c49-b7b5-8a4624dc5a5b</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_12</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>6b9ffd31-a7c1-4d0d-97e4-e655b70622af</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Assigin Experian Values To CBR Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>cb5e758d-2040-43da-9a98-5c37741a069e</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>29378b95-1be5-473d-b690-35971d6881da</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>49db4748-2b7b-475c-9603-cfa17f0a14ca</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CallShape</shapeType>      <ShapeID>498a3174-d167-4170-a9cb-f4d2216d8058</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Call Experian</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ParameterShape</shapeType>      <ShapeID>8c14af2f-86b2-4bb8-bc36-2f507f7526f3</ShapeID>      <ParentLink>InvokeStatement_Parameter</ParentLink>                <shapeText>ExperianCreditProfileResponseMsg</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ParameterShape</shapeType>      <ShapeID>6be42257-b2fd-4782-a2db-540b6ec65da0</ShapeID>      <ParentLink>InvokeStatement_Parameter</ParentLink>                <shapeText>ExperianCreditProfileRequestMsg</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ParameterShape</shapeType>      <ShapeID>ecd326dd-408f-4f02-810b-daac00782bd2</ShapeID>      <ParentLink>InvokeStatement_Parameter</ParentLink>                <shapeText>GetExperianValues</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ParameterShape</shapeType>      <ShapeID>fff940ff-feba-46bd-8552-011a698ef075</ShapeID>      <ParentLink>InvokeStatement_Parameter</ParentLink>                <shapeText>RealAddress</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>0da99da2-46c5-47fe-a3c9-087c5f20b352</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct BTSMessage GUID</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>5c7d75fe-309f-4db1-8a0c-f8d94a4fc3f9</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>150caaca-ddc4-4f24-8719-a69b9ded12b0</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>b6dc8acb-72a4-4f5c-a8cb-1b2d704bc424</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make BTS Message</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionShape</shapeType>      <ShapeID>7631ae8d-135e-4607-923a-87ae5612f1c8</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Decide Experian Status</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>2f23de8c-40ec-4d14-a3fc-8da8608c71c0</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Failure</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>b0d25145-adad-4a67-a220-e8e7f46006a0</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Invalid Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>52ff19a4-090d-4747-905c-546e9a125441</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>6d90ed81-6ecd-4a10-a506-b26a414b2144</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make ErrorRequest</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>2db8d922-ffe8-403a-b2f9-2d8528799738</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_10</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>92bc6f1e-5fb7-4d1c-8cd2-6a8f14c40976</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_9</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>cc3d36f0-5c9d-445e-b94d-95c9cc8e4636</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_25</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>1d3c449c-5d24-49b2-b468-8751b63b1870</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_26</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>6ecb37a4-76eb-4af2-aeab-a56bc30eec61</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>927cfde3-f25a-42e0-9519-93c8317727d4</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>9ecf73a8-4990-421a-a1f7-71a37407df43</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Flag</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>3d1820f2-9255-4e20-bbdb-f4639dda8886</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Error</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>TaskShape</shapeType>      <ShapeID>e7e61001-9137-4ba1-a469-9b53843bbfbd</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Experian Success Response</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionShape</shapeType>      <ShapeID>613283e1-51fe-47e7-a711-f3b7d2a2cfe5</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Decide Success Codes</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>6e76e4a7-6e58-4075-a224-1356e00b479b</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Field Level WarningInfo</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>048525ec-3a33-4bd9-a614-e9c1713b905a</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Frozen Message</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>16a449e1-82ed-4e5c-bc92-cdfc14e4af51</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>7c9e7526-78c0-4db6-b64d-4fd6b97525c1</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Frozen or Locked Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>acd09a32-8113-4927-a310-c8558bdd8753</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_13</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>e7fe4c23-7ba2-4280-abb7-e726f9594cc6</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_21</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>834c9024-8ac2-4202-9609-cc6eead9cb2a</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_22</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>147ebc59-b198-4f47-b2fc-1a4fe20291b4</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_14</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>f44b021e-4ba9-4ddd-901a-1d62e657523a</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send CreditBureau Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>9bec3628-dde5-4009-9e9d-f2981bc68a3e</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>aa004203-d197-417a-9419-adacb2227270</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>No Records Found</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>76e06d59-33f9-4c82-8046-92780dbfdcb9</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Field Error Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>8830686f-934c-40cd-a3fd-6de70bdbd442</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make NoRecord Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>8617b8e4-1c32-46f5-8c8d-431abdbcfe51</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_4</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b3a7099b-0783-449b-9524-347e76516007</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_5</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>4652bb14-121b-4e37-8a26-bdd596c96faa</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_16</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>75476865-6f2d-41a1-8426-a3ec2fa876f4</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_15</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>bb44a598-0804-440c-b90c-e17d24d9ddc4</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>a2e9aade-6ba9-4d7c-b34d-dcf694567f33</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>6f8730d5-8272-479c-b73f-631033b10c87</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>5e43ee1b-7c9f-4456-97c2-0eb5176d0730</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>CBRInfo</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>TaskShape</shapeType>      <ShapeID>8c845b30-3bb0-4a90-ba29-08b1806fc261</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send CreditProfile Information To Salesforce</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ScopeShape</shapeType>      <ShapeID>0e23109b-df0d-47a4-b524-55f7cdb53526</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>SF Insertion Scope</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>fabb4833-77ce-41d1-9439-88ec1ed5cef4</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct SF CreditBureau Insert Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>cc4de129-7892-4641-b0ad-41aebc31775b</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>fc402fb2-1ee1-4ee5-b8c6-293a34b6a5cb</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make SF CreditBureau Insert Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>fe15aca0-d780-4f6c-ad51-fdeeea0b979b</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_1</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>fd79dc67-66f4-41e8-ad9f-b74dc3c5571f</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_3</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>acef41e2-604d-4cec-a5eb-0eb2e02dee59</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_23</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>44a2159e-fe04-4124-aa0f-c2db23101ca7</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_24</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>21308833-cbdb-4647-b481-d374a91ceb4c</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_2</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>eeacbc48-66e1-4e8a-9624-a13c58f7cfc6</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send CreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>f99080fc-ee35-4774-96c1-505ce5000b31</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive CreditBureau Response</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>8a2b5447-9583-4d25-a4b5-2c2636942b24</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct SF LineItems Insert Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>f12b1572-792b-40a4-9201-78f65bbccc92</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make SF LineItems Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>a26a6a82-04b5-4a88-8fac-34a0053f6929</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_6</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b19e7118-6de6-4553-ba5b-88b67ba1a9b1</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_7</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b006ce90-2283-4578-8bba-5668cd893391</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_8</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>6c3af5cf-d4c9-465c-b198-b9b070f1544f</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>d3887c9e-42be-4e6b-9f18-816803ca9611</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send LineItem Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>5b011c9f-0eb6-4661-b12d-3a07d083f6ce</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive LineItem Response</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CatchShape</shapeType>      <ShapeID>78ff7814-16cd-469a-8c8e-2c5b92f9ca1a</ShapeID>      <ParentLink>Scope_Catch</ParentLink>                <shapeText>SF System Exception</shapeText>                      <ExceptionType>System.SystemException</ExceptionType>            
<children>                          
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>d035ca79-2347-486e-b08e-7b340a6d607c</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Expression</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>3af9c5ed-4189-423f-8d0c-c3145bf1abc3</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Flag</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>2ecf16ef-f313-45f3-920c-fa8d05f67576</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>LoopIncrement</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>c111e6c6-3c3e-400b-b7d7-e75e07f5486e</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Else</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>VariableAssignmentShape</shapeType>      <ShapeID>e47787ac-f710-4289-9389-f2b551334935</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Flag</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>0715f746-7350-4d32-a0c4-d087f9f2c6e2</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Error CBR Request Msg</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>2f73724e-1a26-47d3-9620-afaa54bd0a09</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>02c00e57-0c5d-48f9-a378-be32f1f35932</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Transform</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>be3d2b45-9bf4-477d-8d25-8950f736dc7e</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_29</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b63b4cd0-e9d7-45d5-b1cb-0111691fe54d</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_31</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>b8420b62-eb4a-4c02-a5b4-d4feff2b2679</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>e4027b2d-8a87-4b3b-a3a7-87bb4ce14bca</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CatchShape</shapeType>      <ShapeID>457e6dba-a406-4840-9472-d8371ce68b8b</ShapeID>      <ParentLink>Scope_Catch</ParentLink>                <shapeText>Rules Exception</shapeText>                      <ExceptionType>Microsoft.RuleEngine.PolicyExecutionException</ExceptionType>            
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>5259c6ba-3646-4219-abe0-abf8d3ef95c9</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Construct Error CBR Request Msg</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>6b973cd8-e3b7-472a-b769-db117e9dd827</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Experian Exception</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>2b721528-ab35-4334-8984-b8817f80af85</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Transform</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>83b2f0f6-74fc-4240-9a97-6ff2dcfeeed4</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_35</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>4cfb2b81-5ec0-4de0-82e6-621e61d9ceab</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_36</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>edcb4849-feef-4c1b-a585-fbd87dc740da</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_37</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>22cd2356-2c08-482b-a7a7-9cfb103c40dd</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>4734b05b-afe1-4477-af70-f52674be753b</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>67906371-2939-4b02-a059-550f27c23120</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>12a10061-a925-4d19-a368-6bddefd80cef</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>937564fd-7367-423e-9149-6fe7d68ffd15</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>CatchShape</shapeType>      <ShapeID>6f2d249c-5985-43d5-9582-54feffc49319</ShapeID>      <ParentLink>Scope_Catch</ParentLink>                <shapeText>Ssystem Exception</shapeText>                      <ExceptionType>System.Exception</ExceptionType>            
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>1ad307d3-daeb-49c2-a072-64159bed5320</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Construct Error CBR Request Msg</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>08f6fc2d-209a-45f6-9a99-d25fa350e320</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Experian Exception</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>7910a671-9f5d-45ba-82e4-03d8bf010312</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Transform</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>5d886c68-bff0-45df-9c8f-991d27a4d6d7</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_32</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>ad3943c8-a14b-4880-936e-d5d2f1882d54</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_33</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>beb5782b-049e-45e2-b016-43bd938d1d0c</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_34</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>645ffa4f-3854-4fdb-b55a-f45223f3cecc</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>4f830141-02d8-47b4-9661-3967484595e0</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>e1ca6317-bf62-4dc7-935d-61201a3f88b9</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>93d406c7-f7e1-4fc7-8c62-392eafee8da4</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Send To SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>90c1a1a6-f566-4742-a9b2-85ee3c2abc2d</ShapeID>      <ParentLink>Catch_Statement</ParentLink>                <shapeText>Receive From SFCreditBureau</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TaskShape</shapeType>      <ShapeID>b99c7c48-c72d-4c33-b5f7-f45499252120</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send Application Status Information To Salesforce EventRequest</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionShape</shapeType>      <ShapeID>3e37ff95-22a9-4fff-890b-66d6093d026a</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Decide Event Status</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>edfd123f-4588-4fb1-8980-61d0e17332d9</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Success</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>683f48ba-09f9-4610-85f1-68052b2a90b3</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Success EventRequest Message</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>c3a8776d-b491-4679-9730-86647d13f4df</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>33aac55d-7c2e-41f2-ba00-d92ef5bdb951</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Success Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b20aa6d8-e46c-47be-b49f-7f53ac9fc7b8</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_11</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>2e20458c-fed7-42f9-9795-5791ae69b11d</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_18</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>218ce861-41e6-4b82-bdb1-abfc1c05b4b9</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send Event Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>ea8a02e7-9330-4381-8787-0dbb334bef31</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive Event Response</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>DecisionBranchShape</shapeType>      <ShapeID>fae9573f-75ed-4bd6-87cb-6192366d385b</ShapeID>      <ParentLink>ReallyComplexStatement_Branch</ParentLink>                <shapeText>Else</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>901a1a80-b6b7-4e1c-8707-f7623452eb34</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Construct Error EventReques Message</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>bba9e9c7-f6c2-4950-8e6c-d759e1d61c4a</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>TransformShape</shapeType>      <ShapeID>ddb487a2-20fa-44ff-81c0-8e0cffb7ed17</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make Error Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>b012ac93-0133-4782-9f4d-4a3b8d1e8269</ShapeID>      <ParentLink>Transform_OutputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_20</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessagePartRefShape</shapeType>      <ShapeID>c21eeaa2-c82b-48ca-9776-096a4fe50037</ShapeID>      <ParentLink>Transform_InputMessagePartRef</ParentLink>                <shapeText>MessagePartReference_19</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>d97a49a1-f0d9-4749-9616-156de641ba97</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Send Event Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>d517f92a-29d5-4e7d-bf48-946cea159683</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Receive Event Response</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                  </children>
  </ProcessFlow><Metadata>

<TrkMetadata>
<ActionName>'CBRProcessingLogic'</ActionName><IsAtomic>0</IsAtomic><Line>1703</Line><Position>14</Position><ShapeID>'e211a116-cb8b-44e7-a052-0de295aa0001'</ShapeID>
</TrkMetadata>

<TrkMetadata>
<Line>1747</Line><Position>22</Position><ShapeID>'a4a545ba-ff35-4a9e-a619-ff1d781eb2ba'</ShapeID>
<Messages>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<ActionName>'??__scope33'</ActionName><IsAtomic>0</IsAtomic><Line>1760</Line><Position>13</Position><ShapeID>'d1449556-428c-422a-9634-1202c0300ad2'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<ActionName>'??__scope34'</ActionName><IsAtomic>0</IsAtomic><Line>1766</Line><Position>21</Position><ShapeID>'778ecee9-8a90-4b9e-8d8f-06087cc2704e'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1784</Line><Position>21</Position><ShapeID>'c5e8fbcd-4105-49a6-9133-e0cbbd80bab4'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1787</Line><Position>25</Position><ShapeID>'78530606-81d2-40b1-988c-e9fe6f7d80aa'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1802</Line><Position>25</Position><ShapeID>'cfa1433f-d873-4d67-ab8a-cc8f6e66ae6a'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1804</Line><Position>25</Position><ShapeID>'34c352f3-c4e3-420b-b798-f6e07837a4c4'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<ActionName>'??__scope36'</ActionName><IsAtomic>0</IsAtomic><Line>1809</Line><Position>13</Position><ShapeID>'6341733d-b749-4128-80e9-59a88aa3758e'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1814</Line><Position>33</Position><ShapeID>'dde6eb62-66b1-4638-979a-c26c9c4cff9d'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1822</Line><Position>21</Position><ShapeID>'4b25ff9b-6dcd-4a75-8a84-dbe5e60342bf'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1826</Line><Position>37</Position><ShapeID>'2d9171d2-e3bb-46e7-bac5-cfa9824aca9b'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<ActionName>'??__scope37'</ActionName><IsAtomic>0</IsAtomic><Line>1833</Line><Position>25</Position><ShapeID>'36c2b8fa-17f9-4341-8874-55977be79319'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1848</Line><Position>25</Position><ShapeID>'61b984fd-743f-449b-9f52-612cc56ce926'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1851</Line><Position>29</Position><ShapeID>'d12da5b5-c81a-4705-95ea-2b5a638ade56'</ShapeID>
<Messages>
	<MsgInfo><name>ExperianCreditProfileRequestMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>TempSingleApplicantMsg</name><part>MessagePart</part><schema>Microsoft.XLANGs.BaseTypes.Any</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicantDebatchedJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicantDebatchedJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1889</Line><Position>69</Position><ShapeID>'498a3174-d167-4170-a9cb-f4d2216d8058'</ShapeID>
<Messages>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileRequestMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1891</Line><Position>29</Position><ShapeID>'0da99da2-46c5-47fe-a3c9-087c5f20b352'</ShapeID>
<Messages>
	<MsgInfo><name>BTSMessageGUIDMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.BTSMessageGUID</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1916</Line><Position>29</Position><ShapeID>'7631ae8d-135e-4607-923a-87ae5612f1c8'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1919</Line><Position>33</Position><ShapeID>'b0d25145-adad-4a67-a220-e8e7f46006a0'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>BTSMessageGUIDMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.BTSMessageGUID</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1925</Line><Position>33</Position><ShapeID>'6ecb37a4-76eb-4af2-aeab-a56bc30eec61'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1927</Line><Position>33</Position><ShapeID>'927cfde3-f25a-42e0-9519-93c8317727d4'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1929</Line><Position>38</Position><ShapeID>'9ecf73a8-4990-421a-a1f7-71a37407df43'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1934</Line><Position>33</Position><ShapeID>'613283e1-51fe-47e7-a711-f3b7d2a2cfe5'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1937</Line><Position>37</Position><ShapeID>'048525ec-3a33-4bd9-a614-e9c1713b905a'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>BTSMessageGUIDMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.BTSMessageGUID</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1943</Line><Position>37</Position><ShapeID>'f44b021e-4ba9-4ddd-901a-1d62e657523a'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1945</Line><Position>37</Position><ShapeID>'9bec3628-dde5-4009-9e9d-f2981bc68a3e'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1950</Line><Position>37</Position><ShapeID>'76e06d59-33f9-4c82-8046-92780dbfdcb9'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>BTSMessageGUIDMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.BTSMessageGUID</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1956</Line><Position>37</Position><ShapeID>'a2e9aade-6ba9-4d7c-b34d-dcf694567f33'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1958</Line><Position>37</Position><ShapeID>'6f8730d5-8272-479c-b73f-631033b10c87'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<ActionName>'??__scope38'</ActionName><IsAtomic>0</IsAtomic><Line>1963</Line><Position>37</Position><ShapeID>'0e23109b-df0d-47a4-b524-55f7cdb53526'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1968</Line><Position>45</Position><ShapeID>'fabb4833-77ce-41d1-9439-88ec1ed5cef4'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>SFApplicantDebatchedJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>BTSMessageGUIDMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.BTSMessageGUID</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1974</Line><Position>45</Position><ShapeID>'eeacbc48-66e1-4e8a-9624-a13c58f7cfc6'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1976</Line><Position>45</Position><ShapeID>'f99080fc-ee35-4774-96c1-505ce5000b31'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1978</Line><Position>45</Position><ShapeID>'8a2b5447-9583-4d25-a4b5-2c2636942b24'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauLineItemInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>ExperianCreditProfileResponseMsg</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1984</Line><Position>45</Position><ShapeID>'d3887c9e-42be-4e6b-9f18-816803ca9611'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauLineItemInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1986</Line><Position>45</Position><ShapeID>'5b011c9f-0eb6-4661-b12d-3a07d083f6ce'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauLineItemInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1991</Line><Position>45</Position><ShapeID>'78ff7814-16cd-469a-8c8e-2c5b92f9ca1a'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>1994</Line><Position>87</Position><ShapeID>'d035ca79-2347-486e-b08e-7b340a6d607c'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2003</Line><Position>29</Position><ShapeID>'3af9c5ed-4189-423f-8d0c-c3145bf1abc3'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2006</Line><Position>39</Position><ShapeID>'2ecf16ef-f313-45f3-920c-fa8d05f67576'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2012</Line><Position>30</Position><ShapeID>'e47787ac-f710-4289-9389-f2b551334935'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2014</Line><Position>25</Position><ShapeID>'0715f746-7350-4d32-a0c4-d087f9f2c6e2'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2020</Line><Position>25</Position><ShapeID>'b8420b62-eb4a-4c02-a5b4-d4feff2b2679'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2022</Line><Position>25</Position><ShapeID>'e4027b2d-8a87-4b3b-a3a7-87bb4ce14bca'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2028</Line><Position>21</Position><ShapeID>'457e6dba-a406-4840-9472-d8371ce68b8b'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2031</Line><Position>25</Position><ShapeID>'5259c6ba-3646-4219-abe0-abf8d3ef95c9'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2046</Line><Position>25</Position><ShapeID>'12a10061-a925-4d19-a368-6bddefd80cef'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2048</Line><Position>25</Position><ShapeID>'937564fd-7367-423e-9149-6fe7d68ffd15'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2051</Line><Position>21</Position><ShapeID>'6f2d249c-5985-43d5-9582-54feffc49319'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2054</Line><Position>25</Position><ShapeID>'1ad307d3-daeb-49c2-a072-64159bed5320'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
	<MsgInfo><name>ExceptionInformationMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.ExceptionInformation</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2069</Line><Position>25</Position><ShapeID>'93d406c7-f7e1-4fc7-8c62-392eafee8da4'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2071</Line><Position>25</Position><ShapeID>'90c1a1a6-f566-4742-a9b2-85ee3c2abc2d'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauInsertResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2076</Line><Position>13</Position><ShapeID>'3e37ff95-22a9-4fff-890b-66d6093d026a'</ShapeID>
<Messages>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2079</Line><Position>17</Position><ShapeID>'683f48ba-09f9-4610-85f1-68052b2a90b3'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2085</Line><Position>17</Position><ShapeID>'218ce861-41e6-4b82-bdb1-abfc1c05b4b9'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2087</Line><Position>17</Position><ShapeID>'ea8a02e7-9330-4381-8787-0dbb334bef31'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2092</Line><Position>17</Position><ShapeID>'901a1a80-b6b7-4e1c-8707-f7623452eb34'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>SFApplicationJSONRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2098</Line><Position>17</Position><ShapeID>'d97a49a1-f0d9-4749-9616-156de641ba97'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventRequestMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>2100</Line><Position>17</Position><ShapeID>'d517f92a-29d5-4e7d-bf48-946cea159683'</ShapeID>
<Messages>
	<MsgInfo><name>SFCreditBureauEventResponseMsg</name><part>MessagePart</part><schema>SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>
</Metadata>
</XsymFile>";

        public override string odXml { get { return _symODXML; } }

        private const string _symODXML = @"
<?xml version='1.0' encoding='utf-8' standalone='yes'?>
<om:MetaModel MajorVersion='1' MinorVersion='3' Core='2b131234-7959-458d-834f-2dc0769ce683' ScheduleModel='66366196-361d-448d-976f-cab5e87496d2' xmlns:om='http://schemas.microsoft.com/BizTalk/2003/DesignerData'>
    <om:Element Type='Module' OID='4b87a6e1-03e0-4173-9749-56b5108fdc13' LowerBound='1.1' HigherBound='494.1'>
        <om:Property Name='ReportToAnalyst' Value='True' />
        <om:Property Name='Name' Value='CreditProfileLogic' />
        <om:Property Name='Signal' Value='False' />
        <om:Element Type='MultipartMessageType' OID='137a776b-c7ad-4601-abbd-41303fafd8a5' ParentLink='Module_MessageType' LowerBound='4.1' HigherBound='8.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFApplicationJSONRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='9355ffc6-081c-451f-aa2d-b615c2ab0afb' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='6.1' HigherBound='7.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='73927ff6-7cd7-4caf-bcda-1b7b164b5400' ParentLink='Module_MessageType' LowerBound='8.1' HigherBound='12.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFApplicantDebatchedJSONRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='f236c2d0-6079-48b0-b531-615c8be10889' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='10.1' HigherBound='11.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='09ab186a-8b29-43c8-9ee5-1de0a539a07c' ParentLink='Module_MessageType' LowerBound='12.1' HigherBound='16.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='ExperianCreditProfileRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='be3f676c-025c-4cbc-b4c5-d3152319f29f' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='14.1' HigherBound='15.1'>
                <om:Property Name='ClassName' Value='ExperianCreditProfileDefinition.NetConnectRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='59048d10-3e1c-4eed-bf32-476441918d4d' ParentLink='Module_MessageType' LowerBound='16.1' HigherBound='20.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='ExperianCreditProfileResponseType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='bb3be013-b374-442e-8171-0b162e350a86' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='18.1' HigherBound='19.1'>
                <om:Property Name='ClassName' Value='ExperianCreditProfileDefinition.NetConnectResponse' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='48e8406b-de7b-419b-a6ab-bc0a39907d08' ParentLink='Module_MessageType' LowerBound='20.1' HigherBound='24.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauInsertRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='7478821d-06a5-4669-9a96-870a8c409395' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='22.1' HigherBound='23.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='2e9eaa8f-d143-4a32-b3c4-841197745274' ParentLink='Module_MessageType' LowerBound='24.1' HigherBound='28.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauInsertResponseType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='0e21b336-d576-422c-afe2-a2dfd8873ae9' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='26.1' HigherBound='27.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='36b1da53-f4de-46c9-98bc-8b63baa2ed7e' ParentLink='Module_MessageType' LowerBound='28.1' HigherBound='32.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauLineItemInsertRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='78f84fbd-dccc-4b55-94ba-2e29d519275f' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='30.1' HigherBound='31.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='d9ec77c0-e928-4cd1-9b7b-93619c127973' ParentLink='Module_MessageType' LowerBound='32.1' HigherBound='36.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauLineItemInsertResponseType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='4038b582-c0cb-46a2-a9d1-3c86def7a95b' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='34.1' HigherBound='35.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertResponse' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='59ee85da-3f36-41a9-8c84-110d44e49ac1' ParentLink='Module_MessageType' LowerBound='36.1' HigherBound='40.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauEventRequestType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='d2dba33b-5c67-42f5-a464-9db22f011391' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='38.1' HigherBound='39.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='21694e94-3901-475c-b836-e69f22ff4b41' ParentLink='Module_MessageType' LowerBound='40.1' HigherBound='44.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauEventResponseType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='f3e372e7-83d0-4665-9aec-7d20ea81ca5a' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='42.1' HigherBound='43.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventResponse' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='3cc13ef6-b6c1-40e0-8b69-21cf3d0cf7ee' ParentLink='Module_MessageType' LowerBound='44.1' HigherBound='48.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='BTSMessageGUIDType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='2d3b945e-ec5b-4496-99c3-df271c8d5263' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='46.1' HigherBound='47.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileJSONDefinition.BTSMessageGUID' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='207b1b2b-bbb5-4559-a1c3-c291c1076310' ParentLink='Module_MessageType' LowerBound='48.1' HigherBound='52.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='TempSingleApplicantType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='7882d2f7-0ccc-42d5-9ac4-fe1fee7025ed' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='50.1' HigherBound='51.1'>
                <om:Property Name='ClassName' Value='System.Xml.XmlDocument' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='MultipartMessageType' OID='69bd965e-45a4-49a7-bba3-c43afec503c0' ParentLink='Module_MessageType' LowerBound='52.1' HigherBound='56.1'>
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='ExceptionInformationType' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='PartDeclaration' OID='939ed189-dd5a-4f28-83a8-e561d5cf4b40' ParentLink='MultipartMessageType_PartDeclaration' LowerBound='54.1' HigherBound='55.1'>
                <om:Property Name='ClassName' Value='SalesForceCreditProfileJSONDefinition.ExceptionInformation' />
                <om:Property Name='IsBodyPart' Value='True' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='MessagePart' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
        </om:Element>
        <om:Element Type='ServiceDeclaration' OID='0597dd95-dfc1-468b-b6a0-ae643eb12663' ParentLink='Module_ServiceDeclaration' LowerBound='91.1' HigherBound='493.1'>
            <om:Property Name='InitializedTransactionType' Value='False' />
            <om:Property Name='IsInvokable' Value='False' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='CBRProcessingLogic' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='VariableDeclaration' OID='3b8c1b09-8613-4431-9315-001f358fa308' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='121.1' HigherBound='122.1'>
                <om:Property Name='UseDefaultConstructor' Value='True' />
                <om:Property Name='Type' Value='sba.gov.ActualAddress' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='GetActualAddress' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='859b744c-a5b3-4140-bb8d-d145822fc2f4' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='122.1' HigherBound='123.1'>
                <om:Property Name='UseDefaultConstructor' Value='True' />
                <om:Property Name='Type' Value='Experian.Values' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='GetExperianValues' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='607addd9-dead-4b8c-80fe-782662326ebb' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='123.1' HigherBound='124.1'>
                <om:Property Name='InitialValue' Value='0' />
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.Int32' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='RecordCount' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='dcb17ef2-bd07-4fc0-9466-31cda19f5c28' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='124.1' HigherBound='125.1'>
                <om:Property Name='InitialValue' Value='1' />
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.Int32' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='LoopCount' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='5963f44d-1273-4edf-a752-15b7ab6ba3b0' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='125.1' HigherBound='126.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='sSingleApplicantXpath' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='31f84444-527f-4d4d-93bb-599d46b16ccb' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='126.1' HigherBound='127.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='GetCompletionCode' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='0b495afa-dab8-414c-8142-6c6b869c4a6e' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='127.1' HigherBound='128.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='GetNoRecordsMessageCode' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='d436cf5c-5bc0-4cf8-be72-860ce7b87aed' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='128.1' HigherBound='129.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.Int32' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='GetFieldLevelWarningInfo' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='2b794dae-4563-44fb-ab7a-e7584319f976' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='129.1' HigherBound='130.1'>
                <om:Property Name='InitialValue' Value='true' />
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.Boolean' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Flag' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='7e972215-eae5-48ef-98a3-776040a0f8cc' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='130.1' HigherBound='131.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ExperianStatus' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='6fb8703b-2a56-4e70-bb4e-2e1399557e43' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='131.1' HigherBound='132.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='RealAddress' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='VariableDeclaration' OID='c41b7db1-19a6-42c7-a475-c9eade11dc5b' ParentLink='ServiceDeclaration_VariableDeclaration' LowerBound='132.1' HigherBound='133.1'>
                <om:Property Name='UseDefaultConstructor' Value='False' />
                <om:Property Name='Type' Value='System.String' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='sTempExceptionInformation' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='1618a6f6-6e93-4223-92e3-d6ee537b0e44' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='104.1' HigherBound='105.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFApplicationJSONRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFApplicationJSONRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='af74642a-3d02-45b9-bf9f-67717a9a30ea' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='105.1' HigherBound='106.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFApplicantDebatchedJSONRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFApplicantDebatchedJSONRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='ac4406d8-23bb-4b22-830e-9d6dc7c9abc6' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='106.1' HigherBound='107.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ExperianCreditProfileRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='e5c0a1d8-0e8c-4f5a-961e-377fe5e3cf72' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='107.1' HigherBound='108.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileResponseType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ExperianCreditProfileResponseMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='d848293a-9489-440c-9c43-a5dd603d606b' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='108.1' HigherBound='109.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauInsertRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauInsertRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='7d34cd60-1c5a-468a-9d18-c593a2592ed9' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='109.1' HigherBound='110.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauInsertResponseType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauInsertResponseMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='bc6472e8-8067-4f37-a342-d76cf7b725f3' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='110.1' HigherBound='111.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauLineItemInsertRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauLineItemInsertRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='a275a1fe-7d21-45bd-a26f-135c9c8f1465' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='111.1' HigherBound='112.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauLineItemInsertResponseType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauLineItemInsertResponseMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='3d2f35c0-f858-4dfa-bae6-1a575d21e937' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='112.1' HigherBound='113.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauEventRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauEventRequestMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='9a77ce00-b999-4626-a767-94b0dcdc26f9' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='113.1' HigherBound='114.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauEventResponseType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauEventResponseMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='f8779b4b-434f-4ab8-8ff7-082d50c18a8d' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='114.1' HigherBound='115.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.BTSMessageGUIDType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='BTSMessageGUIDMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='e827fcf7-823e-4394-8be5-92d39cabe200' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='115.1' HigherBound='116.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.TempSingleApplicantType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='TempSingleApplicantMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='7fd55f6d-dafc-45e3-b256-e4c483fd30da' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='116.1' HigherBound='117.1'>
                <om:Property Name='Type' Value='System.Xml.XmlDocument' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='TempBTSMessageGUIDXml' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='d51230f1-461f-418f-a682-5b91b272df29' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='117.1' HigherBound='118.1'>
                <om:Property Name='Type' Value='System.Xml.XmlDocument' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='TempExperianXml' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='8318dce2-ff39-4fb1-9ea9-a4f87c76a512' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='118.1' HigherBound='119.1'>
                <om:Property Name='Type' Value='ExperianCreditProfileDefinition.GetExperianValues' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SetExperianValues' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='b96b6b78-260e-4008-9216-66f069a501ee' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='119.1' HigherBound='120.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.ExceptionInformationType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ExceptionInformationMsg' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='MessageDeclaration' OID='6068fac4-1cc7-45e4-8980-77aa0c572517' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='120.1' HigherBound='121.1'>
                <om:Property Name='Type' Value='System.Xml.XmlDocument' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='TempExceptionInformation' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='ServiceBody' OID='f2ffff82-6154-4551-9076-56aa4f33f97d' ParentLink='ServiceDeclaration_ServiceBody'>
                <om:Property Name='Signal' Value='False' />
                <om:Element Type='Task' OID='c69b5a51-d449-44fc-8b19-89cc67056961' ParentLink='ServiceBody_Statement' LowerBound='135.1' HigherBound='491.1'>
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='CBR' />
                    <om:Property Name='Signal' Value='True' />
                    <om:Element Type='Receive' OID='a4a545ba-ff35-4a9e-a619-ff1d781eb2ba' ParentLink='ComplexStatement_Statement' LowerBound='135.1' HigherBound='148.1'>
                        <om:Property Name='Activate' Value='True' />
                        <om:Property Name='PortName' Value='SFJSONApplicationRequestPort' />
                        <om:Property Name='MessageName' Value='SFApplicationJSONRequestMsg' />
                        <om:Property Name='OperationName' Value='Operation' />
                        <om:Property Name='OperationMessageName' Value='Request' />
                        <om:Property Name='ReportToAnalyst' Value='True' />
                        <om:Property Name='Name' Value='Receive Application' />
                        <om:Property Name='Signal' Value='True' />
                    </om:Element>
                    <om:Element Type='Scope' OID='d1449556-428c-422a-9634-1202c0300ad2' ParentLink='ComplexStatement_Statement' LowerBound='148.1' HigherBound='197.1'>
                        <om:Property Name='InitializedTransactionType' Value='True' />
                        <om:Property Name='IsSynchronized' Value='False' />
                        <om:Property Name='ReportToAnalyst' Value='True' />
                        <om:Property Name='Name' Value='Call Experian ECAL' />
                        <om:Property Name='Signal' Value='True' />
                        <om:Element Type='Catch' OID='c5e8fbcd-4105-49a6-9133-e0cbbd80bab4' ParentLink='Scope_Catch' LowerBound='172.1' HigherBound='195.1'>
                            <om:Property Name='ExceptionName' Value='REx' />
                            <om:Property Name='ExceptionType' Value='Microsoft.RuleEngine.PolicyExecutionException' />
                            <om:Property Name='IsFaultMessage' Value='False' />
                            <om:Property Name='ReportToAnalyst' Value='True' />
                            <om:Property Name='Name' Value='ECAL Rules Exception' />
                            <om:Property Name='Signal' Value='True' />
                            <om:Element Type='Construct' OID='78530606-81d2-40b1-988c-e9fe6f7d80aa' ParentLink='Catch_Statement' LowerBound='175.1' HigherBound='190.1'>
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Construct Error CBR Request Msg' />
                                <om:Property Name='Signal' Value='True' />
                                <om:Element Type='MessageAssignment' OID='0382fd53-2ea0-4e98-9c55-3e9512f4e9a9' ParentLink='ComplexStatement_Statement' LowerBound='178.1' HigherBound='187.1'>
                                    <om:Property Name='Expression' Value='Flag = false;&#xD;&#xA;TempExceptionInformation = new System.Xml.XmlDocument();&#xD;&#xA;&#xD;&#xA;sTempExceptionInformation = REx.Message;&#xD;&#xA;&#xD;&#xA;//TempExceptionInformation.LoadXml(System.String.Format(&quot;&lt;ns0:ExceptionInfo xmlns:ns0=&apos;http://SBACBRCreditProfile.ExceptionInformation&apos;&gt;&lt;Message&gt;&lt;![CDATA[{0}]]&gt;&lt;/Message&gt;&lt;OrchestrationId&gt;{1}&lt;/OrchestrationId&gt;&lt;/ns0:ExceptionInfo&gt;&quot;,sTempExceptionInformation,System.Convert.ToString(Microsoft.XLANGs.Core.Service.RootService.InstanceId)));&#xD;&#xA;&#xD;&#xA;ExceptionInformationMsg.MessagePart = TempExceptionInformation;&#xD;&#xA;' />
                                    <om:Property Name='ReportToAnalyst' Value='False' />
                                    <om:Property Name='Name' Value='Make Experian Exception' />
                                    <om:Property Name='Signal' Value='False' />
                                </om:Element>
                                <om:Element Type='Transform' OID='c003072e-741e-4453-858a-3294b7e2841e' ParentLink='ComplexStatement_Statement' LowerBound='187.1' HigherBound='189.1'>
                                    <om:Property Name='ClassName' Value='CreditProfileLogic.TransformException_To_SFCreditBureau' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Transform' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='MessagePartRef' OID='c2770fb9-7f4e-4c99-9217-121ae3a3315e' ParentLink='Transform_OutputMessagePartRef' LowerBound='188.40' HigherBound='188.82'>
                                        <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                        <om:Property Name='PartRef' Value='MessagePart' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='MessagePartReference_28' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessagePartRef' OID='cc445481-4267-4923-a711-fe7c30ff43d6' ParentLink='Transform_InputMessagePartRef' LowerBound='188.143' HigherBound='188.182'>
                                        <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                        <om:Property Name='PartRef' Value='MessagePart' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='MessagePartReference_27' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessagePartRef' OID='1d9eb84b-0d09-442d-94fd-4175bb6692d5' ParentLink='Transform_InputMessagePartRef' LowerBound='188.184' HigherBound='188.219'>
                                        <om:Property Name='MessageRef' Value='ExceptionInformationMsg' />
                                        <om:Property Name='PartRef' Value='MessagePart' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='MessagePartReference_30' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='MessageRef' OID='2cda30ad-3a93-49e0-93c7-07f97cac2ff2' ParentLink='Construct_MessageRef' LowerBound='176.35' HigherBound='176.65'>
                                    <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Signal' Value='False' />
                                </om:Element>
                                <om:Element Type='MessageRef' OID='b2ecad1d-6975-419a-93fd-3d1a82b3747f' ParentLink='Construct_MessageRef' LowerBound='176.67' HigherBound='176.90'>
                                    <om:Property Name='Ref' Value='ExceptionInformationMsg' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Signal' Value='False' />
                                </om:Element>
                                <om:Element Type='MessageRef' OID='ea885e4a-633d-4859-b1ca-eca79b72fd4e' ParentLink='Construct_MessageRef' LowerBound='176.92' HigherBound='176.116'>
                                    <om:Property Name='Ref' Value='TempExceptionInformation' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Signal' Value='False' />
                                </om:Element>
                            </om:Element>
                            <om:Element Type='Send' OID='cfa1433f-d873-4d67-ab8a-cc8f6e66ae6a' ParentLink='Catch_Statement' LowerBound='190.1' HigherBound='192.1'>
                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                <om:Property Name='OperationName' Value='Operation' />
                                <om:Property Name='OperationMessageName' Value='Request' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                <om:Property Name='Signal' Value='True' />
                            </om:Element>
                            <om:Element Type='Receive' OID='34c352f3-c4e3-420b-b798-f6e07837a4c4' ParentLink='Catch_Statement' LowerBound='192.1' HigherBound='194.1'>
                                <om:Property Name='Activate' Value='False' />
                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                <om:Property Name='OperationName' Value='Operation' />
                                <om:Property Name='OperationMessageName' Value='Response' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                <om:Property Name='Signal' Value='True' />
                            </om:Element>
                        </om:Element>
                        <om:Element Type='CallRules' OID='778ecee9-8a90-4b9e-8d8f-06087cc2704e' ParentLink='ComplexStatement_Statement' LowerBound='153.1' HigherBound='169.1'>
                            <om:Property Name='PolicyName' Value='Get Dynamic Address' />
                            <om:Property Name='PolicyVersion' Value='-1' />
                            <om:Property Name='ReportToAnalyst' Value='True' />
                            <om:Property Name='Name' Value='Call Get Address Rules' />
                            <om:Property Name='Signal' Value='True' />
                            <om:Element Type='RulesParameterRef' OID='c13d3b88-ae8f-48be-a95e-a9d775e27ed5' ParentLink='CallRules_RulesParameterRef'>
                                <om:Property Name='Reference' Value='GetActualAddress' />
                                <om:Property Name='Alias' Value='sba.gov.ActualAddress' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='RulesParameterRef_2' />
                                <om:Property Name='Signal' Value='False' />
                            </om:Element>
                        </om:Element>
                    </om:Element>
                    <om:Element Type='Task' OID='e153eaa3-ae0d-4293-81eb-9e355b33c92b' ParentLink='ComplexStatement_Statement' LowerBound='197.1' HigherBound='464.1'>
                        <om:Property Name='ReportToAnalyst' Value='True' />
                        <om:Property Name='Name' Value='SF Experian Process Logic' />
                        <om:Property Name='Signal' Value='True' />
                        <om:Element Type='Scope' OID='6341733d-b749-4128-80e9-59a88aa3758e' ParentLink='ComplexStatement_Statement' LowerBound='197.1' HigherBound='464.1'>
                            <om:Property Name='InitializedTransactionType' Value='True' />
                            <om:Property Name='IsSynchronized' Value='False' />
                            <om:Property Name='ReportToAnalyst' Value='True' />
                            <om:Property Name='Name' Value='Scope Experian SF' />
                            <om:Property Name='Signal' Value='True' />
                            <om:Element Type='VariableAssignment' OID='dde6eb62-66b1-4638-979a-c26c9c4cff9d' ParentLink='ComplexStatement_Statement' LowerBound='202.1' HigherBound='210.1'>
                                <om:Property Name='Expression' Value='RealAddress = GetActualAddress.Address;&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;//RealAddress = Static.Address;&#xD;&#xA;&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;RealAddress Value&quot;, RealAddress);' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Get Address' />
                                <om:Property Name='Signal' Value='False' />
                            </om:Element>
                            <om:Element Type='Decision' OID='4b25ff9b-6dcd-4a75-8a84-dbe5e60342bf' ParentLink='ComplexStatement_Statement' LowerBound='210.1' HigherBound='413.1'>
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Address Exists' />
                                <om:Property Name='Signal' Value='True' />
                                <om:Element Type='DecisionBranch' OID='1065f2fe-42d6-46dd-a1b3-f02bdf869870' ParentLink='ReallyComplexStatement_Branch' LowerBound='211.21' HigherBound='398.1'>
                                    <om:Property Name='Expression' Value='&#xD;&#xA;RealAddress.Contains(&quot;.experian.com&quot;)' />
                                    <om:Property Name='IsGhostBranch' Value='True' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Yes' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='VariableAssignment' OID='2d9171d2-e3bb-46e7-bac5-cfa9824aca9b' ParentLink='ComplexStatement_Statement' LowerBound='214.1' HigherBound='220.1'>
                                        <om:Property Name='Expression' Value='RecordCount = System.Convert.ToInt32(xpath(SFApplicationJSONRequestMsg.MessagePart, &quot;count(/*[local-name()=&apos;SFApplicationRequest&apos; and namespace-uri()=&apos;http://SBA.CBR.CreditProfile.SFApplicationRequest&apos;]/*[local-name()=&apos;applicants&apos; and namespace-uri()=&apos;&apos;])&quot;));&#xD;&#xA;&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;Count Of Applicants&quot;, System.Convert.ToString(RecordCount));&#xD;&#xA;&#xD;&#xA;GetExperianValues = new Experian.Values();&#xD;&#xA;' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Applicants Count' />
                                        <om:Property Name='Signal' Value='True' />
                                    </om:Element>
                                    <om:Element Type='CallRules' OID='36c2b8fa-17f9-4341-8874-55977be79319' ParentLink='ComplexStatement_Statement' LowerBound='220.1' HigherBound='236.1'>
                                        <om:Property Name='PolicyName' Value='Set Experian Values' />
                                        <om:Property Name='PolicyVersion' Value='-1' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Get Experian Values' />
                                        <om:Property Name='Signal' Value='False' />
                                        <om:Element Type='RulesParameterRef' OID='c9bae39e-d562-447f-862b-5e6387fa4423' ParentLink='CallRules_RulesParameterRef'>
                                            <om:Property Name='Reference' Value='GetExperianValues' />
                                            <om:Property Name='Alias' Value='Experian.Values' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='RulesParameterRef_1' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                    </om:Element>
                                    <om:Element Type='While' OID='61b984fd-743f-449b-9f52-612cc56ce926' ParentLink='ComplexStatement_Statement' LowerBound='236.1' HigherBound='397.1'>
                                        <om:Property Name='Expression' Value='LoopCount &lt;= RecordCount' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Loop through Applicant' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='Construct' OID='d12da5b5-c81a-4705-95ea-2b5a638ade56' ParentLink='ComplexStatement_Statement' LowerBound='239.1' HigherBound='277.1'>
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Construct Applicant CBRRequest' />
                                            <om:Property Name='Signal' Value='True' />
                                            <om:Element Type='MessageAssignment' OID='9d71fb1e-85d2-49ef-a21b-e40f105dbeac' ParentLink='ComplexStatement_Statement' LowerBound='242.1' HigherBound='260.1'>
                                                <om:Property Name='Expression' Value='&#xD;&#xA;sSingleApplicantXpath = System.String.Format(&quot;/*[local-name()=&apos;SFApplicationRequest&apos; and namespace-uri()=&apos;http://SBA.CBR.CreditProfile.SFApplicationRequest&apos;]/*[local-name()=&apos;applicants&apos; and namespace-uri()=&apos;&apos;][{0}]&quot;,LoopCount);&#xD;&#xA;&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;xpath&quot;, sSingleApplicantXpath);&#xD;&#xA;&#xD;&#xA;TempSingleApplicantMsg.MessagePart = xpath(SFApplicationJSONRequestMsg.MessagePart,sSingleApplicantXpath);&#xD;&#xA;&#xD;&#xA;SFApplicantDebatchedJSONRequestMsg.MessagePart = AddNameSpaceToDebachedMessage.AddNameSpace.GetMessageFromXmlStr(TempSingleApplicantMsg.MessagePart.OuterXml,&quot;http://SBA.CBR.CreditProfile.Applicants&quot;);&#xD;&#xA;&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;Debacthed&quot;, TempSingleApplicantMsg.MessagePart.OuterXml);&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;//TempExperianXml = new System.Xml.XmlDocument();&#xD;&#xA;&#xD;&#xA;//TempExperianXml.LoadXml(@&quot;&lt;ns0:Values xmlns:ns0=&apos;http://ExperianValues.GetExperianValues&apos;&gt;&lt;EAI&gt;&quot;+GetExperianValues.EAI+&quot;&lt;/EAI&gt;&lt;DBHost&gt;&quot;+GetExperianValues.DBHost+&quot;&lt;/DBHost&gt;&lt;ReferenceId&gt;&quot;+GetExperianValues.ReferenceId+&quot;&lt;/ReferenceId&gt;&lt;Preamble&gt;&quot;+GetExperianValues.Preamble+&quot;&lt;/Preamble&gt;&lt;OpInitials&gt;&quot;+GetExperianValues.OpInitials+&quot;&lt;/OpInitials&gt;&lt;SubCode&gt;&quot;+GetExperianValues.SubCode+&quot;&lt;/SubCode&gt;&lt;VendorNumber&gt;&quot;+GetExperianValues.VendorNumber+&quot;&lt;/VendorNumber&gt;&lt;/ns0:Values&gt;&quot;); &#xD;&#xA;&#xD;&#xA;//SetExperianValues = TempExperianXml;&#xD;&#xA;' />
                                                <om:Property Name='ReportToAnalyst' Value='False' />
                                                <om:Property Name='Name' Value='Make SingleApplicant' />
                                                <om:Property Name='Signal' Value='True' />
                                            </om:Element>
                                            <om:Element Type='Transform' OID='5cdf32fa-5ef3-4d86-b485-b5d6b2aba9ed' ParentLink='ComplexStatement_Statement' LowerBound='260.1' HigherBound='262.1'>
                                                <om:Property Name='ClassName' Value='CreditProfileLogic.Transform_SingleApplicant_To_CBR_Request' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Name' Value='Make Applicant Experian Request' />
                                                <om:Property Name='Signal' Value='True' />
                                                <om:Element Type='MessagePartRef' OID='bc5a7f29-6779-4a0f-a9e3-76112fad7076' ParentLink='Transform_OutputMessagePartRef' LowerBound='261.44' HigherBound='261.87'>
                                                    <om:Property Name='MessageRef' Value='ExperianCreditProfileRequestMsg' />
                                                    <om:Property Name='PartRef' Value='MessagePart' />
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='MessagePartReference_17' />
                                                    <om:Property Name='Signal' Value='False' />
                                                </om:Element>
                                                <om:Element Type='MessagePartRef' OID='f433840c-0152-4c49-b7b5-8a4624dc5a5b' ParentLink='Transform_InputMessagePartRef' LowerBound='261.152' HigherBound='261.198'>
                                                    <om:Property Name='MessageRef' Value='SFApplicantDebatchedJSONRequestMsg' />
                                                    <om:Property Name='PartRef' Value='MessagePart' />
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='MessagePartReference_12' />
                                                    <om:Property Name='Signal' Value='False' />
                                                </om:Element>
                                            </om:Element>
                                            <om:Element Type='MessageAssignment' OID='6b9ffd31-a7c1-4d0d-97e4-e655b70622af' ParentLink='ComplexStatement_Statement' LowerBound='262.1' HigherBound='276.1'>
                                                <om:Property Name='Expression' Value='xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;EAI&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]&quot;) = GetExperianValues.EAI;&#xA;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;DBHost&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]&quot;) = GetExperianValues.DBHost;&#xA;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;ReferenceId&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]&quot;) = GetExperianValues.ReferenceId;&#xA;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;Request&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Products&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;CreditProfile&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Subscriber&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Preamble&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]&quot;) = GetExperianValues.Preamble;&#xD;&#xA;&#xD;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;Request&apos; and namespace-uri()=&apos;http://www.e"+
@"xperian.com/WebDelivery&apos;]/*[local-name()=&apos;Products&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;CreditProfile&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Subscriber&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;OpInitials&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]&quot;) = GetExperianValues.OpInitials;&#xD;&#xA;&#xD;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;Request&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Products&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;CreditProfile&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Subscriber&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;SubCode&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]&quot;) = GetExperianValues.SubCode;&#xD;&#xA;&#xD;&#xA;xpath(ExperianCreditProfileRequestMsg.MessagePart,&quot;/*[local-name()=&apos;NetConnectRequest&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnect&apos;]/*[local-name()=&apos;Request&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Products&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;CreditProfile&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;Vendor&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]/*[local-name()=&apos;VendorNumber&apos; and namespace-uri()=&apos;http://www.experian.com/WebDelivery&apos;]&quot;) = GetExperianValues.VendorNumber;' />
                                                <om:Property Name='ReportToAnalyst' Value='False' />
                                                <om:Property Name='Name' Value='Assigin Experian Values To CBR Request' />
                                                <om:Property Name='Signal' Value='True' />
                                            </om:Element>
                                            <om:Element Type='MessageRef' OID='cb5e758d-2040-43da-9a98-5c37741a069e' ParentLink='Construct_MessageRef' LowerBound='240.39' HigherBound='240.70'>
                                                <om:Property Name='Ref' Value='ExperianCreditProfileRequestMsg' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='MessageRef' OID='29378b95-1be5-473d-b690-35971d6881da' ParentLink='Construct_MessageRef' LowerBound='240.72' HigherBound='240.94'>
                                                <om:Property Name='Ref' Value='TempSingleApplicantMsg' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='MessageRef' OID='49db4748-2b7b-475c-9603-cfa17f0a14ca' ParentLink='Construct_MessageRef' LowerBound='240.96' HigherBound='240.130'>
                                                <om:Property Name='Ref' Value='SFApplicantDebatchedJSONRequestMsg' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                        </om:Element>
                                        <om:Element Type='Call' OID='498a3174-d167-4170-a9cb-f4d2216d8058' ParentLink='ComplexStatement_Statement' LowerBound='277.1' HigherBound='279.1'>
                                            <om:Property Name='Identifier' Value='CallOrchestration_1' />
                                            <om:Property Name='Invokee' Value='SBACBRCreditProfile.SendToExperian' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Call Experian' />
                                            <om:Property Name='Signal' Value='True' />
                                            <om:Element Type='Parameter' OID='8c14af2f-86b2-4bb8-bc36-2f507f7526f3' ParentLink='InvokeStatement_Parameter'>
                                                <om:Property Name='Direction' Value='Out' />
                                                <om:Property Name='Name' Value='ExperianCreditProfileResponseMsg' />
                                                <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileResponseType' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='Parameter' OID='6be42257-b2fd-4782-a2db-540b6ec65da0' ParentLink='InvokeStatement_Parameter'>
                                                <om:Property Name='Direction' Value='In' />
                                                <om:Property Name='Name' Value='ExperianCreditProfileRequestMsg' />
                                                <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileRequestType' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='Parameter' OID='ecd326dd-408f-4f02-810b-daac00782bd2' ParentLink='InvokeStatement_Parameter'>
                                                <om:Property Name='Direction' Value='In' />
                                                <om:Property Name='Name' Value='GetExperianValues' />
                                                <om:Property Name='Type' Value='Experian.Values' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='Parameter' OID='fff940ff-feba-46bd-8552-011a698ef075' ParentLink='InvokeStatement_Parameter'>
                                                <om:Property Name='Direction' Value='In' />
                                                <om:Property Name='Name' Value='RealAddress' />
                                                <om:Property Name='Type' Value='System.String' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                        </om:Element>
                                        <om:Element Type='Construct' OID='0da99da2-46c5-47fe-a3c9-087c5f20b352' ParentLink='ComplexStatement_Statement' LowerBound='279.1' HigherBound='304.1'>
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Construct BTSMessage GUID' />
                                            <om:Property Name='Signal' Value='True' />
                                            <om:Element Type='MessageRef' OID='5c7d75fe-309f-4db1-8a0c-f8d94a4fc3f9' ParentLink='Construct_MessageRef' LowerBound='280.39' HigherBound='280.56'>
                                                <om:Property Name='Ref' Value='BTSMessageGUIDMsg' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='MessageRef' OID='150caaca-ddc4-4f24-8719-a69b9ded12b0' ParentLink='Construct_MessageRef' LowerBound='280.58' HigherBound='280.79'>
                                                <om:Property Name='Ref' Value='TempBTSMessageGUIDXml' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='MessageAssignment' OID='b6dc8acb-72a4-4f5c-a8cb-1b2d704bc424' ParentLink='ComplexStatement_Statement' LowerBound='282.1' HigherBound='303.1'>
                                                <om:Property Name='Expression' Value='&#xD;&#xA;//Check Completion Code&#xD;&#xA;GetCompletionCode = xpath(ExperianCreditProfileResponseMsg.MessagePart, &quot;string(/*[local-name()=&apos;NetConnectResponse&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnectResponse&apos;]/*[local-name()=&apos;CompletionCode&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnectResponse&apos;]/text())&quot;);&#xD;&#xA;System.Diagnostics.EventLog.WriteEntry(&quot;Completion Code Value Exits&quot;, GetCompletionCode);&#xD;&#xA;&#xD;&#xA;//HostResponseExist = System.Convert.ToInt32(xpath(CB_Response, &quot;count(/*[local-name()=&apos;NetConnectResponse&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnectResponse&apos;]/*[local-name()=&apos;HostResponse&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnectResponse&apos;]/text())&quot;));&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;HostResponseExist Value count&quot;, System.Convert.ToString(HostResponseExist));&#xD;&#xA;&#xD;&#xA;GetNoRecordsMessageCode = xpath(ExperianCreditProfileResponseMsg.MessagePart, &quot;string(/*[local-name()=&apos;NetConnectResponse&apos;]/*[local-name()=&apos;Products&apos;]/*[local-name()=&apos;CreditProfile&apos;]/*[local-name()=&apos;InformationalMessage&apos;][*[local-name()=&apos;MessageNumber&apos;][text()=&apos;07&apos;]]/*[local-name()=&apos;MessageNumber&apos;])&quot;);&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;NoRecordsMessageCode Value&quot;, NoRecordsMessageCode);&#xD;&#xA;&#xD;&#xA;GetFieldLevelWarningInfo = System.Convert.ToInt32(xpath(ExperianCreditProfileResponseMsg.MessagePart, &quot;count(/*[local-name()=&apos;NetConnectResponse&apos; and namespace-uri()=&apos;http://www.experian.com/NetConnectResponse&apos;]/*[local-name()=&apos;Products&apos; and namespace-uri()=&apos;http://www.experian.com/ARFResponse&apos;]/*[local-name()=&apos;CreditProfile&apos; and namespace-uri()=&apos;http://www.experian.com/A"+
@"RFResponse&apos;]/*[local-name()=&apos;Statement&apos; and namespace-uri()=&apos;http://www.experian.com/ARFResponse&apos;]/*[local-name()=&apos;Type&apos; and @code =&apos;25&apos; or @code =&apos;32&apos;])&quot;));&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;FrozenErrorInfo Exits&quot;, System.Convert.ToString(FrozenErrorInfo));&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;&#xD;&#xA;TempBTSMessageGUIDXml = new System.Xml.XmlDocument();&#xD;&#xA;TempBTSMessageGUIDXml.LoadXml(@&quot;&lt;ns0:BTSMessageGUID xmlns:ns0=&apos;http://SBA.CBR.CreditProfile&apos;&gt;&lt;ns0:ID&gt;&quot;+&quot;BT Message ID: &quot;+ExperianCreditProfileResponseMsg(BTS.MessageID)+&quot;&lt;/ns0:ID&gt;&lt;/ns0:BTSMessageGUID&gt;&quot;); &#xD;&#xA;BTSMessageGUIDMsg.MessagePart = TempBTSMessageGUIDXml;&#xD;&#xA;//System.Diagnostics.EventLog.WriteEntry(&quot;BTS Message ID&quot;, TempBTSMessageGUIDXml.OuterXml);&#xD;&#xA;' />
                                                <om:Property Name='ReportToAnalyst' Value='False' />
                                                <om:Property Name='Name' Value='Make BTS Message' />
                                                <om:Property Name='Signal' Value='True' />
                                            </om:Element>
                                        </om:Element>
                                        <om:Element Type='Decision' OID='7631ae8d-135e-4607-923a-87ae5612f1c8' ParentLink='ComplexStatement_Statement' LowerBound='304.1' HigherBound='391.1'>
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Decide Experian Status' />
                                            <om:Property Name='Signal' Value='True' />
                                            <om:Element Type='DecisionBranch' OID='2f23de8c-40ec-4d14-a3fc-8da8608c71c0' ParentLink='ReallyComplexStatement_Branch' LowerBound='305.29' HigherBound='320.1'>
                                                <om:Property Name='Expression' Value='GetCompletionCode != &quot;0000&quot;' />
                                                <om:Property Name='IsGhostBranch' Value='True' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Name' Value='Failure' />
                                                <om:Property Name='Signal' Value='False' />
                                                <om:Element Type='Construct' OID='b0d25145-adad-4a67-a220-e8e7f46006a0' ParentLink='ComplexStatement_Statement' LowerBound='307.1' HigherBound='313.1'>
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='Construct Invalid Request' />
                                                    <om:Property Name='Signal' Value='True' />
                                                    <om:Element Type='MessageRef' OID='52ff19a4-090d-4747-905c-546e9a125441' ParentLink='Construct_MessageRef' LowerBound='308.43' HigherBound='308.73'>
                                                        <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                        <om:Property Name='Signal' Value='False' />
                                                    </om:Element>
                                                    <om:Element Type='Transform' OID='6d90ed81-6ecd-4a10-a506-b26a414b2144' ParentLink='ComplexStatement_Statement' LowerBound='310.1' HigherBound='312.1'>
                                                        <om:Property Name='ClassName' Value='CreditProfileLogic.Transform_ErrorRequest' />
                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                        <om:Property Name='Name' Value='Make ErrorRequest' />
                                                        <om:Property Name='Signal' Value='True' />
                                                        <om:Element Type='MessagePartRef' OID='2db8d922-ffe8-403a-b2f9-2d8528799738' ParentLink='Transform_OutputMessagePartRef' LowerBound='311.48' HigherBound='311.90'>
                                                            <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                                            <om:Property Name='PartRef' Value='MessagePart' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='MessagePartReference_10' />
                                                            <om:Property Name='Signal' Value='False' />
                                                        </om:Element>
                                                        <om:Element Type='MessagePartRef' OID='92bc6f1e-5fb7-4d1c-8cd2-6a8f14c40976' ParentLink='Transform_InputMessagePartRef' LowerBound='311.137' HigherBound='311.181'>
                                                            <om:Property Name='MessageRef' Value='ExperianCreditProfileResponseMsg' />
                                                            <om:Property Name='PartRef' Value='MessagePart' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='MessagePartReference_9' />
                                                            <om:Property Name='Signal' Value='False' />
                                                        </om:Element>
                                                        <om:Element Type='MessagePartRef' OID='cc3d36f0-5c9d-445e-b94d-95c9cc8e4636' ParentLink='Transform_InputMessagePartRef' LowerBound='311.183' HigherBound='311.222'>
                                                            <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                                            <om:Property Name='PartRef' Value='MessagePart' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='MessagePartReference_25' />
                                                            <om:Property Name='Signal' Value='False' />
                                                        </om:Element>
                                                        <om:Element Type='MessagePartRef' OID='1d3c449c-5d24-49b2-b468-8751b63b1870' ParentLink='Transform_InputMessagePartRef' LowerBound='311.224' HigherBound='311.253'>
                                                            <om:Property Name='MessageRef' Value='BTSMessageGUIDMsg' />
                                                            <om:Property Name='PartRef' Value='MessagePart' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='MessagePartReference_26' />
                                                            <om:Property Name='Signal' Value='False' />
                                                        </om:Element>
                                                    </om:Element>
                                                </om:Element>
                                                <om:Element Type='Send' OID='6ecb37a4-76eb-4af2-aeab-a56bc30eec61' ParentLink='ComplexStatement_Statement' LowerBound='313.1' HigherBound='315.1'>
                                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                                    <om:Property Name='OperationName' Value='Operation' />
                                                    <om:Property Name='OperationMessageName' Value='Request' />
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                                    <om:Property Name='Signal' Value='True' />
                                                </om:Element>
                                                <om:Element Type='Receive' OID='927cfde3-f25a-42e0-9519-93c8317727d4' ParentLink='ComplexStatement_Statement' LowerBound='315.1' HigherBound='317.1'>
                                                    <om:Property Name='Activate' Value='False' />
                                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                                    <om:Property Name='OperationName' Value='Operation' />
                                                    <om:Property Name='OperationMessageName' Value='Response' />
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                                    <om:Property Name='Signal' Value='True' />
                                                </om:Element>
                                                <om:Element Type='VariableAssignment' OID='9ecf73a8-4990-421a-a1f7-71a37407df43' ParentLink='ComplexStatement_Statement' LowerBound='317.1' HigherBound='319.1'>
                                                    <om:Property Name='Expression' Value='Flag = false;' />
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='Flag' />
                                                    <om:Property Name='Signal' Value='False' />
                                                </om:Element>
                                            </om:Element>
                                            <om:Element Type='DecisionBranch' OID='3d1820f2-9255-4e20-bbdb-f4639dda8886' ParentLink='ReallyComplexStatement_Branch'>
                                                <om:Property Name='IsGhostBranch' Value='True' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Name' Value='Error' />
                                                <om:Property Name='Signal' Value='True' />
                                                <om:Element Type='Task' OID='e7e61001-9137-4ba1-a469-9b53843bbfbd' ParentLink='ComplexStatement_Statement' LowerBound='322.1' HigherBound='390.1'>
                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                    <om:Property Name='Name' Value='Experian Success Response' />
                                                    <om:Property Name='Signal' Value='True' />
                                                    <om:Element Type='Decision' OID='613283e1-51fe-47e7-a711-f3b7d2a2cfe5' ParentLink='ComplexStatement_Statement' LowerBound='322.1' HigherBound='390.1'>
                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                        <om:Property Name='Name' Value='Decide Success Codes' />
                                                        <om:Property Name='Signal' Value='True' />
                                                        <om:Element Type='DecisionBranch' OID='6e76e4a7-6e58-4075-a224-1356e00b479b' ParentLink='ReallyComplexStatement_Branch' LowerBound='323.33' HigherBound='336.1'>
                                                            <om:Property Name='Expression' Value='GetFieldLevelWarningInfo &gt; 0' />
                                                            <om:Property Name='IsGhostBranch' Value='True' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='Field Level WarningInfo' />
                                                            <om:Property Name='Signal' Value='True' />
                                                            <om:Element Type='Construct' OID='048525ec-3a33-4bd9-a614-e9c1713b905a' ParentLink='ComplexStatement_Statement' LowerBound='325.1' HigherBound='331.1'>
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Construct Frozen Message' />
                                                                <om:Property Name='Signal' Value='True' />
                                                                <om:Element Type='MessageRef' OID='16a449e1-82ed-4e5c-bc92-cdfc14e4af51' ParentLink='Construct_MessageRef' LowerBound='326.47' HigherBound='326.77'>
                                                                    <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                                    <om:Property Name='Signal' Value='False' />
                                                                </om:Element>
                                                                <om:Element Type='Transform' OID='7c9e7526-78c0-4db6-b64d-4fd6b97525c1' ParentLink='ComplexStatement_Statement' LowerBound='328.1' HigherBound='330.1'>
                                                                    <om:Property Name='ClassName' Value='CreditProfileLogic.Transform_FieldLevelWarning_To_SFCBRRequest' />
                                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                                    <om:Property Name='Name' Value='Make Frozen or Locked Request' />
                                                                    <om:Property Name='Signal' Value='True' />
                                                                    <om:Element Type='MessagePartRef' OID='acd09a32-8113-4927-a310-c8558bdd8753' ParentLink='Transform_InputMessagePartRef' LowerBound='329.162' HigherBound='329.206'>
                                                                        <om:Property Name='MessageRef' Value='ExperianCreditProfileResponseMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_13' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='e7fe4c23-7ba2-4280-abb7-e726f9594cc6' ParentLink='Transform_InputMessagePartRef' LowerBound='329.208' HigherBound='329.247'>
                                                                        <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_21' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='834c9024-8ac2-4202-9609-cc6eead9cb2a' ParentLink='Transform_InputMessagePartRef' LowerBound='329.249' HigherBound='329.278'>
                                                                        <om:Property Name='MessageRef' Value='BTSMessageGUIDMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_22' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='147ebc59-b198-4f47-b2fc-1a4fe20291b4' ParentLink='Transform_OutputMessagePartRef' LowerBound='329.52' HigherBound='329.94'>
                                                                        <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_14' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                </om:Element>
                                                            </om:Element>
                                                            <om:Element Type='Send' OID='f44b021e-4ba9-4ddd-901a-1d62e657523a' ParentLink='ComplexStatement_Statement' LowerBound='331.1' HigherBound='333.1'>
                                                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                                                <om:Property Name='OperationName' Value='Operation' />
                                                                <om:Property Name='OperationMessageName' Value='Request' />
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Send CreditBureau Request' />
                                                                <om:Property Name='Signal' Value='True' />
                                                            </om:Element>
                                                            <om:Element Type='Receive' OID='9bec3628-dde5-4009-9e9d-f2981bc68a3e' ParentLink='ComplexStatement_Statement' LowerBound='333.1' HigherBound='335.1'>
                                                                <om:Property Name='Activate' Value='False' />
                                                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                                                <om:Property Name='OperationName' Value='Operation' />
                                                                <om:Property Name='OperationMessageName' Value='Response' />
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                                                <om:Property Name='Signal' Value='True' />
                                                            </om:Element>
                                                        </om:Element>
                                                        <om:Element Type='DecisionBranch' OID='aa004203-d197-417a-9419-adacb2227270' ParentLink='ReallyComplexStatement_Branch' LowerBound='336.38' HigherBound='349.1'>
                                                            <om:Property Name='Expression' Value='GetNoRecordsMessageCode == &quot;07&quot;' />
                                                            <om:Property Name='IsGhostBranch' Value='True' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='No Records Found' />
                                                            <om:Property Name='Signal' Value='True' />
                                                            <om:Element Type='Construct' OID='76e06d59-33f9-4c82-8046-92780dbfdcb9' ParentLink='ComplexStatement_Statement' LowerBound='338.1' HigherBound='344.1'>
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Construct Field Error Request' />
                                                                <om:Property Name='Signal' Value='True' />
                                                                <om:Element Type='Transform' OID='8830686f-934c-40cd-a3fd-6de70bdbd442' ParentLink='ComplexStatement_Statement' LowerBound='341.1' HigherBound='343.1'>
                                                                    <om:Property Name='ClassName' Value='CreditProfileLogic.Transform_CBRWaringNoRecordResponse_To_SFCreditBureau' />
                                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                                    <om:Property Name='Name' Value='Make NoRecord Request' />
                                                                    <om:Property Name='Signal' Value='True' />
                                                                    <om:Element Type='MessagePartRef' OID='8617b8e4-1c32-46f5-8c8d-431abdbcfe51' ParentLink='Transform_InputMessagePartRef' LowerBound='342.172' HigherBound='342.216'>
                                                                        <om:Property Name='MessageRef' Value='ExperianCreditProfileResponseMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_4' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='b3a7099b-0783-449b-9524-347e76516007' ParentLink='Transform_InputMessagePartRef' LowerBound='342.218' HigherBound='342.257'>
                                                                        <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_5' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='4652bb14-121b-4e37-8a26-bdd596c96faa' ParentLink='Transform_InputMessagePartRef' LowerBound='342.259' HigherBound='342.288'>
                                                                        <om:Property Name='MessageRef' Value='BTSMessageGUIDMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_16' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                    <om:Element Type='MessagePartRef' OID='75476865-6f2d-41a1-8426-a3ec2fa876f4' ParentLink='Transform_OutputMessagePartRef' LowerBound='342.52' HigherBound='342.94'>
                                                                        <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                                                        <om:Property Name='PartRef' Value='MessagePart' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='MessagePartReference_15' />
                                                                        <om:Property Name='Signal' Value='False' />
                                                                    </om:Element>
                                                                </om:Element>
                                                                <om:Element Type='MessageRef' OID='bb44a598-0804-440c-b90c-e17d24d9ddc4' ParentLink='Construct_MessageRef' LowerBound='339.47' HigherBound='339.77'>
                                                                    <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                                    <om:Property Name='Signal' Value='False' />
                                                                </om:Element>
                                                            </om:Element>
                                                            <om:Element Type='Send' OID='a2e9aade-6ba9-4d7c-b34d-dcf694567f33' ParentLink='ComplexStatement_Statement' LowerBound='344.1' HigherBound='346.1'>
                                                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                                                <om:Property Name='OperationName' Value='Operation' />
                                                                <om:Property Name='OperationMessageName' Value='Request' />
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                                                <om:Property Name='Signal' Value='True' />
                                                            </om:Element>
                                                            <om:Element Type='Receive' OID='6f8730d5-8272-479c-b73f-631033b10c87' ParentLink='ComplexStatement_Statement' LowerBound='346.1' HigherBound='348.1'>
                                                                <om:Property Name='Activate' Value='False' />
                                                                <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                                                <om:Property Name='OperationName' Value='Operation' />
                                                                <om:Property Name='OperationMessageName' Value='Response' />
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                                                <om:Property Name='Signal' Value='True' />
                                                            </om:Element>
                                                        </om:Element>
                                                        <om:Element Type='DecisionBranch' OID='5e43ee1b-7c9f-4456-97c2-0eb5176d0730' ParentLink='ReallyComplexStatement_Branch'>
                                                            <om:Property Name='IsGhostBranch' Value='True' />
                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                            <om:Property Name='Name' Value='CBRInfo' />
                                                            <om:Property Name='Signal' Value='True' />
                                                            <om:Element Type='Task' OID='8c845b30-3bb0-4a90-ba29-08b1806fc261' ParentLink='ComplexStatement_Statement' LowerBound='351.1' HigherBound='389.1'>
                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                <om:Property Name='Name' Value='Send CreditProfile Information To Salesforce' />
                                                                <om:Property Name='Signal' Value='True' />
                                                                <om:Element Type='Scope' OID='0e23109b-df0d-47a4-b524-55f7cdb53526' ParentLink='ComplexStatement_Statement' LowerBound='351.1' HigherBound='389.1'>
                                                                    <om:Property Name='InitializedTransactionType' Value='True' />
                                                                    <om:Property Name='IsSynchronized' Value='False' />
                                                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                                                    <om:Property Name='Name' Value='SF Insertion Scope' />
                                                                    <om:Property Name='Signal' Value='True' />
                                                                    <om:Element Type='Construct' OID='fabb4833-77ce-41d1-9439-88ec1ed5cef4' ParentLink='ComplexStatement_Statement' LowerBound='356.1' HigherBound='362.1'>
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Construct SF CreditBureau Insert Request' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                        <om:Element Type='MessageRef' OID='cc4de129-7892-4641-b0ad-41aebc31775b' ParentLink='Construct_MessageRef' LowerBound='357.55' HigherBound='357.85'>
                                                                            <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                                            <om:Property Name='Signal' Value='False' />
                                                                        </om:Element>
                                                                        <om:Element Type='Transform' OID='fc402fb2-1ee1-4ee5-b8c6-293a34b6a5cb' ParentLink='ComplexStatement_Statement' LowerBound='359.1' HigherBound='361.1'>
                                                                            <om:Property Name='ClassName' Value='CreditProfileLogic.Transform_ExperianResponse_To_SFCreditBureau' />
                                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                                            <om:Property Name='Name' Value='Make SF CreditBureau Insert Request' />
                                                                            <om:Property Name='Signal' Value='False' />
                                                                            <om:Element Type='MessagePartRef' OID='fe15aca0-d780-4f6c-ad51-fdeeea0b979b' ParentLink='Transform_InputMessagePartRef' LowerBound='360.171' HigherBound='360.215'>
                                                                                <om:Property Name='MessageRef' Value='ExperianCreditProfileResponseMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_1' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='fd79dc67-66f4-41e8-ad9f-b74dc3c5571f' ParentLink='Transform_InputMessagePartRef' LowerBound='360.217' HigherBound='360.256'>
                                                                                <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_3' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='acef41e2-604d-4cec-a5eb-0eb2e02dee59' ParentLink='Transform_InputMessagePartRef' LowerBound='360.258' HigherBound='360.304'>
                                                                                <om:Property Name='MessageRef' Value='SFApplicantDebatchedJSONRequestMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_23' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='44a2159e-fe04-4124-aa0f-c2db23101ca7' ParentLink='Transform_InputMessagePartRef' LowerBound='360.306' HigherBound='360.335'>
                                                                                <om:Property Name='MessageRef' Value='BTSMessageGUIDMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_24' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='21308833-cbdb-4647-b481-d374a91ceb4c' ParentLink='Transform_OutputMessagePartRef' LowerBound='360.60' HigherBound='360.102'>
                                                                                <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_2' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                        </om:Element>
                                                                    </om:Element>
                                                                    <om:Element Type='Send' OID='eeacbc48-66e1-4e8a-9624-a13c58f7cfc6' ParentLink='ComplexStatement_Statement' LowerBound='362.1' HigherBound='364.1'>
                                                                        <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                        <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                                                        <om:Property Name='OperationName' Value='Operation' />
                                                                        <om:Property Name='OperationMessageName' Value='Request' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Send CreditBureau' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                    </om:Element>
                                                                    <om:Element Type='Receive' OID='f99080fc-ee35-4774-96c1-505ce5000b31' ParentLink='ComplexStatement_Statement' LowerBound='364.1' HigherBound='366.1'>
                                                                        <om:Property Name='Activate' Value='False' />
                                                                        <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                                                        <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                                                        <om:Property Name='OperationName' Value='Operation' />
                                                                        <om:Property Name='OperationMessageName' Value='Response' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Receive CreditBureau Response' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                    </om:Element>
                                                                    <om:Element Type='Construct' OID='8a2b5447-9583-4d25-a4b5-2c2636942b24' ParentLink='ComplexStatement_Statement' LowerBound='366.1' HigherBound='372.1'>
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Construct SF LineItems Insert Request' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                        <om:Element Type='Transform' OID='f12b1572-792b-40a4-9201-78f65bbccc92' ParentLink='ComplexStatement_Statement' LowerBound='369.1' HigherBound='371.1'>
                                                                            <om:Property Name='ClassName' Value='CreditProfileLogic.TransformExperianResponse_To_SFCreditBureauLineItems' />
                                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                                            <om:Property Name='Name' Value='Make SF LineItems Request' />
                                                                            <om:Property Name='Signal' Value='True' />
                                                                            <om:Element Type='MessagePartRef' OID='a26a6a82-04b5-4a88-8fac-34a0053f6929' ParentLink='Transform_InputMessagePartRef' LowerBound='370.187' HigherBound='370.230'>
                                                                                <om:Property Name='MessageRef' Value='SFCreditBureauInsertResponseMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_6' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='b19e7118-6de6-4553-ba5b-88b67ba1a9b1' ParentLink='Transform_InputMessagePartRef' LowerBound='370.232' HigherBound='370.276'>
                                                                                <om:Property Name='MessageRef' Value='ExperianCreditProfileResponseMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_7' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                            <om:Element Type='MessagePartRef' OID='b006ce90-2283-4578-8bba-5668cd893391' ParentLink='Transform_OutputMessagePartRef' LowerBound='370.60' HigherBound='370.110'>
                                                                                <om:Property Name='MessageRef' Value='SFCreditBureauLineItemInsertRequestMsg' />
                                                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                                                <om:Property Name='Name' Value='MessagePartReference_8' />
                                                                                <om:Property Name='Signal' Value='False' />
                                                                            </om:Element>
                                                                        </om:Element>
                                                                        <om:Element Type='MessageRef' OID='6c3af5cf-d4c9-465c-b198-b9b070f1544f' ParentLink='Construct_MessageRef' LowerBound='367.55' HigherBound='367.93'>
                                                                            <om:Property Name='Ref' Value='SFCreditBureauLineItemInsertRequestMsg' />
                                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                                            <om:Property Name='Signal' Value='False' />
                                                                        </om:Element>
                                                                    </om:Element>
                                                                    <om:Element Type='Send' OID='d3887c9e-42be-4e6b-9f18-816803ca9611' ParentLink='ComplexStatement_Statement' LowerBound='372.1' HigherBound='374.1'>
                                                                        <om:Property Name='PortName' Value='SFCreditBureauLineItemInsertRequestResponsePort' />
                                                                        <om:Property Name='MessageName' Value='SFCreditBureauLineItemInsertRequestMsg' />
                                                                        <om:Property Name='OperationName' Value='Operation' />
                                                                        <om:Property Name='OperationMessageName' Value='Request' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Send LineItem Request' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                    </om:Element>
                                                                    <om:Element Type='Receive' OID='5b011c9f-0eb6-4661-b12d-3a07d083f6ce' ParentLink='ComplexStatement_Statement' LowerBound='374.1' HigherBound='376.1'>
                                                                        <om:Property Name='Activate' Value='False' />
                                                                        <om:Property Name='PortName' Value='SFCreditBureauLineItemInsertRequestResponsePort' />
                                                                        <om:Property Name='MessageName' Value='SFCreditBureauLineItemInsertResponseMsg' />
                                                                        <om:Property Name='OperationName' Value='Operation' />
                                                                        <om:Property Name='OperationMessageName' Value='Response' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='Receive LineItem Response' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                    </om:Element>
                                                                    <om:Element Type='Catch' OID='78ff7814-16cd-469a-8c8e-2c5b92f9ca1a' ParentLink='Scope_Catch' LowerBound='379.1' HigherBound='387.1'>
                                                                        <om:Property Name='ExceptionName' Value='Ex' />
                                                                        <om:Property Name='ExceptionType' Value='System.SystemException' />
                                                                        <om:Property Name='IsFaultMessage' Value='False' />
                                                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                                                        <om:Property Name='Name' Value='SF System Exception' />
                                                                        <om:Property Name='Signal' Value='True' />
                                                                        <om:Element Type='VariableAssignment' OID='d035ca79-2347-486e-b08e-7b340a6d607c' ParentLink='Catch_Statement' LowerBound='382.1' HigherBound='386.1'>
                                                                            <om:Property Name='Expression' Value='System.Diagnostics.EventLog.WriteEntry(&quot;SF Insertion Exception&quot;,Ex.Message);&#xD;&#xA;&#xD;&#xA;Flag = false;' />
                                                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                                                            <om:Property Name='Name' Value='Expression' />
                                                                            <om:Property Name='Signal' Value='True' />
                                                                        </om:Element>
                                                                    </om:Element>
                                                                </om:Element>
                                                            </om:Element>
                                                        </om:Element>
                                                    </om:Element>
                                                </om:Element>
                                            </om:Element>
                                        </om:Element>
                                        <om:Element Type='VariableAssignment' OID='3af9c5ed-4189-423f-8d0c-c3145bf1abc3' ParentLink='ComplexStatement_Statement' LowerBound='391.1' HigherBound='394.1'>
                                            <om:Property Name='Expression' Value='if((RecordCount == LoopCount)&amp;&amp;(Flag != false))&#xD;&#xA;{Flag = true;}' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Flag' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='VariableAssignment' OID='2ecf16ef-f313-45f3-920c-fa8d05f67576' ParentLink='ComplexStatement_Statement' LowerBound='394.1' HigherBound='396.1'>
                                            <om:Property Name='Expression' Value='LoopCount = LoopCount + 1;' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='LoopIncrement' />
                                            <om:Property Name='Signal' Value='True' />
                                        </om:Element>
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='DecisionBranch' OID='c111e6c6-3c3e-400b-b7d7-e75e07f5486e' ParentLink='ReallyComplexStatement_Branch'>
                                    <om:Property Name='IsGhostBranch' Value='True' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Else' />
                                    <om:Property Name='Signal' Value='False' />
                                    <om:Element Type='VariableAssignment' OID='e47787ac-f710-4289-9389-f2b551334935' ParentLink='ComplexStatement_Statement' LowerBound='400.1' HigherBound='402.1'>
                                        <om:Property Name='Expression' Value='Flag = false;' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Flag' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='Construct' OID='0715f746-7350-4d32-a0c4-d087f9f2c6e2' ParentLink='ComplexStatement_Statement' LowerBound='402.1' HigherBound='408.1'>
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Construct Error CBR Request Msg' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='MessageRef' OID='2f73724e-1a26-47d3-9620-afaa54bd0a09' ParentLink='Construct_MessageRef' LowerBound='403.35' HigherBound='403.65'>
                                            <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='Transform' OID='02c00e57-0c5d-48f9-a378-be32f1f35932' ParentLink='ComplexStatement_Statement' LowerBound='405.1' HigherBound='407.1'>
                                            <om:Property Name='ClassName' Value='CreditProfileLogic.TransformInvalidURLException_To_SFCreditBureau' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='Transform' />
                                            <om:Property Name='Signal' Value='True' />
                                            <om:Element Type='MessagePartRef' OID='be3d2b45-9bf4-477d-8d25-8950f736dc7e' ParentLink='Transform_InputMessagePartRef' LowerBound='406.153' HigherBound='406.192'>
                                                <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Name' Value='MessagePartReference_29' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                            <om:Element Type='MessagePartRef' OID='b63b4cd0-e9d7-45d5-b1cb-0111691fe54d' ParentLink='Transform_OutputMessagePartRef' LowerBound='406.40' HigherBound='406.82'>
                                                <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                                <om:Property Name='PartRef' Value='MessagePart' />
                                                <om:Property Name='ReportToAnalyst' Value='True' />
                                                <om:Property Name='Name' Value='MessagePartReference_31' />
                                                <om:Property Name='Signal' Value='False' />
                                            </om:Element>
                                        </om:Element>
                                    </om:Element>
                                    <om:Element Type='Send' OID='b8420b62-eb4a-4c02-a5b4-d4feff2b2679' ParentLink='ComplexStatement_Statement' LowerBound='408.1' HigherBound='410.1'>
                                        <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                        <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                        <om:Property Name='OperationName' Value='Operation' />
                                        <om:Property Name='OperationMessageName' Value='Request' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                        <om:Property Name='Signal' Value='True' />
                                    </om:Element>
                                    <om:Element Type='Receive' OID='e4027b2d-8a87-4b3b-a3a7-87bb4ce14bca' ParentLink='ComplexStatement_Statement' LowerBound='410.1' HigherBound='412.1'>
                                        <om:Property Name='Activate' Value='False' />
                                        <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                        <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                        <om:Property Name='OperationName' Value='Operation' />
                                        <om:Property Name='OperationMessageName' Value='Response' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                        <om:Property Name='Signal' Value='True' />
                                    </om:Element>
                                </om:Element>
                            </om:Element>
                            <om:Element Type='Catch' OID='457e6dba-a406-4840-9472-d8371ce68b8b' ParentLink='Scope_Catch' LowerBound='416.1' HigherBound='439.1'>
                                <om:Property Name='ExceptionName' Value='CREx' />
                                <om:Property Name='ExceptionType' Value='Microsoft.RuleEngine.PolicyExecutionException' />
                                <om:Property Name='IsFaultMessage' Value='False' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Rules Exception' />
                                <om:Property Name='Signal' Value='True' />
                                <om:Element Type='Construct' OID='5259c6ba-3646-4219-abe0-abf8d3ef95c9' ParentLink='Catch_Statement' LowerBound='419.1' HigherBound='434.1'>
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Construct Error CBR Request Msg' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='MessageAssignment' OID='6b973cd8-e3b7-472a-b769-db117e9dd827' ParentLink='ComplexStatement_Statement' LowerBound='422.1' HigherBound='431.1'>
                                        <om:Property Name='Expression' Value='System.Diagnostics.EventLog.WriteEntry(&quot;Experian Exception&quot;, CREx.Message, System.Diagnostics.EventLogEntryType.Error);&#xD;&#xA;Flag = false;&#xD;&#xA;ExperianStatus = &quot;Fail&quot;;&#xD;&#xA;TempExceptionInformation = new System.Xml.XmlDocument();&#xD;&#xA;sTempExceptionInformation = CREx.Message;&#xD;&#xA;TempExceptionInformation.LoadXml(System.String.Format(&quot;&lt;ns0:ExceptionInfo xmlns:ns0=&apos;http://SBACBRCreditProfile.ExceptionInformation&apos;&gt;&lt;Message&gt;&lt;![CDATA[{0}]]&gt;&lt;/Message&gt;&lt;OrchestrationId&gt;{1}&lt;/OrchestrationId&gt;&lt;/ns0:ExceptionInfo&gt;&quot;,sTempExceptionInformation,System.Convert.ToString(Microsoft.XLANGs.Core.Service.RootService.InstanceId)));&#xD;&#xA;ExceptionInformationMsg.MessagePart = TempExceptionInformation;&#xD;&#xA;&#xD;&#xA;' />
                                        <om:Property Name='ReportToAnalyst' Value='False' />
                                        <om:Property Name='Name' Value='Make Experian Exception' />
                                        <om:Property Name='Signal' Value='True' />
                                    </om:Element>
                                    <om:Element Type='Transform' OID='2b721528-ab35-4334-8984-b8817f80af85' ParentLink='ComplexStatement_Statement' LowerBound='431.1' HigherBound='433.1'>
                                        <om:Property Name='ClassName' Value='CreditProfileLogic.TransformException_To_SFCreditBureau' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Transform' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='MessagePartRef' OID='83b2f0f6-74fc-4240-9a97-6ff2dcfeeed4' ParentLink='Transform_InputMessagePartRef' LowerBound='432.143' HigherBound='432.182'>
                                            <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_35' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='4cfb2b81-5ec0-4de0-82e6-621e61d9ceab' ParentLink='Transform_InputMessagePartRef' LowerBound='432.184' HigherBound='432.219'>
                                            <om:Property Name='MessageRef' Value='ExceptionInformationMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_36' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='edcb4849-feef-4c1b-a585-fbd87dc740da' ParentLink='Transform_OutputMessagePartRef' LowerBound='432.40' HigherBound='432.82'>
                                            <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_37' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='22cd2356-2c08-482b-a7a7-9cfb103c40dd' ParentLink='Construct_MessageRef' LowerBound='420.35' HigherBound='420.65'>
                                        <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='4734b05b-afe1-4477-af70-f52674be753b' ParentLink='Construct_MessageRef' LowerBound='420.67' HigherBound='420.90'>
                                        <om:Property Name='Ref' Value='ExceptionInformationMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='67906371-2939-4b02-a059-550f27c23120' ParentLink='Construct_MessageRef' LowerBound='420.92' HigherBound='420.116'>
                                        <om:Property Name='Ref' Value='TempExceptionInformation' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='Send' OID='12a10061-a925-4d19-a368-6bddefd80cef' ParentLink='Catch_Statement' LowerBound='434.1' HigherBound='436.1'>
                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Request' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                                <om:Element Type='Receive' OID='937564fd-7367-423e-9149-6fe7d68ffd15' ParentLink='Catch_Statement' LowerBound='436.1' HigherBound='438.1'>
                                    <om:Property Name='Activate' Value='False' />
                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Response' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                            </om:Element>
                            <om:Element Type='Catch' OID='6f2d249c-5985-43d5-9582-54feffc49319' ParentLink='Scope_Catch' LowerBound='439.1' HigherBound='462.1'>
                                <om:Property Name='ExceptionName' Value='OSEx' />
                                <om:Property Name='ExceptionType' Value='System.Exception' />
                                <om:Property Name='IsFaultMessage' Value='False' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Ssystem Exception' />
                                <om:Property Name='Signal' Value='True' />
                                <om:Element Type='Construct' OID='1ad307d3-daeb-49c2-a072-64159bed5320' ParentLink='Catch_Statement' LowerBound='442.1' HigherBound='457.1'>
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Construct Error CBR Request Msg' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='MessageAssignment' OID='08f6fc2d-209a-45f6-9a99-d25fa350e320' ParentLink='ComplexStatement_Statement' LowerBound='445.1' HigherBound='454.1'>
                                        <om:Property Name='Expression' Value='System.Diagnostics.EventLog.WriteEntry(&quot;Delivery Experian Exception&quot;, OSEx.Message, System.Diagnostics.EventLogEntryType.Error);&#xD;&#xA;Flag = false;&#xD;&#xA;ExperianStatus = &quot;Fail&quot;;&#xD;&#xA;TempExceptionInformation = new System.Xml.XmlDocument();&#xD;&#xA;sTempExceptionInformation = OSEx.Message;&#xD;&#xA;TempExceptionInformation.LoadXml(System.String.Format(&quot;&lt;ns0:ExceptionInfo xmlns:ns0=&apos;http://SBACBRCreditProfile.ExceptionInformation&apos;&gt;&lt;Message&gt;&lt;![CDATA[{0}]]&gt;&lt;/Message&gt;&lt;OrchestrationId&gt;{1}&lt;/OrchestrationId&gt;&lt;/ns0:ExceptionInfo&gt;&quot;,sTempExceptionInformation,System.Convert.ToString(Microsoft.XLANGs.Core.Service.RootService.InstanceId)));&#xD;&#xA;&#xD;&#xA;ExceptionInformationMsg.MessagePart = TempExceptionInformation;&#xD;&#xA;' />
                                        <om:Property Name='ReportToAnalyst' Value='False' />
                                        <om:Property Name='Name' Value='Make Experian Exception' />
                                        <om:Property Name='Signal' Value='True' />
                                    </om:Element>
                                    <om:Element Type='Transform' OID='7910a671-9f5d-45ba-82e4-03d8bf010312' ParentLink='ComplexStatement_Statement' LowerBound='454.1' HigherBound='456.1'>
                                        <om:Property Name='ClassName' Value='CreditProfileLogic.TransformException_To_SFCreditBureau' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Transform' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='MessagePartRef' OID='5d886c68-bff0-45df-9c8f-991d27a4d6d7' ParentLink='Transform_InputMessagePartRef' LowerBound='455.143' HigherBound='455.182'>
                                            <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_32' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='ad3943c8-a14b-4880-936e-d5d2f1882d54' ParentLink='Transform_InputMessagePartRef' LowerBound='455.184' HigherBound='455.219'>
                                            <om:Property Name='MessageRef' Value='ExceptionInformationMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_33' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='beb5782b-049e-45e2-b016-43bd938d1d0c' ParentLink='Transform_OutputMessagePartRef' LowerBound='455.40' HigherBound='455.82'>
                                            <om:Property Name='MessageRef' Value='SFCreditBureauInsertRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_34' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='645ffa4f-3854-4fdb-b55a-f45223f3cecc' ParentLink='Construct_MessageRef' LowerBound='443.35' HigherBound='443.65'>
                                        <om:Property Name='Ref' Value='SFCreditBureauInsertRequestMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='4f830141-02d8-47b4-9661-3967484595e0' ParentLink='Construct_MessageRef' LowerBound='443.67' HigherBound='443.90'>
                                        <om:Property Name='Ref' Value='ExceptionInformationMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='MessageRef' OID='e1ca6317-bf62-4dc7-935d-61201a3f88b9' ParentLink='Construct_MessageRef' LowerBound='443.92' HigherBound='443.116'>
                                        <om:Property Name='Ref' Value='TempExceptionInformation' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='Send' OID='93d406c7-f7e1-4fc7-8c62-392eafee8da4' ParentLink='Catch_Statement' LowerBound='457.1' HigherBound='459.1'>
                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertRequestMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Request' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Send To SFCreditBureau' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                                <om:Element Type='Receive' OID='90c1a1a6-f566-4742-a9b2-85ee3c2abc2d' ParentLink='Catch_Statement' LowerBound='459.1' HigherBound='461.1'>
                                    <om:Property Name='Activate' Value='False' />
                                    <om:Property Name='PortName' Value='SFCreditBureauInsertRequestResponse' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauInsertResponseMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Response' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Receive From SFCreditBureau' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                            </om:Element>
                        </om:Element>
                    </om:Element>
                    <om:Element Type='Task' OID='b99c7c48-c72d-4c33-b5f7-f45499252120' ParentLink='ComplexStatement_Statement' LowerBound='464.1' HigherBound='491.1'>
                        <om:Property Name='ReportToAnalyst' Value='True' />
                        <om:Property Name='Name' Value='Send Application Status Information To Salesforce EventRequest' />
                        <om:Property Name='Signal' Value='True' />
                        <om:Element Type='Decision' OID='3e37ff95-22a9-4fff-890b-66d6093d026a' ParentLink='ComplexStatement_Statement' LowerBound='464.1' HigherBound='491.1'>
                            <om:Property Name='ReportToAnalyst' Value='True' />
                            <om:Property Name='Name' Value='Decide Event Status' />
                            <om:Property Name='Signal' Value='True' />
                            <om:Element Type='DecisionBranch' OID='edfd123f-4588-4fb1-8980-61d0e17332d9' ParentLink='ReallyComplexStatement_Branch' LowerBound='465.13' HigherBound='478.1'>
                                <om:Property Name='Expression' Value='Flag == true' />
                                <om:Property Name='IsGhostBranch' Value='True' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Success' />
                                <om:Property Name='Signal' Value='False' />
                                <om:Element Type='Construct' OID='683f48ba-09f9-4610-85f1-68052b2a90b3' ParentLink='ComplexStatement_Statement' LowerBound='467.1' HigherBound='473.1'>
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Construct Success EventRequest Message' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='MessageRef' OID='c3a8776d-b491-4679-9730-86647d13f4df' ParentLink='Construct_MessageRef' LowerBound='468.27' HigherBound='468.56'>
                                        <om:Property Name='Ref' Value='SFCreditBureauEventRequestMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='Transform' OID='33aac55d-7c2e-41f2-ba00-d92ef5bdb951' ParentLink='ComplexStatement_Statement' LowerBound='470.1' HigherBound='472.1'>
                                        <om:Property Name='ClassName' Value='CreditProfileLogic.SuccessResponse_To_SFEvent' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Make Success Request' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='MessagePartRef' OID='b20aa6d8-e46c-47be-b49f-7f53ac9fc7b8' ParentLink='Transform_InputMessagePartRef' LowerBound='471.124' HigherBound='471.163'>
                                            <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_11' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='2e20458c-fed7-42f9-9795-5791ae69b11d' ParentLink='Transform_OutputMessagePartRef' LowerBound='471.32' HigherBound='471.73'>
                                            <om:Property Name='MessageRef' Value='SFCreditBureauEventRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_18' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='Send' OID='218ce861-41e6-4b82-bdb1-abfc1c05b4b9' ParentLink='ComplexStatement_Statement' LowerBound='473.1' HigherBound='475.1'>
                                    <om:Property Name='PortName' Value='SFCreditBureauEventRequestResponsePort' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauEventRequestMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Request' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Send Event Request' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                                <om:Element Type='Receive' OID='ea8a02e7-9330-4381-8787-0dbb334bef31' ParentLink='ComplexStatement_Statement' LowerBound='475.1' HigherBound='477.1'>
                                    <om:Property Name='Activate' Value='False' />
                                    <om:Property Name='PortName' Value='SFCreditBureauEventRequestResponsePort' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauEventResponseMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Response' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Receive Event Response' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                            </om:Element>
                            <om:Element Type='DecisionBranch' OID='fae9573f-75ed-4bd6-87cb-6192366d385b' ParentLink='ReallyComplexStatement_Branch'>
                                <om:Property Name='IsGhostBranch' Value='True' />
                                <om:Property Name='ReportToAnalyst' Value='True' />
                                <om:Property Name='Name' Value='Else' />
                                <om:Property Name='Signal' Value='False' />
                                <om:Element Type='Construct' OID='901a1a80-b6b7-4e1c-8707-f7623452eb34' ParentLink='ComplexStatement_Statement' LowerBound='480.1' HigherBound='486.1'>
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Construct Error EventReques Message' />
                                    <om:Property Name='Signal' Value='True' />
                                    <om:Element Type='MessageRef' OID='bba9e9c7-f6c2-4950-8e6c-d759e1d61c4a' ParentLink='Construct_MessageRef' LowerBound='481.27' HigherBound='481.56'>
                                        <om:Property Name='Ref' Value='SFCreditBureauEventRequestMsg' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Signal' Value='False' />
                                    </om:Element>
                                    <om:Element Type='Transform' OID='ddb487a2-20fa-44ff-81c0-8e0cffb7ed17' ParentLink='ComplexStatement_Statement' LowerBound='483.1' HigherBound='485.1'>
                                        <om:Property Name='ClassName' Value='CreditProfileLogic.ErrorResponse_To_SFEvent' />
                                        <om:Property Name='ReportToAnalyst' Value='True' />
                                        <om:Property Name='Name' Value='Make Error Request' />
                                        <om:Property Name='Signal' Value='True' />
                                        <om:Element Type='MessagePartRef' OID='b012ac93-0133-4782-9f4d-4a3b8d1e8269' ParentLink='Transform_OutputMessagePartRef' LowerBound='484.32' HigherBound='484.73'>
                                            <om:Property Name='MessageRef' Value='SFCreditBureauEventRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_20' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                        <om:Element Type='MessagePartRef' OID='c21eeaa2-c82b-48ca-9776-096a4fe50037' ParentLink='Transform_InputMessagePartRef' LowerBound='484.122' HigherBound='484.161'>
                                            <om:Property Name='MessageRef' Value='SFApplicationJSONRequestMsg' />
                                            <om:Property Name='PartRef' Value='MessagePart' />
                                            <om:Property Name='ReportToAnalyst' Value='True' />
                                            <om:Property Name='Name' Value='MessagePartReference_19' />
                                            <om:Property Name='Signal' Value='False' />
                                        </om:Element>
                                    </om:Element>
                                </om:Element>
                                <om:Element Type='Send' OID='d97a49a1-f0d9-4749-9616-156de641ba97' ParentLink='ComplexStatement_Statement' LowerBound='486.1' HigherBound='488.1'>
                                    <om:Property Name='PortName' Value='SFCreditBureauEventRequestResponsePort' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauEventRequestMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Request' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Send Event Request' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                                <om:Element Type='Receive' OID='d517f92a-29d5-4e7d-bf48-946cea159683' ParentLink='ComplexStatement_Statement' LowerBound='488.1' HigherBound='490.1'>
                                    <om:Property Name='Activate' Value='False' />
                                    <om:Property Name='PortName' Value='SFCreditBureauEventRequestResponsePort' />
                                    <om:Property Name='MessageName' Value='SFCreditBureauEventResponseMsg' />
                                    <om:Property Name='OperationName' Value='Operation' />
                                    <om:Property Name='OperationMessageName' Value='Response' />
                                    <om:Property Name='ReportToAnalyst' Value='True' />
                                    <om:Property Name='Name' Value='Receive Event Response' />
                                    <om:Property Name='Signal' Value='True' />
                                </om:Element>
                            </om:Element>
                        </om:Element>
                    </om:Element>
                </om:Element>
            </om:Element>
            <om:Element Type='PortDeclaration' OID='bd8f3678-a146-417c-925a-4f6c368ac3f4' ParentLink='ServiceDeclaration_PortDeclaration' LowerBound='94.1' HigherBound='96.1'>
                <om:Property Name='PortModifier' Value='Implements' />
                <om:Property Name='Orientation' Value='Left' />
                <om:Property Name='PortIndex' Value='-1' />
                <om:Property Name='IsWebPort' Value='False' />
                <om:Property Name='OrderedDelivery' Value='False' />
                <om:Property Name='DeliveryNotification' Value='None' />
                <om:Property Name='Type' Value='CreditProfileLogic.SFJSONApplicationRequestPortType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFJSONApplicationRequestPort' />
                <om:Property Name='Signal' Value='False' />
                <om:Element Type='LogicalBindingAttribute' OID='17858c37-9c88-4656-9712-3383619b002b' ParentLink='PortDeclaration_CLRAttribute' LowerBound='94.1' HigherBound='95.1'>
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
            <om:Element Type='PortDeclaration' OID='646ee64e-3969-4cac-baf8-54e43db6888f' ParentLink='ServiceDeclaration_PortDeclaration' LowerBound='96.1' HigherBound='98.1'>
                <om:Property Name='PortModifier' Value='Uses' />
                <om:Property Name='Orientation' Value='Right' />
                <om:Property Name='PortIndex' Value='70' />
                <om:Property Name='IsWebPort' Value='False' />
                <om:Property Name='OrderedDelivery' Value='False' />
                <om:Property Name='DeliveryNotification' Value='None' />
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauInsertRequestResponseType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauInsertRequestResponse' />
                <om:Property Name='Signal' Value='False' />
                <om:Element Type='LogicalBindingAttribute' OID='beb88844-a33e-4434-a1b7-7004ec0d809e' ParentLink='PortDeclaration_CLRAttribute' LowerBound='96.1' HigherBound='97.1'>
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
            <om:Element Type='PortDeclaration' OID='f248b985-8c72-4c0a-bb38-c2c52f28df5e' ParentLink='ServiceDeclaration_PortDeclaration' LowerBound='98.1' HigherBound='101.1'>
                <om:Property Name='PortModifier' Value='Uses' />
                <om:Property Name='Orientation' Value='Right' />
                <om:Property Name='PortIndex' Value='205' />
                <om:Property Name='IsWebPort' Value='False' />
                <om:Property Name='OrderedDelivery' Value='False' />
                <om:Property Name='DeliveryNotification' Value='Transmitted' />
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauLineItemInsertRequestResponsePortType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauLineItemInsertRequestResponsePort' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='LogicalBindingAttribute' OID='dbfff07e-3f4e-4b3e-9b36-606157aeb199' ParentLink='PortDeclaration_CLRAttribute' LowerBound='98.1' HigherBound='99.1'>
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
            <om:Element Type='PortDeclaration' OID='0930958c-067b-4a08-887a-63c1fae00c62' ParentLink='ServiceDeclaration_PortDeclaration' LowerBound='101.1' HigherBound='104.1'>
                <om:Property Name='PortModifier' Value='Uses' />
                <om:Property Name='Orientation' Value='Right' />
                <om:Property Name='PortIndex' Value='292' />
                <om:Property Name='IsWebPort' Value='False' />
                <om:Property Name='OrderedDelivery' Value='False' />
                <om:Property Name='DeliveryNotification' Value='Transmitted' />
                <om:Property Name='Type' Value='CreditProfileLogic.SFCreditBureauEventRequestResponsePortType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='SFCreditBureauEventRequestResponsePort' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='LogicalBindingAttribute' OID='4ead0e5b-6f33-401c-8f6c-fcf7aedda7fe' ParentLink='PortDeclaration_CLRAttribute' LowerBound='101.1' HigherBound='102.1'>
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='f9cb93d3-d0ca-4d22-9fa8-217960530503' ParentLink='Module_PortType' LowerBound='56.1' HigherBound='63.1'>
            <om:Property Name='Synchronous' Value='False' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFJSONApplicationRequestPortType' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='6cd218c6-4bc8-48d6-bb81-58530e054b52' ParentLink='PortType_OperationDeclaration' LowerBound='58.1' HigherBound='62.1'>
                <om:Property Name='OperationType' Value='OneWay' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='MessageRef' OID='74b3b4f0-4653-4321-9815-17a9ca2d23dd' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='60.13' HigherBound='60.41'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFApplicationJSONRequestType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='1dc810f9-4a12-4c50-829e-d42b19326964' ParentLink='Module_PortType' LowerBound='63.1' HigherBound='70.1'>
            <om:Property Name='Synchronous' Value='True' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauLineItemInsertRequestResponsePortType' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='9ce568ee-c4a8-44a8-a23d-208ca536224a' ParentLink='PortType_OperationDeclaration' LowerBound='65.1' HigherBound='69.1'>
                <om:Property Name='OperationType' Value='RequestResponse' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='MessageRef' OID='54c6bbe3-2420-47a3-b38c-2bcb55a6b239' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='67.13' HigherBound='67.52'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauLineItemInsertRequestType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='MessageRef' OID='3d98f67b-eda0-4d08-af21-5f5cfbcd6861' ParentLink='OperationDeclaration_ResponseMessageRef' LowerBound='67.54' HigherBound='67.94'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauLineItemInsertResponseType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Response' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='2a7c4501-2068-4158-9fc0-4af6b79c6de0' ParentLink='Module_PortType' LowerBound='70.1' HigherBound='77.1'>
            <om:Property Name='Synchronous' Value='True' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauEventRequestResponsePortType' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='51c128c7-1d28-40a3-84c2-511153fcfac2' ParentLink='PortType_OperationDeclaration' LowerBound='72.1' HigherBound='76.1'>
                <om:Property Name='OperationType' Value='RequestResponse' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='MessageRef' OID='d03abac9-fa51-4d1f-b796-0c7f0ffaa417' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='74.13' HigherBound='74.43'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauEventRequestType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
                <om:Element Type='MessageRef' OID='8569a93c-fe90-45f1-b262-61afecde1000' ParentLink='OperationDeclaration_ResponseMessageRef' LowerBound='74.45' HigherBound='74.76'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauEventResponseType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Response' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='5195a02b-d0b8-46d6-a3be-64fdbfad6694' ParentLink='Module_PortType' LowerBound='77.1' HigherBound='84.1'>
            <om:Property Name='Synchronous' Value='False' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='TestType' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='fa5904cf-f769-4d73-8301-2b84f31e1caf' ParentLink='PortType_OperationDeclaration' LowerBound='79.1' HigherBound='83.1'>
                <om:Property Name='OperationType' Value='OneWay' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation_1' />
                <om:Property Name='Signal' Value='False' />
                <om:Element Type='MessageRef' OID='b51bf8b6-b342-4366-89c5-17a473d73dec' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='81.13' HigherBound='81.62'>
                    <om:Property Name='Ref' Value='ExperianCreditProfileDefinition.NetConnectRequest' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='590417e5-5ad3-44cf-804c-f6397ab9e6aa' ParentLink='Module_PortType' LowerBound='84.1' HigherBound='91.1'>
            <om:Property Name='Synchronous' Value='True' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SFCreditBureauInsertRequestResponseType' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='bc6ac2d5-569e-41d1-8e9b-65e55a9c8fe7' ParentLink='PortType_OperationDeclaration' LowerBound='86.1' HigherBound='90.1'>
                <om:Property Name='OperationType' Value='RequestResponse' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='MessageRef' OID='d1aef998-0fc9-4177-aeb7-afb2a442582c' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='88.13' HigherBound='88.44'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauInsertRequestType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
                <om:Element Type='MessageRef' OID='c3a682f3-b668-4d88-a599-dd92b09c9d5d' ParentLink='OperationDeclaration_ResponseMessageRef' LowerBound='88.46' HigherBound='88.78'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.SFCreditBureauInsertResponseType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Response' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
    </om:Element>
</om:MetaModel>
";

        [System.SerializableAttribute]
        public class __CBRProcessingLogic_root_0 : Microsoft.XLANGs.Core.ServiceContext
        {
            public __CBRProcessingLogic_root_0(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "CBRProcessingLogic")
            {
            }

            public override int Index { get { return 0; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[0]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[0]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)(__svc__._stateMgrs[0]);

                if (__svc__.SFCreditBureauInsertRequestResponse != null)
                {
                    __svc__.SFCreditBureauInsertRequestResponse.Close(this, null);
                    __svc__.SFCreditBureauInsertRequestResponse = null;
                }
                if (__svc__.SFCreditBureauLineItemInsertRequestResponsePort != null)
                {
                    __svc__.SFCreditBureauLineItemInsertRequestResponsePort.Close(this, null);
                    __svc__.SFCreditBureauLineItemInsertRequestResponsePort = null;
                }
                if (__svc__.SFJSONApplicationRequestPort != null)
                {
                    __svc__.SFJSONApplicationRequestPort.Close(this, null);
                    __svc__.SFJSONApplicationRequestPort = null;
                }
                if (__svc__.SFCreditBureauEventRequestResponsePort != null)
                {
                    __svc__.SFCreditBureauEventRequestResponsePort.Close(this, null);
                    __svc__.SFCreditBureauEventRequestResponsePort = null;
                }
                base.Finally();
            }

            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper0;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper1;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper2;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper3;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper4;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper5;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper6;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper7;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper8;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper9;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper10;
            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper11;
        }


        [System.SerializableAttribute]
        public class __CBRProcessingLogic_1 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public __CBRProcessingLogic_1(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "CBRProcessingLogic")
            {
            }

            public override int Index { get { return 1; } }

            public override bool CombineParentCommit { get { return true; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[1]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[1]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)(__svc__._stateMgrs[1]);
                __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)(__svc__._stateMgrs[0]);

                if (__ctx1__ != null)
                    __ctx1__.__GetNoRecordsMessageCode = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetCompletionCode = null;
                if (__ctx1__ != null)
                    __ctx1__.__ExperianStatus = null;
                if (__ctx1__ != null)
                    __ctx1__.__RealAddress = null;
                if (__ctx1__ != null)
                    __ctx1__.__sTempExceptionInformation = null;
                if (__ctx1__ != null && __ctx1__.__ExceptionInformationMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExceptionInformationMsg);
                    __ctx1__.__ExceptionInformationMsg = null;
                }
                if (__ctx1__ != null)
                    __ctx1__.__GetExperianValues = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetActualAddress = null;
                if (__ctx1__ != null)
                    __ctx1__.__sSingleApplicantXpath = null;
                if (__ctx1__ != null && __ctx1__.__TempExceptionInformation != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempExceptionInformation);
                    __ctx1__.__TempExceptionInformation = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper10 != null)
                {
                    __ctx0__.__subWrapper10.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper10 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper11 != null)
                {
                    __ctx0__.__subWrapper11.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper11 = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauEventRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __ctx1__.__SFCreditBureauEventRequestMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauInsertRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauInsertResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __ctx1__.__SFCreditBureauInsertResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauEventResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                    __ctx1__.__SFCreditBureauEventResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFApplicationJSONRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFApplicationJSONRequestMsg);
                    __ctx1__.__SFApplicationJSONRequestMsg = null;
                }
                base.Finally();
            }

            [Microsoft.XLANGs.Core.UserVariableAttribute("SFApplicationJSONRequestMsg")]
            internal SFApplicationJSONRequestType __SFApplicationJSONRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFApplicantDebatchedJSONRequestMsg")]
            internal SFApplicantDebatchedJSONRequestType __SFApplicantDebatchedJSONRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ExperianCreditProfileRequestMsg")]
            internal ExperianCreditProfileRequestType __ExperianCreditProfileRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ExperianCreditProfileResponseMsg")]
            internal ExperianCreditProfileResponseType __ExperianCreditProfileResponseMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauInsertRequestMsg")]
            internal SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauInsertResponseMsg")]
            internal SFCreditBureauInsertResponseType __SFCreditBureauInsertResponseMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauLineItemInsertRequestMsg")]
            internal SFCreditBureauLineItemInsertRequestType __SFCreditBureauLineItemInsertRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauLineItemInsertResponseMsg")]
            internal SFCreditBureauLineItemInsertResponseType __SFCreditBureauLineItemInsertResponseMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauEventRequestMsg")]
            internal SFCreditBureauEventRequestType __SFCreditBureauEventRequestMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauEventResponseMsg")]
            internal SFCreditBureauEventResponseType __SFCreditBureauEventResponseMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("BTSMessageGUIDMsg")]
            internal BTSMessageGUIDType __BTSMessageGUIDMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("TempSingleApplicantMsg")]
            internal TempSingleApplicantType __TempSingleApplicantMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("TempBTSMessageGUIDXml")]
            public __messagetype_System_Xml_XmlDocument __TempBTSMessageGUIDXml;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ExceptionInformationMsg")]
            internal ExceptionInformationType __ExceptionInformationMsg;
            [Microsoft.XLANGs.Core.UserVariableAttribute("TempExceptionInformation")]
            public __messagetype_System_Xml_XmlDocument __TempExceptionInformation;
            [Microsoft.XLANGs.Core.UserVariableAttribute("GetActualAddress")]
            internal sba.gov.ActualAddress __GetActualAddress;
            [Microsoft.XLANGs.Core.UserVariableAttribute("GetExperianValues")]
            internal Experian.Values __GetExperianValues;
            [Microsoft.XLANGs.Core.UserVariableAttribute("RecordCount")]
            internal System.Int32 __RecordCount;
            [Microsoft.XLANGs.Core.UserVariableAttribute("LoopCount")]
            internal System.Int32 __LoopCount;
            [Microsoft.XLANGs.Core.UserVariableAttribute("sSingleApplicantXpath")]
            internal System.String __sSingleApplicantXpath;
            [Microsoft.XLANGs.Core.UserVariableAttribute("GetCompletionCode")]
            internal System.String __GetCompletionCode;
            [Microsoft.XLANGs.Core.UserVariableAttribute("GetNoRecordsMessageCode")]
            internal System.String __GetNoRecordsMessageCode;
            [Microsoft.XLANGs.Core.UserVariableAttribute("GetFieldLevelWarningInfo")]
            internal System.Int32 __GetFieldLevelWarningInfo;
            [Microsoft.XLANGs.Core.UserVariableAttribute("Flag")]
            internal System.Boolean __Flag;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ExperianStatus")]
            internal System.String __ExperianStatus;
            [Microsoft.XLANGs.Core.UserVariableAttribute("RealAddress")]
            internal System.String __RealAddress;
            [Microsoft.XLANGs.Core.UserVariableAttribute("sTempExceptionInformation")]
            internal System.String __sTempExceptionInformation;
        }


        [System.SerializableAttribute]
        public class ____scope33_2 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public ____scope33_2(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "??__scope33")
            {
            }

            public override int Index { get { return 2; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[2]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[2]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Microsoft.XLANGs.Core.Segment __seg__;
                Microsoft.XLANGs.Core.FaultReceiveException __fault__;

                __exv__ = _exception;
                if (!(__exv__ is Microsoft.XLANGs.Core.UnknownException))
                {
                    __fault__ = __exv__ as Microsoft.XLANGs.Core.FaultReceiveException;
                    if ((__fault__ == null) && (__exv__ is Microsoft.RuleEngine.PolicyExecutionException))
                    {
                        __seg__ = _service._segments[4];
                        __seg__.Reset(1);
                        __seg__.PredecessorDone(_service);
                        return true;
                    }
                }

                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                ____scope33_2 __ctx2__ = (____scope33_2)(__svc__._stateMgrs[2]);
                __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)(__svc__._stateMgrs[0]);

                if (__ctx0__ != null && __ctx0__.__subWrapper1 != null)
                {
                    __ctx0__.__subWrapper1.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper1 = null;
                }
                if (__ctx2__ != null)
                    __ctx2__.__REx_0 = null;
                base.Finally();
            }

            internal object __exv__;
            internal Microsoft.RuleEngine.PolicyExecutionException __REx_0
            {
                get { return (Microsoft.RuleEngine.PolicyExecutionException)__exv__; }
                set { __exv__ = value; }
            }
        }


        [System.SerializableAttribute]
        public class ____scope34_3 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public ____scope34_3(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "??__scope34")
            {
            }

            public override int Index { get { return 3; } }

            public override bool CombineParentCommit { get { return true; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[3]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[3]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                ____scope34_3 __ctx3__ = (____scope34_3)(__svc__._stateMgrs[3]);

                if (__ctx3__ != null)
                    __ctx3__.__policy_48__ = null;
                base.Finally();
            }

            [Microsoft.XLANGs.Core.UserVariableAttribute("policy_48__")]
            internal Microsoft.RuleEngine.Policy __policy_48__;
        }


        [System.SerializableAttribute]
        public class ____scope36_4 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public ____scope36_4(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "??__scope36")
            {
            }

            public override int Index { get { return 4; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[5]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[5]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Microsoft.XLANGs.Core.Segment __seg__;
                Microsoft.XLANGs.Core.FaultReceiveException __fault__;

                __exv__ = _exception;
                if (!(__exv__ is Microsoft.XLANGs.Core.UnknownException))
                {
                    __fault__ = __exv__ as Microsoft.XLANGs.Core.FaultReceiveException;
                    if ((__fault__ == null) && (__exv__ is Microsoft.RuleEngine.PolicyExecutionException))
                    {
                        __seg__ = _service._segments[9];
                        __seg__.Reset(1);
                        __seg__.PredecessorDone(_service);
                        return true;
                    }
                    if ((__fault__ == null) && (__exv__ is System.Exception))
                    {
                        __seg__ = _service._segments[10];
                        __seg__.Reset(1);
                        __seg__.PredecessorDone(_service);
                        return true;
                    }
                }

                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                ____scope36_4 __ctx4__ = (____scope36_4)(__svc__._stateMgrs[4]);
                __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)(__svc__._stateMgrs[1]);
                __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)(__svc__._stateMgrs[0]);

                if (__ctx0__ != null && __ctx0__.__subWrapper2 != null)
                {
                    __ctx0__.__subWrapper2.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper2 = null;
                }
                if (__ctx4__ != null)
                    __ctx4__.__OSEx_1 = null;
                if (__ctx4__ != null)
                    __ctx4__.__CREx_0 = null;
                if (__ctx1__ != null && __ctx1__.__TempSingleApplicantMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempSingleApplicantMsg);
                    __ctx1__.__TempSingleApplicantMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__TempBTSMessageGUIDXml != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempBTSMessageGUIDXml);
                    __ctx1__.__TempBTSMessageGUIDXml = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper4 != null)
                {
                    __ctx0__.__subWrapper4.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper4 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper9 != null)
                {
                    __ctx0__.__subWrapper9.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper9 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper7 != null)
                {
                    __ctx0__.__subWrapper7.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper7 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper3 != null)
                {
                    __ctx0__.__subWrapper3.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper3 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper8 != null)
                {
                    __ctx0__.__subWrapper8.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper8 = null;
                }
                if (__ctx1__ != null && __ctx1__.__ExperianCreditProfileResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __ctx1__.__ExperianCreditProfileResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__BTSMessageGUIDMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__BTSMessageGUIDMsg);
                    __ctx1__.__BTSMessageGUIDMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFApplicantDebatchedJSONRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                    __ctx1__.__SFApplicantDebatchedJSONRequestMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__ExperianCreditProfileRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExperianCreditProfileRequestMsg);
                    __ctx1__.__ExperianCreditProfileRequestMsg = null;
                }
                base.Finally();
            }

            internal object __exv__;
            internal Microsoft.RuleEngine.PolicyExecutionException __CREx_0
            {
                get { return (Microsoft.RuleEngine.PolicyExecutionException)__exv__; }
                set { __exv__ = value; }
            }
            internal System.Exception __OSEx_1
            {
                get { return (System.Exception)__exv__; }
                set { __exv__ = value; }
            }
        }


        [System.SerializableAttribute]
        public class ____scope37_5 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public ____scope37_5(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "??__scope37")
            {
            }

            public override int Index { get { return 5; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[6]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[6]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                ____scope37_5 __ctx5__ = (____scope37_5)(__svc__._stateMgrs[5]);

                if (__ctx5__ != null)
                    __ctx5__.__policy_49__ = null;
                base.Finally();
            }

            [Microsoft.XLANGs.Core.UserVariableAttribute("policy_49__")]
            internal Microsoft.RuleEngine.Policy __policy_49__;
        }


        [System.SerializableAttribute]
        public class ____scope38_6 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public ____scope38_6(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "??__scope38")
            {
            }

            public override int Index { get { return 6; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[7]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[7]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Microsoft.XLANGs.Core.Segment __seg__;
                Microsoft.XLANGs.Core.FaultReceiveException __fault__;

                __exv__ = _exception;
                if (!(__exv__ is Microsoft.XLANGs.Core.UnknownException))
                {
                    __fault__ = __exv__ as Microsoft.XLANGs.Core.FaultReceiveException;
                    if ((__fault__ == null) && (__exv__ is System.SystemException))
                    {
                        __seg__ = _service._segments[8];
                        __seg__.Reset(1);
                        __seg__.PredecessorDone(_service);
                        return true;
                    }
                }

                Finally();
                return false;
            }

            public override void Finally()
            {
                CBRProcessingLogic __svc__ = (CBRProcessingLogic)_service;
                ____scope38_6 __ctx6__ = (____scope38_6)(__svc__._stateMgrs[6]);
                __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)(__svc__._stateMgrs[1]);
                __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)(__svc__._stateMgrs[0]);

                if (__ctx6__ != null)
                    __ctx6__.__Ex_0 = null;
                if (__ctx0__ != null && __ctx0__.__subWrapper6 != null)
                {
                    __ctx0__.__subWrapper6.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper6 = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper5 != null)
                {
                    __ctx0__.__subWrapper5.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper5 = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauLineItemInsertRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                    __ctx1__.__SFCreditBureauLineItemInsertRequestMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauLineItemInsertResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertResponseMsg);
                    __ctx1__.__SFCreditBureauLineItemInsertResponseMsg = null;
                }
                base.Finally();
            }

            internal object __exv__;
            internal System.SystemException __Ex_0
            {
                get { return (System.SystemException)__exv__; }
                set { __exv__ = value; }
            }
        }

        private static Microsoft.XLANGs.Core.CorrelationType[] _correlationTypes = null;
        public override Microsoft.XLANGs.Core.CorrelationType[] CorrelationTypes { get { return _correlationTypes; } }

        private static System.Guid[] _convoySetIds;

        public override System.Guid[] ConvoySetGuids
        {
            get { return _convoySetIds; }
            set { _convoySetIds = value; }
        }

        public static object[] StaticConvoySetInformation
        {
            get {
                return null;
            }
        }

        [Microsoft.XLANGs.BaseTypes.LogicalBindingAttribute()]
        [Microsoft.XLANGs.BaseTypes.PortAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.eImplements
        )]
        [Microsoft.XLANGs.Core.UserVariableAttribute("SFJSONApplicationRequestPort")]
        internal SFJSONApplicationRequestPortType SFJSONApplicationRequestPort;
        [Microsoft.XLANGs.BaseTypes.LogicalBindingAttribute()]
        [Microsoft.XLANGs.BaseTypes.PortAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses
        )]
        [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauInsertRequestResponse")]
        internal SFCreditBureauInsertRequestResponseType SFCreditBureauInsertRequestResponse;
        [Microsoft.XLANGs.BaseTypes.LogicalBindingAttribute()]
        [Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute(Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute.NotificationLevel.Transmitted)]
        [Microsoft.XLANGs.BaseTypes.PortAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses
        )]
        [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauLineItemInsertRequestResponsePort")]
        internal SFCreditBureauLineItemInsertRequestResponsePortType SFCreditBureauLineItemInsertRequestResponsePort;
        [Microsoft.XLANGs.BaseTypes.LogicalBindingAttribute()]
        [Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute(Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute.NotificationLevel.Transmitted)]
        [Microsoft.XLANGs.BaseTypes.PortAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses
        )]
        [Microsoft.XLANGs.Core.UserVariableAttribute("SFCreditBureauEventRequestResponsePort")]
        internal SFCreditBureauEventRequestResponsePortType SFCreditBureauEventRequestResponsePort;

        public static Microsoft.XLANGs.Core.PortInfo[] _portInfo = new Microsoft.XLANGs.Core.PortInfo[] {
            new Microsoft.XLANGs.Core.PortInfo(new Microsoft.XLANGs.Core.OperationInfo[] {SFJSONApplicationRequestPortType.Operation},
                                               typeof(CBRProcessingLogic).GetField("SFJSONApplicationRequestPort", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance),
                                               Microsoft.XLANGs.BaseTypes.Polarity.implements,
                                               false,
                                               Microsoft.XLANGs.Core.HashHelper.HashPort(typeof(CBRProcessingLogic), "SFJSONApplicationRequestPort"),
                                               null),
            new Microsoft.XLANGs.Core.PortInfo(new Microsoft.XLANGs.Core.OperationInfo[] {SFCreditBureauInsertRequestResponseType.Operation},
                                               typeof(CBRProcessingLogic).GetField("SFCreditBureauInsertRequestResponse", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance),
                                               Microsoft.XLANGs.BaseTypes.Polarity.uses,
                                               false,
                                               Microsoft.XLANGs.Core.HashHelper.HashPort(typeof(CBRProcessingLogic), "SFCreditBureauInsertRequestResponse"),
                                               null),
            new Microsoft.XLANGs.Core.PortInfo(new Microsoft.XLANGs.Core.OperationInfo[] {SFCreditBureauLineItemInsertRequestResponsePortType.Operation},
                                               typeof(CBRProcessingLogic).GetField("SFCreditBureauLineItemInsertRequestResponsePort", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance),
                                               Microsoft.XLANGs.BaseTypes.Polarity.uses,
                                               false,
                                               Microsoft.XLANGs.Core.HashHelper.HashPort(typeof(CBRProcessingLogic), "SFCreditBureauLineItemInsertRequestResponsePort"),
                                               null),
            new Microsoft.XLANGs.Core.PortInfo(new Microsoft.XLANGs.Core.OperationInfo[] {SFCreditBureauEventRequestResponsePortType.Operation},
                                               typeof(CBRProcessingLogic).GetField("SFCreditBureauEventRequestResponsePort", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance),
                                               Microsoft.XLANGs.BaseTypes.Polarity.uses,
                                               false,
                                               Microsoft.XLANGs.Core.HashHelper.HashPort(typeof(CBRProcessingLogic), "SFCreditBureauEventRequestResponsePort"),
                                               null)
        };

        public override Microsoft.XLANGs.Core.PortInfo[] PortInformation
        {
            get { return _portInfo; }
        }

        static public System.Collections.Hashtable PortsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[_portInfo[0].Name] = _portInfo[0];
                h[_portInfo[1].Name] = _portInfo[1];
                h[_portInfo[2].Name] = _portInfo[2];
                h[_portInfo[3].Name] = _portInfo[3];
                return h;
            }
        }

        public static System.Type[] InvokedServicesTypes
        {
            get
            {
                return new System.Type[] {
                    // type of each service invoked by this service
                };
            }
        }

        public static System.Type[] CalledServicesTypes
        {
            get
            {
                return new System.Type[] {
                    typeof(SBACBRCreditProfile.SendToExperian)                    
                };
            }
        }

        public static System.Type[] ExecedServicesTypes
        {
            get
            {
                return new System.Type[] {
                };
            }
        }

        public static object[] StaticSubscriptionsInformation {
            get {
                return new object[1]{
                     new object[5] { _portInfo[0], 0, null , -1, true }
                };
            }
        }

        public static Microsoft.XLANGs.RuntimeTypes.Location[] __eventLocations = new Microsoft.XLANGs.RuntimeTypes.Location[] {
            new Microsoft.XLANGs.RuntimeTypes.Location(0, "00000000-0000-0000-0000-000000000000", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(1, "a4a545ba-ff35-4a9e-a619-ff1d781eb2ba", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(2, "a4a545ba-ff35-4a9e-a619-ff1d781eb2ba", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(3, "00000000-0000-0000-0000-000000000000", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(4, "d1449556-428c-422a-9634-1202c0300ad2", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(5, "00000000-0000-0000-0000-000000000000", 2, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(6, "778ecee9-8a90-4b9e-8d8f-06087cc2704e", 2, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(7, "00000000-0000-0000-0000-000000000000", 3, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(8, "00000000-0000-0000-0000-000000000000", 3, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(9, "778ecee9-8a90-4b9e-8d8f-06087cc2704e", 2, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(10, "c5e8fbcd-4105-49a6-9133-e0cbbd80bab4", 4, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(11, "78530606-81d2-40b1-988c-e9fe6f7d80aa", 4, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(12, "78530606-81d2-40b1-988c-e9fe6f7d80aa", 4, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(13, "cfa1433f-d873-4d67-ab8a-cc8f6e66ae6a", 4, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(14, "cfa1433f-d873-4d67-ab8a-cc8f6e66ae6a", 4, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(15, "34c352f3-c4e3-420b-b798-f6e07837a4c4", 4, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(16, "34c352f3-c4e3-420b-b798-f6e07837a4c4", 4, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(17, "c5e8fbcd-4105-49a6-9133-e0cbbd80bab4", 4, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(18, "d1449556-428c-422a-9634-1202c0300ad2", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(19, "6341733d-b749-4128-80e9-59a88aa3758e", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(20, "00000000-0000-0000-0000-000000000000", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(21, "dde6eb62-66b1-4638-979a-c26c9c4cff9d", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(22, "dde6eb62-66b1-4638-979a-c26c9c4cff9d", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(23, "4b25ff9b-6dcd-4a75-8a84-dbe5e60342bf", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(24, "2d9171d2-e3bb-46e7-bac5-cfa9824aca9b", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(25, "2d9171d2-e3bb-46e7-bac5-cfa9824aca9b", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(26, "00000000-0000-0000-0000-000000000000", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(27, "36c2b8fa-17f9-4341-8874-55977be79319", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(28, "00000000-0000-0000-0000-000000000000", 6, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(29, "00000000-0000-0000-0000-000000000000", 6, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(30, "36c2b8fa-17f9-4341-8874-55977be79319", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(31, "61b984fd-743f-449b-9f52-612cc56ce926", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(32, "d12da5b5-c81a-4705-95ea-2b5a638ade56", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(33, "d12da5b5-c81a-4705-95ea-2b5a638ade56", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(34, "498a3174-d167-4170-a9cb-f4d2216d8058", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(35, "498a3174-d167-4170-a9cb-f4d2216d8058", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(36, "0da99da2-46c5-47fe-a3c9-087c5f20b352", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(37, "0da99da2-46c5-47fe-a3c9-087c5f20b352", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(38, "7631ae8d-135e-4607-923a-87ae5612f1c8", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(39, "b0d25145-adad-4a67-a220-e8e7f46006a0", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(40, "b0d25145-adad-4a67-a220-e8e7f46006a0", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(41, "6ecb37a4-76eb-4af2-aeab-a56bc30eec61", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(42, "6ecb37a4-76eb-4af2-aeab-a56bc30eec61", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(43, "927cfde3-f25a-42e0-9519-93c8317727d4", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(44, "927cfde3-f25a-42e0-9519-93c8317727d4", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(45, "9ecf73a8-4990-421a-a1f7-71a37407df43", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(46, "9ecf73a8-4990-421a-a1f7-71a37407df43", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(47, "613283e1-51fe-47e7-a711-f3b7d2a2cfe5", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(48, "048525ec-3a33-4bd9-a614-e9c1713b905a", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(49, "048525ec-3a33-4bd9-a614-e9c1713b905a", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(50, "f44b021e-4ba9-4ddd-901a-1d62e657523a", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(51, "f44b021e-4ba9-4ddd-901a-1d62e657523a", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(52, "9bec3628-dde5-4009-9e9d-f2981bc68a3e", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(53, "9bec3628-dde5-4009-9e9d-f2981bc68a3e", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(54, "76e06d59-33f9-4c82-8046-92780dbfdcb9", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(55, "76e06d59-33f9-4c82-8046-92780dbfdcb9", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(56, "a2e9aade-6ba9-4d7c-b34d-dcf694567f33", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(57, "a2e9aade-6ba9-4d7c-b34d-dcf694567f33", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(58, "6f8730d5-8272-479c-b73f-631033b10c87", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(59, "6f8730d5-8272-479c-b73f-631033b10c87", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(60, "0e23109b-df0d-47a4-b524-55f7cdb53526", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(61, "00000000-0000-0000-0000-000000000000", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(62, "fabb4833-77ce-41d1-9439-88ec1ed5cef4", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(63, "fabb4833-77ce-41d1-9439-88ec1ed5cef4", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(64, "eeacbc48-66e1-4e8a-9624-a13c58f7cfc6", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(65, "eeacbc48-66e1-4e8a-9624-a13c58f7cfc6", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(66, "f99080fc-ee35-4774-96c1-505ce5000b31", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(67, "f99080fc-ee35-4774-96c1-505ce5000b31", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(68, "8a2b5447-9583-4d25-a4b5-2c2636942b24", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(69, "8a2b5447-9583-4d25-a4b5-2c2636942b24", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(70, "d3887c9e-42be-4e6b-9f18-816803ca9611", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(71, "d3887c9e-42be-4e6b-9f18-816803ca9611", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(72, "5b011c9f-0eb6-4661-b12d-3a07d083f6ce", 7, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(73, "5b011c9f-0eb6-4661-b12d-3a07d083f6ce", 7, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(74, "78ff7814-16cd-469a-8c8e-2c5b92f9ca1a", 8, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(75, "d035ca79-2347-486e-b08e-7b340a6d607c", 8, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(76, "d035ca79-2347-486e-b08e-7b340a6d607c", 8, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(77, "00000000-0000-0000-0000-000000000000", 8, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(78, "00000000-0000-0000-0000-000000000000", 8, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(79, "78ff7814-16cd-469a-8c8e-2c5b92f9ca1a", 8, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(80, "0e23109b-df0d-47a4-b524-55f7cdb53526", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(81, "613283e1-51fe-47e7-a711-f3b7d2a2cfe5", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(82, "7631ae8d-135e-4607-923a-87ae5612f1c8", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(83, "3af9c5ed-4189-423f-8d0c-c3145bf1abc3", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(84, "3af9c5ed-4189-423f-8d0c-c3145bf1abc3", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(85, "2ecf16ef-f313-45f3-920c-fa8d05f67576", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(86, "2ecf16ef-f313-45f3-920c-fa8d05f67576", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(87, "61b984fd-743f-449b-9f52-612cc56ce926", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(88, "e47787ac-f710-4289-9389-f2b551334935", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(89, "e47787ac-f710-4289-9389-f2b551334935", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(90, "0715f746-7350-4d32-a0c4-d087f9f2c6e2", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(91, "0715f746-7350-4d32-a0c4-d087f9f2c6e2", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(92, "b8420b62-eb4a-4c02-a5b4-d4feff2b2679", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(93, "b8420b62-eb4a-4c02-a5b4-d4feff2b2679", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(94, "e4027b2d-8a87-4b3b-a3a7-87bb4ce14bca", 5, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(95, "e4027b2d-8a87-4b3b-a3a7-87bb4ce14bca", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(96, "4b25ff9b-6dcd-4a75-8a84-dbe5e60342bf", 5, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(97, "457e6dba-a406-4840-9472-d8371ce68b8b", 9, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(98, "5259c6ba-3646-4219-abe0-abf8d3ef95c9", 9, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(99, "5259c6ba-3646-4219-abe0-abf8d3ef95c9", 9, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(100, "12a10061-a925-4d19-a368-6bddefd80cef", 9, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(101, "12a10061-a925-4d19-a368-6bddefd80cef", 9, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(102, "937564fd-7367-423e-9149-6fe7d68ffd15", 9, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(103, "937564fd-7367-423e-9149-6fe7d68ffd15", 9, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(104, "457e6dba-a406-4840-9472-d8371ce68b8b", 9, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(105, "6f2d249c-5985-43d5-9582-54feffc49319", 10, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(106, "1ad307d3-daeb-49c2-a072-64159bed5320", 10, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(107, "1ad307d3-daeb-49c2-a072-64159bed5320", 10, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(108, "93d406c7-f7e1-4fc7-8c62-392eafee8da4", 10, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(109, "93d406c7-f7e1-4fc7-8c62-392eafee8da4", 10, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(110, "90c1a1a6-f566-4742-a9b2-85ee3c2abc2d", 10, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(111, "90c1a1a6-f566-4742-a9b2-85ee3c2abc2d", 10, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(112, "6f2d249c-5985-43d5-9582-54feffc49319", 10, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(113, "6341733d-b749-4128-80e9-59a88aa3758e", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(114, "3e37ff95-22a9-4fff-890b-66d6093d026a", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(115, "683f48ba-09f9-4610-85f1-68052b2a90b3", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(116, "683f48ba-09f9-4610-85f1-68052b2a90b3", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(117, "218ce861-41e6-4b82-bdb1-abfc1c05b4b9", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(118, "218ce861-41e6-4b82-bdb1-abfc1c05b4b9", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(119, "ea8a02e7-9330-4381-8787-0dbb334bef31", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(120, "ea8a02e7-9330-4381-8787-0dbb334bef31", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(121, "901a1a80-b6b7-4e1c-8707-f7623452eb34", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(122, "901a1a80-b6b7-4e1c-8707-f7623452eb34", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(123, "d97a49a1-f0d9-4749-9616-156de641ba97", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(124, "d97a49a1-f0d9-4749-9616-156de641ba97", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(125, "d517f92a-29d5-4e7d-bf48-946cea159683", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(126, "d517f92a-29d5-4e7d-bf48-946cea159683", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(127, "3e37ff95-22a9-4fff-890b-66d6093d026a", 1, false)
        };

        public override Microsoft.XLANGs.RuntimeTypes.Location[] EventLocations
        {
            get { return __eventLocations; }
        }

        public static Microsoft.XLANGs.RuntimeTypes.EventData[] __eventData = new Microsoft.XLANGs.RuntimeTypes.EventData[] {
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Body),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Receive),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Scope),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Scope),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Catch),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Construct),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Send),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Catch),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Expression),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Expression),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.If),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.WhileBody),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.While),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Call),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Call),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.If),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.While),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.WhileBody),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Body)
        };

        public static int[] __progressLocation0 = new int[] { 0,0,0,3,3,};
        public static int[] __progressLocation1 = new int[] { 0,0,1,1,2,2,2,2,2,2,2,2,2,2,2,2,4,4,4,18,19,19,19,113,114,114,115,115,116,117,117,117,118,119,119,120,114,121,121,122,123,123,123,124,125,125,126,127,3,3,3,3,};
        public static int[] __progressLocation2 = new int[] { 6,6,6,6,9,9,9,9,};
        public static int[] __progressLocation3 = new int[] { 0,0,0,0,0,0,0,};
        public static int[] __progressLocation4 = new int[] { 10,10,11,11,12,13,13,13,14,15,15,16,17,17,};
        public static int[] __progressLocation5 = new int[] { 21,21,21,22,23,23,24,24,25,25,27,27,27,30,31,31,31,32,32,33,34,34,35,36,36,37,38,38,39,39,40,41,41,41,42,43,43,44,45,45,46,38,47,47,48,48,49,50,50,50,51,52,52,53,47,47,47,54,54,55,56,56,56,57,58,58,59,59,60,60,60,80,80,81,82,83,83,83,84,85,85,86,87,87,87,23,88,88,89,90,90,91,92,92,92,93,94,94,95,96,96,96,96,};
        public static int[] __progressLocation6 = new int[] { 0,0,0,0,0,0,0,};
        public static int[] __progressLocation7 = new int[] { 62,62,62,63,64,64,64,65,66,66,67,68,68,69,70,70,70,71,72,72,73,73,73,73,};
        public static int[] __progressLocation8 = new int[] { 74,74,75,75,76,76,79,79,};
        public static int[] __progressLocation9 = new int[] { 97,97,98,98,99,100,100,100,101,102,102,103,104,104,};
        public static int[] __progressLocation10 = new int[] { 105,105,106,106,107,108,108,108,109,110,110,111,112,112,};

        public static int[][] __progressLocations = new int[11] [] {__progressLocation0,__progressLocation1,__progressLocation2,__progressLocation3,__progressLocation4,__progressLocation5,__progressLocation6,__progressLocation7,__progressLocation8,__progressLocation9,__progressLocation10};
        public override int[][] ProgressLocations {get {return __progressLocations;} }

        public Microsoft.XLANGs.Core.StopConditions segment0(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[0];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[0];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                SFJSONApplicationRequestPort = new SFJSONApplicationRequestPortType(0, this);
                SFCreditBureauInsertRequestResponse = new SFCreditBureauInsertRequestResponseType(1, this);
                SFCreditBureauLineItemInsertRequestResponsePort = new SFCreditBureauLineItemInsertRequestResponsePortType(2, this);
                SFCreditBureauEventRequestResponsePort = new SFCreditBureauEventRequestResponsePortType(3, this);
                __ctx__.PrologueCompleted = true;
                __ctx0__.__subWrapper0 = new Microsoft.XLANGs.Core.SubscriptionWrapper(ActivationSubGuids[0], SFJSONApplicationRequestPort, this);
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.Initialized) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.Initialized;
                goto case 1;
            case 1:
                __ctx1__ = new __CBRProcessingLogic_1(this);
                _stateMgrs[1] = __ctx1__;
                if ( !PostProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 2;
            case 2:
                __ctx0__.StartContext(__seg__, __ctx1__);
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 3:
                if (!__ctx0__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                __ctx1__.Finally();
                ServiceDone(__seg__, (Microsoft.XLANGs.Core.Context)_stateMgrs[0]);
                __ctx0__.OnCommit();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment1(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            bool __condition__;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[1];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[1];
            ____scope33_2 __ctx2__ = (____scope33_2)_stateMgrs[2];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx1__.__GetActualAddress = default(sba.gov.ActualAddress);
                __ctx1__.__GetExperianValues = default(Experian.Values);
                __ctx1__.__RecordCount = default(System.Int32);
                __ctx1__.__LoopCount = default(System.Int32);
                __ctx1__.__sSingleApplicantXpath = default(System.String);
                __ctx1__.__GetCompletionCode = default(System.String);
                __ctx1__.__GetNoRecordsMessageCode = default(System.String);
                __ctx1__.__GetFieldLevelWarningInfo = default(System.Int32);
                __ctx1__.__Flag = default(System.Boolean);
                __ctx1__.__ExperianStatus = default(System.String);
                __ctx1__.__RealAddress = default(System.String);
                __ctx1__.__sTempExceptionInformation = default(System.String);
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[0],__eventData[0],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[1],__eventData[1],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                if (!SFJSONApplicationRequestPort.GetMessageId(__ctx0__.__subWrapper0.getSubscription(this), __seg__, __ctx1__, out __msgEnv__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx1__.__SFApplicationJSONRequestMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFApplicationJSONRequestMsg);
                __ctx1__.__SFApplicationJSONRequestMsg = new SFApplicationJSONRequestType("SFApplicationJSONRequestMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFApplicationJSONRequestMsg);
                SFJSONApplicationRequestPort.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFApplicationJSONRequestMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[1], __seg__);
                if (SFJSONApplicationRequestPort != null)
                {
                    SFJSONApplicationRequestPort.Close(__ctx1__, __seg__);
                    SFJSONApplicationRequestPort = null;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    __edata.PortName = @"SFJSONApplicationRequestPort";
                    Tracker.FireEvent(__eventLocations[2],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                __ctx1__.__GetActualAddress = new sba.gov.ActualAddress();
                if ( !PostProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 6;
            case 6:
                __ctx1__.__GetExperianValues = new Experian.Values();
                if ( !PostProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 7;
            case 7:
                __ctx1__.__RecordCount = 0;
                if ( !PostProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 8;
            case 8:
                __ctx1__.__LoopCount = 1;
                if ( !PostProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 9;
            case 9:
                __ctx1__.__sSingleApplicantXpath = "";
                if ( !PostProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 10;
            case 10:
                __ctx1__.__GetCompletionCode = "";
                if ( !PostProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 11;
            case 11:
                __ctx1__.__GetNoRecordsMessageCode = "";
                if ( !PostProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 12;
            case 12:
                __ctx1__.__Flag = true;
                if ( !PostProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 13;
            case 13:
                __ctx1__.__ExperianStatus = "";
                if ( !PostProgressInc( __seg__, __ctx__, 14 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 14;
            case 14:
                __ctx1__.__RealAddress = "";
                if ( !PostProgressInc( __seg__, __ctx__, 15 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 15;
            case 15:
                __ctx1__.__sTempExceptionInformation = "";
                if ( !PostProgressInc( __seg__, __ctx__, 16 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 16;
            case 16:
                if ( !PreProgressInc( __seg__, __ctx__, 17 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[4],__eventData[2],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 17;
            case 17:
                __ctx2__ = new ____scope33_2(this);
                _stateMgrs[2] = __ctx2__;
                if ( !PostProgressInc( __seg__, __ctx__, 18 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 18;
            case 18:
                __ctx1__.StartContext(__seg__, __ctx2__);
                if ( !PostProgressInc( __seg__, __ctx__, 19 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 19:
                if ( !PreProgressInc( __seg__, __ctx__, 20 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[18],__eventData[3],_stateMgrs[1].TrackDataStream );
                __ctx2__.Finally();
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 20;
            case 20:
                if ( !PreProgressInc( __seg__, __ctx__, 21 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[19],__eventData[2],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 21;
            case 21:
                __ctx4__ = new ____scope36_4(this);
                _stateMgrs[4] = __ctx4__;
                if ( !PostProgressInc( __seg__, __ctx__, 22 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 22;
            case 22:
                __ctx1__.StartContext(__seg__, __ctx4__);
                if ( !PostProgressInc( __seg__, __ctx__, 23 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 23:
                if ( !PreProgressInc( __seg__, __ctx__, 24 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                if (__ctx1__ != null)
                    __ctx1__.__sTempExceptionInformation = null;
                if (__ctx1__ != null)
                    __ctx1__.__RealAddress = null;
                if (__ctx1__ != null)
                    __ctx1__.__ExperianStatus = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetNoRecordsMessageCode = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetCompletionCode = null;
                if (__ctx1__ != null)
                    __ctx1__.__sSingleApplicantXpath = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetExperianValues = null;
                if (__ctx1__ != null)
                    __ctx1__.__GetActualAddress = null;
                if (__ctx1__ != null && __ctx1__.__TempExceptionInformation != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempExceptionInformation);
                    __ctx1__.__TempExceptionInformation = null;
                }
                if (__ctx1__ != null && __ctx1__.__ExceptionInformationMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExceptionInformationMsg);
                    __ctx1__.__ExceptionInformationMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauInsertResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __ctx1__.__SFCreditBureauInsertResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauInsertRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = null;
                }
                if (SFCreditBureauLineItemInsertRequestResponsePort != null)
                {
                    SFCreditBureauLineItemInsertRequestResponsePort.Close(__ctx1__, __seg__);
                    SFCreditBureauLineItemInsertRequestResponsePort = null;
                }
                if (SFCreditBureauInsertRequestResponse != null)
                {
                    SFCreditBureauInsertRequestResponse.Close(__ctx1__, __seg__);
                    SFCreditBureauInsertRequestResponse = null;
                }
                Tracker.FireEvent(__eventLocations[113],__eventData[3],_stateMgrs[1].TrackDataStream );
                __ctx4__.Finally();
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 24;
            case 24:
                if ( !PreProgressInc( __seg__, __ctx__, 25 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[114],__eventData[10],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 25;
            case 25:
                __condition__ = __ctx1__.__Flag;
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 37 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 37;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 26 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 26;
            case 26:
                if ( !PreProgressInc( __seg__, __ctx__, 27 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[115],__eventData[5],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 27;
            case 27:
                {
                    SFCreditBureauEventRequestType __SFCreditBureauEventRequestMsg = new SFCreditBureauEventRequestType("SFCreditBureauEventRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.SuccessResponse_To_SFEvent), new object[] {__SFCreditBureauEventRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauEventRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __ctx1__.__SFCreditBureauEventRequestMsg = __SFCreditBureauEventRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                }
                __ctx1__.__SFCreditBureauEventRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 28 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 28;
            case 28:
                if ( !PreProgressInc( __seg__, __ctx__, 29 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[116],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 29;
            case 29:
                if ( !PreProgressInc( __seg__, __ctx__, 30 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[117],__eventData[6],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 30;
            case 30:
                if (!__ctx1__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 31 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 31;
            case 31:
                if ( !PreProgressInc( __seg__, __ctx__, 32 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauEventRequestResponsePort.SendMessage(0, __ctx1__.__SFCreditBureauEventRequestMsg, null, null, out __ctx0__.__subWrapper10, __ctx1__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 32;
            case 32:
                if ( !PreProgressInc( __seg__, __ctx__, 33 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __edata.PortName = @"SFCreditBureauEventRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[118],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 33;
            case 33:
                if ( !PreProgressInc( __seg__, __ctx__, 34 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[119],__eventData[1],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 34;
            case 34:
                if (!SFCreditBureauEventRequestResponsePort.GetMessageId(__ctx0__.__subWrapper10.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[0]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper10 != null)
                {
                    __ctx0__.__subWrapper10.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper10 = null;
                }
                if (__ctx1__.__SFCreditBureauEventResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                __ctx1__.__SFCreditBureauEventResponseMsg = new SFCreditBureauEventResponseType("SFCreditBureauEventResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                SFCreditBureauEventRequestResponsePort.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauEventResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[1], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 35 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 35;
            case 35:
                if ( !PreProgressInc( __seg__, __ctx__, 36 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventResponseMsg);
                    __edata.PortName = @"SFCreditBureauEventRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[120],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 36;
            case 36:
                if ( !PostProgressInc( __seg__, __ctx__, 47 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 47;
            case 37:
                if ( !PreProgressInc( __seg__, __ctx__, 38 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[121],__eventData[5],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 38;
            case 38:
                {
                    SFCreditBureauEventRequestType __SFCreditBureauEventRequestMsg = new SFCreditBureauEventRequestType("SFCreditBureauEventRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.ErrorResponse_To_SFEvent), new object[] {__SFCreditBureauEventRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauEventRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __ctx1__.__SFCreditBureauEventRequestMsg = __SFCreditBureauEventRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                }
                __ctx1__.__SFCreditBureauEventRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 39 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 39;
            case 39:
                if ( !PreProgressInc( __seg__, __ctx__, 40 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[122],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 40;
            case 40:
                if ( !PreProgressInc( __seg__, __ctx__, 41 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[123],__eventData[6],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 41;
            case 41:
                if (!__ctx1__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 42 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 42;
            case 42:
                if ( !PreProgressInc( __seg__, __ctx__, 43 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauEventRequestResponsePort.SendMessage(0, __ctx1__.__SFCreditBureauEventRequestMsg, null, null, out __ctx0__.__subWrapper11, __ctx1__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 43;
            case 43:
                if ( !PreProgressInc( __seg__, __ctx__, 44 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __edata.PortName = @"SFCreditBureauEventRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[124],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 44;
            case 44:
                if ( !PreProgressInc( __seg__, __ctx__, 45 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[125],__eventData[1],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 45;
            case 45:
                if (!SFCreditBureauEventRequestResponsePort.GetMessageId(__ctx0__.__subWrapper11.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[1]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper11 != null)
                {
                    __ctx0__.__subWrapper11.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper11 = null;
                }
                if (__ctx1__.__SFCreditBureauEventResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                __ctx1__.__SFCreditBureauEventResponseMsg = new SFCreditBureauEventResponseType("SFCreditBureauEventResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                SFCreditBureauEventRequestResponsePort.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauEventResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[1], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 46 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 46;
            case 46:
                if ( !PreProgressInc( __seg__, __ctx__, 47 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauEventResponseMsg);
                    __edata.PortName = @"SFCreditBureauEventRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[126],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 47;
            case 47:
                if ( !PreProgressInc( __seg__, __ctx__, 48 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauEventResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventResponseMsg);
                    __ctx1__.__SFCreditBureauEventResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauEventRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauEventRequestMsg);
                    __ctx1__.__SFCreditBureauEventRequestMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFApplicationJSONRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFApplicationJSONRequestMsg);
                    __ctx1__.__SFApplicationJSONRequestMsg = null;
                }
                if (SFCreditBureauEventRequestResponsePort != null)
                {
                    SFCreditBureauEventRequestResponsePort.Close(__ctx1__, __seg__);
                    SFCreditBureauEventRequestResponsePort = null;
                }
                Tracker.FireEvent(__eventLocations[127],__eventData[15],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 48;
            case 48:
                if ( !PreProgressInc( __seg__, __ctx__, 49 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[3],__eventData[18],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 49;
            case 49:
                if (!__ctx1__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 50 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 50;
            case 50:
                if ( !PreProgressInc( __seg__, __ctx__, 51 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx1__.OnCommit();
                goto case 51;
            case 51:
                __seg__.SegmentDone();
                _segments[0].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment2(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[2];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[2];
            ____scope34_3 __ctx3__ = (____scope34_3)_stateMgrs[3];
            ____scope33_2 __ctx2__ = (____scope33_2)_stateMgrs[2];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[6],__eventData[2],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                __ctx3__ = new ____scope34_3(this);
                _stateMgrs[3] = __ctx3__;
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 3;
            case 3:
                __ctx2__.StartContext(__seg__, __ctx3__);
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[9],__eventData[3],_stateMgrs[2].TrackDataStream );
                __ctx3__.Finally();
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                if (!__ctx2__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 6;
            case 6:
                if ( !PreProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx2__.OnCommit();
                goto case 7;
            case 7:
                __seg__.SegmentDone();
                _segments[1].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment3(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[3];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[3];
            ____scope34_3 __ctx3__ = (____scope34_3)_stateMgrs[3];
            ____scope33_2 __ctx2__ = (____scope33_2)_stateMgrs[2];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx3__.__policy_48__ = default(Microsoft.RuleEngine.Policy);
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                __ctx3__.__policy_48__ = new Microsoft.RuleEngine.Policy("Get Dynamic Address");
                if ( !PostProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 2;
            case 2:
                __ctx3__.__policy_48__.Execute(__ctx1__.__GetActualAddress);
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 3;
            case 3:
                __ctx3__.__policy_48__.Dispose();
                if (__ctx3__ != null)
                    __ctx3__.__policy_48__ = null;
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if (!__ctx3__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx3__.OnCommit();
                goto case 6;
            case 6:
                __seg__.SegmentDone();
                _segments[2].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment4(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[4];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[2];
            ____scope33_2 __ctx2__ = (____scope33_2)_stateMgrs[2];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                OnBeginCatchHandler(2);
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[10],__eventData[4],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[11],__eventData[5],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);
                    ExceptionInformationType __ExceptionInformationMsg = new ExceptionInformationType("ExceptionInformationMsg", __ctx1__);
                    __messagetype_System_Xml_XmlDocument __TempExceptionInformation = new __messagetype_System_Xml_XmlDocument("TempExceptionInformation", __ctx1__);

                    __ctx1__.__Flag = false;
                    __TempExceptionInformation.part.LoadFrom(new System.Xml.XmlDocument());
                    __ctx1__.__sTempExceptionInformation = __ctx2__.__REx_0.Message;
                    if (__ctx2__ != null)
                        __ctx2__.__REx_0 = null;
                    __ExceptionInformationMsg.MessagePart.CopyFrom(__TempExceptionInformation.part);
                    ApplyTransform(typeof(CreditProfileLogic.TransformException_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ExceptionInformationMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    if (__ctx1__.__ExceptionInformationMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__ExceptionInformationMsg);
                    __ctx1__.__ExceptionInformationMsg = __ExceptionInformationMsg;
                    __ctx1__.RefMessage(__ctx1__.__ExceptionInformationMsg);
                    if (__ctx1__.__TempExceptionInformation != null)
                        __ctx1__.UnrefMessage(__ctx1__.__TempExceptionInformation);
                    __ctx1__.__TempExceptionInformation = __TempExceptionInformation;
                    __ctx1__.RefMessage(__ctx1__.__TempExceptionInformation);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                __ctx1__.__ExceptionInformationMsg.ConstructionCompleteEvent(true);
                __ctx1__.__TempExceptionInformation.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExceptionInformationMsg);
                    __edata.Messages.Add(__ctx1__.__TempExceptionInformation);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[12],__edata,_stateMgrs[2].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[13],__eventData[6],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 6;
            case 6:
                if (!__ctx2__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 7;
            case 7:
                if ( !PreProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper1, __ctx2__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[14],__edata,_stateMgrs[2].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 9;
            case 9:
                if ( !PreProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[15],__eventData[1],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 10;
            case 10:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper1.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[2]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper1 != null)
                {
                    __ctx0__.__subWrapper1.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper1 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[2], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 11;
            case 11:
                if ( !PreProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[16],__edata,_stateMgrs[2].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 12;
            case 12:
                if ( !PreProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[17],__eventData[7],_stateMgrs[2].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 13;
            case 13:
                __ctx2__.__exv__ = null;
                OnEndCatchHandler(2, __seg__);
                __seg__.SegmentDone();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment5(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            bool __condition__;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[5];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[4];
            ____scope38_6 __ctx6__ = (____scope38_6)_stateMgrs[6];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            ____scope37_5 __ctx5__ = (____scope37_5)_stateMgrs[5];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[21],__eventData[8],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                __ctx1__.__RealAddress = __ctx1__.__GetActualAddress.Address;
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 3;
            case 3:
                if ( !PreProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[22],__eventData[9],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[23],__eventData[10],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                __condition__ = __ctx1__.__RealAddress.Contains(".experian.com");
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 86 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 86;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 6;
            case 6:
                if ( !PreProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[24],__eventData[8],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 7;
            case 7:
                __ctx1__.__RecordCount = System.Convert.ToInt32(Microsoft.XLANGs.Core.Part.XPathLoad(__ctx1__.__SFApplicationJSONRequestMsg.MessagePart, "count(/*[local-name()='SFApplicationRequest' and namespace-uri()='http://SBA.CBR.CreditProfile.SFApplicationRequest']/*[local-name()='applicants' and namespace-uri()=''])", typeof(System.Object)));
                if ( !PostProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[25],__eventData[9],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 9;
            case 9:
                __ctx1__.__GetExperianValues = new Experian.Values();
                if ( !PostProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 10;
            case 10:
                if ( !PreProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[27],__eventData[2],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 11;
            case 11:
                __ctx5__ = new ____scope37_5(this);
                _stateMgrs[5] = __ctx5__;
                if ( !PostProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 12;
            case 12:
                __ctx4__.StartContext(__seg__, __ctx5__);
                if ( !PostProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 13:
                if ( !PreProgressInc( __seg__, __ctx__, 14 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[30],__eventData[3],_stateMgrs[4].TrackDataStream );
                __ctx5__.Finally();
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 14;
            case 14:
                if ( !PreProgressInc( __seg__, __ctx__, 15 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[31],__eventData[11],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 15;
            case 15:
                __condition__ = __ctx1__.__LoopCount <= __ctx1__.__RecordCount;
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 84 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 84;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 16 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 16;
            case 16:
                if ( !PreProgressInc( __seg__, __ctx__, 17 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[31],__eventData[12],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 17;
            case 17:
                if ( !PreProgressInc( __seg__, __ctx__, 18 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[32],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 18;
            case 18:
                {
                    ExperianCreditProfileRequestType __ExperianCreditProfileRequestMsg = new ExperianCreditProfileRequestType("ExperianCreditProfileRequestMsg", __ctx1__);
                    TempSingleApplicantType __TempSingleApplicantMsg = new TempSingleApplicantType("TempSingleApplicantMsg", __ctx1__);
                    SFApplicantDebatchedJSONRequestType __SFApplicantDebatchedJSONRequestMsg = new SFApplicantDebatchedJSONRequestType("SFApplicantDebatchedJSONRequestMsg", __ctx1__);

                    __ctx1__.__sSingleApplicantXpath = System.String.Format("/*[local-name()='SFApplicationRequest' and namespace-uri()='http://SBA.CBR.CreditProfile.SFApplicationRequest']/*[local-name()='applicants' and namespace-uri()=''][{0}]", __ctx1__.__LoopCount);
                    __TempSingleApplicantMsg.MessagePart.XPathAssign(__ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ctx1__.__sSingleApplicantXpath);
                    __SFApplicantDebatchedJSONRequestMsg.MessagePart.LoadFrom(AddNameSpaceToDebachedMessage.AddNameSpace.GetMessageFromXmlStr(__TempSingleApplicantMsg.MessagePart.TypedValue.OuterXml, "http://SBA.CBR.CreditProfile.Applicants"));
                    ApplyTransform(typeof(CreditProfileLogic.Transform_SingleApplicant_To_CBR_Request), new object[] {__ExperianCreditProfileRequestMsg.MessagePart}, new object[] {__SFApplicantDebatchedJSONRequestMsg.MessagePart});
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.EAI, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='EAI' and namespace-uri()='http://www.experian.com/NetConnect']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.DBHost, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='DBHost' and namespace-uri()='http://www.experian.com/NetConnect']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.ReferenceId, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='ReferenceId' and namespace-uri()='http://www.experian.com/NetConnect']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.Preamble, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='Request' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Products' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='CreditProfile' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Subscriber' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Preamble' and namespace-uri()='http://www.experian.com/WebDelivery']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.OpInitials, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='Request' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Products' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='CreditProfile' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Subscriber' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='OpInitials' and namespace-uri()='http://www.experian.com/WebDelivery']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.SubCode, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='Request' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Products' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='CreditProfile' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Subscriber' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='SubCode' and namespace-uri()='http://www.experian.com/WebDelivery']");
                    __ExperianCreditProfileRequestMsg.MessagePart.XPathStore(__ctx1__.__GetExperianValues.VendorNumber, "/*[local-name()='NetConnectRequest' and namespace-uri()='http://www.experian.com/NetConnect']/*[local-name()='Request' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Products' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='CreditProfile' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='Vendor' and namespace-uri()='http://www.experian.com/WebDelivery']/*[local-name()='VendorNumber' and namespace-uri()='http://www.experian.com/WebDelivery']");

                    if (__ctx1__.__ExperianCreditProfileRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__ExperianCreditProfileRequestMsg);
                    __ctx1__.__ExperianCreditProfileRequestMsg = __ExperianCreditProfileRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__ExperianCreditProfileRequestMsg);
                    if (__ctx1__.__TempSingleApplicantMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__TempSingleApplicantMsg);
                    __ctx1__.__TempSingleApplicantMsg = __TempSingleApplicantMsg;
                    __ctx1__.RefMessage(__ctx1__.__TempSingleApplicantMsg);
                    if (__ctx1__.__SFApplicantDebatchedJSONRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                    __ctx1__.__SFApplicantDebatchedJSONRequestMsg = __SFApplicantDebatchedJSONRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                }
                __ctx1__.__ExperianCreditProfileRequestMsg.ConstructionCompleteEvent(true);
                __ctx1__.__TempSingleApplicantMsg.ConstructionCompleteEvent(true);
                __ctx1__.__SFApplicantDebatchedJSONRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 19 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 19;
            case 19:
                if ( !PreProgressInc( __seg__, __ctx__, 20 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileRequestMsg);
                    __edata.Messages.Add(__ctx1__.__TempSingleApplicantMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[33],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__TempSingleApplicantMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempSingleApplicantMsg);
                    __ctx1__.__TempSingleApplicantMsg = null;
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 20;
            case 20:
                if ( !PreProgressInc( __seg__, __ctx__, 21 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[34],__eventData[13],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 21;
            case 21:
                {
                    Microsoft.XLANGs.Core.Service svc = new SBACBRCreditProfile.SendToExperian(7, InstanceId, this);
                    _stateMgrs[7] = svc;
                    __ctx4__.StartCall(__seg__, svc, __eventLocations[34],new object[] {new Microsoft.XLANGs.Core.MessageTuple((Microsoft.XLANGs.Core.Context)_stateMgrs[1], null), __ctx1__.__ExperianCreditProfileRequestMsg, __ctx1__.__GetExperianValues, __ctx1__.__RealAddress});
                }
                if ( !PostProgressInc( __seg__, __ctx__, 22 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 22:
                {
                    object[] args = ((Microsoft.XLANGs.Core.Service)_stateMgrs[7]).Args;
                    __ctx1__.__ExperianCreditProfileResponseMsg = (ExperianCreditProfileResponseType)((Microsoft.XLANGs.Core.MessageTuple)(args[0])).message;
                    __ctx1__.__ExperianCreditProfileResponseMsg.MessageName = "ExperianCreditProfileResponseMsg";
                }
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Call);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileRequestMsg);
                    Tracker.FireEvent(__eventLocations[35],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__ExperianCreditProfileRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExperianCreditProfileRequestMsg);
                    __ctx1__.__ExperianCreditProfileRequestMsg = null;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 23 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 23;
            case 23:
                if ( !PreProgressInc( __seg__, __ctx__, 24 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[36],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 24;
            case 24:
                {
                    BTSMessageGUIDType __BTSMessageGUIDMsg = new BTSMessageGUIDType("BTSMessageGUIDMsg", __ctx1__);
                    __messagetype_System_Xml_XmlDocument __TempBTSMessageGUIDXml = new __messagetype_System_Xml_XmlDocument("TempBTSMessageGUIDXml", __ctx1__);

                    __ctx1__.__GetCompletionCode = (System.String)Microsoft.XLANGs.Core.Part.XPathLoad(__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, "string(/*[local-name()='NetConnectResponse' and namespace-uri()='http://www.experian.com/NetConnectResponse']/*[local-name()='CompletionCode' and namespace-uri()='http://www.experian.com/NetConnectResponse']/text())", typeof(System.String));
                    System.Diagnostics.EventLog.WriteEntry("Completion Code Value Exits", __ctx1__.__GetCompletionCode);
                    __ctx1__.__GetNoRecordsMessageCode = (System.String)Microsoft.XLANGs.Core.Part.XPathLoad(__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, "string(/*[local-name()='NetConnectResponse']/*[local-name()='Products']/*[local-name()='CreditProfile']/*[local-name()='InformationalMessage'][*[local-name()='MessageNumber'][text()='07']]/*[local-name()='MessageNumber'])", typeof(System.String));
                    __ctx1__.__GetFieldLevelWarningInfo = System.Convert.ToInt32(Microsoft.XLANGs.Core.Part.XPathLoad(__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, "count(/*[local-name()='NetConnectResponse' and namespace-uri()='http://www.experian.com/NetConnectResponse']/*[local-name()='Products' and namespace-uri()='http://www.experian.com/ARFResponse']/*[local-name()='CreditProfile' and namespace-uri()='http://www.experian.com/ARFResponse']/*[local-name()='Statement' and namespace-uri()='http://www.experian.com/ARFResponse']/*[local-name()='Type' and @code ='25' or @code ='32'])", typeof(System.Object)));
                    __TempBTSMessageGUIDXml.part.LoadFrom(new System.Xml.XmlDocument());
                    __TempBTSMessageGUIDXml.part.TypedValue.LoadXml(@"<ns0:BTSMessageGUID xmlns:ns0='http://SBA.CBR.CreditProfile'><ns0:ID>" + "BT Message ID: " + (System.String)__ctx1__.__ExperianCreditProfileResponseMsg.GetPropertyValueThrows(typeof(BTS.MessageID)) + "</ns0:ID></ns0:BTSMessageGUID>");
                    __BTSMessageGUIDMsg.MessagePart.CopyFrom(__TempBTSMessageGUIDXml.part);

                    if (__ctx1__.__BTSMessageGUIDMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__BTSMessageGUIDMsg);
                    __ctx1__.__BTSMessageGUIDMsg = __BTSMessageGUIDMsg;
                    __ctx1__.RefMessage(__ctx1__.__BTSMessageGUIDMsg);
                    if (__ctx1__.__TempBTSMessageGUIDXml != null)
                        __ctx1__.UnrefMessage(__ctx1__.__TempBTSMessageGUIDXml);
                    __ctx1__.__TempBTSMessageGUIDXml = __TempBTSMessageGUIDXml;
                    __ctx1__.RefMessage(__ctx1__.__TempBTSMessageGUIDXml);
                }
                __ctx1__.__BTSMessageGUIDMsg.ConstructionCompleteEvent(false);
                __ctx1__.__TempBTSMessageGUIDXml.ConstructionCompleteEvent(false);
                if ( !PostProgressInc( __seg__, __ctx__, 25 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 25;
            case 25:
                if ( !PreProgressInc( __seg__, __ctx__, 26 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__BTSMessageGUIDMsg);
                    __edata.Messages.Add(__ctx1__.__TempBTSMessageGUIDXml);
                    Tracker.FireEvent(__eventLocations[37],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__TempBTSMessageGUIDXml != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__TempBTSMessageGUIDXml);
                    __ctx1__.__TempBTSMessageGUIDXml = null;
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 26;
            case 26:
                if ( !PreProgressInc( __seg__, __ctx__, 27 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[38],__eventData[10],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 27;
            case 27:
                __condition__ = __ctx1__.__GetCompletionCode != "0000";
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 42 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 42;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 28 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 28;
            case 28:
                if ( !PreProgressInc( __seg__, __ctx__, 29 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[39],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 29;
            case 29:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.Transform_ErrorRequest), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, __ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ctx1__.__BTSMessageGUIDMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 30 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 30;
            case 30:
                if ( !PreProgressInc( __seg__, __ctx__, 31 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    __edata.Messages.Add(__ctx1__.__BTSMessageGUIDMsg);
                    Tracker.FireEvent(__eventLocations[40],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 31;
            case 31:
                if ( !PreProgressInc( __seg__, __ctx__, 32 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[41],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 32;
            case 32:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 33 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 33;
            case 33:
                if ( !PreProgressInc( __seg__, __ctx__, 34 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper2, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 34;
            case 34:
                if ( !PreProgressInc( __seg__, __ctx__, 35 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[42],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 35;
            case 35:
                if ( !PreProgressInc( __seg__, __ctx__, 36 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[43],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 36;
            case 36:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper2.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[3]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper2 != null)
                {
                    __ctx0__.__subWrapper2.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper2 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 37 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 37;
            case 37:
                if ( !PreProgressInc( __seg__, __ctx__, 38 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[44],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 38;
            case 38:
                if ( !PreProgressInc( __seg__, __ctx__, 39 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[45],__eventData[8],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 39;
            case 39:
                __ctx1__.__Flag = false;
                if ( !PostProgressInc( __seg__, __ctx__, 40 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 40;
            case 40:
                if ( !PreProgressInc( __seg__, __ctx__, 41 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[46],__eventData[9],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 41;
            case 41:
                if ( !PostProgressInc( __seg__, __ctx__, 74 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 74;
            case 42:
                if ( !PreProgressInc( __seg__, __ctx__, 43 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[47],__eventData[10],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 43;
            case 43:
                __condition__ = __ctx1__.__GetFieldLevelWarningInfo > 0;
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 55 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 55;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 44 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 44;
            case 44:
                if ( !PreProgressInc( __seg__, __ctx__, 45 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[48],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 45;
            case 45:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.Transform_FieldLevelWarning_To_SFCBRRequest), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, __ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ctx1__.__BTSMessageGUIDMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 46 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 46;
            case 46:
                if ( !PreProgressInc( __seg__, __ctx__, 47 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    __edata.Messages.Add(__ctx1__.__BTSMessageGUIDMsg);
                    Tracker.FireEvent(__eventLocations[49],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 47;
            case 47:
                if ( !PreProgressInc( __seg__, __ctx__, 48 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[50],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 48;
            case 48:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 49 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 49;
            case 49:
                if ( !PreProgressInc( __seg__, __ctx__, 50 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper3, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 50;
            case 50:
                if ( !PreProgressInc( __seg__, __ctx__, 51 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[51],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 51;
            case 51:
                if ( !PreProgressInc( __seg__, __ctx__, 52 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[52],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 52;
            case 52:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper3.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[4]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper3 != null)
                {
                    __ctx0__.__subWrapper3.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper3 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 53 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 53;
            case 53:
                if ( !PreProgressInc( __seg__, __ctx__, 54 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[53],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 54;
            case 54:
                if ( !PostProgressInc( __seg__, __ctx__, 73 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 73;
            case 55:
                if ( !PreProgressInc( __seg__, __ctx__, 56 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[20],__eventData[10],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 56;
            case 56:
                __condition__ = __ctx1__.__GetNoRecordsMessageCode == "07";
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 68 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 68;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 57 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 57;
            case 57:
                if ( !PreProgressInc( __seg__, __ctx__, 58 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[54],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 58;
            case 58:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.Transform_CBRWaringNoRecordResponse_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, __ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ctx1__.__BTSMessageGUIDMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 59 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 59;
            case 59:
                if ( !PreProgressInc( __seg__, __ctx__, 60 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    __edata.Messages.Add(__ctx1__.__BTSMessageGUIDMsg);
                    Tracker.FireEvent(__eventLocations[55],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 60;
            case 60:
                if ( !PreProgressInc( __seg__, __ctx__, 61 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[56],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 61;
            case 61:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 62 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 62;
            case 62:
                if ( !PreProgressInc( __seg__, __ctx__, 63 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper4, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 63;
            case 63:
                if ( !PreProgressInc( __seg__, __ctx__, 64 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[57],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 64;
            case 64:
                if ( !PreProgressInc( __seg__, __ctx__, 65 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[58],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 65;
            case 65:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper4.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[5]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper4 != null)
                {
                    __ctx0__.__subWrapper4.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper4 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 66 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 66;
            case 66:
                if ( !PreProgressInc( __seg__, __ctx__, 67 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[59],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 67;
            case 67:
                if ( !PostProgressInc( __seg__, __ctx__, 72 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 72;
            case 68:
                if ( !PreProgressInc( __seg__, __ctx__, 69 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[60],__eventData[2],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 69;
            case 69:
                __ctx6__ = new ____scope38_6(this);
                _stateMgrs[6] = __ctx6__;
                if ( !PostProgressInc( __seg__, __ctx__, 70 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 70;
            case 70:
                __ctx4__.StartContext(__seg__, __ctx6__);
                if ( !PostProgressInc( __seg__, __ctx__, 71 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 71:
                if ( !PreProgressInc( __seg__, __ctx__, 72 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[80],__eventData[3],_stateMgrs[4].TrackDataStream );
                __ctx6__.Finally();
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 72;
            case 72:
                if ( !PreProgressInc( __seg__, __ctx__, 73 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[26],__eventData[15],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 73;
            case 73:
                if ( !PreProgressInc( __seg__, __ctx__, 74 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[81],__eventData[15],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 74;
            case 74:
                if ( !PreProgressInc( __seg__, __ctx__, 75 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                if (__ctx1__ != null && __ctx1__.__BTSMessageGUIDMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__BTSMessageGUIDMsg);
                    __ctx1__.__BTSMessageGUIDMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__ExperianCreditProfileResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __ctx1__.__ExperianCreditProfileResponseMsg = null;
                }
                if (__ctx1__ != null && __ctx1__.__SFApplicantDebatchedJSONRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                    __ctx1__.__SFApplicantDebatchedJSONRequestMsg = null;
                }
                Tracker.FireEvent(__eventLocations[82],__eventData[15],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 75;
            case 75:
                if ( !PreProgressInc( __seg__, __ctx__, 76 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[83],__eventData[10],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 76;
            case 76:
                __condition__ = (__ctx1__.__RecordCount == __ctx1__.__LoopCount) && (__ctx1__.__Flag);
                if (!__condition__)
                {
                    if ( !PostProgressInc( __seg__, __ctx__, 78 ) )
                        return Microsoft.XLANGs.Core.StopConditions.Paused;
                    goto case 78;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 77 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 77;
            case 77:
                __ctx1__.__Flag = true;
                if ( !PostProgressInc( __seg__, __ctx__, 78 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 78;
            case 78:
                if ( !PreProgressInc( __seg__, __ctx__, 79 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[84],__eventData[15],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 79;
            case 79:
                if ( !PreProgressInc( __seg__, __ctx__, 80 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[85],__eventData[8],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 80;
            case 80:
                __ctx1__.__LoopCount = __ctx1__.__LoopCount + 1;
                if ( !PostProgressInc( __seg__, __ctx__, 81 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 81;
            case 81:
                if ( !PreProgressInc( __seg__, __ctx__, 82 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[86],__eventData[9],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 82;
            case 82:
                if ( !PreProgressInc( __seg__, __ctx__, 83 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[87],__eventData[16],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 83;
            case 83:
                if ( !PostProgressInc( __seg__, __ctx__, 15 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 15;
            case 84:
                if ( !PreProgressInc( __seg__, __ctx__, 85 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[87],__eventData[17],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 85;
            case 85:
                if ( !PostProgressInc( __seg__, __ctx__, 99 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 99;
            case 86:
                if ( !PreProgressInc( __seg__, __ctx__, 87 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[88],__eventData[8],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 87;
            case 87:
                __ctx1__.__Flag = false;
                if ( !PostProgressInc( __seg__, __ctx__, 88 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 88;
            case 88:
                if ( !PreProgressInc( __seg__, __ctx__, 89 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[89],__eventData[9],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 89;
            case 89:
                if ( !PreProgressInc( __seg__, __ctx__, 90 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[90],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 90;
            case 90:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.TransformInvalidURLException_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 91 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 91;
            case 91:
                if ( !PreProgressInc( __seg__, __ctx__, 92 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[91],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 92;
            case 92:
                if ( !PreProgressInc( __seg__, __ctx__, 93 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[92],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 93;
            case 93:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 94 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 94;
            case 94:
                if ( !PreProgressInc( __seg__, __ctx__, 95 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper7, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 95;
            case 95:
                if ( !PreProgressInc( __seg__, __ctx__, 96 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[93],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 96;
            case 96:
                if ( !PreProgressInc( __seg__, __ctx__, 97 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[94],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 97;
            case 97:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper7.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[6]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper7 != null)
                {
                    __ctx0__.__subWrapper7.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper7 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 98 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 98;
            case 98:
                if ( !PreProgressInc( __seg__, __ctx__, 99 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[95],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 99;
            case 99:
                if ( !PreProgressInc( __seg__, __ctx__, 100 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[96],__eventData[15],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 100;
            case 100:
                if (!__ctx4__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 101 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 101;
            case 101:
                if ( !PreProgressInc( __seg__, __ctx__, 102 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx4__.OnCommit();
                goto case 102;
            case 102:
                __seg__.SegmentDone();
                _segments[1].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment6(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[6];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[5];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            ____scope37_5 __ctx5__ = (____scope37_5)_stateMgrs[5];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx5__.__policy_49__ = default(Microsoft.RuleEngine.Policy);
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                __ctx5__.__policy_49__ = new Microsoft.RuleEngine.Policy("Set Experian Values");
                if ( !PostProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 2;
            case 2:
                __ctx5__.__policy_49__.Execute(__ctx1__.__GetExperianValues);
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 3;
            case 3:
                __ctx5__.__policy_49__.Dispose();
                if (__ctx5__ != null)
                    __ctx5__.__policy_49__ = null;
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if (!__ctx5__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx5__.OnCommit();
                goto case 6;
            case 6:
                __seg__.SegmentDone();
                _segments[5].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment7(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[7];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[6];
            ____scope38_6 __ctx6__ = (____scope38_6)_stateMgrs[6];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[62],__eventData[5],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.Transform_ExperianResponse_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__ExperianCreditProfileResponseMsg.MessagePart, __ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ctx1__.__SFApplicantDebatchedJSONRequestMsg.MessagePart, __ctx1__.__BTSMessageGUIDMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 3;
            case 3:
                if ( !PreProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    __edata.Messages.Add(__ctx1__.__SFApplicantDebatchedJSONRequestMsg);
                    __edata.Messages.Add(__ctx1__.__BTSMessageGUIDMsg);
                    Tracker.FireEvent(__eventLocations[63],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[64],__eventData[6],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                if (!__ctx6__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 6;
            case 6:
                if ( !PreProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper5, __ctx6__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 7;
            case 7:
                if ( !PreProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[65],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[66],__eventData[1],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 9;
            case 9:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper5.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[7]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper5 != null)
                {
                    __ctx0__.__subWrapper5.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper5 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[6], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 10;
            case 10:
                if ( !PreProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[67],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 11;
            case 11:
                if ( !PreProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[68],__eventData[5],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 12;
            case 12:
                {
                    SFCreditBureauLineItemInsertRequestType __SFCreditBureauLineItemInsertRequestMsg = new SFCreditBureauLineItemInsertRequestType("SFCreditBureauLineItemInsertRequestMsg", __ctx1__);

                    ApplyTransform(typeof(CreditProfileLogic.TransformExperianResponse_To_SFCreditBureauLineItems), new object[] {__SFCreditBureauLineItemInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__SFCreditBureauInsertResponseMsg.MessagePart, __ctx1__.__ExperianCreditProfileResponseMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauLineItemInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                    __ctx1__.__SFCreditBureauLineItemInsertRequestMsg = __SFCreditBureauLineItemInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                }
                __ctx1__.__SFCreditBureauLineItemInsertRequestMsg.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 13;
            case 13:
                if ( !PreProgressInc( __seg__, __ctx__, 14 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.Messages.Add(__ctx1__.__ExperianCreditProfileResponseMsg);
                    Tracker.FireEvent(__eventLocations[69],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 14;
            case 14:
                if ( !PreProgressInc( __seg__, __ctx__, 15 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[70],__eventData[6],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 15;
            case 15:
                if (!__ctx6__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 16 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 16;
            case 16:
                if ( !PreProgressInc( __seg__, __ctx__, 17 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauLineItemInsertRequestResponsePort.SendMessage(0, __ctx1__.__SFCreditBureauLineItemInsertRequestMsg, null, null, out __ctx0__.__subWrapper6, __ctx6__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 17;
            case 17:
                if ( !PreProgressInc( __seg__, __ctx__, 18 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauLineItemInsertRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[71],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauLineItemInsertRequestMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertRequestMsg);
                    __ctx1__.__SFCreditBureauLineItemInsertRequestMsg = null;
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 18;
            case 18:
                if ( !PreProgressInc( __seg__, __ctx__, 19 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[72],__eventData[1],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 19;
            case 19:
                if (!SFCreditBureauLineItemInsertRequestResponsePort.GetMessageId(__ctx0__.__subWrapper6.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[8]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper6 != null)
                {
                    __ctx0__.__subWrapper6.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper6 = null;
                }
                if (__ctx1__.__SFCreditBureauLineItemInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertResponseMsg);
                __ctx1__.__SFCreditBureauLineItemInsertResponseMsg = new SFCreditBureauLineItemInsertResponseType("SFCreditBureauLineItemInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauLineItemInsertResponseMsg);
                SFCreditBureauLineItemInsertRequestResponsePort.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauLineItemInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[6], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 20 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 20;
            case 20:
                if ( !PreProgressInc( __seg__, __ctx__, 21 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauLineItemInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauLineItemInsertRequestResponsePort";
                    Tracker.FireEvent(__eventLocations[73],__edata,_stateMgrs[6].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__SFCreditBureauLineItemInsertResponseMsg != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauLineItemInsertResponseMsg);
                    __ctx1__.__SFCreditBureauLineItemInsertResponseMsg = null;
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 21;
            case 21:
                if (!__ctx6__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 22 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 22;
            case 22:
                if ( !PreProgressInc( __seg__, __ctx__, 23 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx6__.OnCommit();
                goto case 23;
            case 23:
                __seg__.SegmentDone();
                _segments[5].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment8(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[8];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[6];
            ____scope38_6 __ctx6__ = (____scope38_6)_stateMgrs[6];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];

            switch (__seg__.Progress)
            {
            case 0:
                OnBeginCatchHandler(6);
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[74],__eventData[4],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[75],__eventData[8],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                System.Diagnostics.EventLog.WriteEntry("SF Insertion Exception", __ctx6__.__Ex_0.Message);
                if (__ctx6__ != null)
                    __ctx6__.__Ex_0 = null;
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[76],__eventData[9],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                __ctx1__.__Flag = false;
                if ( !PostProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 6;
            case 6:
                if ( !PreProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[79],__eventData[7],_stateMgrs[6].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 7;
            case 7:
                __ctx6__.__exv__ = null;
                OnEndCatchHandler(6, __seg__);
                __seg__.SegmentDone();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment9(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[9];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[4];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                OnBeginCatchHandler(4);
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[97],__eventData[4],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[98],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);
                    ExceptionInformationType __ExceptionInformationMsg = new ExceptionInformationType("ExceptionInformationMsg", __ctx1__);
                    __messagetype_System_Xml_XmlDocument __TempExceptionInformation = new __messagetype_System_Xml_XmlDocument("TempExceptionInformation", __ctx1__);

                    System.Diagnostics.EventLog.WriteEntry("Experian Exception", __ctx4__.__CREx_0.Message, System.Diagnostics.EventLogEntryType.Error);
                    __ctx1__.__Flag = false;
                    __ctx1__.__ExperianStatus = "Fail";
                    __TempExceptionInformation.part.LoadFrom(new System.Xml.XmlDocument());
                    __ctx1__.__sTempExceptionInformation = __ctx4__.__CREx_0.Message;
                    if (__ctx4__ != null)
                        __ctx4__.__CREx_0 = null;
                    __TempExceptionInformation.part.TypedValue.LoadXml(System.String.Format("<ns0:ExceptionInfo xmlns:ns0='http://SBACBRCreditProfile.ExceptionInformation'><Message><![CDATA[{0}]]></Message><OrchestrationId>{1}</OrchestrationId></ns0:ExceptionInfo>", __ctx1__.__sTempExceptionInformation, System.Convert.ToString(Microsoft.XLANGs.Core.Service.RootService.InstanceId)));
                    __ExceptionInformationMsg.MessagePart.CopyFrom(__TempExceptionInformation.part);
                    ApplyTransform(typeof(CreditProfileLogic.TransformException_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ExceptionInformationMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    if (__ctx1__.__ExceptionInformationMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__ExceptionInformationMsg);
                    __ctx1__.__ExceptionInformationMsg = __ExceptionInformationMsg;
                    __ctx1__.RefMessage(__ctx1__.__ExceptionInformationMsg);
                    if (__ctx1__.__TempExceptionInformation != null)
                        __ctx1__.UnrefMessage(__ctx1__.__TempExceptionInformation);
                    __ctx1__.__TempExceptionInformation = __TempExceptionInformation;
                    __ctx1__.RefMessage(__ctx1__.__TempExceptionInformation);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                __ctx1__.__ExceptionInformationMsg.ConstructionCompleteEvent(true);
                __ctx1__.__TempExceptionInformation.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExceptionInformationMsg);
                    __edata.Messages.Add(__ctx1__.__TempExceptionInformation);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[99],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[100],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 6;
            case 6:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 7;
            case 7:
                if ( !PreProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper8, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[101],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 9;
            case 9:
                if ( !PreProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[102],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 10;
            case 10:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper8.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[9]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper8 != null)
                {
                    __ctx0__.__subWrapper8.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper8 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 11;
            case 11:
                if ( !PreProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[103],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 12;
            case 12:
                if ( !PreProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[104],__eventData[7],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 13;
            case 13:
                __ctx4__.__exv__ = null;
                OnEndCatchHandler(4, __seg__);
                __seg__.SegmentDone();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment10(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[10];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[4];
            ____scope36_4 __ctx4__ = (____scope36_4)_stateMgrs[4];
            __CBRProcessingLogic_1 __ctx1__ = (__CBRProcessingLogic_1)_stateMgrs[1];
            __CBRProcessingLogic_root_0 __ctx0__ = (__CBRProcessingLogic_root_0)_stateMgrs[0];

            switch (__seg__.Progress)
            {
            case 0:
                OnBeginCatchHandler(4);
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                if ( !PreProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[105],__eventData[4],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[106],__eventData[5],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                {
                    SFCreditBureauInsertRequestType __SFCreditBureauInsertRequestMsg = new SFCreditBureauInsertRequestType("SFCreditBureauInsertRequestMsg", __ctx1__);
                    ExceptionInformationType __ExceptionInformationMsg = new ExceptionInformationType("ExceptionInformationMsg", __ctx1__);
                    __messagetype_System_Xml_XmlDocument __TempExceptionInformation = new __messagetype_System_Xml_XmlDocument("TempExceptionInformation", __ctx1__);

                    System.Diagnostics.EventLog.WriteEntry("Delivery Experian Exception", __ctx4__.__OSEx_1.Message, System.Diagnostics.EventLogEntryType.Error);
                    __ctx1__.__Flag = false;
                    __ctx1__.__ExperianStatus = "Fail";
                    __TempExceptionInformation.part.LoadFrom(new System.Xml.XmlDocument());
                    __ctx1__.__sTempExceptionInformation = __ctx4__.__OSEx_1.Message;
                    if (__ctx4__ != null)
                        __ctx4__.__OSEx_1 = null;
                    __TempExceptionInformation.part.TypedValue.LoadXml(System.String.Format("<ns0:ExceptionInfo xmlns:ns0='http://SBACBRCreditProfile.ExceptionInformation'><Message><![CDATA[{0}]]></Message><OrchestrationId>{1}</OrchestrationId></ns0:ExceptionInfo>", __ctx1__.__sTempExceptionInformation, System.Convert.ToString(Microsoft.XLANGs.Core.Service.RootService.InstanceId)));
                    __ExceptionInformationMsg.MessagePart.CopyFrom(__TempExceptionInformation.part);
                    ApplyTransform(typeof(CreditProfileLogic.TransformException_To_SFCreditBureau), new object[] {__SFCreditBureauInsertRequestMsg.MessagePart}, new object[] {__ctx1__.__SFApplicationJSONRequestMsg.MessagePart, __ExceptionInformationMsg.MessagePart});

                    if (__ctx1__.__SFCreditBureauInsertRequestMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __ctx1__.__SFCreditBureauInsertRequestMsg = __SFCreditBureauInsertRequestMsg;
                    __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    if (__ctx1__.__ExceptionInformationMsg != null)
                        __ctx1__.UnrefMessage(__ctx1__.__ExceptionInformationMsg);
                    __ctx1__.__ExceptionInformationMsg = __ExceptionInformationMsg;
                    __ctx1__.RefMessage(__ctx1__.__ExceptionInformationMsg);
                    if (__ctx1__.__TempExceptionInformation != null)
                        __ctx1__.UnrefMessage(__ctx1__.__TempExceptionInformation);
                    __ctx1__.__TempExceptionInformation = __TempExceptionInformation;
                    __ctx1__.RefMessage(__ctx1__.__TempExceptionInformation);
                }
                __ctx1__.__SFCreditBureauInsertRequestMsg.ConstructionCompleteEvent(true);
                __ctx1__.__ExceptionInformationMsg.ConstructionCompleteEvent(true);
                __ctx1__.__TempExceptionInformation.ConstructionCompleteEvent(true);
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                if ( !PreProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.Messages.Add(__ctx1__.__ExceptionInformationMsg);
                    __edata.Messages.Add(__ctx1__.__TempExceptionInformation);
                    __edata.Messages.Add(__ctx1__.__SFApplicationJSONRequestMsg);
                    Tracker.FireEvent(__eventLocations[107],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[108],__eventData[6],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 6;
            case 6:
                if (!__ctx4__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 7;
            case 7:
                if ( !PreProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                SFCreditBureauInsertRequestResponse.SendMessage(0, __ctx1__.__SFCreditBureauInsertRequestMsg, null, null, out __ctx0__.__subWrapper9, __ctx4__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertRequestMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[109],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 9;
            case 9:
                if ( !PreProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[110],__eventData[1],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 10;
            case 10:
                if (!SFCreditBureauInsertRequestResponse.GetMessageId(__ctx0__.__subWrapper9.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[10]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper9 != null)
                {
                    __ctx0__.__subWrapper9.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper9 = null;
                }
                if (__ctx1__.__SFCreditBureauInsertResponseMsg != null)
                    __ctx1__.UnrefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                __ctx1__.__SFCreditBureauInsertResponseMsg = new SFCreditBureauInsertResponseType("SFCreditBureauInsertResponseMsg", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__SFCreditBureauInsertResponseMsg);
                SFCreditBureauInsertRequestResponse.ReceiveMessage(0, __msgEnv__, __ctx1__.__SFCreditBureauInsertResponseMsg, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[4], __seg__);
                if ( !PostProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 11;
            case 11:
                if ( !PreProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__SFCreditBureauInsertResponseMsg);
                    __edata.PortName = @"SFCreditBureauInsertRequestResponse";
                    Tracker.FireEvent(__eventLocations[111],__edata,_stateMgrs[4].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 12;
            case 12:
                if ( !PreProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[112],__eventData[7],_stateMgrs[4].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 13;
            case 13:
                __ctx4__.__exv__ = null;
                OnEndCatchHandler(4, __seg__);
                __seg__.SegmentDone();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }
        private static Microsoft.XLANGs.Core.CachedObject[] _locations = new Microsoft.XLANGs.Core.CachedObject[] {
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{3887D74D-5A20-46AF-96CF-2AC5AB398E36}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{B32BF65D-F8C9-4238-B68C-6E3F5B01C9A0}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{DEECEC77-340D-475F-A43F-038B261344A0}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{7B232380-6AD5-4656-9786-E8C1FCA23439}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{7D1C4C0B-0A47-406B-96EA-9980332EA653}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{4306A181-6128-4C61-B63E-EC1D7C15AEB5}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{2DD175C5-9261-4741-B611-DC56B7616AA6}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{66E0FA05-8B0C-4938-80CC-4DD2B990A04B}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{3EC23E1D-166C-497B-9EC1-E2BF9C14E325}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{3C55ABA8-359E-4534-87A1-2514550B9F43}")),
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{FD34AD47-0D77-4031-8D6C-EF75DEA84305}"))
        };

    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.ePublic,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eThirdKind,
        "System.Xml.XmlDocument",
        new System.Type[]{
            typeof(Microsoft.XLANGs.BaseTypes.Any)
        },
        new string[]{
            "part"
        },
        new System.Type[]{
            typeof(__Microsoft_XLANGs_BaseTypes_Any__)
        },
        0,
        Microsoft.XLANGs.Core.XMessage.AnyMessageTypeName
    )]
    [System.SerializableAttribute]
    sealed public class __messagetype_System_Xml_XmlDocument : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __Microsoft_XLANGs_BaseTypes_Any__ part;

        private void __CreatePartWrappers()
        {
            part = new __Microsoft_XLANGs_BaseTypes_Any__(this, "part", 0);
            this.AddPart("part", 0, part);
        }

        public __messagetype_System_Xml_XmlDocument(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [System.SerializableAttribute]
    sealed public class __ExperianCreditProfileDefinition_GetExperianValues__ : Microsoft.XLANGs.Core.XSDPart
    {
        private static ExperianCreditProfileDefinition.GetExperianValues _schema = new ExperianCreditProfileDefinition.GetExperianValues();

        public __ExperianCreditProfileDefinition_GetExperianValues__(Microsoft.XLANGs.Core.XMessage msg, string name, int index) : base(msg, name, index) { }

        
        #region part reflection support
        public static Microsoft.XLANGs.BaseTypes.SchemaBase PartSchema { get { return (Microsoft.XLANGs.BaseTypes.SchemaBase)_schema; } }
        #endregion // part reflection support
    }

    [Microsoft.XLANGs.BaseTypes.MessageTypeAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.ePublic,
        Microsoft.XLANGs.BaseTypes.EXLangSMessageInfo.eThirdKind,
        "ExperianCreditProfileDefinition.GetExperianValues",
        new System.Type[]{
            typeof(ExperianCreditProfileDefinition.GetExperianValues)
        },
        new string[]{
            "part"
        },
        new System.Type[]{
            typeof(__ExperianCreditProfileDefinition_GetExperianValues__)
        },
        0,
        @"http://ExperianValues.GetExperianValues#Values"
    )]
    [System.SerializableAttribute]
    sealed public class __messagetype_ExperianCreditProfileDefinition_GetExperianValues : Microsoft.BizTalk.XLANGs.BTXEngine.BTXMessage
    {
        public __ExperianCreditProfileDefinition_GetExperianValues__ part;

        private void __CreatePartWrappers()
        {
            part = new __ExperianCreditProfileDefinition_GetExperianValues__(this, "part", 0);
            this.AddPart("part", 0, part);
        }

        public __messagetype_ExperianCreditProfileDefinition_GetExperianValues(string msgName, Microsoft.XLANGs.Core.Context ctx) : base(msgName, ctx)
        {
            __CreatePartWrappers();
        }
    }

    [Microsoft.XLANGs.BaseTypes.BPELExportableAttribute(false)]
    sealed public class _MODULE_PROXY_ { }
}
