
#pragma warning disable 162

namespace SBACBRCreditProfile
{

    [Microsoft.XLANGs.BaseTypes.PortTypeOperationAttribute(
        "Operation",
        new System.Type[]{
            typeof(CreditProfileLogic.ExperianCreditProfileRequestType), 
            typeof(CreditProfileLogic.ExperianCreditProfileResponseType)
        },
        new string[]{
        }
    )]
    [Microsoft.XLANGs.BaseTypes.PortTypeAttribute(Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal, "")]
    [System.SerializableAttribute]
    sealed internal class ExperianPortTyp : Microsoft.BizTalk.XLANGs.BTXEngine.BTXPortBase
    {
        public ExperianPortTyp(int portInfo, Microsoft.XLANGs.Core.IServiceProxy s)
            : base(portInfo, s)
        { }
        public ExperianPortTyp(ExperianPortTyp p)
            : base(p)
        { }

        public override Microsoft.XLANGs.Core.PortBase Clone()
        {
            ExperianPortTyp p = new ExperianPortTyp(this);
            return p;
        }

        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        #region port reflection support
        static public Microsoft.XLANGs.Core.OperationInfo Operation = new Microsoft.XLANGs.Core.OperationInfo
        (
            "Operation",
            System.Web.Services.Description.OperationFlow.RequestResponse,
            typeof(ExperianPortTyp),
            typeof(CreditProfileLogic.ExperianCreditProfileRequestType),
            typeof(CreditProfileLogic.ExperianCreditProfileResponseType),
            new System.Type[]{},
            new string[]{}
        );
        static public System.Collections.Hashtable OperationsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[ "Operation" ] = Operation;
                return h;
            }
        }
        #endregion // port reflection support
    }
    //#line 152 "C:\Users\ashok.pudota\Documents\visual studio 2015\Projects\SBACBRCreditProfile\CreditProfileLogic\SendToExperian.odx"
    [Microsoft.XLANGs.BaseTypes.ServicePortsAttribute(
        new Microsoft.XLANGs.BaseTypes.EXLangSParameter[] {
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.ePort|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eDynamic
        },
        new System.Type[] {
            typeof(SBACBRCreditProfile.ExperianPortTyp)
        },
        new System.String[] {
            "ExperianPort"
        },
        new System.Type[] {
            null
        }
    )]
    [Microsoft.XLANGs.BaseTypes.ServiceCallTreeAttribute(
        new System.Type[] {
        },
        new System.Type[] {
        },
        new System.Type[] {
        }
    )]
    [Microsoft.XLANGs.BaseTypes.ServiceAttribute(
        Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal,
        Microsoft.XLANGs.BaseTypes.EXLangSServiceInfo.eCallable
    )]
    [System.SerializableAttribute]
    [Microsoft.XLANGs.BaseTypes.BPELExportableAttribute(false)]
    sealed internal class SendToExperian : Microsoft.BizTalk.XLANGs.BTXEngine.BTXService
    {
        public static readonly Microsoft.XLANGs.BaseTypes.EXLangSAccess __access = Microsoft.XLANGs.BaseTypes.EXLangSAccess.eInternal;
        public static readonly bool __execable = false;
        [Microsoft.XLANGs.BaseTypes.CallCompensationAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSCallCompensationInfo.eHasRequestResponse
,
            new System.String[] {
            },
            new System.String[] {
            }
        )]
        public static void __bodyProxy(
            [ Microsoft.XLANGs.BaseTypes.ServiceParameterAttribute(Microsoft.XLANGs.BaseTypes.EXLangSParameter.eMessage, "") ] out CreditProfileLogic.ExperianCreditProfileResponseType ActualCBRResponse,
            [ Microsoft.XLANGs.BaseTypes.ServiceParameterAttribute(Microsoft.XLANGs.BaseTypes.EXLangSParameter.eMessage, "") ] CreditProfileLogic.ExperianCreditProfileRequestType OriginalCBRRequest,
            [ Microsoft.XLANGs.BaseTypes.ServiceParameterAttribute(Microsoft.XLANGs.BaseTypes.EXLangSParameter.eVariable, "") ] Experian.Values ExperianValues,
            [ Microsoft.XLANGs.BaseTypes.ServiceParameterAttribute(Microsoft.XLANGs.BaseTypes.EXLangSParameter.eVariable, "") ] System.String Address)
        {
            ActualCBRResponse = default(CreditProfileLogic.ExperianCreditProfileResponseType);
        }
        private static System.Guid _serviceId = Microsoft.XLANGs.Core.HashHelper.HashServiceType(typeof(SendToExperian));
        private static volatile System.Guid[] _activationSubIds;

        private static new object _lockIdentity = new object();

        public static System.Guid UUID { get { return _serviceId; } }
        public override System.Guid ServiceId { get { return UUID; } }

        protected override System.Guid[] ActivationSubGuids
        {
            get { return _activationSubIds; }
            set { _activationSubIds = value; }
        }

        protected override object StaleStateLock
        {
            get { return _lockIdentity; }
        }

        protected override bool HasActivation { get { return false; } }

        internal bool IsExeced = false;

        static SendToExperian()
        {
            Microsoft.BizTalk.XLANGs.BTXEngine.BTXService.CacheStaticState( _serviceId );
        }

        private void ConstructorHelper()
        {
            _segments = new Microsoft.XLANGs.Core.Segment[] {
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment0), 0, 0, 0),
                new Microsoft.XLANGs.Core.Segment( new Microsoft.XLANGs.Core.Segment.SegmentCode(this.segment1), 1, 1, 1)
            };

            _Locks = 1;
            _rootContext = new __SendToExperian_root_0(this);
            _stateMgrs = new Microsoft.XLANGs.Core.IStateManager[2];
            _stateMgrs[0] = _rootContext;
            FinalConstruct();
        }

        public SendToExperian(System.Guid instanceId, Microsoft.BizTalk.XLANGs.BTXEngine.BTXSession session, Microsoft.BizTalk.XLANGs.BTXEngine.BTXEvents tracker)
            : base(instanceId, session, "SendToExperian", tracker)
        {
            ConstructorHelper();
        }

        public SendToExperian(int callIndex, System.Guid instanceId, Microsoft.BizTalk.XLANGs.BTXEngine.BTXService parent)
            : base(callIndex, instanceId, parent, "SendToExperian")
        {
            ConstructorHelper();
        }

        private const string _symInfo = @"
<XsymFile>
<ProcessFlow xmlns:om='http://schemas.microsoft.com/BizTalk/2003/DesignerData'>      <shapeType>RootShape</shapeType>      <ShapeID>7e974251-c91f-45e3-909c-b00f60b054b6</ShapeID>      
<children>                          
<ShapeInfo>      <shapeType>MessageDeclarationShape</shapeType>      <ShapeID>9f263f9a-8c76-44ca-8203-2b79da0fd38b</ShapeID>      <ParentLink>ServiceBody_Declaration</ParentLink>                <shapeText>ActualCBRResponse</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageDeclarationShape</shapeType>      <ShapeID>bea79f7d-2ae3-4926-aadf-641b933b7a6d</ShapeID>      <ParentLink>ServiceBody_Declaration</ParentLink>                <shapeText>OriginalCBRRequest</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>VariableDeclarationShape</shapeType>      <ShapeID>1a501958-5f0e-4240-b476-693d98eee929</ShapeID>      <ParentLink>ServiceBody_Declaration</ParentLink>                <shapeText>ExperianValues</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>VariableDeclarationShape</shapeType>      <ShapeID>acb83750-6233-4275-8c62-767440b03585</ShapeID>      <ParentLink>ServiceBody_Declaration</ParentLink>                <shapeText>Address</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ConstructShape</shapeType>      <ShapeID>f1afa377-3bda-4dfd-9eff-669deab5d4ce</ShapeID>      <ParentLink>ServiceBody_Statement</ParentLink>                <shapeText>Construct Actual CBR Request</shapeText>                  
<children>                          
<ShapeInfo>      <shapeType>MessageAssignmentShape</shapeType>      <ShapeID>1c64456b-1f49-411f-8ee9-3936a5603d74</ShapeID>      <ParentLink>ComplexStatement_Statement</ParentLink>                <shapeText>Make CBR Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>MessageRefShape</shapeType>      <ShapeID>48b7b99e-d37d-419b-b0ea-b68371ea96ef</ShapeID>      <ParentLink>Construct_MessageRef</ParentLink>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>SendShape</shapeType>      <ShapeID>c7c4c9fe-2f12-410c-a417-cfc1666cc0fa</ShapeID>      <ParentLink>ServiceBody_Statement</ParentLink>                <shapeText>Send Experian Request</shapeText>                  
<children>                </children>
  </ShapeInfo>
                            
<ShapeInfo>      <shapeType>ReceiveShape</shapeType>      <ShapeID>d6a04938-9715-4afe-b21e-b44c2ddc0ff3</ShapeID>      <ParentLink>ServiceBody_Statement</ParentLink>                <shapeText>Receive Experian Response</shapeText>                  
<children>                </children>
  </ShapeInfo>
                  </children>
  </ProcessFlow><Metadata>

<TrkMetadata>
<ActionName>'SendToExperian'</ActionName><IsAtomic>0</IsAtomic><Line>152</Line><Position>14</Position><ShapeID>'e211a116-cb8b-44e7-a052-0de295aa0001'</ShapeID>
<Messages>
	<MsgInfo><name>ActualCBRResponse</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>Out</direction></MsgInfo>
	<MsgInfo><name>OriginalCBRRequest</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectRequest</schema><direction>In</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>161</Line><Position>13</Position><ShapeID>'f1afa377-3bda-4dfd-9eff-669deab5d4ce'</ShapeID>
<Messages>
	<MsgInfo><name>ActualCBRRequest</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>177</Line><Position>13</Position><ShapeID>'c7c4c9fe-2f12-410c-a417-cfc1666cc0fa'</ShapeID>
<Messages>
	<MsgInfo><name>ActualCBRRequest</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectRequest</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>

<TrkMetadata>
<Line>179</Line><Position>13</Position><ShapeID>'d6a04938-9715-4afe-b21e-b44c2ddc0ff3'</ShapeID>
<Messages>
	<MsgInfo><name>ActualCBRResponse</name><part>MessagePart</part><schema>ExperianCreditProfileDefinition.NetConnectResponse</schema><direction>Out</direction></MsgInfo>
</Messages>
</TrkMetadata>
</Metadata>
</XsymFile>";

        public override string odXml { get { return _symODXML; } }

        private const string _symODXML = @"
<?xml version='1.0' encoding='utf-8' standalone='yes'?>
<om:MetaModel MajorVersion='1' MinorVersion='3' Core='2b131234-7959-458d-834f-2dc0769ce683' ScheduleModel='66366196-361d-448d-976f-cab5e87496d2' xmlns:om='http://schemas.microsoft.com/BizTalk/2003/DesignerData'>
    <om:Element Type='Module' OID='92782867-2d0d-4458-9644-2d65cdb77e1b' LowerBound='1.1' HigherBound='43.1'>
        <om:Property Name='ReportToAnalyst' Value='True' />
        <om:Property Name='Name' Value='SBACBRCreditProfile' />
        <om:Property Name='Signal' Value='False' />
        <om:Element Type='ServiceDeclaration' OID='0290af2b-691f-4da6-ba51-a8c6abc15a77' ParentLink='Module_ServiceDeclaration' LowerBound='11.1' HigherBound='42.1'>
            <om:Property Name='InitializedTransactionType' Value='False' />
            <om:Property Name='IsInvokable' Value='True' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='SendToExperian' />
            <om:Property Name='Signal' Value='True' />
            <om:Element Type='MessageDeclaration' OID='ef610de6-48e3-4c98-943e-8e4b2256e74c' ParentLink='ServiceDeclaration_MessageDeclaration' LowerBound='17.1' HigherBound='18.1'>
                <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileRequestType' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ActualCBRRequest' />
                <om:Property Name='Signal' Value='True' />
            </om:Element>
            <om:Element Type='ServiceBody' OID='7e974251-c91f-45e3-909c-b00f60b054b6' ParentLink='ServiceDeclaration_ServiceBody'>
                <om:Property Name='Signal' Value='False' />
                <om:Element Type='MessageDeclaration' OID='9f263f9a-8c76-44ca-8203-2b79da0fd38b' ParentLink='ServiceBody_Declaration' LowerBound='18.15' HigherBound='18.97'>
                    <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileResponseType' />
                    <om:Property Name='ParamDirection' Value='Out' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='ActualCBRResponse' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='MessageDeclaration' OID='bea79f7d-2ae3-4926-aadf-641b933b7a6d' ParentLink='ServiceBody_Declaration' LowerBound='18.99' HigherBound='18.177'>
                    <om:Property Name='Type' Value='CreditProfileLogic.ExperianCreditProfileRequestType' />
                    <om:Property Name='ParamDirection' Value='In' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='OriginalCBRRequest' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='VariableDeclaration' OID='1a501958-5f0e-4240-b476-693d98eee929' ParentLink='ServiceBody_Declaration' LowerBound='18.179' HigherBound='18.209'>
                    <om:Property Name='UseDefaultConstructor' Value='True' />
                    <om:Property Name='Type' Value='Experian.Values' />
                    <om:Property Name='ParamDirection' Value='In' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='ExperianValues' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='VariableDeclaration' OID='acb83750-6233-4275-8c62-767440b03585' ParentLink='ServiceBody_Declaration' LowerBound='18.211' HigherBound='18.232'>
                    <om:Property Name='UseDefaultConstructor' Value='False' />
                    <om:Property Name='Type' Value='System.String' />
                    <om:Property Name='ParamDirection' Value='In' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Address' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='Construct' OID='f1afa377-3bda-4dfd-9eff-669deab5d4ce' ParentLink='ServiceBody_Statement' LowerBound='20.1' HigherBound='36.1'>
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Construct Actual CBR Request' />
                    <om:Property Name='Signal' Value='True' />
                    <om:Element Type='MessageAssignment' OID='1c64456b-1f49-411f-8ee9-3936a5603d74' ParentLink='ComplexStatement_Statement' LowerBound='23.1' HigherBound='35.1'>
                        <om:Property Name='Expression' Value='ActualCBRRequest.MessagePart = OriginalCBRRequest.MessagePart; &#xD;&#xA;&#xD;&#xA;ActualCBRRequest(HTTP.Username) = ExperianValues.Username;&#xD;&#xA;ActualCBRRequest(HTTP.Password) = ExperianValues.Password;&#xD;&#xA;ActualCBRRequest(HTTP.AuthenticationScheme) = ExperianValues.TransportClientCredentialType;&#xD;&#xA;ActualCBRRequest(HTTP.EnableChunkedEncoding) = true; &#xD;&#xA;ActualCBRRequest(HTTP.ContentType) = ExperianValues.HttpHeaders;&#xD;&#xA;&#xD;&#xA;ExperianPort(Microsoft.XLANGs.BaseTypes.Address) = Address; &#xD;&#xA;ExperianPort(Microsoft.XLANGs.BaseTypes.TransportType) = &quot;HTTP&quot;;&#xA;&#xA;' />
                        <om:Property Name='ReportToAnalyst' Value='False' />
                        <om:Property Name='Name' Value='Make CBR Request' />
                        <om:Property Name='Signal' Value='True' />
                    </om:Element>
                    <om:Element Type='MessageRef' OID='48b7b99e-d37d-419b-b0ea-b68371ea96ef' ParentLink='Construct_MessageRef' LowerBound='21.23' HigherBound='21.39'>
                        <om:Property Name='Ref' Value='ActualCBRRequest' />
                        <om:Property Name='ReportToAnalyst' Value='True' />
                        <om:Property Name='Signal' Value='False' />
                    </om:Element>
                </om:Element>
                <om:Element Type='Send' OID='c7c4c9fe-2f12-410c-a417-cfc1666cc0fa' ParentLink='ServiceBody_Statement' LowerBound='36.1' HigherBound='38.1'>
                    <om:Property Name='PortName' Value='ExperianPort' />
                    <om:Property Name='MessageName' Value='ActualCBRRequest' />
                    <om:Property Name='OperationName' Value='Operation' />
                    <om:Property Name='OperationMessageName' Value='Request' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Send Experian Request' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
                <om:Element Type='Receive' OID='d6a04938-9715-4afe-b21e-b44c2ddc0ff3' ParentLink='ServiceBody_Statement' LowerBound='38.1' HigherBound='40.1'>
                    <om:Property Name='Activate' Value='False' />
                    <om:Property Name='PortName' Value='ExperianPort' />
                    <om:Property Name='MessageName' Value='ActualCBRResponse' />
                    <om:Property Name='OperationName' Value='Operation' />
                    <om:Property Name='OperationMessageName' Value='Response' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Receive Experian Response' />
                    <om:Property Name='Signal' Value='True' />
                </om:Element>
            </om:Element>
            <om:Element Type='PortDeclaration' OID='3c96a272-b41e-4d9c-9a09-7726cf11418d' ParentLink='ServiceDeclaration_PortDeclaration' LowerBound='14.1' HigherBound='17.1'>
                <om:Property Name='PortModifier' Value='Uses' />
                <om:Property Name='Orientation' Value='Left' />
                <om:Property Name='PortIndex' Value='-1' />
                <om:Property Name='IsWebPort' Value='False' />
                <om:Property Name='OrderedDelivery' Value='False' />
                <om:Property Name='DeliveryNotification' Value='Transmitted' />
                <om:Property Name='Type' Value='SBACBRCreditProfile.ExperianPortTyp' />
                <om:Property Name='ParamDirection' Value='In' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='ExperianPort' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='PhysicalBindingAttribute' OID='1a1d3638-5d11-489d-b911-cc38e68b8987' ParentLink='PortDeclaration_CLRAttribute' LowerBound='14.1' HigherBound='15.1'>
                    <om:Property Name='InPipeline' Value='Microsoft.BizTalk.DefaultPipelines.XMLReceive' />
                    <om:Property Name='OutPipeline' Value='Microsoft.BizTalk.DefaultPipelines.XMLTransmit' />
                    <om:Property Name='TransportType' Value='HTTP' />
                    <om:Property Name='URI' Value='http://tempURI' />
                    <om:Property Name='IsDynamic' Value='True' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
        <om:Element Type='PortType' OID='3e87987d-a123-4b49-8b9a-1e8bee56c2db' ParentLink='Module_PortType' LowerBound='4.1' HigherBound='11.1'>
            <om:Property Name='Synchronous' Value='True' />
            <om:Property Name='TypeModifier' Value='Internal' />
            <om:Property Name='ReportToAnalyst' Value='True' />
            <om:Property Name='Name' Value='ExperianPortTyp' />
            <om:Property Name='Signal' Value='False' />
            <om:Element Type='OperationDeclaration' OID='74b588bf-8dfc-45ae-b311-745002db815e' ParentLink='PortType_OperationDeclaration' LowerBound='6.1' HigherBound='10.1'>
                <om:Property Name='OperationType' Value='RequestResponse' />
                <om:Property Name='ReportToAnalyst' Value='True' />
                <om:Property Name='Name' Value='Operation' />
                <om:Property Name='Signal' Value='True' />
                <om:Element Type='MessageRef' OID='22d235eb-c2d4-41cb-a95a-183bd859d4c5' ParentLink='OperationDeclaration_RequestMessageRef' LowerBound='8.13' HigherBound='8.64'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.ExperianCreditProfileRequestType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Request' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
                <om:Element Type='MessageRef' OID='cd645731-5d54-4410-bc98-ad613e7728ca' ParentLink='OperationDeclaration_ResponseMessageRef' LowerBound='8.66' HigherBound='8.118'>
                    <om:Property Name='Ref' Value='CreditProfileLogic.ExperianCreditProfileResponseType' />
                    <om:Property Name='ReportToAnalyst' Value='True' />
                    <om:Property Name='Name' Value='Response' />
                    <om:Property Name='Signal' Value='False' />
                </om:Element>
            </om:Element>
        </om:Element>
    </om:Element>
</om:MetaModel>
";

        [System.SerializableAttribute]
        public class __SendToExperian_root_0 : Microsoft.XLANGs.Core.ServiceContext
        {
            public __SendToExperian_root_0(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "SendToExperian")
            {
            }

            public override int Index { get { return 0; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[0]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[0]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                SendToExperian __svc__ = (SendToExperian)_service;
                base.Finally();
            }

            internal Microsoft.XLANGs.Core.SubscriptionWrapper __subWrapper0;
        }


        [System.SerializableAttribute]
        public class __SendToExperian_1 : Microsoft.XLANGs.Core.ExceptionHandlingContext
        {
            public __SendToExperian_1(Microsoft.XLANGs.Core.Service svc)
                : base(svc, "SendToExperian")
            {
            }

            public override int Index { get { return 1; } }

            public override bool CombineParentCommit { get { return true; } }

            public override Microsoft.XLANGs.Core.Segment InitialSegment
            {
                get { return _service._segments[1]; }
            }
            public override Microsoft.XLANGs.Core.Segment FinalSegment
            {
                get { return _service._segments[1]; }
            }

            public override int CompensationSegment { get { return -1; } }
            public override bool OnError()
            {
                Finally();
                return false;
            }

            public override void Finally()
            {
                SendToExperian __svc__ = (SendToExperian)_service;
                __SendToExperian_root_0 __ctx0__ = (__SendToExperian_root_0)(__svc__._stateMgrs[0]);
                __SendToExperian_1 __ctx1__ = (__SendToExperian_1)(__svc__._stateMgrs[1]);

                if (__ctx1__ != null)
                    __ctx1__.__Address = null;
                if (__ctx1__ != null && __ctx1__.__ActualCBRRequest != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ActualCBRRequest);
                    __ctx1__.__ActualCBRRequest = null;
                }
                if (__ctx1__ != null && __ctx1__.__OriginalCBRRequest != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__OriginalCBRRequest);
                    __ctx1__.__OriginalCBRRequest = null;
                }
                if (__ctx1__ != null)
                    __ctx1__.__ExperianValues = null;
                if (__svc__.ExperianPort != null)
                {
                    __svc__.ExperianPort.Close(this, null);
                    __svc__.ExperianPort = null;
                }
                if (__ctx0__ != null && __ctx0__.__subWrapper0 != null)
                {
                    __ctx0__.__subWrapper0.Destroy(__svc__, __ctx0__);
                    __ctx0__.__subWrapper0 = null;
                }
                base.Finally();
            }

            [Microsoft.XLANGs.Core.UserVariableAttribute("ActualCBRRequest")]
            internal CreditProfileLogic.ExperianCreditProfileRequestType __ActualCBRRequest;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ActualCBRResponse")]
            internal CreditProfileLogic.ExperianCreditProfileResponseType __ActualCBRResponse;
            [Microsoft.XLANGs.Core.UserVariableAttribute("OriginalCBRRequest")]
            internal CreditProfileLogic.ExperianCreditProfileRequestType __OriginalCBRRequest;
            [Microsoft.XLANGs.Core.UserVariableAttribute("ExperianValues")]
            internal Experian.Values __ExperianValues;
            [Microsoft.XLANGs.Core.UserVariableAttribute("Address")]
            internal System.String __Address;
        }

        private static Microsoft.XLANGs.Core.CorrelationType[] _correlationTypes = null;
        public override Microsoft.XLANGs.Core.CorrelationType[] CorrelationTypes { get { return _correlationTypes; } }

        private static System.Guid[] _convoySetIds;

        public override System.Guid[] ConvoySetGuids
        {
            get { return _convoySetIds; }
            set { _convoySetIds = value; }
        }

        public static object[] StaticConvoySetInformation
        {
            get {
                return null;
            }
        }

        [Microsoft.XLANGs.BaseTypes.PhysicalBindingAttribute(typeof(Microsoft.BizTalk.DefaultPipelines.XMLReceive), typeof(Microsoft.BizTalk.DefaultPipelines.XMLTransmit))]
        [Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute(Microsoft.XLANGs.BaseTypes.DeliveryNotificationAttribute.NotificationLevel.Transmitted)]
        [Microsoft.XLANGs.BaseTypes.PortAttribute(
            Microsoft.XLANGs.BaseTypes.EXLangSParameter.eUses|Microsoft.XLANGs.BaseTypes.EXLangSParameter.eDynamic
        )]
        [Microsoft.XLANGs.Core.UserVariableAttribute("ExperianPort")]
        internal ExperianPortTyp ExperianPort;  // lock index = 0

        public static Microsoft.XLANGs.Core.PortInfo[] _portInfo = new Microsoft.XLANGs.Core.PortInfo[] {
            new Microsoft.XLANGs.Core.PortInfo(new Microsoft.XLANGs.Core.OperationInfo[] {ExperianPortTyp.Operation},
                                               typeof(SendToExperian).GetField("ExperianPort", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance),
                                               Microsoft.XLANGs.BaseTypes.Polarity.uses,
                                               true,
                                               Microsoft.XLANGs.Core.HashHelper.HashPort(typeof(SendToExperian), "ExperianPort"),
                                               null)
        };

        public override Microsoft.XLANGs.Core.PortInfo[] PortInformation
        {
            get { return _portInfo; }
        }

        static public System.Collections.Hashtable PortsInformation
        {
            get
            {
                System.Collections.Hashtable h = new System.Collections.Hashtable();
                h[_portInfo[0].Name] = _portInfo[0];
                return h;
            }
        }

        public static System.Type[] InvokedServicesTypes
        {
            get
            {
                return new System.Type[] {
                    // type of each service invoked by this service
                };
            }
        }

        public static System.Type[] CalledServicesTypes
        {
            get
            {
                return new System.Type[] {
                };
            }
        }

        public static System.Type[] ExecedServicesTypes
        {
            get
            {
                return new System.Type[] {
                };
            }
        }


        public static Microsoft.XLANGs.RuntimeTypes.Location[] __eventLocations = new Microsoft.XLANGs.RuntimeTypes.Location[] {
            new Microsoft.XLANGs.RuntimeTypes.Location(0, "00000000-0000-0000-0000-000000000000", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(1, "00000000-0000-0000-0000-000000000000", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(2, "f1afa377-3bda-4dfd-9eff-669deab5d4ce", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(3, "f1afa377-3bda-4dfd-9eff-669deab5d4ce", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(4, "c7c4c9fe-2f12-410c-a417-cfc1666cc0fa", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(5, "c7c4c9fe-2f12-410c-a417-cfc1666cc0fa", 1, false),
            new Microsoft.XLANGs.RuntimeTypes.Location(6, "d6a04938-9715-4afe-b21e-b44c2ddc0ff3", 1, true),
            new Microsoft.XLANGs.RuntimeTypes.Location(7, "d6a04938-9715-4afe-b21e-b44c2ddc0ff3", 1, false)
        };

        public override Microsoft.XLANGs.RuntimeTypes.Location[] EventLocations
        {
            get { return __eventLocations; }
        }

        public static Microsoft.XLANGs.RuntimeTypes.EventData[] __eventData = new Microsoft.XLANGs.RuntimeTypes.EventData[] {
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Body),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Body),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Construct),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Send),
            new Microsoft.XLANGs.RuntimeTypes.EventData( Microsoft.XLANGs.RuntimeTypes.Operation.Start | Microsoft.XLANGs.RuntimeTypes.Operation.Receive)
        };

        public static int[] __progressLocation0 = new int[] { 0,0,0,1,1,};
        public static int[] __progressLocation1 = new int[] { 0,0,1,2,2,3,4,4,4,5,6,6,7,1,1,1,1,1,};

        public static int[][] __progressLocations = new int[2] [] {__progressLocation0,__progressLocation1};
        public override int[][] ProgressLocations {get {return __progressLocations;} }

        public Microsoft.XLANGs.Core.StopConditions segment0(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[0];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[0];
            __SendToExperian_root_0 __ctx0__ = (__SendToExperian_root_0)_stateMgrs[0];
            __SendToExperian_1 __ctx1__ = (__SendToExperian_1)_stateMgrs[1];

            switch (__seg__.Progress)
            {
            case 0:
                ExperianPort = new ExperianPortTyp(0, this);
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.Initialized) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.Initialized;
                goto case 1;
            case 1:
                __ctx1__ = new __SendToExperian_1(this);
                _stateMgrs[1] = __ctx1__;
                if ( !PostProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 2;
            case 2:
                __ctx0__.StartContext(__seg__, __ctx1__);
                if ( !PostProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                return Microsoft.XLANGs.Core.StopConditions.Blocked;
            case 3:
                if (!__ctx0__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 4;
            case 4:
                __ctx1__.Finally();
                ServiceDone(__seg__, (Microsoft.XLANGs.Core.Context)_stateMgrs[0]);
                __ctx0__.OnCommit();
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }

        public Microsoft.XLANGs.Core.StopConditions segment1(Microsoft.XLANGs.Core.StopConditions stopOn)
        {
            Microsoft.XLANGs.Core.Envelope __msgEnv__ = null;
            Microsoft.XLANGs.Core.Segment __seg__ = _segments[1];
            Microsoft.XLANGs.Core.Context __ctx__ = (Microsoft.XLANGs.Core.Context)_stateMgrs[1];
            __SendToExperian_root_0 __ctx0__ = (__SendToExperian_root_0)_stateMgrs[0];
            __SendToExperian_1 __ctx1__ = (__SendToExperian_1)_stateMgrs[1];

            switch (__seg__.Progress)
            {
            case 0:
                __ctx1__.__ExperianValues = (Experian.Values)Args[2];
                __ctx1__.__Address = (System.String)Args[3];
                __ctx1__.__ActualCBRResponse = null;
                __ctx1__.__OriginalCBRRequest = new CreditProfileLogic.ExperianCreditProfileRequestType("OriginalCBRRequest", __ctx1__);
                __ctx1__.__OriginalCBRRequest.CopyFrom((Microsoft.XLANGs.Core.XMessage)Args[1]);
                __ctx1__.RefMessage(__ctx1__.__OriginalCBRRequest);
                __ctx1__.__OriginalCBRRequest.ConstructionCompleteEvent();
                if ( !PostProgressInc( __seg__, __ctx__, 1 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 1;
            case 1:
                __ctx__.PrologueCompleted = true;
                if ( !PostProgressInc( __seg__, __ctx__, 2 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 2;
            case 2:
                if ( !PreProgressInc( __seg__, __ctx__, 3 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[1],__eventData[1],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 3;
            case 3:
                if ( !PreProgressInc( __seg__, __ctx__, 4 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[2],__eventData[2],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 4;
            case 4:
                {
                    CreditProfileLogic.ExperianCreditProfileRequestType __ActualCBRRequest = new CreditProfileLogic.ExperianCreditProfileRequestType("ActualCBRRequest", __ctx1__);

                    __ActualCBRRequest.MessagePart.CopyFrom(__ctx1__.__OriginalCBRRequest.MessagePart);
                    __ActualCBRRequest.SetPropertyValue(typeof(HTTP.Username), __ctx1__.__ExperianValues.Username);
                    __ActualCBRRequest.SetPropertyValue(typeof(HTTP.Password), __ctx1__.__ExperianValues.Password);
                    __ActualCBRRequest.SetPropertyValue(typeof(HTTP.AuthenticationScheme), __ctx1__.__ExperianValues.TransportClientCredentialType);
                    __ActualCBRRequest.SetPropertyValue(typeof(HTTP.EnableChunkedEncoding), true);
                    __ActualCBRRequest.SetPropertyValue(typeof(HTTP.ContentType), __ctx1__.__ExperianValues.HttpHeaders);
                    if (__ctx1__ != null)
                        __ctx1__.__ExperianValues = null;
                    ExperianPort.SetPropertyValue(typeof(Microsoft.XLANGs.BaseTypes.Address), __ctx1__.__Address);
                    if (__ctx1__ != null)
                        __ctx1__.__Address = null;
                    ExperianPort.SetPropertyValue(typeof(Microsoft.XLANGs.BaseTypes.TransportType), "HTTP");

                    if (__ctx1__.__ActualCBRRequest != null)
                        __ctx1__.UnrefMessage(__ctx1__.__ActualCBRRequest);
                    __ctx1__.__ActualCBRRequest = __ActualCBRRequest;
                    __ctx1__.RefMessage(__ctx1__.__ActualCBRRequest);
                }
                __ctx1__.__ActualCBRRequest.ConstructionCompleteEvent(false);
                if ( !PostProgressInc( __seg__, __ctx__, 5 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 5;
            case 5:
                if ( !PreProgressInc( __seg__, __ctx__, 6 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Construct);
                    __edata.Messages.Add(__ctx1__.__ActualCBRRequest);
                    Tracker.FireEvent(__eventLocations[3],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 6;
            case 6:
                if ( !PreProgressInc( __seg__, __ctx__, 7 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[4],__eventData[3],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 7;
            case 7:
                if (!__ctx1__.PrepareToPendingCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 8 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 8;
            case 8:
                if ( !PreProgressInc( __seg__, __ctx__, 9 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                ExperianPort.SendMessage(0, __ctx1__.__ActualCBRRequest, null, null, out __ctx0__.__subWrapper0, __ctx1__, __seg__ );
                if ((stopOn & Microsoft.XLANGs.Core.StopConditions.OutgoingRqst) != 0)
                    return Microsoft.XLANGs.Core.StopConditions.OutgoingRqst;
                goto case 9;
            case 9:
                if ( !PreProgressInc( __seg__, __ctx__, 10 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Send);
                    __edata.Messages.Add(__ctx1__.__ActualCBRRequest);
                    __edata.PortName = @"ExperianPort";
                    Tracker.FireEvent(__eventLocations[5],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__ActualCBRRequest != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__ActualCBRRequest);
                    __ctx1__.__ActualCBRRequest = null;
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 10;
            case 10:
                if ( !PreProgressInc( __seg__, __ctx__, 11 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                Tracker.FireEvent(__eventLocations[6],__eventData[4],_stateMgrs[1].TrackDataStream );
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 11;
            case 11:
                if (!ExperianPort.GetMessageId(__ctx0__.__subWrapper0.getSubscription(this), __seg__, __ctx1__, out __msgEnv__, _locations[0]))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if (__ctx0__ != null && __ctx0__.__subWrapper0 != null)
                {
                    __ctx0__.__subWrapper0.Destroy(this, __ctx0__);
                    __ctx0__.__subWrapper0 = null;
                }
                if (__ctx1__.__ActualCBRResponse != null)
                    __ctx1__.UnrefMessage(__ctx1__.__ActualCBRResponse);
                __ctx1__.__ActualCBRResponse = new CreditProfileLogic.ExperianCreditProfileResponseType("ActualCBRResponse", __ctx1__);
                __ctx1__.RefMessage(__ctx1__.__ActualCBRResponse);
                ExperianPort.ReceiveMessage(0, __msgEnv__, __ctx1__.__ActualCBRResponse, null, (Microsoft.XLANGs.Core.Context)_stateMgrs[1], __seg__);
                if (ExperianPort != null)
                {
                    ExperianPort.Close(__ctx1__, __seg__);
                    ExperianPort = null;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 12 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 12;
            case 12:
                if ( !PreProgressInc( __seg__, __ctx__, 13 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Receive);
                    __edata.Messages.Add(__ctx1__.__ActualCBRResponse);
                    __edata.PortName = @"ExperianPort";
                    Tracker.FireEvent(__eventLocations[7],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (IsDebugged)
                    return Microsoft.XLANGs.Core.StopConditions.InBreakpoint;
                goto case 13;
            case 13:
                {
                    Microsoft.XLANGs.RuntimeTypes.EventData __edata = new Microsoft.XLANGs.RuntimeTypes.EventData(Microsoft.XLANGs.RuntimeTypes.Operation.End | Microsoft.XLANGs.RuntimeTypes.Operation.Body);
                    __edata.Messages.Add(__ctx1__.__ActualCBRResponse);
                    __edata.Messages.Add(__ctx1__.__OriginalCBRRequest);
                    Tracker.FireEvent(__eventLocations[0],__edata,_stateMgrs[1].TrackDataStream );
                }
                if (__ctx1__ != null && __ctx1__.__OriginalCBRRequest != null)
                {
                    __ctx1__.UnrefMessage(__ctx1__.__OriginalCBRRequest);
                    __ctx1__.__OriginalCBRRequest = null;
                }
                if ( !PostProgressInc( __seg__, __ctx__, 14 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 14;
            case 14:
                if (!__ctx1__.CleanupAndPrepareToCommit(__seg__))
                    return Microsoft.XLANGs.Core.StopConditions.Blocked;
                if ( !PostProgressInc( __seg__, __ctx__, 15 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 15;
            case 15:
                if ( !PreProgressInc( __seg__, __ctx__, 16 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                __ctx1__.OnCommit();
                goto case 16;
            case 16:
                ((Microsoft.XLANGs.Core.MessageTuple)(Args[0])).message = __ctx1__.__ActualCBRResponse;
                ((Microsoft.XLANGs.Core.MessageTuple)(Args[0])).context.RefMessage(__ctx1__.__ActualCBRResponse);
                if ( !PostProgressInc( __seg__, __ctx__, 17 ) )
                    return Microsoft.XLANGs.Core.StopConditions.Paused;
                goto case 17;
            case 17:
                __seg__.SegmentDone();
                _segments[0].PredecessorDone(this);
                break;
            }
            return Microsoft.XLANGs.Core.StopConditions.Completed;
        }
        private static Microsoft.XLANGs.Core.CachedObject[] _locations = new Microsoft.XLANGs.Core.CachedObject[] {
            new Microsoft.XLANGs.Core.CachedObject(new System.Guid("{FC5A558A-B7B1-40D9-9298-E7FB0B76AAED}"))
        };

    }

    [Microsoft.XLANGs.BaseTypes.BPELExportableAttribute(false)]
    sealed public class _MODULE_PROXY_ { }
}
