namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest))]
    public sealed class SuccessResponse_To_SFEvent : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0"" version=""1.0"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s0=""http://SBA.CBR.CreditProfile.SFApplicationRequest"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:SFApplicationRequest"" />
  </xsl:template>
  <xsl:template match=""/s0:SFApplicationRequest"">
    <ns0:SFCreditBureauEventRequest>
      <ns0:sync>
        <ns0:after>
          <ns0:Event_Request__c>
            <xsl:attribute name=""Status__c"">
              <xsl:text>Success</xsl:text>
            </xsl:attribute>
            <xsl:attribute name=""Stage__c"">
              <xsl:text>Received CBR</xsl:text>
            </xsl:attribute>
            <xsl:if test=""applicationNumber"">
              <xsl:attribute name=""Application_Number__c"">
                <xsl:value-of select=""applicationNumber/text()"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name=""SObject_Name__c"">
              <xsl:text>Application</xsl:text>
            </xsl:attribute>
            <xsl:if test=""applicationSFID"">
              <xsl:attribute name=""SObject_ID__c"">
                <xsl:value-of select=""applicationSFID/text()"" />
              </xsl:attribute>
            </xsl:if>
          </ns0:Event_Request__c>
        </ns0:after>
      </ns0:sync>
    </ns0:SFCreditBureauEventRequest>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017.SFCreditBureauEventRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauEventRequest__x32017+SFCreditBureauEventRequest";
                return _TrgSchemas;
            }
        }
    }
}
