namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.NetConnectResponse", typeof(global::ExperianCreditProfileDefinition.NetConnectResponse))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest))]
    public sealed class TransformExperianResponse_To_SFCreditBureauLineItems : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s1 s2 userCSharp"" version=""1.0"" xmlns:s0=""http://www.experian.com/NetConnectResponse"" xmlns:s1=""http://schemas.microsoft.com/BizTalk/2003/aggschema"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s2=""http://www.experian.com/ARFResponse"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s1:Root"" />
  </xsl:template>
  <xsl:template match=""/s1:Root"">
    <ns0:SFCreditBureauLineItemsInsertRequest>
      <ns0:sync>
        <xsl:for-each select=""InputMessagePart_1/s0:NetConnectResponse/s2:Products"">
          <xsl:for-each select=""s2:CreditProfile"">
            <xsl:for-each select=""s2:PublicRecord"">
              <xsl:variable name=""var:v1"" select=""userCSharp:LogicalExistence(boolean(.))"" />
              <xsl:variable name=""var:v3"" select=""userCSharp:LogicalExistence(boolean(s2:Status))"" />
              <xsl:variable name=""var:v12"" select=""boolean(.)"" />
              <xsl:variable name=""var:v13"" select=""userCSharp:LogicalExistence($var:v12)"" />
              <ns0:after>
                <ns0:Credit_Bureau_Line_Item__c>
                  <xsl:if test=""string($var:v1)='true'"">
                    <xsl:variable name=""var:v2"" select=""&quot;Public Record&quot;"" />
                    <xsl:attribute name=""Credit_Bureau_Line_Item_Type__c"">
                      <xsl:value-of select=""$var:v2"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name=""Credit_Bureau__c"">
                    <xsl:value-of select=""../../../../../InputMessagePart_0/ns0:SFCreditBureauInsertResponse/ns0:row/ns0:Id/text()"" />
                  </xsl:attribute>
                  <xsl:if test=""string($var:v3)='true'"">
                    <xsl:variable name=""var:v4"" select=""s2:Status/text()"" />
                    <xsl:attribute name=""Public_Record_Type__c"">
                      <xsl:value-of select=""$var:v4"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v5"" select=""userCSharp:NullWhite(string(s2:Amount/text()))"" />
                  <xsl:attribute name=""Public_Record_Amount__c"">
                    <xsl:value-of select=""$var:v5"" />
                  </xsl:attribute>
                  <xsl:variable name=""var:v6"" select=""userCSharp:DateUP(string(s2:FilingDate/text()))"" />
                  <xsl:variable name=""var:v7"" select=""userCSharp:LogicalExistence(string($var:v6))"" />
                  <xsl:if test=""string($var:v7)='true'"">
                    <xsl:variable name=""var:v8"" select=""string($var:v6)"" />
                    <xsl:attribute name=""Public_Record_Filed_Date__c"">
                      <xsl:value-of select=""$var:v8"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v9"" select=""userCSharp:DateUP(string(s2:StatusDate/text()))"" />
                  <xsl:variable name=""var:v10"" select=""userCSharp:LogicalExistence(string($var:v9))"" />
                  <xsl:if test=""string($var:v10)='true'"">
                    <xsl:variable name=""var:v11"" select=""string($var:v9)"" />
                    <xsl:attribute name=""Public_Record_Satisified_Date__c"">
                      <xsl:value-of select=""$var:v11"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""string($var:v13)='true'"">
                    <xsl:variable name=""var:v14"" select=""&quot;false&quot;"" />
                    <xsl:attribute name=""Is_Mortgage__c"">
                      <xsl:value-of select=""$var:v14"" />
                    </xsl:attribute>
                  </xsl:if>
                </ns0:Credit_Bureau_Line_Item__c>
              </ns0:after>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:for-each select=""InputMessagePart_1/s0:NetConnectResponse/s2:Products"">
          <xsl:for-each select=""s2:CreditProfile"">
            <xsl:for-each select=""s2:TradeLine"">
              <xsl:variable name=""var:v15"" select=""position()"" />
              <xsl:variable name=""var:v16"" select=""boolean(.)"" />
              <xsl:variable name=""var:v17"" select=""userCSharp:LogicalExistence($var:v16)"" />
              <xsl:variable name=""var:v19"" select=""userCSharp:LogicalEq(string(s2:EnhancedPaymentData/s2:PaymentStatus/@code) , &quot;93&quot;)"" />
              <xsl:variable name=""var:v20"" select=""userCSharp:LogicalExistence(boolean(s2:OriginalCreditorName))"" />
              <xsl:variable name=""var:v21"" select=""userCSharp:LogicalAnd(string($var:v19) , string($var:v20))"" />
              <xsl:variable name=""var:v29"" select=""userCSharp:StringSubstring(string(s2:Subcode/text()) , &quot;2&quot; , &quot;2&quot;)"" />
              <xsl:variable name=""var:v34"" select=""string(s2:StatusDate/text())"" />
              <ns0:after>
                <ns0:Credit_Bureau_Line_Item__c>
                  <xsl:if test=""string($var:v17)='true'"">
                    <xsl:variable name=""var:v18"" select=""&quot;Trade Line&quot;"" />
                    <xsl:attribute name=""Credit_Bureau_Line_Item_Type__c"">
                      <xsl:value-of select=""$var:v18"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""string($var:v21)='true'"">
                    <xsl:variable name=""var:v22"" select=""&quot;Collection&quot;"" />
                    <xsl:attribute name=""Credit_Bureau_Line_Item_Type__c"">
                      <xsl:value-of select=""$var:v22"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name=""Credit_Bureau__c"">
                    <xsl:value-of select=""../../../../../InputMessagePart_0/ns0:SFCreditBureauInsertResponse/ns0:row/ns0:Id/text()"" />
                  </xsl:attribute>
                  <xsl:if test=""s2:SubscriberDisplayName"">
                    <xsl:attribute name=""Creditor_Name__c"">
                      <xsl:value-of select=""s2:SubscriberDisplayName/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s2:AccountNumber"">
                    <xsl:attribute name=""Account_Number__c"">
                      <xsl:value-of select=""s2:AccountNumber/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v23"" select=""userCSharp:DateUP(string(s2:BalanceDate/text()))"" />
                  <xsl:variable name=""var:v24"" select=""userCSharp:LogicalExistence(string($var:v23))"" />
                  <xsl:if test=""string($var:v24)='true'"">
                    <xsl:variable name=""var:v25"" select=""string($var:v23)"" />
                    <xsl:attribute name=""Balance_Date__c"">
                      <xsl:value-of select=""$var:v25"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v26"" select=""userCSharp:DateUP(string(s2:OpenDate/text()))"" />
                  <xsl:variable name=""var:v27"" select=""userCSharp:LogicalExistence(string($var:v26))"" />
                  <xsl:if test=""string($var:v27)='true'"">
                    <xsl:variable name=""var:v28"" select=""string($var:v26)"" />
                    <xsl:attribute name=""Open_Date__c"">
                      <xsl:value-of select=""$var:v28"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s2:TermsDuration"">
                    <xsl:attribute name=""Term_Type__c"">
                      <xsl:value-of select=""s2:TermsDuration/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v30"" select=""userCSharp:AccountType(string($var:v29))"" />
                  <xsl:attribute name=""Account_Type__c"">
                    <xsl:value-of select=""$var:v30"" />
                  </xsl:attribute>
                  <xsl:call-template name=""CreditLimit"">
                    <xsl:with-param name=""Iteration"" select=""string($var:v15)"" />
                  </xsl:call-template>
                  <xsl:if test=""s2:EnhancedPaymentData/s2:AccountCondition"">
                    <xsl:attribute name=""Account_Condition__c"">
                      <xsl:value-of select=""s2:EnhancedPaymentData/s2:AccountCondition/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s2:EnhancedPaymentData/s2:PaymentStatus"">
                    <xsl:attribute name=""Payment_Status__c"">
                      <xsl:value-of select=""s2:EnhancedPaymentData/s2:PaymentStatus/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v31"" select=""userCSharp:NullCheck(string(s2:BalanceAmount/text()))"" />
                  <xsl:attribute name=""Account_Balance__c"">
                    <xsl:value-of select=""$var:v31"" />
                  </xsl:attribute>
                  <xsl:attribute name=""Payment_Type__c"">
                    <xsl:value-of select=""s2:MonthlyPaymentType/@code"" />
                  </xsl:attribute>
                  <xsl:variable name=""var:v32"" select=""userCSharp:Nul(string(s2:MonthlyPaymentAmount/text()))"" />
                  <xsl:attribute name=""Monthly_Payments__c"">
                    <xsl:value-of select=""$var:v32"" />
                  </xsl:attribute>
                  <xsl:attribute name=""Payment_Status_Code__c"">
                    <xsl:value-of select=""s2:EnhancedPaymentData/s2:PaymentStatus/@code"" />
                  </xsl:attribute>
                  <xsl:attribute name=""Account_Condition_Code__c"">
                    <xsl:value-of select=""s2:EnhancedPaymentData/s2:AccountCondition/@code"" />
                  </xsl:attribute>
                  <xsl:if test=""s2:Subcode"">
                    <xsl:attribute name=""Subcode__c"">
                      <xsl:value-of select=""s2:Subcode/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s2:KOB"">
                    <xsl:attribute name=""Kind_of_Business__c"">
                      <xsl:value-of select=""s2:KOB/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v33"" select=""userCSharp:NullCheck(string(s2:AmountPastDue/text()))"" />
                  <xsl:attribute name=""Amount_Past_Due__c"">
                    <xsl:value-of select=""$var:v33"" />
                  </xsl:attribute>
                  <xsl:variable name=""var:v35"" select=""userCSharp:DateUP($var:v34)"" />
                  <xsl:variable name=""var:v36"" select=""userCSharp:LogicalExistence(string($var:v35))"" />
                  <xsl:if test=""string($var:v36)='true'"">
                    <xsl:variable name=""var:v37"" select=""string($var:v35)"" />
                    <xsl:attribute name=""Status_Date__c"">
                      <xsl:value-of select=""$var:v37"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test=""s2:ECOA"">
                    <xsl:attribute name=""ECOA__c"">
                      <xsl:value-of select=""s2:ECOA/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:variable name=""var:v38"" select=""userCSharp:IsMortgage(string(s2:AccountType/@code))"" />
                  <xsl:attribute name=""Is_Mortgage__c"">
                    <xsl:value-of select=""$var:v38"" />
                  </xsl:attribute>
                  <xsl:call-template name=""HighBalance"">
                    <xsl:with-param name=""Iteration"" select=""string($var:v15)"" />
                  </xsl:call-template>
                  <xsl:attribute name=""Creditor_Type__c"">
                    <xsl:value-of select=""s2:RevolvingOrInstallment/@code"" />
                  </xsl:attribute>
                  <xsl:if test=""s2:OpenOrClosed"">
                    <xsl:attribute name=""Open_Or_Closed__c"">
                      <xsl:value-of select=""s2:OpenOrClosed/text()"" />
                    </xsl:attribute>
                  </xsl:if>
                </ns0:Credit_Bureau_Line_Item__c>
              </ns0:after>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
      </ns0:sync>
    </ns0:SFCreditBureauLineItemsInsertRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalExistence(bool val)
{
	return val;
}


public bool LogicalAnd(string param0, string param1)
{
	return ValToBool(param0) && ValToBool(param1);
	return false;
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public string IsMortgage(string IM) {
               if (!string.IsNullOrWhiteSpace(IM)) {
                              if ((IM == ""27"")||(IM == ""33"")||(IM == ""5B"")||(IM == ""6B"")||(IM == ""85"")||(IM == ""86"")||(IM == ""87"")||(IM == ""25"")||(IM == ""26""))
                              {return ""true"";}                 
                              else {return ""false"";}
               }
              return ""false"";
}


public string NullWhite(string param10)
{
 int i;
 if (string.IsNullOrWhiteSpace(param10))  
  {     
     return""""; 
  }
return param10;
}

public string DateUP(string iUDate)
{
 if((!string.IsNullOrWhiteSpace(iUDate))&&(iUDate.Trim().Length >5))
   {

         String ouDate;
               System.DateTime date = DateTime.ParseExact(iUDate, ""MMddyyyy"", null);
             ouDate = String.Format(""{0:MM/dd/yyyy}"", date);
            return ouDate;
			}
   
return """";		
}

public string StringSubstring(string str, string left, string right)
{
	string retval = """";
	double dleft = 0;
	double dright = 0;
	if (str != null && IsNumeric(left, ref dleft) && IsNumeric(right, ref dright))
	{
		int lt = (int)dleft;
		int rt = (int)dright;
		lt--; rt--;
		if (lt >= 0 && rt >= lt && lt < str.Length)
		{
			if (rt < str.Length)
			{
				retval = str.Substring(lt, rt-lt+1);
			}
			else
			{
				retval = str.Substring(lt, str.Length-lt);
			}
		}
	}
	return retval;
}


public string AccountType(string sAT) {



	if (!string.IsNullOrWhiteSpace(sAT)) {
int AT = System.Convert.ToInt32(sAT);
		if (AT == 1) {
			return ""Bank"";
		} else if (AT == 2) {
			return ""Bank Credit Card"";
		} else if (AT == 3) {
			return ""Retail"";
		} else if (AT == 4) {
			return ""Credit Card"";
		} else if (AT == 5) {
			return ""Loan Finance"";
		} else if (AT == 6) {
			return ""Sales Finance"";
		} else if (AT == 7) {
			return ""Credit Union"";
		} else if (AT == 8) {
			return ""Savings & Loan"";
		} else if (AT == 9) {
			return ""Service & Professional"";
		} else {
			return """";
		}
	}
	return """";
}

public string NullCheck(string param2)
{
 int i;
 if (!string.IsNullOrWhiteSpace(param2))  
  {
     if ((int.TryParse(param2,out i)))
         { return System.Convert.ToString(i);}  
  }
   return String.Empty; 
}


public string Nul(string param3)
{
   
    if((!string.IsNullOrWhiteSpace(param3))&&(param3.StartsWith(""0"")))
      {
          string Payment = param3;
          Payment=param3.TrimStart(new char[]{'0'});           
          return Payment;
  }

return"""";
}

public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
  <xsl:template name=""HighBalance"">
    <xsl:param name=""Iteration"" />
    <xsl:variable name=""HighBalance"" select=""//*[local-name()='NetConnectResponse']/*[local-name()='Products']/*[local-name()='CreditProfile']/*[local-name()='TradeLine'][number($Iteration)]/*[local-name()='Amount'][*[local-name()='Qualifier' and @code ='H']]/*[local-name()='Value']/text()"" />
    <xsl:choose>
      <xsl:when test=""string(number($HighBalance)) != 'NaN'"">
        <xsl:attribute name=""High_Balance__c"">
          <xsl:value-of select=""number($HighBalance)"" />
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name=""High_Balance__c"">
          <xsl:value-of select=""''"" />
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:template>
  <xsl:template name=""CreditLimit"">
    <xsl:param name=""Iteration"" />
    <xsl:variable name=""CreditLimit"" select=""//*[local-name()='NetConnectResponse']/*[local-name()='Products']/*[local-name()='CreditProfile']/*[local-name()='TradeLine'][number($Iteration)]/*[local-name()='Amount'][*[local-name()='Qualifier' and @code ='L']]/*[local-name()='Value']/text()"" />
      <xsl:choose>
        <xsl:when test=""string(number($CreditLimit)) != 'NaN'"">
          <xsl:attribute name=""Credit_Limit__c"">
            <xsl:value-of select=""number($CreditLimit)"" />
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name=""Credit_Limit__c"">
            <xsl:value-of select=""''"" />
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertResponse _srcSchemaTypeReference0 = null;
        
        private const string _strSrcSchemasList1 = @"ExperianCreditProfileDefinition.NetConnectResponse";
        
        private const global::ExperianCreditProfileDefinition.NetConnectResponse _srcSchemaTypeReference1 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017.SFCreditBureauLineItemsInsertRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [2];
                _SrcSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertResponse";
                _SrcSchemas[1] = @"ExperianCreditProfileDefinition.NetConnectResponse";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauLineItemsInsert__x32017+SFCreditBureauLineItemsInsertRequest";
                return _TrgSchemas;
            }
        }
    }
}
