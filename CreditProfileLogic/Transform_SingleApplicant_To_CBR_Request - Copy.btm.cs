namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.GetExperianValues", typeof(global::ExperianCreditProfileDefinition.GetExperianValues))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.NetConnectRequest", typeof(global::ExperianCreditProfileDefinition.NetConnectRequest))]
    public sealed class Transform_SingleApplicant_To_CBR_Request_Copy : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s1 s2 userCSharp"" version=""1.0"" xmlns:ns1=""http://www.experian.com/NetConnect"" xmlns:s0=""http://ExperianValues.GetExperianValues"" xmlns:ns0=""http://www.experian.com/WebDelivery"" xmlns:s1=""http://SBA.CBR.CreditProfile.Applicants"" xmlns:s2=""http://schemas.microsoft.com/BizTalk/2003/aggschema"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s2:Root"" />
  </xsl:template>
  <xsl:template match=""/s2:Root"">
    <xsl:variable name=""var:v1"" select=""userCSharp:LogicalEq(string(InputMessagePart_0/s1:applicants/s1:isPrimary/text()) , &quot;true&quot;)"" />
    <xsl:variable name=""var:v2"" select=""string(InputMessagePart_0/s1:applicants/s1:isPrimary/text())"" />
    <xsl:variable name=""var:v3"" select=""userCSharp:LogicalEq($var:v2 , &quot;false&quot;)"" />
    <ns1:NetConnectRequest>
      <ns0:Request>
        <ns0:Products>
          <ns0:CreditProfile>
            <xsl:if test=""$var:v1"">
              <ns0:PrimaryApplicant>
                <xsl:for-each select=""InputMessagePart_0/s1:applicants/s1:name"">
                  <ns0:Name>
                    <xsl:if test=""s1:surName"">
                      <ns0:Surname>
                        <xsl:value-of select=""s1:surName/text()"" />
                      </ns0:Surname>
                    </xsl:if>
                    <xsl:if test=""s1:firstName"">
                      <ns0:First>
                        <xsl:value-of select=""s1:firstName/text()"" />
                      </ns0:First>
                    </xsl:if>
                  </ns0:Name>
                </xsl:for-each>
                <xsl:if test=""InputMessagePart_0/s1:applicants/s1:ssn"">
                  <ns0:SSN>
                    <xsl:value-of select=""InputMessagePart_0/s1:applicants/s1:ssn/text()"" />
                  </ns0:SSN>
                </xsl:if>
                <xsl:for-each select=""InputMessagePart_0/s1:applicants/s1:currentAddress"">
                  <ns0:CurrentAddress>
                    <xsl:if test=""s1:street"">
                      <ns0:Street>
                        <xsl:value-of select=""s1:street/text()"" />
                      </ns0:Street>
                    </xsl:if>
                    <xsl:if test=""s1:city"">
                      <ns0:City>
                        <xsl:value-of select=""s1:city/text()"" />
                      </ns0:City>
                    </xsl:if>
                    <xsl:if test=""s1:state"">
                      <ns0:State>
                        <xsl:value-of select=""s1:state/text()"" />
                      </ns0:State>
                    </xsl:if>
                    <xsl:if test=""s1:zip"">
                      <ns0:Zip>
                        <xsl:value-of select=""s1:zip/text()"" />
                      </ns0:Zip>
                    </xsl:if>
                  </ns0:CurrentAddress>
                </xsl:for-each>
              </ns0:PrimaryApplicant>
            </xsl:if>
            <xsl:if test=""$var:v3"">
              <ns0:SecondaryApplicant>
                <xsl:for-each select=""InputMessagePart_0/s1:applicants/s1:name"">
                  <ns0:Name>
                    <xsl:if test=""s1:surName"">
                      <ns0:Surname>
                        <xsl:value-of select=""s1:surName/text()"" />
                      </ns0:Surname>
                    </xsl:if>
                    <xsl:if test=""s1:firstName"">
                      <ns0:First>
                        <xsl:value-of select=""s1:firstName/text()"" />
                      </ns0:First>
                    </xsl:if>
                  </ns0:Name>
                </xsl:for-each>
                <xsl:for-each select=""InputMessagePart_0/s1:applicants/s1:currentAddress"">
                  <ns0:PreviousAddress>
                    <xsl:if test=""s1:street"">
                      <ns0:Street>
                        <xsl:value-of select=""s1:street/text()"" />
                      </ns0:Street>
                    </xsl:if>
                    <xsl:if test=""s1:city"">
                      <ns0:City>
                        <xsl:value-of select=""s1:city/text()"" />
                      </ns0:City>
                    </xsl:if>
                    <xsl:if test=""s1:state"">
                      <ns0:State>
                        <xsl:value-of select=""s1:state/text()"" />
                      </ns0:State>
                    </xsl:if>
                    <xsl:if test=""s1:zip"">
                      <ns0:Zip>
                        <xsl:value-of select=""s1:zip/text()"" />
                      </ns0:Zip>
                    </xsl:if>
                  </ns0:PreviousAddress>
                </xsl:for-each>
              </ns0:SecondaryApplicant>
            </xsl:if>
            <ns0:AddOns>
              <ns0:RiskModels>
                <ns0:VantageScore3>
                  <xsl:text>Y</xsl:text>
                </ns0:VantageScore3>
              </ns0:RiskModels>
            </ns0:AddOns>
            <ns0:OutputType>
              <ns0:XML>
                <ns0:Verbose>
                  <xsl:text>Y</xsl:text>
                </ns0:Verbose>
              </ns0:XML>
            </ns0:OutputType>
          </ns0:CreditProfile>
        </ns0:Products>
      </ns0:Request>
    </ns1:NetConnectRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest _srcSchemaTypeReference0 = null;
        
        private const string _strSrcSchemasList1 = @"ExperianCreditProfileDefinition.GetExperianValues";
        
        private const global::ExperianCreditProfileDefinition.GetExperianValues _srcSchemaTypeReference1 = null;
        
        private const string _strTrgSchemasList0 = @"ExperianCreditProfileDefinition.NetConnectRequest";
        
        private const global::ExperianCreditProfileDefinition.NetConnectRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [2];
                _SrcSchemas[0] = @"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest";
                _SrcSchemas[1] = @"ExperianCreditProfileDefinition.GetExperianValues";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"ExperianCreditProfileDefinition.NetConnectRequest";
                return _TrgSchemas;
            }
        }
    }
}
