namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.NetConnectResponse", typeof(global::ExperianCreditProfileDefinition.NetConnectResponse))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.BTSMessageGUID", typeof(global::SalesForceCreditProfileJSONDefinition.BTSMessageGUID))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest))]
    public sealed class Transform_FieldLevelWarning_To_SFCBRRequest : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s4 s3 s2 s1 userCSharp"" version=""1.0"" xmlns:s4=""http://www.experian.com/ARFResponse"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s0=""http://SBA.CBR.CreditProfile"" xmlns:s1=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:s2=""http://www.experian.com/NetConnectResponse"" xmlns:s3=""http://schemas.microsoft.com/BizTalk/2003/aggschema"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s3:Root"" />
  </xsl:template>
  <xsl:template match=""/s3:Root"">
    <ns0:SFCreditBureauInsertRequest>
      <ns0:sync>
        <ns0:after>
          <xsl:for-each select=""InputMessagePart_0/s2:NetConnectResponse/s4:Products"">
            <xsl:for-each select=""s4:CreditProfile"">
              <xsl:for-each select=""s4:Statement"">
                <xsl:for-each select=""s4:Type"">
                  <xsl:variable name=""var:v1"" select=""userCSharp:LogicalEq(string(@code) , &quot;25&quot;)"" />
                  <xsl:variable name=""var:v2"" select=""string(@code)"" />
                  <xsl:variable name=""var:v3"" select=""userCSharp:LogicalEq($var:v2 , &quot;32&quot;)"" />
                  <xsl:variable name=""var:v4"" select=""userCSharp:LogicalOr(string($var:v1) , string($var:v3))"" />
                  <xsl:if test=""$var:v4"">
                    <xsl:variable name=""var:v5"" select=""userCSharp:StringConcat(string(../../../../../../InputMessagePart_2/s0:BTSMessageGUID/s0:ID/text()) , &quot;  ( &quot; , string(../s4:StatementText/s4:MessageText/text()) , &quot;  )&quot;)"" />
                    <xsl:variable name=""var:v7"" select=""userCSharp:LogicalEq($var:v2 , &quot;25&quot;)"" />
                    <ns0:Credit_Bureau__c>
                      <xsl:if test=""../../../../../../InputMessagePart_1/s1:SFApplicationRequest/applicationSFID"">
                        <xsl:attribute name=""Application__c"">
                          <xsl:value-of select=""../../../../../../InputMessagePart_1/s1:SFApplicationRequest/applicationSFID/text()"" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name=""CBR_Status__c"">
                        <xsl:text>Completed</xsl:text>
                      </xsl:attribute>
                      <xsl:attribute name=""Error_Message__c"">
                        <xsl:value-of select=""$var:v5"" />
                      </xsl:attribute>
                      <xsl:attribute name=""Error_Code__c"">
                        <xsl:value-of select=""../../../../s2:CompletionCode/text()"" />
                      </xsl:attribute>
                      <xsl:if test=""string($var:v3)='true'"">
                        <xsl:variable name=""var:v6"" select=""&quot;Locked File&quot;"" />
                        <xsl:attribute name=""CBR_Request_Result__c"">
                          <xsl:value-of select=""$var:v6"" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test=""string($var:v7)='true'"">
                        <xsl:variable name=""var:v8"" select=""&quot;Frozen File&quot;"" />
                        <xsl:attribute name=""CBR_Request_Result__c"">
                          <xsl:value-of select=""$var:v8"" />
                        </xsl:attribute>
                      </xsl:if>
                    </ns0:Credit_Bureau__c>
                  </xsl:if>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:for-each>
        </ns0:after>
      </ns0:sync>
    </ns0:SFCreditBureauInsertRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string StringConcat(string param0, string param1, string param2, string param3)
{
   return param0 + param1 + param2 + param3;
}


public bool LogicalEq(string val1, string val2)
{
	bool ret = false;
	double d1 = 0;
	double d2 = 0;
	if (IsNumeric(val1, ref d1) && IsNumeric(val2, ref d2))
	{
		ret = d1 == d2;
	}
	else
	{
		ret = String.Compare(val1, val2, StringComparison.Ordinal) == 0;
	}
	return ret;
}


public bool LogicalOr(string param0, string param1)
{
	return ValToBool(param0) || ValToBool(param1);
	return false;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool ValToBool(string val)
{
	if (val != null)
	{
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		val = val.Trim();
		if (string.Compare(val, bool.TrueString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return true;
		}
		if (string.Compare(val, bool.FalseString, StringComparison.OrdinalIgnoreCase) == 0)
		{
			return false;
		}
		double d = 0;
		if (IsNumeric(val, ref d))
		{
			return (d > 0);
		}
	}
	return false;
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"ExperianCreditProfileDefinition.NetConnectResponse";
        
        private const global::ExperianCreditProfileDefinition.NetConnectResponse _srcSchemaTypeReference0 = null;
        
        private const string _strSrcSchemasList1 = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _srcSchemaTypeReference1 = null;
        
        private const string _strSrcSchemasList2 = @"SalesForceCreditProfileJSONDefinition.BTSMessageGUID";
        
        private const global::SalesForceCreditProfileJSONDefinition.BTSMessageGUID _srcSchemaTypeReference2 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [3];
                _SrcSchemas[0] = @"ExperianCreditProfileDefinition.NetConnectResponse";
                _SrcSchemas[1] = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
                _SrcSchemas[2] = @"SalesForceCreditProfileJSONDefinition.BTSMessageGUID";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
                return _TrgSchemas;
            }
        }
    }
}
