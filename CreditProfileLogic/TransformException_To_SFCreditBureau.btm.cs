namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.ExceptionInformation", typeof(global::SalesForceCreditProfileJSONDefinition.ExceptionInformation))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest))]
    public sealed class TransformException_To_SFCreditBureau : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s2 s1 userCSharp"" version=""1.0"" xmlns:s0=""http://SBACBRCreditProfile.ExceptionInformation"" xmlns:s2=""http://schemas.microsoft.com/BizTalk/2003/aggschema"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s1=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s2:Root"" />
  </xsl:template>
  <xsl:template match=""/s2:Root"">
    <xsl:variable name=""var:v1"" select=""userCSharp:StringConcat(string(InputMessagePart_1/s0:ExceptionInfo/OrchestrationId/text()) , &quot; :(&quot; , string(InputMessagePart_1/s0:ExceptionInfo/Message/text()) , &quot;)&quot; , &quot;OrchestrationId: &quot;)"" />
    <xsl:variable name=""var:v2"" select=""userCSharp:StringSubstring(string($var:v1) , &quot;1&quot; , &quot;1020&quot;)"" />
    <xsl:variable name=""var:v3"" select=""userCSharp:StringSize(string($var:v1))"" />
    <xsl:variable name=""var:v4"" select=""userCSharp:StringConcat(string($var:v2) , &quot; (&quot; , &quot;Toal Size of String :&quot; , string($var:v3) , &quot;)&quot;)"" />
    <ns0:SFCreditBureauInsertRequest>
      <ns0:sync>
        <ns0:after>
          <ns0:Credit_Bureau__c>
            <xsl:if test=""InputMessagePart_0/s1:SFApplicationRequest/applicationSFID"">
              <xsl:attribute name=""Application__c"">
                <xsl:value-of select=""InputMessagePart_0/s1:SFApplicationRequest/applicationSFID/text()"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name=""CBR_Status__c"">
              <xsl:text>Completed</xsl:text>
            </xsl:attribute>
            <xsl:attribute name=""Error_Message__c"">
              <xsl:value-of select=""$var:v4"" />
            </xsl:attribute>
            <xsl:attribute name=""CBR_Request_Result__c"">
              <xsl:text>Error</xsl:text>
            </xsl:attribute>
          </ns0:Credit_Bureau__c>
        </ns0:after>
      </ns0:sync>
    </ns0:SFCreditBureauInsertRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string StringConcat(string param0, string param1, string param2, string param3, string param4)
{
   return param0 + param1 + param2 + param3 + param4;
}


public int StringSize(string str)
{
	if (str == null)
	{
		return 0;
	}
	return str.Length;
}


public string StringSubstring(string str, string left, string right)
{
	string retval = """";
	double dleft = 0;
	double dright = 0;
	if (str != null && IsNumeric(left, ref dleft) && IsNumeric(right, ref dright))
	{
		int lt = (int)dleft;
		int rt = (int)dright;
		lt--; rt--;
		if (lt >= 0 && rt >= lt && lt < str.Length)
		{
			if (rt < str.Length)
			{
				retval = str.Substring(lt, rt-lt+1);
			}
			else
			{
				retval = str.Substring(lt, str.Length-lt);
			}
		}
	}
	return retval;
}


public bool IsNumeric(string val)
{
	if (val == null)
	{
		return false;
	}
	double d = 0;
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}

public bool IsNumeric(string val, ref double d)
{
	if (val == null)
	{
		return false;
	}
	return Double.TryParse(val, System.Globalization.NumberStyles.AllowThousands | System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out d);
}


]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _srcSchemaTypeReference0 = null;
        
        private const string _strSrcSchemasList1 = @"SalesForceCreditProfileJSONDefinition.ExceptionInformation";
        
        private const global::SalesForceCreditProfileJSONDefinition.ExceptionInformation _srcSchemaTypeReference1 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [2];
                _SrcSchemas[0] = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
                _SrcSchemas[1] = @"SalesForceCreditProfileJSONDefinition.ExceptionInformation";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
                return _TrgSchemas;
            }
        }
    }
}
