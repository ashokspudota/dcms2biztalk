namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.NetConnectResponse", typeof(global::ExperianCreditProfileDefinition.NetConnectResponse))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.BTSMessageGUID", typeof(global::SalesForceCreditProfileJSONDefinition.BTSMessageGUID))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest))]
    public sealed class Transform_ExperianResponse_To_SFCreditBureau : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 s5 s1 s2 s3 s4 userCSharp"" version=""1.0"" xmlns:s0=""http://SBA.CBR.CreditProfile.Applicants"" xmlns:s5=""http://www.experian.com/ARFResponse"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s1=""http://SBA.CBR.CreditProfile"" xmlns:s2=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:s3=""http://www.experian.com/NetConnectResponse"" xmlns:s4=""http://schemas.microsoft.com/BizTalk/2003/aggschema"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s4:Root"" />
  </xsl:template>
  <xsl:template match=""/s4:Root"">
    <ns0:SFCreditBureauInsertRequest>
      <ns0:sync>
        <ns0:after>
          <xsl:for-each select=""InputMessagePart_0/s3:NetConnectResponse/s5:Products"">
            <xsl:variable name=""var:v1"" select=""userCSharp:DateCurrentDateTime()"" />
            <xsl:variable name=""var:v2"" select=""userCSharp:StringConcat(string(s5:CreditProfile/s5:AddressInformation/s5:StreetPrefix/text()) , &quot;  &quot; , string(s5:CreditProfile/s5:AddressInformation/s5:StreetName/text()) , &quot;  &quot; , string(s5:CreditProfile/s5:AddressInformation/s5:StreetSuffix/text()))"" />
            <ns0:Credit_Bureau__c>
              <xsl:if test=""../../../InputMessagePart_1/s2:SFApplicationRequest/applicationSFID"">
                <xsl:attribute name=""Application__c"">
                  <xsl:value-of select=""../../../InputMessagePart_1/s2:SFApplicationRequest/applicationSFID/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name=""Bureau_Name__c"">
                <xsl:text>EXPERIAN</xsl:text>
              </xsl:attribute>
              <xsl:if test=""../../../InputMessagePart_2/s0:applicants/s0:applicantSFID"">
                <xsl:attribute name=""Applicant_Name__c"">
                  <xsl:value-of select=""../../../InputMessagePart_2/s0:applicants/s0:applicantSFID/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name=""CBR_Status__c"">
                <xsl:text>Completed</xsl:text>
              </xsl:attribute>
              <xsl:attribute name=""CBR_Pulled_Date__c"">
                <xsl:value-of select=""$var:v1"" />
              </xsl:attribute>
              <xsl:attribute name=""Mailing_Address__c"">
                <xsl:value-of select=""$var:v2"" />
              </xsl:attribute>
              <xsl:if test=""s5:CreditProfile/s5:AddressInformation/s5:City"">
                <xsl:attribute name=""City__c"">
                  <xsl:value-of select=""s5:CreditProfile/s5:AddressInformation/s5:City/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""s5:CreditProfile/s5:AddressInformation/s5:State"">
                <xsl:attribute name=""State__c"">
                  <xsl:value-of select=""s5:CreditProfile/s5:AddressInformation/s5:State/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""s5:CreditProfile/s5:AddressInformation/s5:Zip"">
                <xsl:attribute name=""Zip_Code__c"">
                  <xsl:value-of select=""s5:CreditProfile/s5:AddressInformation/s5:Zip/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=""s5:CreditProfile/s5:RiskModel/s5:Score"">
                <xsl:attribute name=""Credit_Score__c"">
                  <xsl:value-of select=""s5:CreditProfile/s5:RiskModel/s5:Score/text()"" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name=""Error_Message__c"">
                <xsl:value-of select=""../../../InputMessagePart_3/s1:BTSMessageGUID/s1:ID/text()"" />
              </xsl:attribute>
              <xsl:variable name=""var:v3"" select=""userCSharp:ScoreFactor(string(s5:CreditProfile/s5:RiskModel/s5:ScoreFactorCodeOne/text()))"" />
              <xsl:attribute name=""Score_Factor_One__c"">
                <xsl:value-of select=""$var:v3"" />
              </xsl:attribute>
              <xsl:variable name=""var:v4"" select=""userCSharp:ScoreFactor(string(s5:CreditProfile/s5:RiskModel/s5:ScoreFactorCodeTwo/text()))"" />
              <xsl:attribute name=""Score_Factor_Two__c"">
                <xsl:value-of select=""$var:v4"" />
              </xsl:attribute>
              <xsl:variable name=""var:v5"" select=""userCSharp:ScoreFactor(string(s5:CreditProfile/s5:RiskModel/s5:ScoreFactorCodeThree/text()))"" />
              <xsl:attribute name=""Score_Factor_Three__c"">
                <xsl:value-of select=""$var:v5"" />
              </xsl:attribute>
              <xsl:attribute name=""Error_Code__c"">
                <xsl:value-of select=""../s3:CompletionCode/text()"" />
              </xsl:attribute>
              <xsl:variable name=""var:v6"" select=""userCSharp:ScoreFactor(string(s5:CreditProfile/s5:RiskModel/s5:ScoreFactorCodeFour/text()))"" />
              <xsl:attribute name=""Score_Factor_Four__c"">
                <xsl:value-of select=""$var:v6"" />
              </xsl:attribute>
              <xsl:attribute name=""CBR_Request_Result__c"">
                <xsl:text>Success</xsl:text>
              </xsl:attribute>
              <xsl:if test=""s5:CreditProfile/s5:SSN/s5:Number"">
                <xsl:attribute name=""SSN__c"">
                  <xsl:value-of select=""s5:CreditProfile/s5:SSN/s5:Number/text()"" />
                </xsl:attribute>
              </xsl:if>
            </ns0:Credit_Bureau__c>
          </xsl:for-each>
        </ns0:after>
      </ns0:sync>
    </ns0:SFCreditBureauInsertRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string DateCurrentDateTime()
{
	DateTime dt = DateTime.Now;
	string curdate = dt.ToString(""yyyy-MM-dd"", System.Globalization.CultureInfo.InvariantCulture);
	string curtime = dt.ToString(""T"", System.Globalization.CultureInfo.InvariantCulture);
	string retval = curdate + ""T"" + curtime;
	return retval;
}


public string StringConcat(string param0, string param1, string param2, string param3, string param4)
{
   return param0 + param1 + param2 + param3 + param4;
}


 public string ScoreFactor(string SF)
        {          

            if ((!string.IsNullOrWhiteSpace(SF))&&(SF.Trim().Length >0))
            {
 int X = System.Convert.ToInt32(SF);
                if (X == 04)
                { return ""THE BALANCES ON YOUR ACCOUNTS ARE TOO HIGH COMPARED TO LOAN LIMITS""; }

                else if (X == 05)
                { return ""TOO MANY OF THE DELINQUENCIES ON YOUR ACCOUNTS ARE RECENT""; }

                else if (X == 06)
                { return ""YOU HAVE TOO MANY ACCOUNTS THAT WERE OPENED RECENTLY""; }

                else if (X == 07)
                { return ""YOU HAVE TOO MANY DELINQUENT OR DEROGATORY ACCOUNTS""; }

                else if (X == 08)
                { return ""YOU HAVE EITHER TOO FEW LOANS OR TOO MANY LOANS WITH RECENT DELINQUENCIES""; }

                else if (X == 09)
                { return ""THE MOST SERIOUS PAYMENT STATUS ON YOUR ACCTS IS DELINQUENT OR DEROGATORY""; }

                else if (X == 10)
                { return ""YOU HAVE NOT PAID ENOUGH OF YOUR ACCOUNTS ON TIME""; }

                else if (X == 11)
                { return ""THE TOTAL OF YOUR DELINQUENT OR DEROGATORY ACCOUNT BALANCES IS TOO HIGH""; }

                else if (X == 12)
                { return ""THE DATE THAT YOU OPENED YOUR OLDEST ACCOUNT IS TOO RECENT""; }

                else if (X == 13)
                { return ""YOUR MOST RECENTLY OPENED ACCOUNT IS TOO NEW""; }

                else if (X == 14)
                { return ""LACK OF SUFFICIENT CREDIT HISTORY""; }

                else if (X == 15)
                { return ""NEWEST DELINQUENT/DEROGATORY PAYMENT STATUS ON YOUR ACCTS IS TOO RECENT""; }

                else if (X == 16)
                { return ""THE TOTAL OF ALL BALANCES ON YOUR OPEN ACCOUNTS IS TOO HIGH""; }

                else if (X == 17)
                { return ""BALANCE ON PREVIOUSLY DELINQUENT ACCTS ARE TOO HIGH COMPARED TO LOAN AMTS""; }

                else if (X == 18)
                { return ""TOTAL OF BALANCES ON ACCTS NEVER LATE IS TOO HIGH COMPARED TO LOAN AMTS""; }

                else if (X == 21)
                { return ""NO OPEN ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 22)
                { return ""NO RECENTLY REPORTED ACCOUNT INFORMATION""; }

                else if (X == 23)
                { return ""LACK OF SUFFICIENT RELEVANT ACCOUNT INFORMATION""; }

                else if (X == 29)
                { return ""TOO MANY OF YOUR OPEN BANK CARD OR REVOLVING ACCOUNTS HAVE A BALANCE""; }

                else if (X == 30)
                { return ""TOO FEW OF YOUR BANK CARD OR OTHER REVOLVING ACCOUNTS HAVE HIGH LIMITS""; }

                else if (X == 31)
                { return ""TOO MANY BANK CARD OR OTHER REVOLVING ACCOUNTS WERE OPENED RECENTLY""; }

                else if (X == 32)
                { return ""BALANCES ON BANKCARD/REVOLVING ACCTS TOO HIGH COMPARED TO CREDIT LIMITS""; }

                else if (X == 33)
                { return ""YOUR WORST BANKCARD OR REVOLVING ACCOUNTS STATUS IS DELINQUENT/DEROGATORY""; }

                else if (X == 34)
                { return ""TOTAL OF ALL BALANCES ON BANK CARD OR REVOLVING ACCOUNTS IS TOO HIGH""; }

                else if (X == 35)
                { return ""YOUR HIGHEST BANKCARD OR REVOLVING ACCOUNT BALANCE IS TOO HIGH""; }

                else if (X == 36)
                { return ""YOUR LARGEST CREDIT LIMIT ON OPEN BANKCARD OR REVOLVING ACCOUNTS IS TOO LOW""; }

                else if (X == 39)
                { return ""AVAILABLE CREDIT ON YOUR OPEN BANKCARD OR REVOLVING ACCOUNTS IS TOO LOW""; }

                else if (X == 40)
                { return ""THE DATE YOU OPENED YOUR OLDEST BANK CARD OR REVOLVING ACCT IS TOO RECENT""; }

                else if (X == 42)
                { return ""THE DATE YOU OPENED YOUR NEWEST BANK CARD OR REVOLVING ACCT IS TOO RECENT""; }

                else if (X == 43)
                { return ""LACK OF SUFFICIENT CREDIT HISTORY ON BANK CARD OR REVOLVING ACCOUNTS""; }

                else if (X == 44)
                { return ""TOO MANY BANKCARD OR REVOLVING ACCOUNTS WITH DELINQUENT/DEROGATORY STATUS""; }

                else if (X == 45)
                { return ""TOTAL BALANCES TOO HIGH ON DELINQUENT/DEROGATORY BANKCARD/REVOLVING ACCTS""; }

                else if (X == 47)
                { return ""NO OPEN BANK CARD OR REVOLVING ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 48)
                { return ""NO BANKCARD OR REVOLVING RECENTLY REPORTED ACCOUNT INFORMATION""; }

                else if (X == 49)
                { return ""LACK OF SUFFICIENT RELEVANT BANKCARD OR REVOLVING ACCOUNT INFORMATION""; }

                else if (X == 53)
                { return ""THE WORST STATUS ON YOUR REAL ESTATE ACCOUNTS IS DELINQUENT OR DEROGATORY""; }

                else if (X == 54)
                { return ""THE AMT OF BALANCE PAID DOWN ON YOUR OPEN REAL ESTATE ACCOUNTS IS TOO LOW""; }

                else if (X == 55)
                { return ""OPEN REAL ESTATE ACCT BALANCES ARE TOO HIGH COMPARED TO THEIR LOAN AMTS""; }

                else if (X == 57)
                { return ""TOO MANY REAL ESTATE ACCTS WITH DELINQUENT OR DEROGATORY PAYMENT STATUS""; }

                else if (X == 58)
                { return ""THE TOTAL OF ALL BALANCES ON YOUR OPEN REAL ESTATE ACCOUNTS IS TOO HIGH""; }

                else if (X == 61)
                { return ""NO OPEN REAL ESTATE ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 62)
                { return ""NO RECENTLY REPORTED REAL ESTATE ACCOUNT INFORMATION""; }

                else if (X == 63)
                { return ""LACK OF SUFFICIENT RELEVANT REAL ESTATE ACCOUNT INFORMATION""; }

                else if (X == 64)
                { return ""NO OPEN FIRST MORTGAGE ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 65)
                { return ""LACK OF SUFFICIENT RELEVANT FIRST MORTGAGE ACCOUNT INFORMATION""; }

                else if (X == 66)
                { return ""YOUR OPEN AUTO ACCOUNT BALANCES ARE TOO HIGH COMPARED TO THEIR LOAN AMTS""; }

                else if (X == 68)
                { return ""NO OPEN AUTO ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 69)
                { return ""LACK OF SUFFICIENT RELEVANT AUTO ACCOUNT INFORMATION""; }

                else if (X == 71)
                { return ""YOU HAVE NOT PAID ENOUGH OF YOUR INSTALLMENT ACCOUNTS ON TIME""; }

                else if (X == 72)
                { return ""TOO MANY INSTALLMENT ACCTS WITH A DELINQUENT OR DEROGATORY PAYMENT STATUS""; }

                else if (X == 73)
                { return ""THE WORST STATUS ON YOUR INSTALLMENT ACCOUNTS IS DELINQUENT OR DEROGATORY""; }

                else if (X == 74)
                { return ""THE BALANCE AMOUNT PAID DOWN ON YOUR OPEN INSTALLMENT ACCOUNTS IS TOO LOW""; }

                else if (X == 75)
                { return ""THE INSTALLMENT ACCOUNT THAT YOU OPENED MOST RECENTLY IS TOO NEW""; }

                else if (X == 76)
                { return ""YOU HAVE INSUFFICIENT CREDIT HISTORY ON INSTALLMENT LOANS""; }

                else if (X == 77)
                { return ""NEWEST DELINQUENT OR DEROGATORY STATUS ON INSTALLMENT ACCTS IS TOO RECENT""; }

                else if (X == 78)
                { return ""BALANCES ON INSTALLMENT ACCTS ARE TOO HIGH COMPARED TO THEIR LOAN AMOUNTS""; }

                else if (X == 79)
                { return ""TOO MANY OF THE DELINQUENCIES ON YOUR INSTALLMENT ACCOUNTS ARE RECENT""; }

                else if (X == 81)
                { return ""NO OPEN INSTALLMENT ACCOUNTS IN YOUR CREDIT FILE""; }

                else if (X == 83)
                { return ""LACK OF SUFFICIENT RELEVANT INSTALLMENT ACCOUNT INFORMATION""; }

                else if (X == 84)
                { return ""NUMBER OF INQUIRIES IMPACTED YOUR SCOREreturn BUT EFFECT WAS NOT SIGNelse ifICANT""; }

                else if (X == 85)
                { return ""YOU HAVE TOO MANY INQUIRIES ON YOUR CREDIT REPORT""; }

                else if (X == 86)
                { return ""YOUR CREDIT REPORT CONTAINS TOO MANY DEROGATORY PUBLIC RECORDS""; }

                else if (X == 87)
                { return ""YOUR CREDIT REPORT CONTAINS TOO MANY UNSATISFIED PUBLIC RECORDS""; }

                else if (X == 88)
                { return ""ONE OR MORE DEROGATORY PUBLIC RECORDS IN YOUR CREDIT FILE IS TOO RECENT""; }

                else if (X == 90)
                { return ""TOO FEW DISCHARGED BANKRUPTCIES""; }

                else if (X == 93)
                { return ""THE WORST STATUS ON YOUR STUDENT LOAN ACCTS IS DELINQUENT OR DEROGATORY""; }

                else if (X == 94)
                { return ""THE BALANCE AMOUNT PAID DOWN ON YOUR OPEN STUDENT LOAN ACCTS IS TOO LOW""; }

                else if (X == 95)
                { return ""YOU HAVE TOO MANY COLLECTION AGENCY ACCOUNTS THAT ARE UNPAID""; }

                else if (X == 96)
                { return ""THE TOTAL YOU OWE ON COLLECTION AGENCY ACCOUNTS IS HIGH""; }

                else if (X == 97)
                { return ""YOU HAVE TOO FEW CREDIT ACCOUNTS""; }

                else if (X == 98)
                { return ""THERE IS A BANKRUPTCY ON YOUR CREDIT REPORT""; }

                else
                { return """"; }
            }

            return """";
        }



]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"ExperianCreditProfileDefinition.NetConnectResponse";
        
        private const global::ExperianCreditProfileDefinition.NetConnectResponse _srcSchemaTypeReference0 = null;
        
        private const string _strSrcSchemasList1 = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _srcSchemaTypeReference1 = null;
        
        private const string _strSrcSchemasList2 = @"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest _srcSchemaTypeReference2 = null;
        
        private const string _strSrcSchemasList3 = @"SalesForceCreditProfileJSONDefinition.BTSMessageGUID";
        
        private const global::SalesForceCreditProfileJSONDefinition.BTSMessageGUID _srcSchemaTypeReference3 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [4];
                _SrcSchemas[0] = @"ExperianCreditProfileDefinition.NetConnectResponse";
                _SrcSchemas[1] = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
                _SrcSchemas[2] = @"SalesForceCreditProfileJSONDefinition.SFApplicantsDebatchedJSONRequest";
                _SrcSchemas[3] = @"SalesForceCreditProfileJSONDefinition.BTSMessageGUID";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
                return _TrgSchemas;
            }
        }
    }
}
