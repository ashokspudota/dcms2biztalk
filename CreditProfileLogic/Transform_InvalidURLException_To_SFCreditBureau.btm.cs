namespace CreditProfileLogic {
    
    
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest", typeof(global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest))]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest", typeof(global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest))]
    public sealed class TransformInvalidURLException_To_SFCreditBureau : global::Microsoft.XLANGs.BaseTypes.TransformBase {
        
        private const string _strMap = @"<?xml version=""1.0"" encoding=""UTF-16""?>
<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:msxsl=""urn:schemas-microsoft-com:xslt"" xmlns:var=""http://schemas.microsoft.com/BizTalk/2003/var"" exclude-result-prefixes=""msxsl var s0 userCSharp"" version=""1.0"" xmlns:ns0=""http://www.cdata.com/SalesforceProvider/2017"" xmlns:s0=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:userCSharp=""http://schemas.microsoft.com/BizTalk/2003/userCSharp"">
  <xsl:output omit-xml-declaration=""yes"" method=""xml"" version=""1.0"" />
  <xsl:template match=""/"">
    <xsl:apply-templates select=""/s0:SFApplicationRequest"" />
  </xsl:template>
  <xsl:template match=""/s0:SFApplicationRequest"">
    <xsl:variable name=""var:v1"" select=""userCSharp:StringConcat(&quot;ECAL Experian Address Validation Failed:-&quot; , &quot;Invalid Experian URL From ECAL.&quot;)"" />
    <ns0:SFCreditBureauInsertRequest>
      <ns0:sync>
        <ns0:after>
          <ns0:Credit_Bureau__c>
            <xsl:if test=""applicationSFID"">
              <xsl:attribute name=""Application__c"">
                <xsl:value-of select=""applicationSFID/text()"" />
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name=""CBR_Status__c"">
              <xsl:text>Completed</xsl:text>
            </xsl:attribute>
            <xsl:attribute name=""Error_Message__c"">
              <xsl:value-of select=""$var:v1"" />
            </xsl:attribute>
            <xsl:attribute name=""CBR_Request_Result__c"">
              <xsl:text>Error</xsl:text>
            </xsl:attribute>
          </ns0:Credit_Bureau__c>
        </ns0:after>
      </ns0:sync>
    </ns0:SFCreditBureauInsertRequest>
  </xsl:template>
  <msxsl:script language=""C#"" implements-prefix=""userCSharp""><![CDATA[
public string StringConcat(string param0, string param1)
{
   return param0 + param1;
}



]]></msxsl:script>
</xsl:stylesheet>";
        
        private const int _useXSLTransform = 0;
        
        private const string _strArgList = @"<ExtensionObjects />";
        
        private const string _strSrcSchemasList0 = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
        
        private const global::SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest _srcSchemaTypeReference0 = null;
        
        private const string _strTrgSchemasList0 = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
        
        private const global::SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017.SFCreditBureauInsertRequest _trgSchemaTypeReference0 = null;
        
        public override string XmlContent {
            get {
                return _strMap;
            }
        }
        
        public override int UseXSLTransform {
            get {
                return _useXSLTransform;
            }
        }
        
        public override string XsltArgumentListContent {
            get {
                return _strArgList;
            }
        }
        
        public override string[] SourceSchemas {
            get {
                string[] _SrcSchemas = new string [1];
                _SrcSchemas[0] = @"SalesForceCreditProfileJSONDefinition.SFApplicantionBatchedJSONRequest";
                return _SrcSchemas;
            }
        }
        
        public override string[] TargetSchemas {
            get {
                string[] _TrgSchemas = new string [1];
                _TrgSchemas[0] = @"SalesForceCreditProfileDefinition.SFCreditBureauInsert__x32017+SFCreditBureauInsertRequest";
                return _TrgSchemas;
            }
        }
    }
}
