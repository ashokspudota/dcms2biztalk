namespace SalesForceCreditProfileJSONDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://SBA.CBR.CreditProfile.SFApplicationRequest",@"SFApplicationRequest")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"SFApplicationRequest"})]
    public sealed class SFApplicantionBatchedJSONRequest : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" elementFormDefault=""unqualified"" targetNamespace=""http://SBA.CBR.CreditProfile.SFApplicationRequest"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:element name=""SFApplicationRequest"">
    <xs:complexType>
      <xs:sequence>
        <xs:element minOccurs=""0"" name=""applicationNumber"" type=""xs:string"" />
        <xs:element minOccurs=""0"" name=""applicationSFID"" type=""xs:string"" />
        <xs:element minOccurs=""0"" maxOccurs=""unbounded"" name=""applicants"">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs=""0"" name=""isPrimary"" type=""xs:boolean"" />
              <xs:element minOccurs=""0"" name=""ssn"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""applicantSFID"" type=""xs:string"" />
              <xs:element minOccurs=""0"" name=""name"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""surName"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""firstName"" type=""xs:string"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element minOccurs=""0"" name=""currentAddress"">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs=""0"" name=""street"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""city"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""state"" type=""xs:string"" />
                    <xs:element minOccurs=""0"" name=""zip"" type=""xs:unsignedShort"" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public SFApplicantionBatchedJSONRequest() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "SFApplicationRequest";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
