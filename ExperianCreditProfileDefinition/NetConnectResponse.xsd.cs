namespace ExperianCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://www.experian.com/NetConnectResponse",@"NetConnectResponse")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"NetConnectResponse"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.ARFXMLResponse", typeof(global::ExperianCreditProfileDefinition.ARFXMLResponse))]
    public sealed class NetConnectResponse : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""http://www.experian.com/NetConnectResponse"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:arf=""http://www.experian.com/ARFResponse"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.experian.com/NetConnectResponse"" version=""2.701"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:import schemaLocation=""ExperianCreditProfileDefinition.ARFXMLResponse"" namespace=""http://www.experian.com/ARFResponse"" />
  <xsd:annotation>
    <xsd:appinfo>
      <b:references>
        <b:reference targetNamespace=""http://www.experian.com/ARFResponse"" />
      </b:references>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:element name=""NetConnectResponse"">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name=""CompletionCode"" type=""xsd:string"" />
        <xsd:element minOccurs=""0"" name=""ReferenceId"" type=""xsd:string"" />
        <xsd:element minOccurs=""0"" name=""TransactionId"" type=""xsd:string"" />
        <xsd:choice minOccurs=""0"">
          <xsd:element name=""HostResponse"" type=""xsd:string"" />
          <xsd:element ref=""arf:Products"" />
        </xsd:choice>
        <xsd:element minOccurs=""0"" name=""ErrorMessage"" type=""xsd:string"" />
        <xsd:element minOccurs=""0"" name=""ErrorTag"" type=""xsd:string"" />
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
</xsd:schema>";
        
        public NetConnectResponse() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "NetConnectResponse";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
