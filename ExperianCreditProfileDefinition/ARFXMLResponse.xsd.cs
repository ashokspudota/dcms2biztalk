namespace ExperianCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Products", @"CreditProfile", @"KBA", @"SessionID", @"XMLVersion"})]
    public sealed class ARFXMLResponse : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""http://www.experian.com/ARFResponse"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" attributeFormDefault=""unqualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.experian.com/ARFResponse"" version=""2.609"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:element name=""Products"" type=""ProductsType"" />
  <xsd:complexType name=""ProductsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" ref=""CreditProfile"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""CreditProfile"" type=""CreditProfileType"" />
  <xsd:complexType name=""PreciseIDType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" name=""Messages"" type=""PreciseID_Messages"" />
      <xsd:element minOccurs=""0"" name=""Summary"" type=""PreciseID_SummaryType"" />
      <xsd:element minOccurs=""0"" name=""FCRADetail"" type=""PreciseID_FCRADetailType"" />
      <xsd:element minOccurs=""0"" name=""GLBDetail"" type=""PreciseID_GLBDetailType"" />
      <xsd:element minOccurs=""0"" name=""CheckPoint"">
        <xsd:complexType>
          <xsd:group minOccurs=""0"" ref=""CheckPoint_DataSegmentsType"" />
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NationalFraudDatabase"">
        <xsd:complexType>
          <xsd:group minOccurs=""0"" ref=""PreciseID_NFD_InformationType"" />
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdditionalAddresses"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AdditionalAddress"" type=""AddressInformationType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFCCodes"" type=""ProSelect_XFCCode_Type"" />
      <xsd:element minOccurs=""0"" name=""AddressMismatchFlag"" type=""ProSelect_AddressMismatch_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_CustomSolutionType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Summary"" type=""PreciseID_SummaryType"" />
      <xsd:element minOccurs=""0"" name=""FCRADetail"" type=""PreciseID_FCRADetailType"" />
      <xsd:element minOccurs=""0"" name=""GLBDetail"" type=""PreciseID_GLBDetailType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_SummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ReviewReferenceID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PreciseIDType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PreciseIDScore"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""PreciseIDScorecard"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ValidationScore"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""ValidationScorecard"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""VerificationScore"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""VerificationScorecard"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ComplianceIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ComplianceDescription"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FPDScore"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FPDScorecard"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UtilityFunction"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""OutofWalletScore"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""InitialResults"" type=""InitialCall2ResultsType"" />
      <xsd:element minOccurs=""0"" name=""Call2Results"" type=""InitialCall2ResultsType"" />
      <xsd:element minOccurs=""0"" name=""NFDResults"" type=""NFDResultsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_GLBDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CheckpointSummary"" type=""PreciseID_CheckpointSummaryType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""GLBFraudShieldType"" />
      <xsd:element minOccurs=""0"" name=""SharedApplication"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""GLBRule"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_FCRADetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""FCRAFraudShieldType"" />
      <xsd:element minOccurs=""0"" name=""SharedApplication"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""FCRARule"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AdverseActionCode"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NFDSummary"" type=""PreciseID_FCRANFDType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InitialCall2ResultsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AuthenticationIndex"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""MostLikelyFraudType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Reasons"" type=""ReasonType"" />
      <xsd:element minOccurs=""0"" name=""InitialDecision"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FinalDecision"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ActionPath"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFDResultsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NFDIndex"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""NFDScorecard"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Reasons"" type=""ReasonType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ReasonType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Reason1"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Reason2"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Reason3"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Reason4"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Reason5"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_CheckpointSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PrimaryResultCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestPickAcceptDeny"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddrCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhnCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddrTypeCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""COACode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SSNCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddrUnitMismatchCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhnUnitMismatchCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DLResultCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfBirthMatch"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighRiskAddrCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighRiskPhoneCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OFACValidationResult"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddrResMatches"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""AddrBusMatches"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""PhnResMatches"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""PhnBusMatches"" type=""xsd:int"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""GLBFraudShieldType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FS01"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS02"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS03"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS04"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS05"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS06"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS10"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS11"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS13"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS14"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS15"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS16"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS17"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS18"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS21"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS25"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS26"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS27"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FCRAFraudShieldType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FS07"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS08"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS09"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS19"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS20"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FS22"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_FCRANFDType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""RecordHits"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FraudFieldHits"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""VictimRecordHits"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""VictimFieldHits"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByInputAddress"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsBySSN"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByDriverLicense"" type=""xsd:int"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByPhone"" type=""xsd:int"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_Messages"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Message"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Number"" type=""xsd:int"" />
            <xsd:element minOccurs=""0"" name=""Text"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AddrMismatch"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""PreciseID_NFD_InformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Summary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ReviewReferenceID"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecordHits"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FraudFieldHits"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""VictimRecordHits"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""VictimFieldHits"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FieldHitsByInputAddress"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FieldHitsBySSN"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FieldHitsByDriversLicense"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FieldHitsByPhone"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""NFDIndex"" type=""xsd:int"" />
            <xsd:element minOccurs=""0"" name=""NFDScorecard"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Reasons"" type=""ReasonType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Detail"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""MemberID"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""KindOfBusiness"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""FraudOrVictimCategory"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""LoadDate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ApplicationDate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FraudDiscoveryDate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""OwnRecordMatchIndicator"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ConsumerOrBusinessMatch"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""InquirySSNStatus"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""SSNMatchStatus"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""AddressMatchIndicator"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""DriversLicenseMatchIndicator"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""InquiryPhoneNumberCount"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AddressMatchConfidenceLevel"">
              <xsd:complexType>
                <xsd:sequence>
                  <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""PreciseID_NFD_AddressMatchType"" />
                  <xsd:element minOccurs=""0"" name=""PreviousAddress"" type=""PreciseID_NFD_AddressMatchType"" />
                </xsd:sequence>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""PhoneMatch"">
              <xsd:complexType>
                <xsd:sequence>
                  <xsd:element minOccurs=""0"" name=""InquiryPhone1"" type=""PreciseID_NFD_PhoneMatchType"" />
                  <xsd:element minOccurs=""0"" name=""InquiryPhone2"" type=""PreciseID_NFD_PhoneMatchType"" />
                  <xsd:element minOccurs=""0"" name=""InquiryPhone3"" type=""PreciseID_NFD_PhoneMatchType"" />
                </xsd:sequence>
              </xsd:complexType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""PreciseID_NFD_AddressMatchType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MatchedFormatCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""MatchConfidenceLevel"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""HouseStreetSuffixMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""PrePostUnitIDMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""CityStateZipMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""UnitTypeZipPlus4MatchCode"" type=""OptionalCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_NFD_PhoneMatchType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InquiryPhoneType"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone1Type"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone2Type"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone3Type"" type=""OptionalCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""TruvueType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" name=""PINVerify"" type=""PINVerifyType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" name=""InquiryVerification"" type=""InquiryVerificationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Demographics"" type=""Truvue_DemographicsType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""LivingUnitRelationship"" type=""Truvue_LivingUnitRelationshipType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfileType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" name=""PINVerify"" type=""PINVerifyType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RiskModel"" type=""CreditProfile_RiskModelType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""eBureauInformationlMessage"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""eBureauMessageCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""eBureauSecondaryMessageCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""eBureauMessageFCRAAttributeXFC06"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""eBureauMessageFCRAAttributeXFC07"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""eBureauStatement"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""StatementCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""StatementText"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerAssistanceReferralAddress"" type=""ConsumerAssistanceReferralAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummary"" type=""AutoProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""HealthcareProfileSummary"" type=""HealthcareProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CreditTrendsSummary"" type=""CreditTrendsSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ModelAttributes"" type=""ModelAttributesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""STAGGSelect"" type=""STAGG_Type"" />
      <xsd:element minOccurs=""0"" name=""CreditScoreExceptionNotice"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RiskModel"" type=""RiskModelType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerAssistanceReferralAddress"" type=""ConsumerAssistanceReferralAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummary"" type=""AutoProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ModelAttributes"" type=""ModelAttributesType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearchType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerInformation"" type=""SocialSearch_ConsumerInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ErrorType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""HostID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ApplicationID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportTime"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RegionCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FirstName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ErrorCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ActionIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ModuleID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""HeaderType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ReportDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportTime"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PINVerifyType"">
    <xsd:sequence>
      <xsd:element name=""VerificationIndicator"" type=""RequiredCodeType"" />
      <xsd:element name=""EncryptedPIN"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""17"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SSNType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SSNIndicators"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""YOBType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""4"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""DOBType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""8"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ConfirmedVerifiedSSNType"">
    <xsd:sequence>
      <xsd:element name=""TimesReportedForSameName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""SSN"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DriverLicenseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConsumerIdentityType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""YOB"">
        <xsd:simpleType>
          <xsd:restriction base=""YOBType"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"">
        <xsd:simpleType>
          <xsd:restriction base=""DOBType"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Spouse"" type=""SpouseType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriverLicenseType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""X4_ConsumerIdentityType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""Spouse"" type=""SpouseType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriverLicenseType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConfirmedVerifiedSSN"" type=""ConfirmedVerifiedSSNType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""RiskModelType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ModelIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Score"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeOne"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeTwo"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeThree"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeFour"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Evaluation"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_RiskModelType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ModelIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Score"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeOne"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeTwo"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeThree"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeFour"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Evaluation"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ScorePercentile"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PhoneType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Source"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NameType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SecondSurname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"" type=""OptionalCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AddressInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FirstReportedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastUpdatedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Origination"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TimesReported"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastReportingSubcode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DwellingType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""HomeOwnership"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StreetPrefix"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StreetName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StreetSuffix"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnitType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnitID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CensusGeoCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CountyCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StateCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MSACode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_AddressInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FirstReportedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastUpdatedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Origination"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TimesReported"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastReportingSubcode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DwellingType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""HomeOwnership"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StreetPrefix"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StreetName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""20"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StreetSuffix"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnitType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnitID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CensusGeoCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CountyCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StateCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MSACode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""22"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberAddress"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberCity"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberState"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberZip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberPhone"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FirstReportedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastUpdatedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Origination"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Name"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressFirstLine"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressSecondLine"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressExtraLine"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InquiryVerificationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Authentication"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Name"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CurrentAddress"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PreviousAddress"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SSN"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DriverLicense"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Phone"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Spouse"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""FirstName"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""SSN"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""verificationCode"" type=""xsd:string"" use=""required"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PublicRecordType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Status"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StatusDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FilingDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Evaluation"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Amount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConsumerComment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Court"">
        <xsd:complexType>
          <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
          <xsd:attribute name=""name"" type=""xsd:string"" use=""required"" />
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PlaintiffName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DisputeFlag"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ECOA"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""AssetAmount"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"" />
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""LiabilitiesAmount"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"" />
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""RepaymentPercent"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"" />
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""AdjustmentPercent"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"" />
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BookPageSequence"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SpouseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""FirstName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SSN"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InformationalMessageType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MessageNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MessageText"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_TradeLineType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SpecialComment"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Evaluation"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""OpenDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StatusDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MaxDelinquencyDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TermsDuration"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TermsFrequency"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ECOA"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""Amount"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Qualifier"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""Value"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BalanceDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BalanceAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Status"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AmountPastDue"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenOrClosed"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""RevolvingOrInstallment"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ConsumerComment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AccountNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SpecialPayment"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SpecialPaymentDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SpecialPaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Balloon"">
        <xsd:complexType>
          <xsd:attribute name=""dueDate"" type=""xsd:string"" use=""required"" />
          <xsd:attribute name=""amount"" type=""xsd:string"" use=""required"" />
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthsHistory"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver30Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver60Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver90Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DerogCounter"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Collateral"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaymentProfile"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""84"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthlyPaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthlyPaymentType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""LastPaymentDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Subcode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""KOB"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SubscriberDisplayName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""OriginalCreditorName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SoldToName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DisputeFlag"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MaxPayment"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""FirstDelinquencyDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SecondDelinquencyDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""EnhancedPaymentData"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""InitialPaymentLevelDate"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""AccountCondition"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""PaymentStatus"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""AccountType"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""SpecialComment"" type=""OptionalCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdditionalTradeInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""AssociationDate"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ActualPaymentAmountInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ActualPaymentAmount"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TradeSpecialPaymentInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""SpecialPaymentCode"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""SpecialPaymentDate"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""SpecialPaymentAmount"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""10"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CreditInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""TermsDuration"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""TermsFrequency"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""ClassCode"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""CreditLimitAmount"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""HighBalanceAmount"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""OriginalLoanAmount"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""ChargeOffAmount"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""SecondaryAgencyCode"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""SecondaryAgencyID"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""ComplianceConditionCode"" type=""xsd:anyType"" />
            <xsd:element minOccurs=""0"" name=""CIICode"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MortgageID"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaymentProfileInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""PaymentProfile"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name=""TrendedData"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Month"" type=""CreditProfile_TrendedDataMonthType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PurchasedPortfolioInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""PurchasedPortfolio"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_TrendedDataMonthType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MonthID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BalanceAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OriginalLoanAmountClimit"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScheduledPaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ActualPaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LastPaymentDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ARF7_StatementType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DateReported"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StatementText"" type=""StatementTextType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""StatementType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DateReported"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StatementText"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InquiryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Date"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Amount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Terms"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AccountNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Subcode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""KOB"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SubscriberDisplayName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DirectCheckType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SubscriberNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberAddress"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberCity"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberState"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberZip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SubscriberPhone"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FraudServicesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SIC"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Text"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SocialDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SocialCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SocialErrorCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressErrorCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SSNFirstPossibleIssuanceYear"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SSNLastPossibleIssuanceYear"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Indicator"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DateOfBirth"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DateOfDeath"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConsumerAssistanceReferralAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""OfficeName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StreetName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""POBox"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""64"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CityStateZip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""64"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Phone"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProfileSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DisputedAccountsExcluded"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PublicRecordsCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InstallmentBalance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RealEstateBalance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RevolvingBalance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PastDueAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthlyPayment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthlyPaymentPartialFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""RealEstatePayment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RealEstatePaymentPartialFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""RevolvingAvailablePercent"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RevolvingAvailablePartialFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TotalInquiries"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InquiriesDuringLast6Months"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TotalTradeItems"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaidAccounts"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SatisfactoryAccounts"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NowDelinquentDerog"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""WasDelinquentDerog"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OldestTradeOpenDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver30Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver60Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquenciesOver90Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DerogCounter"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""HealthcareProfileSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""OpenRevolvingCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenRevolvingBalance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenRevolvingCreditLimits"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenInstallmentCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenInstallmentBalanceAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OpenInstallmentMonthlyPaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnpaidCollectionsCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UnpaidCollectionsAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesFiledCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesFiledAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesDischargedCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesDischargedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesDismissedCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankruptciesDismissedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LiensCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LiensAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LiensReleasedCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LiensReleasedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Judgments"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JudgmentsAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JudgmentsSatisfiedCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JudgmentsSatisfiedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JudgmentsVacatedCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JudgmentsVacatedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InquiriesCountLast30Days"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InquiriesCountLastTwoYears"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InquiriesAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditTrendsSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RevolvingAccountSummary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MonthIndicator"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TotalRevolvingBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""AverageRevolvingBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""PercentUtilized"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""BankAndNationalCards"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""RetailCards"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""CardWithBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""BankAndTAndECardAccountSummary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MonthIndicator"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TotalBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""AverageBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""PercentUtilized"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfCards"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfCardsWithBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RetailCardAccountSummary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MonthIndicator"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TotalBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""AverageBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""PercentUtilized"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfCards"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfCardsWithBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InstallmentAccountSummary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MonthIndicator"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TotalBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""InstallmentBalanceToLoanPercent"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfTrades"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfTradesWithBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""MortgageAccountSummary"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariationIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MonthIndicator"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TotalBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""8"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""MortgageBalanceToLoanPercent"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfTrades"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""NumberOfTradesWithBalance"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""3"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""StatementTextType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MessageText"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1000"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearch_ConsumerInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""X4_ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DetectType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:sequence>
        <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
        <xsd:choice>
          <xsd:element minOccurs=""0"" name=""FraudSummary"" type=""Detect_FraudSummaryType"" />
          <xsd:element minOccurs=""0"" name=""BureauSummary"" type=""Detect_BureauSummaryType"" />
        </xsd:choice>
      </xsd:sequence>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_FraudSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ReferenceId"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""20"" name=""Rule"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""FraudIndex"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SummaryDataIndicator"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConnectionsSummaryIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RelevantBureauDataSummaryIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InferredAliasDataSummaryIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ExperianDetectSummaryIndicator"" type=""ExperianDetectSummaryIndicatorsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_BureauSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""20"" name=""Rule"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""FraudBureauIndex"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""4"" name=""AdverseActionCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""BureauDataIndicator"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ExperianDetectSummaryIndicator"" type=""ExperianDetectSummaryIndicatorsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ExperianDetectSummaryIndicatorsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SummaryIndicatorCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""Counter"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFDType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:group minOccurs=""0"" ref=""NFD_InformationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""NFD_InformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SummaryInformation"" type=""NFD_SummaryInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DetailInformation"" type=""NFD_DetailInformationType"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""NFD_SummaryInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ReviewReferenceID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RecordHits"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FraudFieldHits"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""VictimRecordHits"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""VictimFieldHits"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByInputAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsBySSN"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByDriversLicense"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FieldHitsByPhone"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFD_DetailInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MemberID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""KindOfBusiness"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""FraudOrVictimCategory"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""LoadDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ApplicationDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FraudDiscoveryDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OwnRecordMatchIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ConsumerOrBusinessMatch"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""InquirySSNStatus"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""SSNMatchStatus"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressMatchIndicator"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""DriversLicenseMatchIndicator"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""InquiryPhoneNumberCount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressMatchConfidenceLevel"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""NFD_AddressMatchType"" />
            <xsd:element minOccurs=""0"" name=""PreviousAddress"" type=""NFD_AddressMatchType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PhoneMatch"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""InquiryPhone1"" type=""NFD_PhoneMatchType"" />
            <xsd:element minOccurs=""0"" name=""InquiryPhone2"" type=""NFD_PhoneMatchType"" />
            <xsd:element minOccurs=""0"" name=""InquiryPhone3"" type=""NFD_PhoneMatchType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFD_AddressMatchType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MatchedFormatCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""MatchConfidenceLevel"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""HouseStreetSuffixMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""PrePostUnitIDMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""CityStateZipMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""UnitTypeZipPlus4MatchCode"" type=""OptionalCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFD_PhoneMatchType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InquiryPhoneType"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone1Type"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone2Type"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""NFDPhone3Type"" type=""OptionalCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPointType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_DataSegmentsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""CheckPoint_DataSegmentsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""GeneralResults"" type=""CheckPoint_GeneralResultsType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""StandardizedAddressDetail"" type=""CheckPoint_StandardizedAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIDVerification"" type=""CheckPoint_SSNAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ResidentialAddressDetail"" type=""CheckPoint_ResidentialAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""BusinessAddressDetail"" type=""CheckPoint_BusinessAddressDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressHighRiskDetail"" type=""CheckPoint_AddressHighRiskDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressHighRiskDescription"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""HighRiskDescription"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ResidentialPhoneDetail"" type=""CheckPoint_ResidentialPhoneDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""BusinessPhoneDetail"" type=""CheckPoint_BusinessPhoneDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PhoneHighRiskDetail"" type=""CheckPoint_PhoneHighRiskDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PhoneHighRiskDescription"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""HighRiskDescription"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ChangeOfAddressDetail"" type=""CheckPoint_ChangeOfAddressDetailType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EDAQuery"" type=""CheckPoint_EDAQueryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EDACaption"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""RecordType"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Caption"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""OriginalInquiryEcho"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""OriginalInput"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ValidationSegment"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""MiddleInitial"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSN"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNIssueState"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNDeceased"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""SSNFormat"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""SSNIssueStartRange"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNIssueEndRange"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNIssueResultCode"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""DriversLicenseState"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DriversLicenseNumber"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DriversLicenseFormatValidation"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""OFACValidationResult"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"">
                    <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
                  </xsd:extension>
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""OFACRecordSegment"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""OFACRecord"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DriversLicenseSegment"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:group ref=""CheckPoint_NameType"" />
            <xsd:group ref=""CheckPoint_AddressType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PreviousAddressSegment"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:group ref=""CheckPoint_AddressType"" />
            <xsd:element minOccurs=""0"" name=""ReportedDate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""LastUpdatedDate"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSNFinderDetail"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Initial"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ZipPlus4"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AreaCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Phone"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""MonthOfBirth"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DayOfBirth"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""YearOfBirth"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DateOfBirthMatchCode"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""InputSSN"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNOnFile"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""SSNResultCode"" type=""OptionalCodeType"" />
            <xsd:element minOccurs=""0"" name=""ReportedDate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""LastTouched"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""CheckPoint_GeneralResultsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ProfileID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AccountInformation"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Version"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PrimaryResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""BestPickAcceptDeny"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""SourceFlag"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""APSSearchCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CheckpointTemplate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NameFlipIndicator"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressVerificationResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressUnitMismatchResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""PhoneVerificationResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""PhoneUnitMismatchResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressTypeResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AddressVerificationResidentialMatches"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressVerificationBusinessMatches"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneVerificationResidentialMatches"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneVerificationBusinessMatches"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressHighRiskResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""PhoneHighRiskResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""COAResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""SSNResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""EDABestPickScore"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""EDARecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AcceptDeclineFlag"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""EDAResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""CheckpointScore"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Result"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""AuditNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ShortAuditNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DriverLicenseResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""MonthOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DayOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""YearOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfBirthMatch"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""OFACResult"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""StandardizedAddressRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ResidentialAddressDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessAddressDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressHighRiskDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressHighRiskDescriptionRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ResidentialPhoneDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessPhoneDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneHighRiskDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneHighRiskDescriptionRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SSNAddressDetailRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ValidationSegmentsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DriverLicenseSegmentsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OFACRecordSegmentsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AuditRequestInformationRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""COARecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""COADescriptionRecordsReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PreviousAddressesReturned"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SSNFinderAddressesReturned"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_StandardizedAddressType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""CheckPoint_NameType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_SSNAddressType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""CheckPoint_NameType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
      <xsd:element minOccurs=""0"" name=""MonthOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DayOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""YearOfBirth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfBirthMatchCode"" type=""OptionalCodeType"" />
      <xsd:element minOccurs=""0"" name=""ReportedDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LastTouched"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_ResidentialAddressType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""CheckPoint_NameType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_SpouseType"" />
      <xsd:element minOccurs=""0"" name=""LastUpdatedDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MonthsAtResidence"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""CheckPoint_NameType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Initial"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:group name=""CheckPoint_AddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipPlus4"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:group name=""CheckPoint_SpouseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SpouseName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OtherHouseholdMemberName1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OtherHouseholdMemberName2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OtherHouseholdMemberName3"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""CheckPoint_BusinessAddressDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""CheckPoint_PhoneType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AreaCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Phone"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""CheckPoint_AddressHighRiskDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_ResidentialPhoneDetailType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""CheckPoint_NameType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_SpouseType"" />
      <xsd:element minOccurs=""0"" name=""LastTouched"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MonthsAtResidence"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_BusinessPhoneDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_PhoneHighRiskDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_ChangeOfAddressDetailType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:element minOccurs=""0"" name=""ReportedDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LastTouched"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPoint_EDAQueryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""RecordType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_AddressType"" />
      <xsd:group minOccurs=""0"" ref=""CheckPoint_PhoneType"" />
      <xsd:element minOccurs=""0"" name=""EDABestPickScore"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategistType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RiskModel"" type=""RiskModelType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SpecialMessage"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AbbreviatedIdentification"" type=""AbbreviatedIdentificationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerAssistanceReferralAddress"" type=""ConsumerAssistanceReferralAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummary"" type=""AutoProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CreditTrendsSummary"" type=""CreditTrendsSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Decision"" type=""DecisionType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Attributes"" type=""AttributesType"" />
      <xsd:element minOccurs=""0"" name=""ProSelect"" type=""ProSelect_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolutionType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" name=""PINVerify"" type=""PINVerifyType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RiskModel"" type=""CS_RiskModelType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" name=""ApplicationSummary"" type=""ApplicationSummaryType"" />
      <xsd:element minOccurs=""0"" name=""XFCCodes"" type=""ProSelect_XFCCode_Type"" />
      <xsd:element minOccurs=""0"" name=""Result"" type=""CustomSolution_Result_Type"" />
      <xsd:element minOccurs=""0"" name=""BackBillingOpportunity"" type=""CustomSolution_BackBillingOpp_Type"" />
      <xsd:element minOccurs=""0"" name=""ChannelMessage"" type=""CustomSolution_ChannelMessage_Type"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerAssistanceReferralAddress"" type=""ConsumerAssistanceReferralAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummary"" type=""AutoProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CreditTrendsSummary"" type=""CreditTrendsSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ModelAttributes"" type=""ModelAttributesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""STAGGSelect"" type=""STAGG_Type"" />
      <xsd:element minOccurs=""0"" name=""TrendView"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Message"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""Attributes"" type=""TrendView_AttributeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TrendViewExclusion"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Exclusion"" type=""RequiredCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PremierAttributes"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Message"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""Attributes"" type=""TrendView_AttributeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Unknown"" type=""UnknownSegmentType"" />
      <xsd:element minOccurs=""0"" name=""ProSelect"" type=""ProSelect_Type"" />
      <xsd:element minOccurs=""0"" name=""PreciseID"" type=""PreciseID_CustomSolutionType"" />
      <xsd:element minOccurs=""0"" name=""CheckPoint"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:group minOccurs=""0"" ref=""CheckPoint_DataSegmentsType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TTYReport"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PreciseIdData"" type=""PreciseIDDataType"" />
      <xsd:element minOccurs=""0"" name=""CreditScoreExceptionNotice"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ExtendedViewAttributes"" type=""Extended_View_Type"" />
      <xsd:element minOccurs=""0"" name=""FifthScoreFactorCode"" type=""FifthScoreFactorCode_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MLAReportType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Unknown"" type=""UnknownSegmentType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ReferenceNumberType"" />
  <xsd:complexType name=""AbbreviatedIdentificationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MiddleInitial"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SpouseInitial"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""GenerationCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StreetNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetInitial"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZIPCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DecisionType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ReferenceId"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Product"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""RecommendedDecision"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ModelTypeIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ApplicantModelIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DecisionReasonIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AssessmentLevel"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PolicyRules"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""20"" name=""Code"">
              <xsd:complexType>
                <xsd:simpleContent>
                  <xsd:extension base=""xsd:string"" />
                </xsd:simpleContent>
              </xsd:complexType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TermsOfBusiness"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ProductStrategyCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ProductCategory"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecommendedLoanOrLimitAmount"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecommendedInterestRate"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecommendedTerms"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecommendedDownPayment"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""RecommendedMonthlyPayment"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomerReference"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""ReferenceNumber"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Scoring"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""DominantModelIndicator"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""Primary"" type=""ScoringType"" />
            <xsd:element minOccurs=""0"" name=""Secondary"" type=""ScoringType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ScoringType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ScoreModelId"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ScoreEvaluation"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ModelScoreFactorCode1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ModelScoreFactorCode2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ModelScoreFactorCode3"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ModelScoreFactorCode4"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AttributesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""5"" name=""Attribute"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariableName"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Value"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ModelAttributesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ModelTypeIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Attribute"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""VariableName"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AttributeSign"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AttributeValue"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_InquiryRedisplayType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SubscriberNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryTransactionNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CPUVersion"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Comments"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Inquiry"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_BusinessSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""RiskCategory"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""JudgmentFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""TaxLienFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""UCCFilingsFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CollectionFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BankFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CorporateFilingStatus"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DateOfIncorporation"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""KeyPersonnel1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""KeyPersonnel2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""KeyPersonnel3"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NumberOfEmployees"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TransactionNumber"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_BusinessNameAndAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ExperianFileNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfileType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ProfileDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfileTime"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TerminalNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FileEstablishDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FileEstablishFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipExtension"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SIC"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ListOfSimilarsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ExperianFileNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RecordSequenceNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessAssociationType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DataContent"" type=""BIS_DataContentType"" />
      <xsd:element minOccurs=""0"" name=""InternationalBusiness"" type=""BIS_InternationalBusinessType"" />
      <xsd:element minOccurs=""0"" name=""ReliabilityCode"" type=""BIS_ReliabilityCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_DataContentType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NumberOfTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FileEstablishDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FileEstablishFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SAndPIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""KeyFactsIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PublicRecordDataIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankDataIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""GovernmentDataIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CollectionIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExecutiveSummaryIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IndustryPremierIndicator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UCCIndicator"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_InternationalBusinessType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InternationalBusinessID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Province"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Country"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PostalCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Class"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ReliabilityCodeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NumberOfTradeLines"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CustomerDisputeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Statement"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ProprietorNameAndAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ProfileType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ProfileDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfileTime"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProprietorName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Title"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ExecutiveSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessDBT"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""PredictedDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PredictedDBTDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IndustryDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AllIndustryDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LowestTotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""HighestTotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentTotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""HighCreditAmountExtended"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MedianCreditAmountExtended"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IndustryPaymentComparison"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""PaymentTrendIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""IndustryDescription"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CommonTerms"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CommonTerms2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CommonTerms3"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_AmountType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Modifier"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Amount"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CollectionDataType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AccountStatus"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DatePlacedForCollection"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateClosed"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AmountPlacedForCollection"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AmountPaid"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CollectionAgencyInfo"" type=""BIS_AgencyInfoType"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_AgencyInfoType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AgencyName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_PaymentExperiencesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PaymentIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BusinessCategory"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateLastActivity"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Terms"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RecentHighCredit"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""AccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentPercentage"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""BIS_DBTGroup"" />
      <xsd:element minOccurs=""0"" name=""Comments"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TradeLineFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NewlyReportedIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""BIS_DBTGroup"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DBT30"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBT60"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBT90"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBT90Plus"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""BIS_PaymentTotalsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NewlyReportedTradeLines"" type=""BIS_TradeLineType"" />
      <xsd:element minOccurs=""0"" name=""ContinouslyReportedTradeLines"" type=""BIS_TradeLineType"" />
      <xsd:element minOccurs=""0"" name=""CombinedTradeLines"" type=""BIS_TradeLineType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_TradeLineType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NumberOfLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotalHighCreditAmount"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""TotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentPercentage"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""BIS_DBTGroup"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_PaymentTrendsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CurrentMonth"">
        <xsd:complexType>
          <xsd:complexContent mixed=""false"">
            <xsd:extension base=""BIS_MonthPaymentType"">
              <xsd:group minOccurs=""0"" ref=""BIS_DBTGroup"" />
            </xsd:extension>
          </xsd:complexContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PriorMonth"">
        <xsd:complexType>
          <xsd:complexContent mixed=""false"">
            <xsd:extension base=""BIS_MonthPaymentType"">
              <xsd:group minOccurs=""0"" ref=""BIS_DBTGroup"" />
            </xsd:extension>
          </xsd:complexContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_MonthPaymentType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Date"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentPercentage"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_IndustryPaymentTrendsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SIC"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CurrentMonth"" type=""BIS_MonthPaymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PriorMonth"" type=""BIS_MonthPaymentType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_QuarterlyPaymentType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotalAccountBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentPercentage"" type=""xsd:string"" />
      <xsd:group minOccurs=""0"" ref=""BIS_DBTGroup"" />
      <xsd:element minOccurs=""0"" name=""QuarterWithinYear"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""YearOfQuarter"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_QuarterlyPaymentTrendsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MostRecentQuarter"" type=""BIS_QuarterlyPaymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PriorQuarter"" type=""BIS_QuarterlyPaymentType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_BankruptcyType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""BIS_PublicInfoGroup"" />
      <xsd:element minOccurs=""0"" name=""LiabilityAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AssetAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExemptAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""BIS_PublicInfoGroup"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DateFiled"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LegalType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""LegalAction"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DocumentNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FilingLocation"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Owner"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""BIS_TaxLienType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""BIS_PublicInfoGroup"" />
      <xsd:element minOccurs=""0"" name=""LiabilityAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TaxLienDescription"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_JudgementOrAttachmentLienType"">
    <xsd:sequence>
      <xsd:group minOccurs=""0"" ref=""BIS_PublicInfoGroup"" />
      <xsd:element minOccurs=""0"" name=""LiabilityAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PlaintiffName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""BIS_UCCInfoType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""StartDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FilingsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FilingsWithDerogatoryCollateral"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ReleasesAndTerminationsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ContinuationsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AmendedAndAssignedTotal"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""BIS_UCCFilingsSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""UCCFilingsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MostRecent6Months"" type=""BIS_UCCFilingsSummarySubType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Previous6Months"" type=""BIS_UCCFilingsSummarySubType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_UCCFilingsSummarySubType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""StartDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FilingsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FilingsWithDerogatoryCollateral"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ReleasesAndTerminationsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ContinuationsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AmendedAndAssignedTotal"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_UCCFilingsType"">
    <xsd:sequence>
      <xsd:group ref=""BIS_PublicInfoGroup"" />
      <xsd:element minOccurs=""0"" name=""CollateralCodes"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Collateral"" type=""RequiredCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OriginalUCCFilingsInfo"" type=""BIS_UCCFilingsInfoType"" />
      <xsd:element minOccurs=""0"" name=""SecuredParty"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Assignee"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_FinanceInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InstitutionType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Relationship"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""InstitutionName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Phone"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfileDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OpenDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CloseDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AccountRating"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BalanceRange"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""FiguresInBalance"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BalanceAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_UCCFilingsInfoType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""LegalAction"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""FilingState"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DocumentNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateFiled"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_LeasingInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""LeasingName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CityAndState"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LeaseCommencementDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OriginalLeaseAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LeaseCloseDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LeaseTerm"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PaymentsPerYear"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PaymentInterval"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""PaymentType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CommentCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CurrentDueDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ScheduledAmountDue"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""CurrentBalanceAmount"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfPaymentsOverdue"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PaymentOverdueAmount"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""TotalRemainingBalance"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfCurrentPayments"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NumberOfLatePayments"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DaysLate30"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DaysLate60"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DaysLate90"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DaysLate90Plus"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CurrentPaymentAmount"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""TotalLatePaymentAmount"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""Amount30DaysLate"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""Amount60DaysLate"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""Amount90DaysLate"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""Amount90PlusDaysLate"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""LeaseProductType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LeaseType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LeaseExtractDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CorporateInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""StateOfOrigin"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OriginalFilingDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RecentFilingDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IncorporatedDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StatusFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StatusDescription"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfitFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CharterNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExistenceTermYears"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExistenceTermDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FederalTaxID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StateTaxID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MergedName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBAName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PriorName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AgentInformation"" type=""BIS_AgentInformationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_AgentInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CorporateOwnerInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""OwnershipIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Phone"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CorporateLinkageInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfSubsidiaries"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NumberOfBranches"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInMidwest"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInNortheast"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInNorthwest"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInSouth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInCentral"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BranchesLocatedInSouthwest"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_CorporateLinkageNameAndAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""RecordType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExperianFileNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IncorporatedDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IncorporationState"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SignificantPublicRecordIndicator"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_DemographicInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PrimarySICCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SICDescription"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SecondarySICCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SecondarySICDescription"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AdditionalSICCodes"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SICCode"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""YearsInBusinessIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""YearsInBusinessOrLowRange"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighRangeYears"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SalesIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SalesRevenueOrLowRange"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighRangeOfSales"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProfitLossIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ProfitLossCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ProfitAmountOrLowRange"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighRangeOfProfit"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NetWorthIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NetWorthAmountOrLowRange"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""HighRangeOrNetWorth"" type=""BIS_AmountType"" />
      <xsd:element minOccurs=""0"" name=""EmployeeIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""EmployeeSizeOrLowRange"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighEmployeeRange"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InBuildingDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BuildingSize"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BuildingOwnership"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Ownership"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Location"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""BusinessType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CustomerCount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FiscalYearMonth"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateFounded"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_KeyPersonnelExecutiveType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""NameFlag"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Title"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_AdditionalNameAndAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DivisionName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PriorName"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_InquiryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InquiryBusinessCategory"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InquiryCount"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Date"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Count"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_DebarredContractorListType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DateReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ContractorID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CompanyBusinessName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CityStateZip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Action"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""OriginalAction"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ExtentOfAction"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Cause"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Agency"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""CrossReference"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Comment"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_StandardAndPoorParentInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ParentName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Address"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FourDigitZip"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_StandardAndPoorTextType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Text"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_IntelliscoreScoreInformation"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PubliclyHeldCompany"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""LimitedProfile"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Filler"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ScoreInfo"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ScoreSign"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ProfileNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ModelInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ModelCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ModelTitle"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""CustomModelCode"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PercentileRanking"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Probability"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Amount"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Text"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Action"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CustomerDispute"" type=""BIS_CustomerDisputeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_IntelliscoreScoreFactors"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ScoreFactor"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_IntelliscoreKeyModelElements"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DaysBeyondTerms"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ContinuousLinesDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NewLinesDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IndustryDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighestDBTInPast6Months"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighestDBTInPast5Quarters"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Previous6MonthsDBT"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""6"" name=""PreviousMonthsDBT"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ContributedAccountDBT"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""DBTLow"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DBTHigh"" type=""xsd:anyType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Previous5QuartersDBT"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""5"" name=""PreviousQuartersDBT"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MonthlyAverageDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""IndustryAverageDBT"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PaymentTrendIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""ActiveAccounts"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""TotalActiveTradePaymentRecords"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AccountBalance"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AllAccounts"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""TotalActiveTradePaymentRecords"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""AccountBalance"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TotalCreditAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SingleHighCreditAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MedianCredit"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""GoodTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BadTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZeroBalanceTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OtherTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Last5QuartersAverageBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DBTZeroTradeLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""JudgmentLiensCollectionLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""JudgmentLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TaxLiensLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CollectionLines"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OriginalUCCFilings"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UCCFilings"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DerogatoryUCCFilings"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TaxLienBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""JudgmentAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LegalAmountTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NumberOfCollectionAccounts"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CollectionAccountAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankruptciesLiensJudgmentsTotal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Past6MonthsInquiries"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Past9MonthsInquiries"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BusinessRegion"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""YearsInFile"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SIC"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AnnualSales"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankAccountsIndicator"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_SmallBusinessKeyModelElements"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ProprietorFirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ProprietorSurname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ExperianCreditRiskScore"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""ScoreType"" type=""RequiredCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SocialSecurityNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NegativeTrades"" type=""BIS_TradeInformation"" />
      <xsd:element minOccurs=""0"" name=""NeutralTrades"" type=""BIS_TradeInformation"" />
      <xsd:element minOccurs=""0"" name=""PositiveTrades"" type=""BIS_TradeInformation"" />
      <xsd:element minOccurs=""0"" name=""TotalTrades"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""OutstandingBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SecuredBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UnsecuredBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MonthlyObligation"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Trades30PlusDPD"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TradeStatus"" type=""BIS_TradeStatus"" />
      <xsd:element minOccurs=""0"" name=""NewestTrade"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RevolvingTrades"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""BankTradesOpenedInLast12Months"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankTradeStatus"" type=""BIS_TradeStatus"" />
      <xsd:element minOccurs=""0"" name=""RealEstateBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RealEstateTrades30PlusDPD"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankCardTradesOpenedInLast12Months"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankCardTradeStatus"" type=""BIS_TradeStatus"" />
      <xsd:element minOccurs=""0"" name=""AutomobileTrades30PlusDPD"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OpenInstallmentLoans"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""RevolvingTrades70PercentBTL"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankCardTrades"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OldestTradeOpenDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TradesOpenedWithin6Months"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""NeverBadRevolvingTrades"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MaxRevolvingLoanAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MaxInstallmentLoanAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OpenBankCardsWith40PercentOutstandingBTL"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PercentTradesNeverDelinquent"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PercentTrades60PlusDPD"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""WorstStatusInPast12Months"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PercentRevolvingTrades"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LegalData"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""CollectionData"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""LegalAndCollectionData"" type=""BIS_TotalTradeInformation"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyCount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DerogatoryPublicRecordCount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HighestCreditAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PercentCurrentOutstandingBalance"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BalanceToLimitRatio"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Accounts90DPD"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CreditLimit"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ScoreCharacteristics"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PercentCurrentTraced"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DisputedTradesCount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""OpenRevolvingPercentOfCreditLimit"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiriesInLast6Months"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_SmallBusinessAdvisorySummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ProprietorBirthDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DwellingType"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DerivedRisk"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RiskScoreFactorCode1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RiskScoreFactorCode2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RiskScoreFactorCode3"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RiskScoreFactorCode4"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_TradeStatus"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Code"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Comment"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_TradeInformation"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Count"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BalanceAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AmountPastDue"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_TotalTradeInformation"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Count"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BalanceAmount"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ConsumerStatementType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DateReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Text"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_NonStandardRecordIndicatorType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ReliabilityCode"" type=""BIS_ReliabilityCodeType"" />
      <xsd:element minOccurs=""0"" name=""Value"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_ProcessingMessage"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ProcessingAction"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""RequiredCodeType"">
    <xsd:simpleContent>
      <xsd:extension base=""xsd:string"">
        <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""OptionalCodeType"">
    <xsd:simpleContent>
      <xsd:extension base=""xsd:string"">
        <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name=""STAGG_Type"">
    <xsd:sequence>
      <xsd:element name=""Message"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""100"" name=""Attribute"" type=""STAGG_AttributeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""STAGG_AttributeType"">
    <xsd:sequence>
      <xsd:element name=""ID"" type=""xsd:string"" />
      <xsd:element name=""Value"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Extended_View_Type"">
    <xsd:sequence>
      <xsd:element name=""Message"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""optional"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""100"" name=""Attribute"" type=""EV_AttributeType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EV_AttributeType"">
    <xsd:sequence>
      <xsd:element name=""ID"" type=""xsd:string"" />
      <xsd:element name=""Value"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FifthScoreFactorCode_Type"">
    <xsd:sequence>
      <xsd:element name=""PrimaryMessageNumber"" type=""xsd:int"" />
      <xsd:element name=""SecondayMessageNumber"" type=""xsd:int"" />
      <xsd:element name=""ModelID"" type=""xsd:string"" />
      <xsd:element name=""ScoreFactorCode"" type=""xsd:string"" />
      <xsd:element name=""ScoreFactorText"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""UnknownSegmentType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""UnknownSegment"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantageType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" name=""Messages"" type=""CollectionAdvantage_MessagesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""StandardOutput"" type=""StandardOutputType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""StandardOutputType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ResponseCodes"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ResultCode"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""ErrorCode"" type=""RequiredCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SSNInformation"" type=""SSNInformationType"" />
      <xsd:element minOccurs=""0"" name=""BestNameAndAddressInformation"" type=""BestNameAndAddressInformationType"" />
      <xsd:element minOccurs=""0"" name=""SSNDeceased"" type=""SSNDeceasedType"" />
      <xsd:element minOccurs=""0"" name=""FileOnePhones"" type=""FileOnePhonesType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""ModelScores"" type=""ModelScoresType"" />
      <xsd:element minOccurs=""0"" name=""CreditAttributes"" type=""CreditAttributesType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyInformation"" type=""BankruptcyInformationType"" />
      <xsd:element minOccurs=""0"" name=""AdditionalNamesPriorAddresses"" type=""AdditionalNamesPriorAddressesType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""SegmentID"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""ResultCode"" type=""RequiredCodeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FCRAAttributes"" type=""FCRAAttributesType"" />
      <xsd:element minOccurs=""0"" name=""OpenTrades"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""NumberOfTradelines"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""OpenTrade"" type=""OpenTradeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MostRecentInquiries"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""MostRecentInquiry"" type=""MostRecentInquiryType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MetroNet"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""SegmentID"" type=""RequiredCodeType"" />
            <xsd:element minOccurs=""0"" name=""MetroNetPhoneAndChangeOfAddress"" type=""MetroNetPhoneAndChangeOfAddressType"" />
            <xsd:element minOccurs=""0"" name=""MetroNetBestPhoneAndChangeOfAddress"" type=""MetroNetBestPhoneAndChangeOfAddressType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SSNInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ValidationResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FraudShieldResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AppendResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BestFileOneSSN"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""AdditionalFileOneSSN"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BestNameAndAddressInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BestNameAndAddressResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DemographicResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AddressFraudIndicator"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""YearOfBirthDemo"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfBirthDemo"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestSurname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestFirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestMiddleNameOrInitial"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestSuffix"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestStreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestCity"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestState"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BestZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DwellingTypeDemo"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CountyCodeDemo"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CensusGEOCodeDemo"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateAddressFirstReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateAddressLastUpdated"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SourceOfTheAddress"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NumberOfTimesAddressReported"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SSNDeceasedType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DeceasedResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DateOfBirthOfDeceased"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfDeath"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FileOnePhonesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PhonesMatchResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AppendResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""FileOnePhone"" type=""FileOnePhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FileOnePhoneType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AreaCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Number"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneSource"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PhoneType"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ToFileDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateLastUpdated"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressLine1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressLine2"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressLine3"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ToFileDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateLastUpdated"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ModelScoresType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AppendResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ModelScore"" type=""ModelScoreType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ModelScoreType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ModelCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""4"" name=""ScoreFactorCode"" type=""xsd:anyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditAttributesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AppendResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Q24DEC"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24BKC7"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Q24BKC11"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Q24BKC13"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Q24LSAT1"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24DG01"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TD01"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TD02"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TD03"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TD04"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TC01"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TC02"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAL15"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TMT11"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TMT12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24THE11"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24THE12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TIN12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TRV11"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TRV12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TRV13"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TBC13"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TBC14"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAU12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAU13"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAE12"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAE13"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TAL21"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TRV22"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TIB22"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24TFN31"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24IAL01"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Q24IAL02"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BankruptcyInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScreenResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StatusCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""StatusDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CourtCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CourtName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DocketNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CreditorName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ECOACode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AssetAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""LiabilitiesAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Repayment"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Adjustment"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BookPage"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""OriginalFilingDate"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AdditionalNamesPriorAddressesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdditionalNamesResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdditionalAddressesResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""10"" name=""AdditionalName"" type=""AdditionalNameType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""10"" name=""PriorAddress"" type=""PriorAddressType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AdditionalNameType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MiddleName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Suffix"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Type"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PriorAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateAddressFirstReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateOfLastUpdate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SourceOfTheAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TimesAddressReported"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CADriverLicenseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ResultCode"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FCRAAttributesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConsumerStatementIndicator"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VoluntaryBKWithdrawnByConsumer"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Chapter7BKReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Chapter11BKReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Chapter12BKReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Chapter13BKReported"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AccountsClosedByConsumer"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AccountsDisputedByConsumer"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressMismatchIndicator"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RecordSizeIndicator"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AdditionalAttributesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SegmentID"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""AppendResultCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfTradelines"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OpenTradeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SubscriberPurposeType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""SubscriberName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CreditAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""BalanceAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AvailableCreditAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ReportedDate"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MostRecentInquiryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""SubscriberName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryPurpose"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""InquiryTerms"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MetroNetPhoneAndChangeOfAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PhoneAppendResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ChangeOfAddressResultCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DAPPrimaryResultCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DAPResultFlag"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""EDASearchResultCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""COAResultCode"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MetroNetBestPhoneAndChangeOfAddressType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""EDAListingType"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AreaCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""SupplierCode"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DateLastUpdated"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Surname"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""MiddleName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Suffix"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HouseNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PreDirectional"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PostDirectional"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""AddressSuffix"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UnitDesignator"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UnitNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip4"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DateAddressLastUpdated"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""BestEDAPhoneNumber"" type=""BestEDAPhoneNumberType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BestEDAPhoneNumberType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""EDABestPickScoreValue"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ListingType"">
        <xsd:complexType>
          <xsd:simpleContent>
            <xsd:extension base=""xsd:string"">
              <xsd:attribute name=""code"" type=""xsd:string"" use=""required"" />
            </xsd:extension>
          </xsd:simpleContent>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AreaCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""PhoneNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""StreetAddress"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""City"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""State"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ZipCode"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Zip4"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Type"">
    <xsd:choice>
      <xsd:element name=""SoftInquiry"" type=""ProSelect_Backend_Type"" />
      <xsd:element name=""BillingEvent"" type=""ProSelect_Backend_Type"" />
      <xsd:element name=""ProSelectPrescreen"" type=""ProSelect_Prescreen_Type"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_LenderList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Lender"" type=""ProSelect_Lender_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_AttributeList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Attribute"" type=""ProSelect_Attribute_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_ProductList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Product"" type=""ProSelect_Product_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Lender_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""ProductList"" type=""ProSelect_ProductList_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Product_Type"">
    <xsd:sequence>
      <xsd:element name=""ProductID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Approved"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[YN]{1}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TermsCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""80"" />
            <xsd:pattern value=""[A-Za-z0-9/]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_XFCCode_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""XFC01"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""99"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC02"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC03"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC04"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC05"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC06"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""99"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFC07"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""99"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""ProSelect_AddressMismatch_Type"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""1"" />
      <xsd:pattern value=""[YN]{1}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ProSelect_Attribute_Type"">
    <xsd:sequence>
      <xsd:element name=""Name"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""6"" />
            <xsd:maxLength value=""6"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Value"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""9"" />
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Backend_Type"">
    <xsd:choice>
      <xsd:element name=""ReturnCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Prescreen_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderList"" type=""ProSelect_LenderList_Type"" />
      <xsd:element name=""XFCCodes"" type=""ProSelect_XFCCode_Type"" />
      <xsd:element minOccurs=""0"" name=""AddressMismatchFlag"" type=""ProSelect_AddressMismatch_Type"" />
      <xsd:element minOccurs=""0"" name=""AttributeList"">
        <xsd:complexType>
          <xsd:complexContent mixed=""false"">
            <xsd:extension base=""ProSelect_AttributeList_Type"" />
          </xsd:complexContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:group name=""Helio_RiskModelTypeGroup"">
    <xsd:sequence>
      <xsd:element name=""ModelName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:group name=""Risk_ModelTypeGroup"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ModelIndicator"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Score"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeOne"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeTwo"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeThree"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ScoreFactorCodeFour"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Evaluation"" type=""RequiredCodeType"" />
    </xsd:sequence>
  </xsd:group>
  <xsd:complexType name=""ApplicationSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Decision"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""DecisionCode"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""DecisionDescription"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""XFCCodes"" type=""ProSelect_XFCCode_Type"" />
      <xsd:element minOccurs=""0"" name=""AddressMismatchFlag"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""Y"" />
            <xsd:enumeration value=""N"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_MessagesType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""ConsumerStatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""ConsumerStatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerStatement"" type=""CA_StatementType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConsumerStatementType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""StatusDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""ConsumerComment"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CS_RiskModelType"">
    <xsd:choice>
      <xsd:group ref=""Risk_ModelTypeGroup"" />
      <xsd:group ref=""Helio_RiskModelTypeGroup"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CA_StatementType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Type"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""DateReported"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Statement"" type=""xsd:anyType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AutoProfileSummaryType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""TotOpenLoanLeaseBal"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotOpenLoanLeaseMnthPayAmt"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotNumLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotNumOpenLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotNum30DayDelLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotNum60DayDelLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""TotNum90DayDelLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""WorstDelLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""WorstDelDateLoansLeases"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummaryDetail"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ItemNumber"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""FinancedAmount"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""MonthlyPayAmount"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Term"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""NumberMonthsRemaining"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""StatusText"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_DemographicsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ToDBDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UpdateDBDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""EnhancedHouseholdIncomeAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""EnhancedHouseholdIncomeCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfPeopleInHousehold"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""HouseholdCompositionCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""EstimatedHouseholdIncomeAmount"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""EstimatedHouseholdIncomeCode"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""NumberOfChildren"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_LivingUnitRelationshipType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ToDBDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""UpdateDBDate"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""FirstName"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Gender"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""EstimatedAge"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""RelationshipType"" type=""RequiredCodeType"" />
      <xsd:element minOccurs=""0"" name=""Occupation"" type=""xsd:string"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""TrendView_AttributeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Attribute"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Name"" type=""xsd:string"" />
            <xsd:element minOccurs=""0"" name=""Value"" type=""xsd:string"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DebtSettlementReportType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""RiskModel"" type=""RiskModelType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ModelAttributes"" type=""ModelAttributesType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseIDDataType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" ref=""KBA"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element name=""KBA"">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element ref=""XMLVersion"" />
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
  <xsd:element name=""SessionID"" type=""SessionID_Type"" />
  <xsd:simpleType name=""SessionID_Type"">
    <xsd:restriction base=""xsd:string"">
      <xsd:whiteSpace value=""collapse"" />
      <xsd:pattern value=""([a-zA-Z0-9# \- / \.]{1,120}){1}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:element default=""02"" name=""XMLVersion"" type=""XMLVersion_Type"" />
  <xsd:simpleType name=""XMLVersion_Type"">
    <xsd:restriction base=""xsd:string"">
      <xsd:whiteSpace value=""collapse"" />
      <xsd:pattern value=""(1|01|2|02|5|05)?"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""CustomSolution_Result_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ResultCode"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""ProductType"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:length value=""2"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""TransactionStatus"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:enumeration value=""AL"" />
                  <xsd:enumeration value=""PC"" />
                  <xsd:enumeration value=""ER"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
            <xsd:element minOccurs=""0"" name=""StatusCode"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""4"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Decision"" type=""xsd:anyType"" />
      <xsd:element minOccurs=""0"" name=""ConsumerAlertFlag"" type=""xsd:anyType"" />
      <xsd:element minOccurs=""0"" name=""FACTAFlag"" type=""xsd:anyType"" />
      <xsd:element minOccurs=""0"" name=""FCRAAttributes"" type=""ProSelect_XFCCode_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolution_BackBillingOpp_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ConditionCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:length value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FirstAddressAssociationDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:length value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LatestAddressAssociationDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:length value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConfidenceIndicator"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolution_ChannelMessage_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""MessageCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""3000"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ChannelCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""CIMS"" />
            <xsd:enumeration value=""INTN"" />
            <xsd:enumeration value=""IVR"" />
            <xsd:enumeration value=""LLRD"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OutputMessage"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""360"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsightType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Error"" type=""ErrorType"" />
      <xsd:element minOccurs=""0"" name=""Header"" type=""HeaderType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerIdentity"" type=""ConsumerIdentityType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AddressInformation"" type=""AddressInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""EmploymentInformation"" type=""EmploymentInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""PublicRecord"" type=""PublicRecordType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""TradeLine"" type=""CreditProfile_TradeLineType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Inquiry"" type=""InquiryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""InformationalMessage"" type=""InformationalMessageType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""Statement"" type=""ARF7_StatementType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""DirectCheck"" type=""DirectCheckType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""FraudServices"" type=""FraudServicesType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ConsumerAssistanceReferralAddress"" type=""ConsumerAssistanceReferralAddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""ProfileSummary"" type=""ProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""AutoProfileSummary"" type=""AutoProfileSummaryType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""unbounded"" name=""CreditTrendsSummary"" type=""CreditTrendsSummaryType"" />
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public ARFXMLResponse() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [5];
                _RootElements[0] = "Products";
                _RootElements[1] = "CreditProfile";
                _RootElements[2] = "KBA";
                _RootElements[3] = "SessionID";
                _RootElements[4] = "XMLVersion";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://www.experian.com/ARFResponse",@"Products")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Products"})]
        public sealed class Products : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Products() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Products";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/ARFResponse",@"CreditProfile")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CreditProfile"})]
        public sealed class CreditProfile : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CreditProfile() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CreditProfile";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/ARFResponse",@"KBA")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"KBA"})]
        public sealed class KBA : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public KBA() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "KBA";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/ARFResponse",@"SessionID")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"SessionID"})]
        public sealed class SessionID : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public SessionID() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "SessionID";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/ARFResponse",@"XMLVersion")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"XMLVersion"})]
        public sealed class XMLVersion : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public XMLVersion() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "XMLVersion";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
