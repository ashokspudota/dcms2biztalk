namespace ExperianCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"Request", @"CreditProfile", @"XMLVersion"})]
    public sealed class WebDelivery : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xsd:schema xmlns=""http://www.experian.com/WebDelivery"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" elementFormDefault=""qualified"" targetNamespace=""http://www.experian.com/WebDelivery"" version=""2.608"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <xsd:element name=""Request"" type=""RequestType"" />
  <xsd:complexType name=""RequestType"">
    <xsd:sequence>
      <xsd:element name=""Products"">
        <xsd:complexType>
          <xsd:choice>
            <xsd:element ref=""CreditProfile"" />
          </xsd:choice>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
    <xsd:attribute name=""version"" type=""xsd:string"" />
  </xsd:complexType>
  <xsd:complexType name=""PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PrimaryApplicantWithPINType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"">
        <xsd:simpleType>
          <xsd:restriction base=""SSNType"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""Truvue_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""Fraud_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""MothersMaidenName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z\-\s']*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NFD_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""Fraud_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""EmailAddress"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""5"" />
            <xsd:maxLength value=""60"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SecondaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SecondaryApplicantWithPINType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CS_SecondaryApplicantWithPINType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Fraud_SecondaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""Fraud_SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Fraud_SecondaryApplicantWithPINType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""Fraud_SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SecondaryApplicant_NameType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z\-\s']*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z\s]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Fraud_SecondaryApplicant_NameType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z\-\s']*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AccountTypeType"">
    <xsd:sequence>
      <xsd:element name=""Type"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""^\s?$|[A-Za-z0-9]{2}|\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Terms"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""^\s?$|[A-Za-z0-9]{3}|\.\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FullAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:decimal"">
            <xsd:maxInclusive value=""9999999"" />
            <xsd:pattern value=""[0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AbbreviatedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""^\s?$|[0-9]{3}|\.\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewAlways"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomModel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""STAGGSelect"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""HealthcareProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EmergingCreditProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AutoProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SSNIndicators"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AdditionalTradeInformation"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditScoreExceptionNotice"" type=""RiskBasedPricingType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Custom_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewAlways"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CustomSolution_RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomModel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""STAGGSelect"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""HealthcareProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EmergingCreditProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AutoProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SSNIndicators"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TimeoutCount"" type=""TimeoutCountType"" />
      <xsd:element minOccurs=""0"" name=""CreditScoreExceptionNotice"" type=""RiskBasedPricingType"" />
      <xsd:element minOccurs=""0"" name=""ActualPaymentAmount"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ExpandedHistory"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DeferredPaymentInformation"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BureauInquiry"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""512"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MLA"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewAlways"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CreditProfile_RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomModel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""STAGGSelect"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""HealthcareProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EmergingCreditProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AutoProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SSNIndicators"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AdditionalTradeInformation"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditScoreExceptionNotice"" type=""RiskBasedPricingType"" />
      <xsd:element minOccurs=""0"" name=""ActualPaymentAmount"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ExpandedHistory"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DeferredPaymentInformation"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MLA"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Trend"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""TimeoutCountType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""1"" />
      <xsd:pattern value=""[0-9]"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""DemographicBandType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""DemographicsAll"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicPhone"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicHomeownerCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicDriverLicense"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicGeoCode"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NameType"">
    <xsd:annotation>
      <xsd:documentation />
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\.\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\.\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z\-'\.\s]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Fraud_NameType"">
    <xsd:annotation>
      <xsd:documentation />
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_NameType"">
    <xsd:annotation>
      <xsd:documentation />
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_NameType"">
    <xsd:annotation>
      <xsd:documentation />
    </xsd:annotation>
    <xsd:sequence minOccurs=""0"">
      <xsd:element name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""65"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\s'\-]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z\s'\-]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AddressType"">
    <xsd:sequence minOccurs=""0"">
      <xsd:element minOccurs=""0"" name=""Street"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""68"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#/\.']*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""City"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""38"" />
            <xsd:pattern value=""[A-Za-z\s\-\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\-]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DriversLicType"">
    <xsd:sequence minOccurs=""0"">
      <xsd:element name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""21"" />
            <xsd:pattern value=""[A-Za-z0-9@#\*\+\s]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentType"">
    <xsd:sequence>
      <xsd:element name=""Company"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""23"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#\.&amp;']+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Address"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""25"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""City"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""11"" />
            <xsd:pattern value=""[A-Za-z0-9\s\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\-]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""AgeType"">
    <xsd:restriction base=""xsd:integer"">
      <xsd:maxInclusive value=""120"" />
      <xsd:minInclusive value=""14"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""DOBType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:pattern value=""[0-9]{8}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""YOBType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:pattern value=""[0-9]{4}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""ConnectCheck_YOBType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""4"" />
      <xsd:pattern value=""[0-9]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""FileUnfreezePINType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""15"" />
      <xsd:pattern value=""[A-Za-z0-9]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""ChoiceType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""Y"" />
      <xsd:enumeration value=""N"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""BooleanType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""T"" />
      <xsd:enumeration value=""F"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""OriginalCreditGrantorType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""30"" />
      <xsd:pattern value=""[A-Za-z0-9\*\.\-\s\(\)'#&amp;]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""OpenDateType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""8"" />
      <xsd:pattern value=""(UNK)|([0-9]*)"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""MessageType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""68"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""AccountNoType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""23"" />
      <xsd:pattern value=""[A-Za-z0-9]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""OccurrenceDateType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""8"" />
      <xsd:pattern value=""([0-9]*)|(UNK)"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""StatusType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:pattern value=""[A-Za-z0-9]{2}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ParallelProfileType"">
    <xsd:sequence>
      <xsd:element name=""FormatType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""1"" />
            <xsd:enumeration value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Heading"" type=""ChoiceType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""ProfileIDType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""10"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""SearchRequestType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""C"" />
      <xsd:enumeration value=""S"" />
      <xsd:enumeration value=""U"" />
      <xsd:enumeration value=""V"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""LevelOfDetailType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""B"" />
      <xsd:enumeration value=""F"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""VersionType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""2"" />
      <xsd:pattern value=""[A-Za-z0-9]+"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""EDABestPickCutOffType"">
    <xsd:restriction base=""xsd:short"">
      <xsd:minInclusive value=""00"" />
      <xsd:maxInclusive value=""89"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""NoOfEDAListingsType"">
    <xsd:restriction base=""xsd:short"">
      <xsd:minInclusive value=""00"" />
      <xsd:maxInclusive value=""10"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""InquiryTypeType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""X"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ProductRequestType"">
    <xsd:choice>
      <xsd:element name=""Checkpoint"" type=""ChoiceType"" />
      <xsd:element name=""CreditProfileAndCheckpointDetail"" type=""ChoiceType"" />
      <xsd:element name=""CreditProfileAndCheckpointSummary"" type=""ChoiceType"" />
      <xsd:element name=""DecisioningDetail"" type=""ChoiceType"" />
      <xsd:element name=""DecisioningSummary"" type=""ChoiceType"" />
      <xsd:element name=""DecisioningEmulation"" type=""ChoiceType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeJointW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""RBP_RiskModelsType"">
    <xsd:choice>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Bankcard"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3Positive"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeJointW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FinancialAssistanceCheckerModel"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BustOutScore"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolution_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Bankcard"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3Positive"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeJointW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BustOutScore"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3Positive"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeJointW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8Bank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO9Bankcard"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3Positive"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncome"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalDebtToIncomeJointW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FirstMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TotalMtgDebtToIncomeW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""PhoneType"">
    <xsd:sequence minOccurs=""0"">
      <xsd:element name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""13"" />
            <xsd:pattern value=""[A-Za-z0-9\.\-\s#\(\)]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Type"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[A-Za-z0-9]"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessPhoneType"">
    <xsd:sequence minOccurs=""0"">
      <xsd:element name=""Number"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""13"" />
            <xsd:pattern value=""[A-Za-z0-9\.\-\s#\(\)]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""SSNType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""11"" />
      <xsd:pattern value=""([0-9]{3}((-[0-9]{2}-[0-9]{4})|([0-9]{6})))?"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""PID_SSNType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""11"" />
      <xsd:pattern value=""([0-9]{3}((-[0-9]{2}-[0-9]{4})|([0-9]{6})|([0-9]{1})))?"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""TTYType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Heading"" type=""ChoiceType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ParallelProfile"" type=""ParallelProfileType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NoPP_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ParallelProfile"" type=""ParallelProfileType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""SubscriberType"">
    <xsd:sequence>
      <xsd:element name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[A-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""OpInitials"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:minLength value=""2"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""SubCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BIS_SubscriberType"">
    <xsd:sequence>
      <xsd:element name=""OpInitials"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:minLength value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""SubCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""BusinessNameType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""40"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""TitleType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""10"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""TINType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:pattern value=""[0-9]{9}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""BISFileNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""15"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""TransactionNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""10"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""SequenceNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""2"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""BISListNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""17"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""DataSegmentType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""20"" />
      <xsd:minLength value=""20"" />
      <xsd:pattern value=""[Y|N]{20}"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""RiskModelCodeType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""6"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""PMIdType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""10"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""MaxType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""6"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:element name=""CreditProfile"" type=""CreditProfileType"" />
  <xsd:complexType name=""CreditProfileType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CreditProfile_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""CreditProfile_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""CreditProfile_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""CreditProfile_ARFType"" />
      <xsd:element name=""XML"" type=""CreditProfile_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:minInclusive value=""0"" />
            <xsd:maxInclusive value=""9999999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EncryptedPIN"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolution_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:minInclusive value=""0"" />
            <xsd:maxInclusive value=""9999999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CustomInformation"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3000"" />
            <xsd:pattern value=""[A-Za-z0-9]+[A-Za-z0-9\s#@'%:`/=$_!~\\\.\-\?\*\(\)\[\]&amp;&quot;&lt;&gt;+]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomInformation2"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6000"" />
            <xsd:pattern value=""[A-Za-z0-9\._]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomInformation3"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9000"" />
            <xsd:pattern value=""[A-Za-z0-9\s_\-]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomInformation4"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""12000"" />
            <xsd:pattern value=""[A-Za-z0-9/=+]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BACD"" type=""BACD_Options_Type"" />
      <xsd:element minOccurs=""0"" name=""USBank"" type=""USBank_Options_Type"" />
      <xsd:element minOccurs=""0"" name=""ProSelect"" type=""ProSelect_Options_Type"" />
      <xsd:element minOccurs=""0"" name=""EXPVFY"" type=""EXPVFY_Options_Type"" />
      <xsd:element minOccurs=""0"" name=""ResponseInfo"" type=""ResponseInfo_Type"" />
      <xsd:element minOccurs=""0"" name=""EncryptedPIN"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearchType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element maxOccurs=""20"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""SocialSearch_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""SocialSearch_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""SocialSearch_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearch_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearch_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""SocialSearch_OutputType_ARFType"" />
      <xsd:element name=""XML"" type=""XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearch_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""SocialSearch_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReportType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CollectionReport_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""CollectionReport_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""CollectionReport_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CollectionReport_RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""CollectionReport_OutputType_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ParallelProfile"" type=""CollectionReport_ParallelProfileType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportType"" type=""CollectionReport_ReportTypeType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""InstantUpdateType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""InstantUpdate_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""InstantUpdate_SecondaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""OriginalCreditGrantor"" type=""OriginalCreditGrantorType"" />
      <xsd:element name=""OpenDate"" type=""OpenDateType"" />
      <xsd:element name=""AccountNo"">
        <xsd:simpleType>
          <xsd:restriction base=""AccountNoType"">
            <xsd:minLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""OccurrenceDate"" type=""OccurrenceDateType"" />
      <xsd:element name=""Status"" type=""StatusType"" />
      <xsd:element name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element name=""OutputType"" type=""InstantUpdate_OutputTypeType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""InstantUpdate_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""VendorType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InstantUpdate_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InstantUpdate_SecondaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InstantUpdate_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""InstantUpdate_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""FullAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:decimal"">
            <xsd:maxInclusive value=""9999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SpecialCommentCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AssociationCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""SubscriberAlertMessageType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""SubscriberAlertMessage_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SubscriberAlertMessage_SecondaryApplicantType"" />
      <xsd:element name=""Message"" type=""MessageType"" />
      <xsd:element name=""OutputType"" type=""SubscriberAlertMessage_OutputTypeType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""VendorType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SubscriberAlertMessage_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SubscriberAlertMessage_SecondaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""SubscriberAlertMessage_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""AccessPointRetailType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""AccessPointRetail_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element name=""ProductRequest"" type=""ProductRequestType"" />
      <xsd:element name=""CheckPointOptions"" type=""CheckPointOptionsType"" />
      <xsd:element name=""OutputType"" type=""AccessPointRetail_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""ReferenceNumberType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AccessPointRetail_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CheckPointOptionsType"">
    <xsd:sequence>
      <xsd:element name=""ProfileID"">
        <xsd:simpleType>
          <xsd:restriction base=""ProfileIDType"">
            <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""IntegratedSolution"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SearchRequest"" type=""SearchRequestType"" />
      <xsd:element minOccurs=""0"" name=""LevelOfDetail"" type=""LevelOfDetailType"" />
      <xsd:element minOccurs=""0"" name=""Version"" type=""VersionType"" />
      <xsd:element minOccurs=""0"" name=""EchoInput"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EDAFallThru"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EDABestPickCutOff"" type=""EDABestPickCutOffType"" />
      <xsd:element minOccurs=""0"" name=""NoOfEDAListings"" type=""NoOfEDAListingsType"" />
      <xsd:element minOccurs=""0"" name=""ScoreFlag"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""InquiryType"" type=""InquiryTypeType"" />
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ValidationFlag"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:enumeration value=""Y"" />
            <xsd:enumeration value=""N"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PrevAddrSegment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:enumeration value=""Y"" />
            <xsd:enumeration value=""N"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AccessPointRetail_OutputType"">
    <xsd:sequence>
      <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AddressUpdateType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""AddressUpdate_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""AddressUpdate_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""AddressUpdate_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""AddressUpdate_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""AddressUpdate_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""AddressUpdate_OutputType_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""AddressUpdate_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""AddressUpdate_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EncryptedPIN"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CrossViewType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""CrossView_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CrossView_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""CrossView_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"">
        <xsd:complexType>
          <xsd:complexContent mixed=""false"">
            <xsd:extension base=""CrossView_OptionsType"" />
          </xsd:complexContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CrossView_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CrossView_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CrossView_AddOns_RiskModelsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CrossView_AddOns_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CrossView_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CrossView_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsightType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""EmploymentInsight_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""EmploymentInsightAccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""EmploymentInsight_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""EmploymentInsight_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""EmploymentInsight_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsightAccountTypeType"">
    <xsd:sequence>
      <xsd:element name=""Type"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""35"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Terms"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""^\s?$|[A-Za-z0-9]{3}|\.\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""FullAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:decimal"">
            <xsd:maxInclusive value=""9999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AbbreviatedAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""^\s?$|[0-9]{3}|\.\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""ARF"" type=""EmploymentInsight_OutputType_ARFType"" />
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""XML"" type=""EmploymentInsight_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConsumerCopy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""DecodeType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""Decode_SubscriberType"" />
      <xsd:element maxOccurs=""16"" name=""SubNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{7}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Decode_SubscriberType"">
    <xsd:sequence>
      <xsd:element name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""SubCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{7}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BullseyeType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""Bullseye_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""Bullseye_SecondaryApplicantType"" />
      <xsd:element name=""OutputType"" type=""Bullseye_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""Bullseye_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Bullseye_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Bullseye_SecondaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Bullseye_OutputTypeType"">
    <xsd:sequence>
      <xsd:element name=""TTY"" type=""TTYType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Bullseye_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""AccountNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""23"" />
            <xsd:minLength value=""1"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BureauPreamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""InstantPrescreenType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""InstantPrescreen_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""InstantPrescreenId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
            <xsd:minLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""OutputType"" type=""InstantPrescreen_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""InstantPrescreen_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InstantPrescreen_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""InstantPrescreen_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""InstantPrescreen_OutputType_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""InstantPrescreen_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""InstantPrescreen_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheckType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""ConnectCheck_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""ConnectCheck_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""ConnectCheck_OutputTypeType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""ConnectCheck_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:choice maxOccurs=""2"">
        <xsd:element name=""SSN"" type=""SSNType"" />
        <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""ConnectCheck_YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_OutputTypeType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""ConnectCheck_OutputType_TTYType"" />
      <xsd:element name=""ARF"" type=""ConnectCheck_OutputType_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_OutputType_TTYType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Heading"" type=""ChoiceType"" />
      <xsd:choice>
        <xsd:element name=""TTYSummary"" type=""ChoiceType"" />
        <xsd:element name=""TTYDetail"" type=""ChoiceType"" />
        <xsd:element name=""TTYFull"" type=""ChoiceType"" />
      </xsd:choice>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ConnectCheck_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ConnectCheckGen3"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""VendorType"">
    <xsd:sequence>
      <xsd:element name=""VendorNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:minLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VendorVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""TruvueType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""Truvue_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""Truvue_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""Truvue_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""Truvue_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
      <xsd:element name=""XML"" type=""XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditProfileAddOn"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""LivingUnitRelationship"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Truvue_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""GetPin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EmailAddress"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:minLength value=""5"" />
            <xsd:pattern value=""^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfileType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""BusinessApplicantType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""BusinessProfile_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfile_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions3_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessOwnerProfileType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""BusinessApplicantType"" />
      <xsd:element name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""BusinessOwnerProfile_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfileV6_OutputType"" />
      <xsd:element name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions3_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfileListOfSimilarsType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""BusinessApplicantListType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""LOS_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfile_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions3_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessSummaryType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""FRPT_BusinessApplicantType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""BusinessSummary_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfileV6_OutputType"" />
      <xsd:element name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions2_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""IntelliscoreType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""FRPT_BusinessApplicantType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""Intelliscore_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfile_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions2_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DataSegmentsType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""BusinessApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""DataSegments_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""DataSegments_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions5_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""IndustryTradeProfileType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""FRPT_BusinessApplicantType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"" type=""BusinessOwnerType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryOwner"" type=""SecondaryOwnerType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""IndustryTradeProfile_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfile_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions2_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""UCCDetailType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""BusinessApplicant"" type=""UCCDetail_BusinessApplicantType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfile_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions1_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PortfolioMonitorSummaryType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfileV6_OutputType"" />
      <xsd:element name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions1_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PortfolioMonitorListType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element minOccurs=""0"" name=""WarningList"" type=""WarningType"" />
      <xsd:element minOccurs=""0"" name=""CRAD"" type=""ChoiceType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfileV6_OutputType"" />
      <xsd:element name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions1_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PortfolioMonitorDetailType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""BIS_SubscriberType"" />
      <xsd:element minOccurs=""0"" name=""WarningNotification"" type=""WarningType"" />
      <xsd:element minOccurs=""0"" name=""PMId"" type=""PMIdType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""PortfolioMonitor_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""BusinessProfileV6_OutputType"" />
      <xsd:element name=""Vendor"" type=""Business_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""BISOptions1_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""WarningType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""All"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Legal"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Payment"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Derog"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Received"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NotReceived"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CurrentWeeks"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""LastWeeks"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Max"" type=""MaxType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Business_VendorType"">
    <xsd:sequence>
      <xsd:element name=""VendorNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6"" />
            <xsd:minLength value=""6"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VendorVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfile_OutputType"">
    <xsd:sequence>
      <xsd:choice minOccurs=""0"">
        <xsd:element name=""Text"" type=""ChoiceType"" />
        <xsd:element name=""Both"" type=""ChoiceType"" />
        <xsd:element name=""XML"" type=""BIS_XMLType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" name=""CPU"" type=""BusinessProfile_CPUType"" />
      <xsd:element minOccurs=""0"" name=""EndInd"" type=""EndIndType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfileV6_OutputType"">
    <xsd:sequence>
      <xsd:choice minOccurs=""0"">
        <xsd:element name=""Text"" type=""ChoiceType"" />
        <xsd:element name=""Both"" type=""ChoiceType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" name=""CPU"" type=""BusinessProfileV6_CPUType"" />
      <xsd:element minOccurs=""0"" name=""EndInd"" type=""EndIndType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DataSegments_OutputType"">
    <xsd:sequence>
      <xsd:element name=""CPU"" type=""BusinessProfile_CPUType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfile_CPUType"">
    <xsd:all>
      <xsd:element name=""CPUVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""CPU005"" />
            <xsd:enumeration value=""CPU006"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfileV6_CPUType"">
    <xsd:all>
      <xsd:element name=""CPUVersion"" type=""V6_CPUVersionType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""V6_CPUVersionType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""CPU006"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""EndIndType"">
    <xsd:restriction base=""xsd:int"">
      <xsd:enumeration value=""1"" />
      <xsd:enumeration value=""2"" />
      <xsd:enumeration value=""3"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""BusinessApplicantType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""BusinessNameType"" />
      <xsd:element minOccurs=""0"" name=""AlternateName"" type=""BusinessNameType"" />
      <xsd:element minOccurs=""0"" name=""TaxId"" type=""TINType"" />
      <xsd:element minOccurs=""0"" name=""BISFileNumber"" type=""BISFileNumberType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""5"" name=""BISListNumber"" type=""BISListNumberType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" name=""AlternateAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""Phone"" type=""BusinessPhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""FRPT_BusinessApplicantType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""BusinessNameType"" />
      <xsd:element minOccurs=""0"" name=""AlternateName"" type=""BusinessNameType"" />
      <xsd:element minOccurs=""0"" name=""TaxId"" type=""TINType"" />
      <xsd:element minOccurs=""0"" name=""TransactionNumber"" type=""TransactionNumberType"" />
      <xsd:element minOccurs=""0"" name=""BISFileNumber"" type=""BISFileNumberType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""5"" name=""BISListNumber"" type=""BISListNumberType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" name=""AlternateAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""Phone"" type=""BusinessPhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""UCCDetail_BusinessApplicantType"">
    <xsd:sequence>
      <xsd:element name=""TransactionNumber"" type=""TransactionNumberType"" />
      <xsd:element name=""BISFileNumber"" type=""BISFileNumberType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessApplicantListType"">
    <xsd:sequence>
      <xsd:element name=""TransactionNumber"" type=""TransactionNumberType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""5"" name=""BISListNumber"" type=""BISListNumberType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessOwnerType"" />
  <xsd:complexType name=""SecondaryOwnerType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""OwnerName"" type=""BIS_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BusinessProfile_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""StandAlone"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BusinessOwnerProfile_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""StandAlone"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""LOS_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""BSUM"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ITP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SCORE"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BOP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AllReports"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DataSegment"" type=""DataSegmentType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BusinessSummary_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""BSUM"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""DataSegments_AddOnsType"">
    <xsd:all>
      <xsd:element name=""DataSegment"" type=""DataSegmentType"" />
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""IndustryTradeProfile_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ITP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModelCode"" type=""RiskModelCodeType"" />
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SCORE"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Intelliscore_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""AllReports"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SCORE"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BOP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModelCode"" type=""RiskModelCodeType"" />
      <xsd:element minOccurs=""0"" name=""BSUM"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DataSegment"" type=""DataSegmentType"" />
      <xsd:element minOccurs=""0"" name=""ITP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""List"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MatchCode"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""UCC"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""PortfolioMonitor_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""BP"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SCORE"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DataSegment"" type=""DataSegmentType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BISOptions1_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PageLength"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BISOptions2_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CustomerName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PageLength"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BISOptions3_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CustomerName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PageLength"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TransactionNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BISOptions5_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PageLength"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TransactionNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BusinessApplicant_AddressType"">
    <xsd:sequence minOccurs=""0"">
      <xsd:element minOccurs=""0"" name=""Street"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""40"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#/]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""City"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z\s]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""5"" />
            <xsd:pattern value=""[A-Za-z0-9\-]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CUDecisionExpertType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""CUDESecondaryApplicant_Type"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CUDEAddOnsType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element name=""OutputType"" type=""CUDecisionExpert_OutputType"" />
      <xsd:choice>
        <xsd:element name=""Information"" type=""InformationType"" />
        <xsd:element name=""Override"" type=""OverrideType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" name=""Options"" type=""CUDecisionExpert_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""ResidenceInformation"" type=""ResidenceInformationType"" />
      <xsd:element minOccurs=""0"" name=""TotalIncome"" type=""TotalIncomeType"" />
      <xsd:element minOccurs=""0"" name=""Dependents"" type=""DependentsType"" />
      <xsd:element minOccurs=""0"" name=""BankingRelationship"" type=""BankingRelationshipType"" />
      <xsd:element minOccurs=""0"" name=""TotalExpenses"" type=""TotalExpenseType"" />
      <xsd:element minOccurs=""0"" name=""JobHistory"" type=""JobHistoryType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategistType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CustomStrategist_AddOnsType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element name=""OutputType"" type=""CustomStrategist_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""CustomStrategist_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""ResidenceInformation"" type=""ResidenceInformationType"" />
      <xsd:element minOccurs=""0"" name=""TotalIncome"" type=""TotalIncomeType"" />
      <xsd:element minOccurs=""0"" name=""Dependents"" type=""DependentsType"" />
      <xsd:element minOccurs=""0"" name=""BankingRelationship"" type=""BankingRelationshipType"" />
      <xsd:element minOccurs=""0"" name=""TotalExpenses"" type=""TotalExpenseType"" />
      <xsd:element minOccurs=""0"" name=""JobHistory"" type=""JobHistoryType"" />
      <xsd:element minOccurs=""0"" name=""AutomobileInformation"" type=""AutomobileInformationType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""ReferenceNumber"" type=""ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""10"" name=""ProductCodeDecisionType"" type=""ProductCodeDecisionTypeType"" />
      <xsd:element minOccurs=""0"" name=""OtherInformation"" type=""CustomStrategist_OtherInformationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DetectType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""Detect_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""Fraud_SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""Detect_AddOnsType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element name=""OutputType"" type=""Detect_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""Detect_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""NFD"" type=""NFDType"" />
      <xsd:element minOccurs=""0"" name=""BankingInformation"" type=""BankingInformationType"" />
      <xsd:element minOccurs=""0"" name=""ResidenceInformation"" type=""Detect_ResidenceInformationType"" />
      <xsd:element minOccurs=""0"" name=""TotalIncome"" type=""TotalIncomeType"" />
      <xsd:element minOccurs=""0"" name=""Dependents"" type=""DependentsType"" />
      <xsd:element minOccurs=""0"" name=""JobHistory"" type=""Detect_JobHistoryType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""ReferenceNumberType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabaseType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""NFD_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""Fraud_SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""NationalFraudDatabase_AddOnsType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element name=""OutputType"" type=""NationalFraudDatabase_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""NationalFraudDatabase_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""NFD"" type=""NFDType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""Detect_OutputType_ARFType"" />
      <xsd:element name=""XML"" type=""Detect_OutputType_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""Detect_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_OutputType_XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Standalone"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabase_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditProfile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CheckPoint"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabase_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""NationalFraudDatabase_OutputType_ARFType"" />
      <xsd:element name=""XML"" type=""NationalFraudDatabase_OutputType_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabase_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabase_OutputType_XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NationalFraudDatabase_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:minInclusive value=""0"" />
            <xsd:maxInclusive value=""9999999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CheckPointOptions"" type=""CheckPointOptionsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NFDType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NFDSubscriberNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CreditApplicationType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""C"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NFDActionCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""R"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DetailedResponse"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BankingInformationType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Primary"" type=""BankingInfoType"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""BankingInfoType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BankingInfoType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AccountNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""RoutingNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""20"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TimeWithBank"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""BankingRelationship"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategist_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossSell"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategist_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
      <xsd:element name=""XML"" type=""XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategist_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ProductCodeDecisionTypeType"">
    <xsd:all>
      <xsd:element name=""ProductCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""DecisionType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""A"" />
            <xsd:enumeration value=""X"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""ReferenceNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""15"" />
      <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""AutomobileInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""LoanType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""R"" />
            <xsd:enumeration value=""L"" />
            <xsd:enumeration value=""B"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleStatus"" type=""VehicleStatusType"" />
      <xsd:element minOccurs=""0"" name=""VehicleYear"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{4}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleMake"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleModel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MSRP"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleSalesPrice"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DownpaymentAmount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Rebate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TradeLoanBalance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TradeAllowance"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TermOfLease"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CapitalizedCost"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResidualFactor"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""8"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResidualValue"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""MoneyFactor"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""VehicleStatusType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""N"" />
      <xsd:enumeration value=""U"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""OtherInformationType"">
    <xsd:sequence>
      <xsd:element name=""ResponseType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""S"" />
            <xsd:enumeration value=""X"" />
            <xsd:enumeration value=""B"" />
            <xsd:enumeration value=""D"" />
            <xsd:enumeration value=""E"" />
            <xsd:enumeration value=""F"" />
            <xsd:enumeration value=""G"" />
            <xsd:enumeration value=""H"" />
            <xsd:enumeration value=""J"" />
            <xsd:enumeration value=""K"" />
            <xsd:enumeration value=""M"" />
            <xsd:enumeration value=""N"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LoanUse"" type=""LoanUseType"" />
      <xsd:element minOccurs=""0"" name=""AssociationCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NewMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OldMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""LoanUseType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""B"" />
      <xsd:enumeration value=""P"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""CustomStrategist_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CustomStrategist_RiskModelType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AutoProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CustomStrategist_OtherInformationType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ResponseType"" type=""CustomStrategist_ResponseType"" />
      <xsd:element minOccurs=""0"" name=""LoanUse"" type=""LoanUseType"" />
      <xsd:element minOccurs=""0"" name=""AssociationCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NewMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OldMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""CustomStrategist_ResponseType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""S"" />
      <xsd:enumeration value=""X"" />
      <xsd:enumeration value=""B"" />
      <xsd:enumeration value=""D"" />
      <xsd:enumeration value=""E"" />
      <xsd:enumeration value=""F"" />
      <xsd:enumeration value=""G"" />
      <xsd:enumeration value=""H"" />
      <xsd:enumeration value=""J"" />
      <xsd:enumeration value=""K"" />
      <xsd:enumeration value=""M"" />
      <xsd:enumeration value=""N"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""CUDecisionExpert_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Joint"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SegmentOption874"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""Y"" />
            <xsd:enumeration value=""N"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_ResidenceInformationType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"" type=""Detect_ResidenceType_Type"" />
      <xsd:element minOccurs=""0"" name=""Primary"" type=""Detect_ResidenceType_Type"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""Detect_ResidenceType_Type"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_ResidenceType_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ResidenceType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""([0][1-8])*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationOfStayInMonths"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PrevResidenceType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""([0][1-8])*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PrevDurationOfStayInMonths"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ResidenceInformationType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"" type=""ResidenceType_Type"" />
      <xsd:element minOccurs=""0"" name=""Primary"" type=""ResidenceType_Type"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""ResidenceType_Type"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ResidenceType_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ResidenceType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""([0][1-8])*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationOfStay"" type=""DurationType"" />
      <xsd:element minOccurs=""0"" name=""PrevResidenceType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""([0][1-8])*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PrevDurationOfStay"" type=""DurationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DurationType"">
    <xsd:choice>
      <xsd:element minOccurs=""0"" name=""DurationInMonths"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationSinceCCYYMMDD"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{8}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationSinceCCYYMM"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{6}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationSinceCCYY"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{4}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationForPeriodYY"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DurationForPeriodYYMM"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{4}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""TotalIncomeType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"" type=""IncomeType"" />
      <xsd:element minOccurs=""0"" name=""Primary"" type=""IncomeType"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""IncomeType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""IncomeType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""TotGrossIncomeFreq"" type=""IncomeFreqType"" />
      <xsd:element minOccurs=""0"" name=""TotGrossIncomeAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SalaryFreq"" type=""IncomeFreqType"" />
      <xsd:element minOccurs=""0"" name=""SalaryAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OtherGrossIncomeFreq"" type=""IncomeFreqType"" />
      <xsd:element minOccurs=""0"" name=""OtherGrossIncomeAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DependentsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Primary"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Secondary"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""InformationType"">
    <xsd:sequence>
      <xsd:element name=""InformationResponseType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""S"" />
            <xsd:enumeration value=""X"" />
            <xsd:enumeration value=""B"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""ProductCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""DateOfApplication"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:minInclusive value=""0"" />
            <xsd:maxInclusive value=""99999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LoanUse"" type=""LoanUseType"" />
      <xsd:element minOccurs=""0"" name=""LoanAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TermOfLoan"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InterestRateRequested"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AssociationCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AgeOfAuto"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleCashPrice"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DownPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NetTradeInValue"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""VehicleStatus"" type=""VehicleStatusType"" />
      <xsd:element minOccurs=""0"" name=""NewMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OldMonthlyPaymentAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OverrideType"">
    <xsd:sequence>
      <xsd:element name=""OverrideResponseType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""S"" />
            <xsd:enumeration value=""X"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""StrategistReferenceID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""19"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OverrideCategory"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""DecisionOverride"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""A"" />
            <xsd:enumeration value=""D"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OverrideAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BankingRelationshipType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:minLength value=""0"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Primary"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:minLength value=""0"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Secondary"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:minLength value=""0"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CUDecisionExpert_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""CUDE_OutputType_ARFType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""TotalExpenseType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"" type=""ExpenseType"" />
      <xsd:element minOccurs=""0"" name=""Primary"" type=""ExpenseType"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""ExpenseType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ExpenseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""TotalExpensesFreq"" type=""ExpenseFreqType"" />
      <xsd:element minOccurs=""0"" name=""TotalExpensesAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResidenceExpenseType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""1"" />
            <xsd:enumeration value=""2"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResidenceExpenseFreq"" type=""ExpenseFreqType"" />
      <xsd:element minOccurs=""0"" name=""ResidenceExpenseAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OtherExpenseFreq"" type=""ExpenseFreqType"" />
      <xsd:element minOccurs=""0"" name=""OtherExpenseAmt"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""JobHistoryType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Common"" type=""JobType"" />
      <xsd:element minOccurs=""0"" name=""Primary"" type=""JobType"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""JobType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""JobType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CurrentJobType"" type=""JobCodeType"" />
      <xsd:element minOccurs=""0"" name=""CurrentJobDuration"" type=""DurationType"" />
      <xsd:element minOccurs=""0"" name=""PrevJobType"" type=""JobCodeType"" />
      <xsd:element minOccurs=""0"" name=""PrevJobDuration"" type=""DurationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_JobHistoryType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""Primary"" type=""Detect_JobType"" />
      <xsd:element minOccurs=""0"" name=""Secondary"" type=""Detect_JobType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""Detect_JobType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CurrentJobType"" type=""JobCodeType"" />
      <xsd:element minOccurs=""0"" name=""CurrentJobDuration"" type=""Detect_DurationType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""Detect_DurationType"">
    <xsd:choice>
      <xsd:element minOccurs=""0"" name=""DurationInMonths"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""CUDESecondaryApplicant_Type"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""SecondaryApplicant_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CUDEAddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""CreditTrendsSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CUDE_OutputType_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""IncomeFreqType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""H"" />
      <xsd:enumeration value=""M"" />
      <xsd:enumeration value=""S"" />
      <xsd:enumeration value=""W"" />
      <xsd:enumeration value=""T"" />
      <xsd:enumeration value=""B"" />
      <xsd:enumeration value=""Q"" />
      <xsd:enumeration value=""Y"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""ExpenseFreqType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""W"" />
      <xsd:enumeration value=""B"" />
      <xsd:enumeration value=""Y"" />
      <xsd:enumeration value=""M"" />
      <xsd:enumeration value=""Q"" />
      <xsd:enumeration value=""S"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""JobCodeType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""H"" />
      <xsd:enumeration value=""R"" />
      <xsd:enumeration value=""U"" />
      <xsd:enumeration value=""L"" />
      <xsd:enumeration value=""S"" />
      <xsd:enumeration value=""E"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""Options_ReferenceNumberType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""90"" />
      <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""Options_EndUserType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""30"" />
      <xsd:pattern value=""[^!;,$\\~'%\[\]\{\}\?\*=]+"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""EmploymentInsight_AddressType"">
    <xsd:sequence>
      <xsd:element name=""Street"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""68"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#/]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""City"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""38"" />
            <xsd:pattern value=""[A-Za-z\s]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\-]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EmploymentInsight_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""EmploymentInsight_AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""EmploymentInsight_AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""FileUnfreezePIN"" type=""FileUnfreezePINType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionReport_ReportTypeType"">
    <xsd:choice>
      <xsd:element minOccurs=""0"" name=""StandardCollectionReport"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CustomCollectionReport"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectionCreditReport"" type=""ChoiceType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:simpleType name=""CollectionReport_ParallelProfileType"">
    <xsd:restriction base=""ChoiceType"" />
  </xsd:simpleType>
  <xsd:complexType name=""CustomStrategist_RiskModelType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalEquiv"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OldNational"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInstall3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAuto3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOFinance3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOBank3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICO8"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Bankruptcy"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Retail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CollectScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CrossViewScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Auto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TeleRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOAdvanced"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI3Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ROI1Digit"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditUnion"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF35"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsHomeownerF4"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoMin"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsStandardAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsPrefAutoGT"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FICOInsNonstandardAuto"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""VantageScore3Positive"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NeverPay"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsight"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""IncomeInsightW2"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AssetInsight"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorePercentile"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ParallelProfile"" type=""ParallelProfileType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CreditProfile_XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Demographics"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Segment130"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorePercentile"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""BIS_XMLType"">
    <xsd:sequence>
      <xsd:element name=""Verbose"" type=""ChoiceType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_CheckPointOptionsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""LevelOfDetail"" type=""LevelOfDetailType"" />
      <xsd:element minOccurs=""0"" name=""EchoInput"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EDAFallThru"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EDABestPickCutOff"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-8][0-9]"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NoOfEDAListings"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""0[0-9]|10"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Validation"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""PrevAddrSegment"" type=""ChoiceType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:minLength value=""2"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_OptionsType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:maxInclusive value=""9999999999"" />
            <xsd:minInclusive value=""0"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""PreciseIDType"" type=""PID_Options_PreciseIDType"" />
      <xsd:element minOccurs=""0"" name=""DetailRequest"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""S"" />
            <xsd:enumeration value=""D"" />
            <xsd:enumeration value=""F"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""StrategyNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""99"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NFDAggregationFlag"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DecisionScoreCutoffLow"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DecisionScoreCutoffHigh"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:int"">
            <xsd:maxInclusive value=""999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PreorPostEnrollment"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:enumeration value=""PREE"" />
            <xsd:enumeration value=""POST"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""InquiryChannel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""MAIL"" />
            <xsd:enumeration value=""PHON"" />
            <xsd:enumeration value=""INTE"" />
            <xsd:enumeration value=""PRE"" />
            <xsd:enumeration value=""ATM"" />
            <xsd:enumeration value=""PDA"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""PID_SSNType"" />
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
      <xsd:element minOccurs=""0"" name=""MothersMaidenName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z\-\s']*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""EmailAddress"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""5"" />
            <xsd:maxLength value=""60"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PreciseID_ProductType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PreciseID_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""PreciseID_AddOnsType"" />
      <xsd:element name=""OutputType"">
        <xsd:complexType>
          <xsd:choice>
            <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
            <xsd:element name=""XML"" type=""CreditProfile_XMLType"" />
          </xsd:choice>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""PreciseID_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""NFD"" type=""NFDType"" />
      <xsd:element minOccurs=""0"" name=""BankingInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Primary"" type=""BankingInfoType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ResidenceInformation"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Primary"" type=""Detect_ResidenceType_Type"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TotalIncome"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Primary"" type=""IncomeType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Dependents"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Primary"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""2"" />
                  <xsd:pattern value=""[0-9]*"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""JobHistory"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs=""0"" name=""Primary"" type=""Detect_JobType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CheckPointOptions"" type=""PreciseID_CheckPointOptionsType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CCNumerics"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element maxOccurs=""10"" name=""CCNumeric"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:int"">
                  <xsd:maxInclusive value=""99999"" />
                  <xsd:minInclusive value=""0"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CCStrings"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element maxOccurs=""5"" name=""CCString"">
              <xsd:simpleType>
                <xsd:restriction base=""xsd:string"">
                  <xsd:maxLength value=""10"" />
                </xsd:restriction>
              </xsd:simpleType>
            </xsd:element>
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""PID_Options_PreciseIDType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""IG"" />
      <xsd:enumeration value=""IF"" />
      <xsd:enumeration value=""NO"" />
      <xsd:enumeration value=""CF"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""MLAReportType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""MLAReport_PrimaryApplicantType"" />
      <xsd:element name=""AccountType"" type=""MLAReport_AccountTypeType"" />
      <xsd:element name=""OutputType"" type=""MLAReport_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""MLAReport_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"">
        <xsd:simpleType>
          <xsd:restriction base=""SSNType"" />
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""CurrentAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_AccountTypeType"">
    <xsd:sequence>
      <xsd:element name=""Type"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""^\s?$|[A-Za-z0-9]{2}|\.\."" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""MLAReport_ARFType"" />
      <xsd:element name=""XML"" type=""MLAReport_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Parsed"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""Y2K"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""MLAReport_XMLType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CustomSolutionType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:choice>
        <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantWithPINType"" />
        <xsd:element name=""BusinessApplicant"" type=""CustomSolution_BusinessApplicantType"" />
      </xsd:choice>
      <xsd:element minOccurs=""0"" name=""SecondaryApplicant"" type=""CS_SecondaryApplicantWithPINType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""Custom_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""CreditProfile_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""CustomSolution_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""PreciseIdData"" type=""PreciseIdData"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiryType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""Applicant"" type=""NumericInquiry_ApplicantType"" />
      <xsd:element minOccurs=""0"" name=""Spouse"" type=""NumericInquiry_SpouseType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""NumericInquiry_AddOnsType"" />
      <xsd:element name=""OutputType"" type=""NumericInquiry_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""NumericInquiry_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_ApplicantType"">
    <xsd:sequence>
      <xsd:element name=""SSN"" type=""SSNType"" />
      <xsd:element name=""Address"" type=""NumericInquiry_AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""PhoneType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_SpouseType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""PreviousAddress"" type=""AddressType"" />
      <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
      <xsd:element minOccurs=""0"" name=""Employment"" type=""EmploymentType"" />
      <xsd:element minOccurs=""0"" name=""Age"">
        <xsd:simpleType>
          <xsd:restriction base=""AgeType"">
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
      <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""NewConsumer"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DirectCheck"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FACSPlus"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FraudShield"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""NumericInquiry_RiskModelsType"" />
      <xsd:element minOccurs=""0"" name=""DemographicBand"" type=""DemographicBandType"" />
      <xsd:element minOccurs=""0"" name=""CustomRRDashKeyword"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""2"" />
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustomModel"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[a-zA-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AutoProfileSummary"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SSNIndicators"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MLA"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_OutputType"">
    <xsd:choice>
      <xsd:element name=""TTY"" type=""TTYType"" />
      <xsd:element name=""ARF"" type=""NoPP_ARFType"" />
      <xsd:element name=""XML"" type=""CreditProfile_XMLType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_OptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element minOccurs=""0"" name=""EndUser"" type=""Options_EndUserType"" />
      <xsd:element minOccurs=""0"" name=""BrokerNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:long"">
            <xsd:minInclusive value=""0"" />
            <xsd:maxInclusive value=""9999999999"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""OFAC"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""OFACMSG"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SB168"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""NumericInquiry_AddressType"">
    <xsd:sequence>
      <xsd:element name=""HouseNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#/\.]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\-]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantageType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""CollectionAdvantage_SubscriberType"" />
      <xsd:element name=""Consumer"" type=""CollectionAdvantage_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""CollectionAdvantage_AddOnsType"" />
      <xsd:element name=""ClientOptions"" type=""ClientOptionsType"" />
      <xsd:element name=""OutputType"" type=""CollectionAdvantage_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_SubscriberType"">
    <xsd:sequence>
      <xsd:element name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[A-Z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""OpInitials"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:minLength value=""2"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""SubCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:element name=""Name"" type=""CollectionAdvantage_NameType"" />
      <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
      <xsd:element name=""Address"" type=""CollectionAdvantage_AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""3"" name=""Phone"" type=""CollectionAdvantage_PhoneType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_NameType"">
    <xsd:annotation>
      <xsd:documentation />
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name=""Surname"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+([A-Za-z\-'\s]*)"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""First"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Middle"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Gen"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""6"" />
            <xsd:pattern value=""[A-Za-z0-9\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_AddressType"">
    <xsd:sequence>
      <xsd:element name=""Street"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#/\.]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""City"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z\s\.]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""State"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z]*"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Zip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\-]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_PhoneType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AreaCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:minLength value=""3"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""PhoneNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:minLength value=""7"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_AddOnsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""RiskModels"" type=""CollectionAdvantage_RiskModelsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_RiskModelsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""VantageScore"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""ScorexPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""SureView"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyWatch"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreBank"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""RecoveryScoreRetail"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NationalRisk"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUS"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyPLUSRescaled"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TEC"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_OutputType"">
    <xsd:sequence>
      <xsd:element name=""XML"" type=""CollectionAdvantage_XMLType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_ARFType"">
    <xsd:all>
      <xsd:element name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""CollectionAdvantage_XMLType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""ARFVersion"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[0-9]{2}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Verbose"" type=""ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""ClientOptionsType"">
    <xsd:all>
      <xsd:element name=""PermissiblePurpose"" type=""PermissiblePurposeType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceFields"" type=""ReferenceFieldsType"" />
      <xsd:element name=""PackageAndDataOptions"" type=""PackageAndDataOptionsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""PermissiblePurposeType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""2H"" />
      <xsd:enumeration value=""4C"" />
      <xsd:enumeration value=""1J"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ReferenceFieldsType"">
    <xsd:choice>
      <xsd:element name=""ReferenceNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element name=""Balance"" type=""Options_BalanceType"" />
      <xsd:element name=""AccountDate"" type=""Options_AccountDateType"" />
      <xsd:element name=""CustomerNumber"" type=""Options_ReferenceNumberType"" />
      <xsd:element name=""BranchID"" type=""Options_ReferenceNumberType"" />
      <xsd:element name=""Other"" type=""Options_ReferenceNumberType"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:simpleType name=""Options_AccountDateType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""8"" />
      <xsd:pattern value=""[0-9]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name=""Options_BalanceType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:maxLength value=""9"" />
      <xsd:pattern value=""[0-9]*"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""PackageAndDataOptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""PackageID"" type=""PackageIDType"" />
      <xsd:element minOccurs=""0"" name=""IndividualDataOptions"" type=""IndividualDataOptionsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""PackageIDType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""I1"" />
      <xsd:enumeration value=""I2"" />
      <xsd:enumeration value=""I3"" />
      <xsd:enumeration value=""I4"" />
      <xsd:enumeration value=""I5"" />
      <xsd:enumeration value=""I6"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""IndividualDataOptionsType"">
    <xsd:all>
      <xsd:element minOccurs=""0"" name=""SSNIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BestNameAndAddressIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""DeceasedIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""FileOnePhonesIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""EmploymentIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""CreditAttributesIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""BankruptcyIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""AddlNameAndAddressIndicator"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""TradelinesAndInquiries"" type=""CollectionAdvantage_ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""MetroNetIndicator"" type=""CollectionAdvantage_ChoiceType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:simpleType name=""CollectionAdvantage_ChoiceType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""Y"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""ProSelect_Options_Lender_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""ProductList"" type=""ProSelect_Options_ProductList_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_Product_Type"">
    <xsd:sequence>
      <xsd:element name=""ProductID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Priority"" type=""xsd:int"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EXPVFY_Options_Type"">
    <xsd:sequence>
      <xsd:element name=""CustomInformation"" type=""EXPVFY_Options_CustomInformation_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""EXPVFY_Options_CustomInformation_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""ConsumerID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""32"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ReportType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""voi"" />
            <xsd:enumeration value=""voa"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BACD_Options_Type"">
    <xsd:sequence>
      <xsd:element name=""CustomInformation"" type=""BACD_Options_CustomInformation_Type"" />
      <xsd:element name=""InputInformation"" type=""BACD_Options_InputInformation_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BACD_Options_CustomInformation_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""timestamp"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""29"" />
            <xsd:pattern value=""[0-9\s\-\.:]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""auditTrace"" type=""BooleanType"" />
      <xsd:element minOccurs=""0"" name=""dataSourceAttributes"" type=""BooleanType"" />
      <xsd:element minOccurs=""0"" name=""additionOutputData"" type=""BooleanType"" />
      <xsd:element minOccurs=""0"" name=""attrcalconly"" type=""BooleanType"" />
      <xsd:element minOccurs=""0"" name=""decisionReasonCode"" type=""BooleanType"" />
      <xsd:element minOccurs=""0"" name=""forStaticProfile"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""S"" />
            <xsd:enumeration value=""L"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""dataSourceID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""5"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""dataSourceProcessInd"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""5"" />
            <xsd:pattern value=""|\s|[A|P]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""bureauModelID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""bureauCallOrder"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:enumeration value=""EX"" />
            <xsd:enumeration value=""QU"" />
            <xsd:enumeration value=""TU"" />
            <xsd:enumeration value=""EXQU"" />
            <xsd:enumeration value=""EXTU"" />
            <xsd:enumeration value=""QUEX"" />
            <xsd:enumeration value=""QUTU"" />
            <xsd:enumeration value=""TUEX"" />
            <xsd:enumeration value=""TUQU"" />
            <xsd:enumeration value=""EXQUTU"" />
            <xsd:enumeration value=""EXTUQU"" />
            <xsd:enumeration value=""QUEXTU"" />
            <xsd:enumeration value=""QUTUEX"" />
            <xsd:enumeration value=""TUEXQU"" />
            <xsd:enumeration value=""TUQUEX"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BACD_Options_InputInformation_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""DataField"" type=""BACD_Options_InputInformation_DataField_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BACD_Options_InputInformation_DataField_Type"">
    <xsd:sequence>
      <xsd:element name=""Name"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""512"" />
            <xsd:pattern value=""[A-Za-z0-9\|:_]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Value"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""512"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-\.:#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CustomInformation"" type=""USBank_Options_CustomInformation_Type"" />
      <xsd:element minOccurs=""0"" maxOccurs=""100"" name=""GuarantorInformation"" type=""USBank_Options_GuarantorInformation_Type"" />
      <xsd:element minOccurs=""0"" maxOccurs=""6"" name=""BureauLogin"" type=""USBank_Options_BureauLogin_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_CustomInformation_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""USBEF"" type=""USBank_Options_USBEF_Type"" />
      <xsd:element minOccurs=""0"" name=""Transaction"" type=""USBank_Options_Transaction_Type"" />
      <xsd:element minOccurs=""0"" name=""Customer"" type=""USBank_Options_Customer_Type"" />
      <xsd:element minOccurs=""0"" name=""ScValue"" type=""USBank_Options_ScValue_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_USBEF_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""InquiryType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CommnEventId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""11"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PmeEvtId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""EnvironmentId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""4"" />
            <xsd:pattern value=""[A-Za-z\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_Transaction_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""TranId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""LevelId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SourceId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TrAmountReq"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""22"" />
            <xsd:pattern value=""[0-9\.]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TrCreatedDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TrTotalExposure"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""22"" />
            <xsd:pattern value=""[0-9\.]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TrBusinessUnit"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""120"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DelinquencyCount"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_Customer_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CustSystemCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustCustomerName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""300"" />
            <xsd:pattern value=""[A-Za-z0-9\s#@'%`$_!~:=\?\[\]\\\.\-\*\+\(\)&amp;&quot;]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustDunsNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustPhoneNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustInBusiness"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""75"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustInBusinessOvr"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""75"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustUserDefined11"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustUserDefined12"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustUserDefined19"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""120"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustSourceStatus"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CeiUserDefChar16"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[A-Za-z\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Address"" type=""USBank_Options_Address_Type"" />
      <xsd:element minOccurs=""0"" name=""CountPreviousDeclines"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SicCodeOverride"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""SicCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""90"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaynetId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NoMatchId"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Arsummary91120"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""20"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ArSummaryLegalStatus"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""120"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ArSummaryNonAccrual"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""22"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""DNBTrdUpInd"" type=""ChoiceType"" />
      <xsd:element minOccurs=""0"" name=""NAICSCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""NAICSCodeOverride"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_ScValue_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""DbAge"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""ConfidenceMatch"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""2"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaynetAge"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""PaynetMatch"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CrbAge"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""3"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_GuarantorInformation_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""CustSystemCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustFirstName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""120"" />
            <xsd:pattern value=""[A-Za-z0-9\s':`\.\-\?\(\)\[&amp;&quot;]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustLastName"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""120"" />
            <xsd:pattern value=""[A-Za-z0-9\s':`\.\-\?\*\(\)\[&amp;]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustSSN"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""60"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-#]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""CustPhoneNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""10"" />
            <xsd:pattern value=""[0-9\s#=_!\.\*\+\(\)\[\]]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Address"" type=""USBank_Options_Address_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_Address_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""AdAddress4"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""240"" />
            <xsd:pattern value=""[A-Za-z0-9\s#@'%:`=$_!~\\\|\.\-\^\?\*\+\{\}\(\)\[\]&amp;&quot;]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdState"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""90"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdCity"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""90"" />
            <xsd:pattern value=""[A-Za-z0-9\s\-\.#']+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AdZip"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""36"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""USBank_Options_BureauLogin_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BureauCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Preamble"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Subcode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""UserLogin"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""45"" />
            <xsd:pattern value=""[a-zA-Z0-9_.!@#$]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""Password"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""15"" />
            <xsd:pattern value=""[a-zA-Z0-9_.!@#$]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_Type"">
    <xsd:choice>
      <xsd:element name=""SoftInquiry"" type=""ProSelect_Options_SoftInquiry_Type"" />
      <xsd:element name=""BillingEvent"" type=""ProSelect_Options_BillingEvent_Type"" />
      <xsd:element name=""ProSelectPrescreen"" type=""ProSelect_Options_Prescreen_Type"" />
    </xsd:choice>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_BillingEvent_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""ProductCode"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""7"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TransactionID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""0"" />
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TransactionDate"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""8"" />
            <xsd:maxLength value=""8"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_SoftInquiry_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""ReferenceNumber"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""30"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""TransactionID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""0"" />
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[A-Za-z0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_Prescreen_Type"">
    <xsd:sequence>
      <xsd:element name=""LenderList"" type=""ProSelect_Options_LenderList_Type"" />
      <xsd:element name=""PathID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""9"" />
            <xsd:pattern value=""[0-9]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" name=""AttributeList"" type=""ProSelect_Options_AttributeList_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_LenderList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Lender"" type=""ProSelect_Options_Lender_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_AttributeList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Attribute"" type=""ProSelect_Options_Attribute_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_ProductList_Type"">
    <xsd:sequence>
      <xsd:element maxOccurs=""unbounded"" name=""Product"" type=""ProSelect_Options_Product_Type"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ProSelect_Options_Attribute_Type"">
    <xsd:sequence>
      <xsd:element name=""Name"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""20"" />
            <xsd:minLength value=""1"" />
            <xsd:pattern value=""[A-Za-z0-9_]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""Value"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:minLength value=""1"" />
            <xsd:maxLength value=""20"" />
            <xsd:pattern value=""[A-Za-z0-9_]+"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""ResponseInfo_Type"">
    <xsd:sequence>
      <xsd:element minOccurs=""1"" maxOccurs=""1"" name=""ResponseType"" type=""CustomSolution_ResponseType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""ResponseData"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[Aa]|[Bb]|[Ss]"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element minOccurs=""0"" maxOccurs=""1"" name=""ThinFileIndicator"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:maxLength value=""1"" />
            <xsd:pattern value=""[0-4]"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""CustomSolution_ResponseType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:enumeration value=""ARF"" />
      <xsd:enumeration value=""PP"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""CustomSolution_BusinessApplicantType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""BusinessName"" type=""BusinessNameType"" />
      <xsd:element minOccurs=""0"" name=""TaxId"" type=""TINType"" />
      <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" name=""AlternateAddress"" type=""BusinessApplicant_AddressType"" />
      <xsd:element minOccurs=""0"" maxOccurs=""2"" name=""Phone"" type=""BusinessPhoneType"" />
      <xsd:element minOccurs=""0"" name=""BusinessOwner"">
        <xsd:complexType>
          <xsd:complexContent mixed=""false"">
            <xsd:extension base=""BusinessOwnerType"">
              <xsd:sequence minOccurs=""0"">
                <xsd:element name=""OwnerName"" type=""BIS_NameType"" />
                <xsd:element minOccurs=""0"" name=""SSN"" type=""SSNType"" />
                <xsd:element minOccurs=""0"" name=""CurrentAddress"" type=""AddressType"" />
                <xsd:element minOccurs=""0"" name=""DriverLicense"" type=""DriversLicType"" />
                <xsd:element minOccurs=""0"" name=""Age"" type=""AgeType"" />
                <xsd:element minOccurs=""0"" name=""DOB"" type=""DOBType"" />
                <xsd:element minOccurs=""0"" name=""YOB"" type=""YOBType"" />
                <xsd:element minOccurs=""0"" name=""Title"" type=""TitleType"" />
              </xsd:sequence>
            </xsd:extension>
          </xsd:complexContent>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""BizID_Options_Type"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""DebtSettlementReportType"">
    <xsd:sequence>
      <xsd:element name=""Subscriber"" type=""SubscriberType"" />
      <xsd:element name=""PrimaryApplicant"" type=""PrimaryApplicantType"" />
      <xsd:element name=""OutputType"" type=""CreditProfile_OutputType"" />
      <xsd:element name=""Vendor"" type=""VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""SocialSearch_OptionsType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""RiskBasedPricingType"">
    <xsd:all>
      <xsd:element name=""NoticeType"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:pattern value=""[Gg][Ee][Nn][Ee][Rr][Ii][Cc]|[Mm][Oo][Rr][Tt][Gg][Aa][Gg][Ee]"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""RiskModel"" type=""RBP_RiskModelsType"" />
    </xsd:all>
  </xsd:complexType>
  <xsd:complexType name=""PreciseIdData"">
    <xsd:sequence>
      <xsd:element ref=""XMLVersion"" />
      <xsd:element name=""KBAAnswers"">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element name=""OutWalletAnswerData"" type=""OutWalletAnswerDataType"" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:element default=""01"" name=""XMLVersion"">
    <xsd:simpleType>
      <xsd:restriction base=""xsd:string"">
        <xsd:whiteSpace value=""collapse"" />
        <xsd:pattern value=""(1|01|2|02|3|03)?"" />
      </xsd:restriction>
    </xsd:simpleType>
  </xsd:element>
  <xsd:complexType name=""OutWalletAnswerDataType"">
    <xsd:sequence>
      <xsd:element name=""SessionID"">
        <xsd:simpleType>
          <xsd:restriction base=""xsd:string"">
            <xsd:whiteSpace value=""collapse"" />
            <xsd:pattern value=""([a-zA-Z0-9# \- / \.]{1,120}){1}"" />
          </xsd:restriction>
        </xsd:simpleType>
      </xsd:element>
      <xsd:element name=""OutWalletAnswers"" type=""OutWalletAnswersType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""OutWalletAnswersType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer1"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer2"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer3"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer4"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer5"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer6"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer7"" type=""OutWalletAnswerType"" />
      <xsd:element minOccurs=""0"" name=""OutWalletAnswer8"" type=""OutWalletAnswerType"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:simpleType name=""OutWalletAnswerType"">
    <xsd:restriction base=""xsd:string"">
      <xsd:whiteSpace value=""collapse"" />
      <xsd:pattern value=""((1|01)|(2|02)|(3|03)|(4|04)|(5|05))?"" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name=""PreciseIDServerType"">
    <xsd:sequence>
      <xsd:element minOccurs=""0"" name=""Subscriber"" type=""PIDServer_SubscriberType"" />
      <xsd:element minOccurs=""0"" name=""PrimaryApplicant"" type=""PIDServer_PrimaryApplicantType"" />
      <xsd:element minOccurs=""0"" name=""AccountType"" type=""PIDServer_AccountTypeType"" />
      <xsd:element minOccurs=""0"" name=""AddOns"" type=""PIDServer_AddOnsType"" />
      <xsd:element minOccurs=""0"" name=""OutputType"" type=""PIDServer_OutputType"" />
      <xsd:element minOccurs=""0"" name=""Vendor"" type=""PIDServer_VendorType"" />
      <xsd:element minOccurs=""0"" name=""Options"" type=""PIDServer_OptionsType"" />
      <xsd:element minOccurs=""0"" name=""NFD"" type=""PIDServer_NFDType"" />
      <xsd:element minOccurs=""0"" name=""BankingInformation"" type=""PIDServer_BankingInformationType"" />
      <xsd:element minOccurs=""0"" name=""ResidenceInformation"" type=""PIDServer_ResidenceInformationType"" />
      <xsd:element minOccurs=""0"" name=""TotalIncome"" type=""PIDServer_TotalIncomeType"" />
      <xsd:element minOccurs=""0"" name=""Dependents"" type=""PIDServer_DependentsType"" />
      <xsd:element minOccurs=""0"" name=""JobHistory"" type=""PIDServer_JobHistoryType"" />
      <xsd:element minOccurs=""0"" name=""CheckPointOptions"" type=""PIDServer_CheckPointOptionsType"" />
      <xsd:element minOccurs=""0"" name=""ReferenceNumber"" type=""xsd:string"" />
      <xsd:element minOccurs=""0"" name=""CCNumerics"" type=""PIDServer_CCNumerics"" />
      <xsd:element minOccurs=""0"" name=""CCStrings"" type=""PIDServer_CCStrings"" />
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
      <xsd:element minOccurs=""0"" name=""KBAAnswers"" type=""KBAAnswers_CCStrings"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_SubscriberType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_PrimaryApplicantType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_AccountTypeType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_AddOnsType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_OutputType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_VendorType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_OptionsType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" processContents=""skip"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_NFDType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_BankingInformationType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_ResidenceInformationType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_TotalIncomeType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_DependentsType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_JobHistoryType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_CheckPointOptionsType"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_ReferenceNumberType"" />
  <xsd:complexType name=""PIDServer_CCNumerics"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""PIDServer_CCStrings"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name=""KBAAnswers_CCStrings"">
    <xsd:sequence>
      <xsd:any minOccurs=""0"" maxOccurs=""unbounded"" namespace=""##other"" />
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>";
        
        public WebDelivery() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [3];
                _RootElements[0] = "Request";
                _RootElements[1] = "CreditProfile";
                _RootElements[2] = "XMLVersion";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
        
        [Schema(@"http://www.experian.com/WebDelivery",@"Request")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"Request"})]
        public sealed class Request : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public Request() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "Request";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/WebDelivery",@"CreditProfile")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"CreditProfile"})]
        public sealed class CreditProfile : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public CreditProfile() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "CreditProfile";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
        
        [Schema(@"http://www.experian.com/WebDelivery",@"XMLVersion")]
        [System.SerializableAttribute()]
        [SchemaRoots(new string[] {@"XMLVersion"})]
        public sealed class XMLVersion : Microsoft.XLANGs.BaseTypes.SchemaBase {
            
            [System.NonSerializedAttribute()]
            private static object _rawSchema;
            
            public XMLVersion() {
            }
            
            public override string XmlContent {
                get {
                    return _strSchema;
                }
            }
            
            public override string[] RootNodes {
                get {
                    string[] _RootElements = new string [1];
                    _RootElements[0] = "XMLVersion";
                    return _RootElements;
                }
            }
            
            protected override object RawSchema {
                get {
                    return _rawSchema;
                }
                set {
                    _rawSchema = value;
                }
            }
        }
    }
}
