namespace ExperianCreditProfileDefinition {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://www.experian.com/NetConnect",@"NetConnectRequest")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"NetConnectRequest"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"ExperianCreditProfileDefinition.WebDelivery", typeof(global::ExperianCreditProfileDefinition.WebDelivery))]
    public sealed class NetConnectRequest : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://www.experian.com/NetConnect"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ns0=""http://www.experian.com/WebDelivery"" attributeFormDefault=""qualified"" elementFormDefault=""qualified"" targetNamespace=""http://www.experian.com/NetConnect"" version=""04.00"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:import schemaLocation=""ExperianCreditProfileDefinition.WebDelivery"" namespace=""http://www.experian.com/WebDelivery"" />
  <xs:annotation>
    <xs:appinfo>
      <b:references>
        <b:reference targetNamespace=""http://www.experian.com/WebDelivery"" />
      </b:references>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""NetConnectRequest"">
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""EAI"">
          <xs:simpleType>
            <xs:restriction base=""xs:string"">
              <xs:maxLength value=""8"" />
              <xs:minLength value=""8"" />
            </xs:restriction>
          </xs:simpleType>
        </xs:element>
        <xs:element name=""DBHost"">
          <xs:simpleType>
            <xs:restriction base=""xs:string"">
              <xs:enumeration value=""BIZ_ID"" />
              <xs:enumeration value=""BIZ_ID_TEST"" />
            </xs:restriction>
          </xs:simpleType>
        </xs:element>
        <xs:element minOccurs=""0"" name=""ReferenceId"">
          <xs:simpleType>
            <xs:restriction base=""xs:string"">
              <xs:maxLength value=""90"" />
            </xs:restriction>
          </xs:simpleType>
        </xs:element>
        <xs:element ref=""ns0:Request"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public NetConnectRequest() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "NetConnectRequest";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
