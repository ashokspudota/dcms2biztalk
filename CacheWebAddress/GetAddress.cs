﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sba.gov;
using Microsoft.RuleEngine;

namespace CacheWebAddress
{
    public class GetAddress
    {
        public string GetStaticAddress()
        {
            StaticWebAddress staticwebaddress = new StaticWebAddress();
            Policy policy = new Policy("Get Static Address");
            policy.Execute(staticwebaddress);
            return staticwebaddress.Address;

        }
        public string GetStaticTime()
        {
            StaticWebAddress StaticTime = new StaticWebAddress();
            Policy policy = new Policy("Get Static Address");
            policy.Execute(StaticTime);
            return StaticTime.Time;

        }
    }
}
