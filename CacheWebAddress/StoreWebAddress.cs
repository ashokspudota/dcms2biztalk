﻿using Microsoft.RuleEngine;
using sba.gov;
using System;
using System.IO;
using System.Net;

namespace CacheWebAddress
{
    public class StoreWebAddress : IFactRetriever
    {
        public object UpdateFacts(RuleSetInfo ruleSetInfo, RuleEngine engine, object factsHandleIn)
        {
            Object factsHandleOut;
            if (factsHandleIn == null || factsareoutofdate(factsHandleIn))
            {
                WebAddressInfo webaddressinfo = new WebAddressInfo();
                webaddressinfo.WhenValid = DateTime.Now;
                GetAddress staticaddress = new GetAddress();               
                webaddressinfo.Address = RealAddress(staticaddress.GetStaticAddress());
                factsHandleOut = webaddressinfo;
                engine.Assert(factsHandleOut);
            }
            else { factsHandleOut = factsHandleIn; }
            return factsHandleOut;
        }
        public bool factsareoutofdate(object factsHandle)
        {
            WebAddressInfo WebInfo;
            GetAddress StaticTime = new GetAddress();
            
            if (factsHandle == null)
                return true;
            else
            {
                WebInfo = (WebAddressInfo)factsHandle;
                return (TimeSpan.Compare(DateTime.Now.Subtract(WebInfo.WhenValid), TimeSpan.FromHours(System.Convert.ToDouble(StaticTime.GetStaticTime()))) > 0);
            }

        }
        public string RealAddress(string staticaddress)
        {
            string result = null;
            WebRequest request = WebRequest.Create(new Uri(staticaddress));
            request.Method = "POST";
            request.ContentType = "application/xml";
            using(Stream stream = request.GetRequestStream())
            {
            }
            using(WebResponse response = request.GetResponse() as WebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadLine();
                reader.Close();
            }
            return result;

        }


    }
}
