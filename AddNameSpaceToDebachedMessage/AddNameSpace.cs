﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AddNameSpaceToDebachedMessage
{
    public class AddNameSpace
    {

        public static XmlDocument GetMessageFromXmlStr(string XmlStr,string Namespace)
        {   //string Namespace = "http://BizTalk_Server_ProjectJSONToXml.SFJsonRequest";
            XmlDocument doc = new XmlDocument();
            XmlStr = DecodeXmlStr(XmlStr);
            XmlStr = InsertNamespaceString(XmlStr, Namespace);
            doc.LoadXml(XmlStr);
            return doc;
        }

        private static string DecodeXmlStr(string XmlStr)
        {
            XmlStr = XmlStr.Replace("&lt;", "<");
            XmlStr = XmlStr.Replace("&gt;", ">");
            XmlStr = XmlStr.Replace("&quot;", "\"");
            XmlStr = XmlStr.Replace("&apos;", "\'");
            XmlStr = XmlStr.Replace("&amp;", "&");

            return XmlStr;
        }

        private static string InsertNamespaceString(string XmlStr, string Namespace)
        {
            return InsertNamespaceString(XmlStr, Namespace, String.Empty);
        }

        private static string InsertNamespaceString(string XmlStr, string Namespace, string NSPrefix)
        {
            int iLoc = XmlStr.IndexOf(">");
            string strNamespace = String.Empty;

            if (NSPrefix.Trim() == String.Empty)
                strNamespace = String.Format(" xmlns=\"{0}\"", Namespace);
            else
                strNamespace = String.Format(" xmlns:{0}=\"{1}\"", NSPrefix, Namespace);

            XmlStr = XmlStr.Insert(iLoc, strNamespace);

            return XmlStr;
        }

    }
}
